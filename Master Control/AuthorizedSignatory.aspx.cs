﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class AuthorizedSignatory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetEmp()
        {
            clsConnection cls = new clsConnection();
            string qry = "select EmpName, NTID from tbl_HRMS_EmployeeMaster ";
            qry += "where NTID in (select ntid from tbl_VPR_Access_User) and EmpLevel in ('AM', 'MNGR', 'SR. MGR', 'DIR', 'VP')";
            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }
    }
}