﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class CompRulesEncode : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static bool ordinaceCtr = false;


        [WebMethod]
        public static string GetSettings()
        {
            clsConnection cls = new clsConnection();
            string qry = "select municipality_name, municipality_code from tbl_VPR_Municipality where municipality_code in (";
            qry += "select distinct city from tbl_VPR_Workable_Registration_PFC ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Registration_REO ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Registration_Property ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Registration_PropReq ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Registration_Cost ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Inspection ";
            qry += "UNION ";
            qry += "select distinct city from tbl_VPR_Workable_Municipality ";
            qry += "UNION ";
            qry += "select distinct city from dbo.tbl_VPR_Workable_Deregistration) group by municipality_name, municipality_code order by municipality_name";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string populateData(string municipalityCode, string Tab)
        {
            clsConnection cls = new clsConnection();
            string qryRegPFC, qryRegREO, qryRegPropType, qryRegPropReq, qryCost, qryCont, qryAddForms, qryMunicipal, qryDereg, qryNotif = "";
            DataTable dtRegPFC = new DataTable();
            DataTable dtRegREO = new DataTable();
            DataTable dtRegPropType = new DataTable();
            DataTable dtRegPropReq = new DataTable();
            DataTable dtCost = new DataTable();
            DataTable dtCont = new DataTable();
            DataTable dtAddForms = new DataTable();
            DataTable dtInspect = new DataTable();
            DataTable dtMunicipal = new DataTable();
            DataTable dtDereg = new DataTable();
            DataTable dtNotif = new DataTable();


            if (Tab == "PFC")
            {
                qryRegPFC = "select top 1 * from tbl_VPR_Workable_Registration_PFC (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                //                qryRegPFC = @"SELECT * INTO #TempTable
                //                        FROM tbl_VPR_Workable_Registration_PFC
                //                        ALTER TABLE #TempTable
                //                        DROP COLUMN tag
                //                        SELECT * FROM #TempTable where city = '" + municipalityCode + "' order by id desc, date_modified desc DROP TABLE #TempTable";
                dtRegPFC = cls.GetData(qryRegPFC);
            }
            else if (Tab == "REO")
            {
                qryRegREO = "select top 1 * from tbl_VPR_Workable_Registration_REO (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                dtRegREO = cls.GetData(qryRegREO);
            }
            else if (Tab == "Property Type")
            {
                qryRegPropType = "select top 1 * from tbl_VPR_Workable_Registration_Property (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                dtRegPropType = cls.GetData(qryRegPropType);
            }
            else if (Tab == "Requirements")
            {
                qryRegPropReq = @"select top 1 a.*, b.*, c.* from tbl_VPR_Workable_Registration_PropReq a (nolock) full join
                                tbl_VPR_Workable_Registration_PropReq_Contact b (nolock) on
                                a.propReqID = b.propReqID 
                                full join  tbl_VPR_Workable_Registration_PropReq_Bond as c  on
                                a.bondID = c.bondID where a.city = '" + municipalityCode + "' order by a.id desc, date_modified desc";
                dtRegPropReq = cls.GetData(qryRegPropReq);
            }
            else if (Tab == "Cost")
            {
                //qryCost = "select top 1 * from tbl_VPR_Workable_Registration_Cost (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                qryCost = @"select top 1 * from tbl_VPR_Workable_Registration_Cost cost
                        inner join tbl_VPR_Cost_PFC pfc 
                        on cost.pfc_id = pfc.pfc_id
                        inner join tbl_VPR_Cost_REO reo
                        on cost.reo_id = reo.reo_id
                        inner join tbl_VPR_Cost_Commercial commercial
                        on cost.commercial_id = commercial.commercial_id
                        inner join tbl_VPR_Cost_Vacant vacant
                        on cost.vacant_id = vacant.vacant_id
                        where cost.city = '" + municipalityCode + "' order by cost.id desc, cost.date_modified desc";
                dtCost = cls.GetData(qryCost);
            }
            else if (Tab == "Continuing Registration")
            {
                qryCont = "select top 1 * from tbl_VPR_Workable (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                dtCont = cls.GetData(qryCont);
            }
            else if (Tab == "Additional Forms")
            {
                qryAddForms = "select top 1 * from tbl_VPR_Workable_Additional_Forms (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
                dtAddForms = cls.GetData(qryAddForms);
            }
            else
            {

            }

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regPFC = dtRegPFC,
                    regREO = dtRegREO,
                    regPropType = dtRegPropType,
                    regPropReq = dtRegPropReq,
                    regCost = dtCost,
                    regCont = dtCont,
                    regAddForms = dtAddForms
                }
            });
        }

        [WebMethod]
        public static string populateMunicipality(string municipalityCode)
        {
            clsConnection cls = new clsConnection();
            string qryMunicipal = "";
            DataTable dtMunicipal = new DataTable();

            qryMunicipal = "select top 1 * from tbl_VPR_Workable_Municipality (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
            dtMunicipal = cls.GetData(qryMunicipal);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    municipal = dtMunicipal
                }
            });
        }

        [WebMethod]
        public static string populateInspection(string municipalityCode)
        {
            clsConnection cls = new clsConnection();
            string qryInspect = "";
            DataTable dtInspect = new DataTable();

            qryInspect = "select top 1 * from tbl_VPR_Workable_Inspection (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
            dtInspect = cls.GetData(qryInspect);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    inspect = dtInspect
                }
            });
        }

        [WebMethod]
        public static string populateDereg(string municipalityCode)
        {
            clsConnection cls = new clsConnection();
            string qryDereg = "";
            DataTable dtDereg = new DataTable();

            qryDereg = "select top 1 * from tbl_VPR_Workable_Deregistration (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
            dtDereg = cls.GetData(qryDereg);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    dereg = dtDereg
                }
            });
        }

        //[WebMethod]
        //public static string populateNotif(string municipalityCode)
        //{
        //    clsConnection cls = new clsConnection();
        //    string qryNotif = "";
        //    DataTable dtNotif = new DataTable();

        //    qryNotif = "select top 1 * from tbl_VPR_Workable_Notification (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";
        //    dtNotif = cls.GetData(qryNotif);

        //    return JsonConvert.SerializeObject(new
        //    {
        //        Success = true,
        //        Message = "Success",
        //        data = new
        //        {
        //            notif = dtNotif
        //        }
        //    });
        //}
        [WebMethod]
        public static string GetCheckerCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply in ('1','2')";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }

        [WebMethod]
        public static string GetApproverCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply  = '2'";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }

        [WebMethod]
        public static string GetState()
        {
            clsConnection cls = new clsConnection();
            DataTable dtState = new DataTable();

            string query = "select DISTINCT(LTRIM(RTRIM(State))) [State], State_Abbr from tbl_DB_US_States (nolock) where isStateActive = 1 and State is not null and State <> '' order by [State]";

            dtState = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
        }

        [WebMethod]
        public static string GetMunicipality(string state)
        {
            clsConnection cls = new clsConnection();
            DataTable dtMunicipality = new DataTable();

            string query = "";

            if (state != "")
            {
                query = "select LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where isActive = 1 and LEFT(municipality_name, 2) = '" + state + "' ";
                query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = '" + state + "') group by municipality_name, municipality_code order by municipality_name";
            }
            else
            {
                query = "select LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where isActive = 1 ";
                query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = 'CT') group by municipality_name, municipality_code order by municipality_name";
            }

            dtMunicipality = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtMunicipality } });
        }

        [WebMethod]
        public static string GetCity(string state)
        {
            clsConnection cls = new clsConnection();
            DataTable dtCity = new DataTable();

            string query = "";

            if (state != "")
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' order by [City]";
            }
            else
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and City is not null and City <> '' order by [City]";
            }

            dtCity = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
        }

        [WebMethod]
        public static string GetZip(string city)
        {
            clsConnection cls = new clsConnection();
            DataTable dtZip = new DataTable();

            string query = "";

            //if (city != "")
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and LTRIM(RTRIM(City)) = '" + city + "' and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            //}
            //else
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            //}

            if (city != "")
            {
                query = "select d.muni_name, dbus.ZipCode, dbus.isZipActive from tbl_DB_US_States [dbus] (nolock) outer apply (select LEFT(a.municipality_name, 2) [State_Abbr], ";
                query += "b.state, RIGHT(a.municipality_name, (LEN(a.municipality_name) - 4)) [muni_name] from tbl_VPR_Municipality a (nolock) outer apply ";
                query += "(select State_Abbr, LTRIM(RTRIM(State)) [state], ZipCode from tbl_DB_US_States (nolock) where ";
                query += "State_Abbr = UPPER(LEFT(a.municipality_name, 2)) group by State_Abbr, LTRIM(RTRIM(State)),ZipCode) b ";
                query += "where UPPER(LEFT(a.municipality_name, 2)) <> '--' and a.municipality_name ";
                query += "not in ('Send to Peak','VACANT REGISTRY','ATTORNEY - STERN & EISENBERG PC')and a.municipality_code = '" + city + "'";
                query += "group by LEFT(a.municipality_name, 2), b.state, a.municipality_name ) d where dbus.State_Abbr = d.State_Abbr ";
                query += "and (LTRIM(RTRIM(dbus.Municipality)) = LTRIM(RTRIM(d.muni_name)) OR ";
                query += "LTRIM(RTRIM(dbus.Municipality)) = REPLACE(LTRIM(RTRIM(d.muni_name)),'(2)','')) order by d.muni_name";
            }
            else
            {
                query = "select d.muni_name, dbus.ZipCode, dbus.isZipActive from tbl_DB_US_States [dbus] outer apply (select LEFT(a.municipality_name, 2) [State_Abbr], ";
                query += "b.state, RIGHT(a.municipality_name, (LEN(a.municipality_name) - 4)) [muni_name] from tbl_VPR_Municipality a outer apply ";
                query += "(select State_Abbr, LTRIM(RTRIM(State)) [state], ZipCode from tbl_DB_US_States where ";
                query += "State_Abbr = UPPER(LEFT(a.municipality_name, 2)) group by State_Abbr, LTRIM(RTRIM(State)),ZipCode) b ";
                query += "where UPPER(LEFT(a.municipality_name, 2)) <> '--' and a.municipality_name ";
                query += "not in ('Send to Peak','VACANT REGISTRY','ATTORNEY - STERN & EISENBERG PC') ";
                query += "group by LEFT(a.municipality_name, 2), b.state, a.municipality_name ) d where dbus.State_Abbr = d.State_Abbr ";
                query += "and (LTRIM(RTRIM(dbus.Municipality)) = LTRIM(RTRIM(d.muni_name)) OR ";
                query += "LTRIM(RTRIM(dbus.Municipality)) = REPLACE(LTRIM(RTRIM(d.muni_name)),'(2)','')) order by d.muni_name";
            }


            dtZip = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtZip } });
        }

        [WebMethod]

        public static string getContactZip(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string strQuery = "";
            DataTable dtContactZip = new DataTable();

            strQuery = @"select distinct ZipCode from tbl_DB_US_States (nolock) 
                                where isCityActive = 1 and 
                                [State] = '" + state + "' and City = '" + city + "' order by ZipCode";

            dtContactZip = cls.GetData(strQuery);

            return JsonConvert.SerializeObject(
                new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        contactZip = dtContactZip
                    }
                });
        }

        [WebMethod]
        public static string getMuniZip(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string strQuery = "";
            DataTable dtmuniZip = new DataTable();

            strQuery = @"select distinct ZipCode from tbl_DB_US_States (nolock) 
                                where isCityActive = 1 and 
                                [State] = '" + state + "' and City = '" + city + "' order by ZipCode";

            dtmuniZip = cls.GetData(strQuery);

            return JsonConvert.SerializeObject(
                new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        muniZip = dtmuniZip
                    }
                });
        }

        //[WebMethod]
        //public static string GetOrdinance(string state, string city, string hist)
        //{
        //    clsConnection cls = new clsConnection();
        //    DataTable dtOrdinance = new DataTable(); 
        //    DataTable dtSettings = new DataTable();

        //    string query = "", qrySetting = "";

        //    qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
        //    dtSettings = cls.GetData(qrySetting);



        //    if (hist != "" && hist != "undefined")
        //    {
        //        if (hist == "hist")
        //        {
        //            query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "'";
        //        }
        //        else
        //        {
        //            query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where id = '" + hist + "'";
        //        }
        //    }
        //    else
        //    {
        //        query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "' and date_deleted is null";
        //    }

        //    dtOrdinance = cls.GetData(query);

        //    if (dtOrdinance.Rows.Count > 0)
        //    {
        //        ordinaceCtr = true;
        //    }
        //    else
        //    {
        //        ordinaceCtr = false;
        //    }

        //    return JsonConvert.SerializeObject(new 
        //    { 
        //        Success = true,
        //        Message = "Success", 
        //        data = new 
        //        { 
        //            record = dtOrdinance, 
        //            forSettings = dtSettings
        //        } 
        //    });
        //}

        //[WebMethod]
        //public static void DeleteOrdinance(string id)
        //{
        //    clsConnection cls = new clsConnection();

        //    string query = "update tbl_VPR_Workable_Ordinance set date_deleted = GETDATE() where id = '" + id + "'";

        //    try
        //    {
        //        cls.ExecuteQuery(query);

        //        ArrayList arrPages = new ArrayList();
        //        arrPages.Add("Ordinance Settings");
        //        arrPages.Add("");

        //        cls.FUNC.Audit("Deleted Ordinance Id: " + id, arrPages);
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}



        [WebMethod]
        public static string GetDataOrdinance(string municipalityCode)//, string hist)
        {
            clsConnection cls = new clsConnection();
            DataTable dtOrdinance = new DataTable();

            string query = "";

            //if (hist != "" && hist != "undefined")
            //{
            //    if (hist == "hist")
            //    {
            //query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance (nolock) where city = '" + municipalityCode + "'";
            //    }
            //    else
            //    {
            //query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where id = '" + hist + "'";
            //    }
            //}
            //else
            //{
            query = "select top 1 * from tbl_VPR_Workable_Ordinance (nolock) where city = '" + municipalityCode + "' order by id desc, date_modified desc";

            //}

            dtOrdinance = cls.GetData(query);



            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtOrdinance } });
        }

        [WebMethod]
        public static string getOrdinanceHistory(string state, string city)
        {
            clsConnection cls = new clsConnection();
            DataTable dtOrdinance = new DataTable();

            string query = @"select id, ordinance_num, ordinance_name, ordinance_file, description, section, source, [enacted_date], 
                            [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance
                            (nolock) where state = '" + state + "' and city = '" + city + "'";

            dtOrdinance = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtOrdinance
            } });
        }

        [WebMethod]
        public static string AuditTrail(List<string> data, string user, string tag)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string insert = "", top = "", muniName = data[0].ToString();

            if (tag == "pfc")
            {
                string mainTab = "Registration Criteria";
                string sub = "PFC";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_PFC where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_PFC ([Municipality Name], [Municipality Code], [PFC Default], [PFC Default Timeline],
                            [PFC Vacant], [PFC Vacant Timeline], [PFC Foreclosure], [PFC Foreclosure Timeline], [PFC Foreclosure and Vacant],
                            [PFC Foreclosure and Vacant Timeline], [PFC City Notice], [PFC City Notice Timeline], [PFC Code Violation], [PFC Code Violation Timeline],
                            [PFC Boarded], [PFC Boarded Timeline], [PFC Other], [PFC Other Timeline], [Payment Type], [Type of Registration], [PDF], [Renewal], [Renewal PDF],
                            [Renewal Every], [Renewal On], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', '" + data[17].ToString() + "', '" +
                            data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" + data[21].ToString() + "', '" + data[22].ToString() + "', '" +
                            data[23].ToString() + "', '" + data[24].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "reo")
            {
                string mainTab = "Registration Criteria";
                string sub = "REO";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_REO where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_REO ([Municipality Name], [Municipality Code], [REO Bank-owned], [REO Bank-owned Timeline],
                            [REO Vacant], [REO Vacant Timeline], [REO City Notice], [REO City Notice Timeline], [REO Code Violation],
                            [REO Code Violation Timeline], [REO Boarded Only], [REO Boarded Timeline], [REO Distressed/Abandoned], [REO Distressed/Abandoned Timeline],
                            [Rental Registration], [Rental Registration Timeline], [REO Other], [REO Other Timeline], [Payment Type], [Type of Registration], [PDF], [Renewal], [Renewal PDF],
                            [Renewal Every], [Renewal On], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', '" + data[17].ToString() + "', '" +
                            data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" + data[21].ToString() + "', '" + data[22].ToString() + "', '" +
                            data[23].ToString() + "', '" + data[24].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "property")
            {
                string mainTab = "Registration Criteria";
                string sub = "Property Type";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_PropertyType where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_PropertyType ([Municipality Name], [Municipality Code], [Residential], [Single Family],
                            [Multi Family], [2 Units], [3 Units], [4 Units], [5 Units or more],
                            [Rental], [Commercial], [Condo], [Townhome], [Vacant Lot],
                            [Mobile Home], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "continue")
            {
                string mainTab = "Registration Criteria";
                string sub = "Continuing Registration";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_ContinuingRegistration where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_ContinuingRegistration ([Municipality Name], [Municipality Code], [Continue Registration from PFC to REO?], [Update Registration with Municipality],
                           [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "municipalityInfo")
            {
                string mainTab = "Municipality Contact Information";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_MunicipalityInfo where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_MunicipalityInfo ([Municipality Name], [Municipality Code], [Municipality Department], 
                            [Municipality Phone Number], [Municipality Email Address], [Municipality Mailing Address], [Municipality Website], 
                            [Contact Person], [Title], [Department], [Phone Number], [Email Address], [Mailing Address], [From Hours],  
                            [To Hours],  [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "deregistration")
            {
                string mainTab = "Deregistration";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_Deregistration where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);

                insert = @"insert into tbl_VPR_Audit_Deregistration ([Municipality Name], [Municipality Code], [Deregistration Required], [Only if Conveyed],  
                        [Only if Occupied], [How to Deregister], [PDF], [New Owner Information Required], [Proof of Conveyance Required], [Date of Sale Required],      
                        [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                        data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                        data[8].ToString() + "', '" + data[9].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "ordinance")
            {
                string mainTab = "Ordinance Settings";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_Ordinance where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_Ordinance ([Municipality Name], [Municipality Code], [Ordinance Number], [Ordinance Name], [Description], [Ordinance Copy], 
                         [Section], [Source], [Enacted Date], [Revision Date], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() +
                        "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                        data[8].ToString() + "', '" + data[9].ToString() + "','" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }


                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }

            int exec = cls.ExecuteQuery(insert);

            string result = (exec > 0) ? "1" : "2";

            return result;
        }
                 
        [WebMethod]
        public static string SaveWorkableOrdinance(List<string> data, string user, string isNull, string settingID)
        {
            clsConnection cls = new clsConnection();

            string query, tag = "";

            if (data[0] == "regOrdinance")
            {
                query = @"insert into tbl_VPR_Workable_Ordinance (state, city, ordinance_num, ordinance_name, description, section, 
                            source, enacted_date, revision_date, ordinance_file, modified_by, date_modified,tag) values " +
                 "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                 "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                 "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + user.ToString() + "', GETDATE(), '');";

                if (isNull != "1")
                {
                    if (settingID != "")
                    {
                        query += @"update tbl_VPR_MainSettings set ordinanceSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        query += @"insert into tbl_VPR_MainSettings ([state], city, ordinanceSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                            "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                    }
                }
                else
                {
                    if (settingID != "")
                    {
                        query += @"update tbl_VPR_MainSettings set ordinanceSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        query += @"insert into tbl_VPR_MainSettings ([state], city, ordinanceSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                            "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            return tag;
        }


        [WebMethod]
        public static string SaveWorkable(List<string> data, string user, string existingID, string isNull, string settingID)
        {
            clsConnection cls = new clsConnection();

            DataTable dt = new DataTable();
            DataTable dtPast = new DataTable();
            DataTable dtPresent = new DataTable();
            string present, past, query, tag = "";

            if (data[0] == "regPFC")
            {
                if (existingID == "")
                {
                    query = @"insert into tbl_VPR_Workable_Registration_PFC
                        ([state], city,
                         pfc_default, def_reg_timeline1, def_reg_timeline2, def_reg_timeline3, def_reg_timeline4, def_reg_timeline5,
                         pfc_vacant, pfc_vacant_timeline1, pfc_vacant_timeline2, pfc_vacant_timeline3, pfc_vacant_timeline4, pfc_vacant_timeline5,
                         pfc_foreclosure, pfc_def_foreclosure_timeline1, pfc_def_foreclosure_timeline2, pfc_def_foreclosure_timeline3, pfc_def_foreclosure_timeline4, pfc_def_foreclosure_timeline5,
                         pfc_foreclosure_vacant, pfc_foreclosure_vacant_timeline1, pfc_foreclosure_vacant_timeline2, pfc_foreclosure_vacant_timeline3, pfc_foreclosure_vacant_timeline4, pfc_foreclosure_vacant_timeline5,
                         pfc_city_notice, pfc_city_notice_timeline1, pfc_city_notice_timeline2, pfc_city_notice_timeline3, pfc_city_notice_timeline4, pfc_city_notice_timeline5,
                         pfc_code_violation, pfc_code_violation_timeline1, pfc_code_violation_timeline2, pfc_code_violation_timeline3, pfc_code_violation_timeline4, pfc_code_violation_timeline5,
                         pfc_boarded, pfc_boarded_timeline1, pfc_boarded_timeline2, pfc_boarded_timeline3,  pfc_boarded_timeline4, pfc_boarded_timeline5,
                         pfc_other, pfc_other_timeline1, pfc_other_timeline2, pfc_other_timeline3, pfc_other_timeline4, pfc_other_timeline5,
                         payment_type, type_of_registration, vms_renewal, upload_path, no_pfc_reg, statewide_reg, renew_every, renew_on, renew_every_num,
                         renew_every_years, renew_on_months, renew_on_num, renew_upload_path, [Trigger],
                         tag, modified_by, date_modified) values
                         ('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" +
                                 data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" +
                                 data[11].ToString() + "', '" + data[12].ToString() + "', '" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" +
                                 data[16].ToString() + "', '" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" +
                                 data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', '" + data[25].ToString() + "', '" +
                                 data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', '" + data[29].ToString() + "', '" + data[30].ToString() + "', '" +
                                 data[31].ToString() + "', '" + data[32].ToString() + "', '" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', '" +
                                 data[36].ToString() + "', '" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "', '" + data[40].ToString() + "', '" +
                                 data[41].ToString() + "', '" + data[42].ToString() + "', '" + data[43].ToString() + "', '" + data[44].ToString() + "', '" + data[45].ToString() + "', '" +
                                 data[46].ToString() + "', '" + data[47].ToString() + "', '" + data[48].ToString() + "', '" + data[49].ToString() + "', '" + data[50].ToString() + "', '" +
                                 data[51].ToString() + "', '" + data[52].ToString() + "', '" + data[53].ToString() + "', '" + data[54].ToString() + "', '" + data[55].ToString() + "', '" +
                                 data[56].ToString() + "', '" + data[57].ToString() + "', '" + data[58].ToString() + "', '" + data[59].ToString() + "', '" + data[60].ToString() + "', '" +
                                 data[61].ToString() + "', '" + data[62].ToString() + "', '" + data[63].ToString() + "', '" + data[64].ToString() + "', '', '" + user.ToString() + "', GETDATE())";
                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set pfcSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, pfcSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set pfcSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, pfcSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Registration_PFC set [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() +
                        "', pfc_default = '" + data[3].ToString() + "', def_reg_timeline1 = '" + data[4].ToString() + "', def_reg_timeline2 = '" + data[5].ToString() + "', def_reg_timeline3 = '" + data[6].ToString() + "', def_reg_timeline4 = '" + data[7].ToString() + "', def_reg_timeline5 = '" + data[8].ToString() +
                        "', pfc_vacant = '" + data[9].ToString() + "', pfc_vacant_timeline1 = '" + data[10].ToString() + "', pfc_vacant_timeline2 = '" + data[11].ToString() + "', pfc_vacant_timeline3 = '" + data[12].ToString() + "', pfc_vacant_timeline4 = '" + data[13].ToString() + "', pfc_vacant_timeline5 = '" + data[14].ToString() +
                        "', pfc_foreclosure = '" + data[15].ToString() + "', pfc_def_foreclosure_timeline1 = '" + data[16].ToString() + "', pfc_def_foreclosure_timeline2 = '" + data[17].ToString() + "', pfc_def_foreclosure_timeline3 = '" + data[18].ToString() + "', pfc_def_foreclosure_timeline4 = '" + data[19].ToString() + "', pfc_def_foreclosure_timeline5 = '" + data[20].ToString() +
                        "', pfc_foreclosure_vacant = '" + data[21].ToString() + "', pfc_foreclosure_vacant_timeline1 = '" + data[22].ToString() + "', pfc_foreclosure_vacant_timeline2 = '" + data[23].ToString() + "', pfc_foreclosure_vacant_timeline3 = '" + data[24].ToString() + "', pfc_foreclosure_vacant_timeline4 = '" + data[25].ToString() + "', pfc_foreclosure_vacant_timeline5 = '" + data[26].ToString() +
                        "', pfc_city_notice = '" + data[27].ToString() + "', pfc_city_notice_timeline1 = '" + data[28].ToString() + "', pfc_city_notice_timeline2 = '" + data[29].ToString() + "', pfc_city_notice_timeline3 = '" + data[30].ToString() + "', pfc_city_notice_timeline4 = '" + data[31].ToString() + "', pfc_city_notice_timeline5 = '" + data[32].ToString() +
                        "', pfc_code_violation = '" + data[33].ToString() + "', pfc_code_violation_timeline1 = '" + data[34].ToString() + "', pfc_code_violation_timeline2 = '" + data[35].ToString() + "', pfc_code_violation_timeline3 = '" + data[36].ToString() + "', pfc_code_violation_timeline4 = '" + data[37].ToString() + "', pfc_code_violation_timeline5 = '" + data[38].ToString() +
                        "', pfc_boarded = '" + data[39].ToString() + "', pfc_boarded_timeline1 = '" + data[40].ToString() + "', pfc_boarded_timeline2 = '" + data[41].ToString() + "', pfc_boarded_timeline3 = '" + data[42].ToString() + "', pfc_boarded_timeline4 = '" + data[43].ToString() + "', pfc_boarded_timeline5 = '" + data[44].ToString() +
                        "', pfc_other = '" + data[45].ToString() + "', pfc_other_timeline1 = '" + data[46].ToString() + "', pfc_other_timeline2 = '" + data[47].ToString() + "', pfc_other_timeline3 = '" + data[48].ToString() + "', pfc_other_timeline4 = '" + data[49].ToString() + "', pfc_other_timeline5 = '" + data[50].ToString() +
                        "', payment_type = '" + data[51].ToString() + "', type_of_registration = '" + data[52].ToString() + "', vms_renewal = '" + data[53].ToString() + "', upload_path = '" + data[54].ToString() + "', no_pfc_reg = '" + data[55].ToString() + "', statewide_reg = '" + data[56].ToString() + "', renew_every = '" + data[57].ToString() + "', renew_on = '" + data[58].ToString() + "', renew_every_num = '" + data[59].ToString() +
                        "', renew_every_years = '" + data[60].ToString() + "', renew_on_months = '" + data[61].ToString() + "', renew_on_num = '" + data[62].ToString() + "', renew_upload_path = '" + data[63].ToString() +
                        "', [Trigger] = '" + data[64].ToString() + "', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set pfcSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, pfcSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set pfcSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, pfcSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regREO")
            {

                if (existingID == "")
                {
                    query = @"insert into tbl_VPR_Workable_Registration_REO
                            ([state], city,
                            reo_bank_owed, reo_bank_owed_timeline1, reo_bank_owed_timeline2, reo_bank_owed_timeline3, reo_bank_owed_timeline4, reo_bank_owed_timeline5,
                            reo_vacant, reo_vacant_timeline1, reo_vacant_timeline2, reo_vacant_timeline3, reo_vacant_timeline4, reo_vacant_timeline5,
                            reo_city_notice, reo_city_notice_timeline1, reo_city_notice_timeline2, reo_city_notice_timeline3, reo_city_notice_timeline4, reo_city_notice_timeline5,
                            reo_code_violation, reo_code_violation_timeline1, reo_code_violation_timeline2, reo_code_violation_timeline3, reo_code_violation_timeline4, reo_code_violation_timeline5,
                            reo_boarded_only, reo_boarded_only_timeline1, reo_boarded_only_timeline2, reo_boarded_only_timeline3, reo_boarded_only_timeline4, reo_boarded_only_timeline5,
                            reo_distressed_abandoned, reo_distressed_abandoned1, reo_distressed_abandoned2, reo_distressed_abandoned3, reo_distressed_abandoned4, reo_distressed_abandoned5,
                            rental_registration, rental_registration_timeline1, rental_registration_timeline2, rental_registration_timeline3, rental_registration_timeline4, rental_registration_timeline5,
                            reo_other, reo_other_timeline1, reo_other_timeline2, reo_other_timeline3, reo_other_timeline4, reo_other_timeline5,
                            payment_type, type_of_registration, vms_renewal, upload_path, no_reo_reg, statewide_reg, renew_every, renew_on, renew_every_num, renew_every_years,
                            renew_on_months, renew_on_num, reo_renew_uploadpath, [Trigger], tag, modified_by, date_modified) values
                             ('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" +
                                 data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" +
                                 data[11].ToString() + "', '" + data[12].ToString() + "', '" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" +
                                 data[16].ToString() + "', '" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" +
                                 data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', '" + data[25].ToString() + "', '" +
                                 data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', '" + data[29].ToString() + "', '" + data[30].ToString() + "', '" +
                                 data[31].ToString() + "', '" + data[32].ToString() + "', '" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', '" +
                                 data[36].ToString() + "', '" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "', '" + data[40].ToString() + "', '" +
                                 data[41].ToString() + "', '" + data[42].ToString() + "', '" + data[43].ToString() + "', '" + data[44].ToString() + "', '" + data[45].ToString() + "', '" +
                                 data[46].ToString() + "', '" + data[47].ToString() + "', '" + data[48].ToString() + "', '" + data[49].ToString() + "', '" + data[50].ToString() + "', '" +
                                 data[51].ToString() + "', '" + data[52].ToString() + "', '" + data[53].ToString() + "', '" + data[54].ToString() + "', '" + data[55].ToString() + "', '" +
                                 data[56].ToString() + "', '" + data[57].ToString() + "', '" + data[58].ToString() + "', '" + data[59].ToString() + "', '" + data[60].ToString() + "', '" +
                                 data[61].ToString() + "', '" + data[62].ToString() + "', '" + data[63].ToString() + "', '" + data[64].ToString() + "', '', '" + user.ToString() + "', GETDATE())";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set reoSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, reoSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set reoSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, reoSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Registration_REO set 
                                                [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() +
                                       "', reo_bank_owed = '" + data[3].ToString() + "', reo_bank_owed_timeline1 = '" + data[4].ToString() + "', reo_bank_owed_timeline2 = '" + data[5].ToString() + "', reo_bank_owed_timeline3 = '" + data[6].ToString() + "', reo_bank_owed_timeline4 = '" + data[7].ToString() + "', reo_bank_owed_timeline5 = '" + data[8].ToString() +
                                       "', reo_vacant = '" + data[9].ToString() + "', reo_vacant_timeline1 = '" + data[10].ToString() + "', reo_vacant_timeline2 = '" + data[11].ToString() + "', reo_vacant_timeline3 = '" + data[12].ToString() + "', reo_vacant_timeline4 = '" + data[13].ToString() + "', reo_vacant_timeline5 = '" + data[14].ToString() +
                                       "', reo_city_notice = '" + data[15].ToString() + "', reo_city_notice_timeline1 = '" + data[16].ToString() + "', reo_city_notice_timeline2 = '" + data[17].ToString() + "', reo_city_notice_timeline3 = '" + data[18].ToString() + "', reo_city_notice_timeline4 = '" + data[19].ToString() + "', reo_city_notice_timeline5 = '" + data[20].ToString() +
                                       "', reo_code_violation = '" + data[21].ToString() + "', reo_code_violation_timeline1 = '" + data[22].ToString() + "', reo_code_violation_timeline2 = '" + data[23].ToString() + "', reo_code_violation_timeline3 = '" + data[24].ToString() + "', reo_code_violation_timeline4 = '" + data[25].ToString() + "', reo_code_violation_timeline5 = '" + data[26].ToString() +
                                       "', reo_boarded_only = '" + data[27].ToString() + "', reo_boarded_only_timeline1 = '" + data[28].ToString() + "', reo_boarded_only_timeline2 = '" + data[29].ToString() + "', reo_boarded_only_timeline3 = '" + data[30].ToString() + "', reo_boarded_only_timeline4 = '" + data[31].ToString() + "', reo_boarded_only_timeline5 = '" + data[32].ToString() +
                                       "', reo_distressed_abandoned = '" + data[33].ToString() + "', reo_distressed_abandoned1 = '" + data[34].ToString() + "', reo_distressed_abandoned2 = '" + data[35].ToString() + "', reo_distressed_abandoned3 = '" + data[36].ToString() + "',reo_distressed_abandoned4 = '" + data[37].ToString() + "', reo_distressed_abandoned5 = '" + data[38].ToString() +
                                       "', rental_registration = '" + data[39].ToString() + "', rental_registration_timeline1 = '" + data[40].ToString() + "', rental_registration_timeline2 = '" + data[41].ToString() + "', rental_registration_timeline3 = '" + data[42].ToString() + "', rental_registration_timeline4 = '" + data[43].ToString() + "', rental_registration_timeline5 = '" + data[44].ToString() +
                                       "', reo_other  = '" + data[45].ToString() + "', reo_other_timeline1 = '" + data[46].ToString() + "', reo_other_timeline2 = '" + data[47].ToString() + "', reo_other_timeline3 = '" + data[48].ToString() + "', reo_other_timeline4 = '" + data[49].ToString() + "', reo_other_timeline5 = '" + data[50].ToString() +
                                       "', payment_type = '" + data[51].ToString() + "', type_of_registration = '" + data[52].ToString() + "', vms_renewal = '" + data[53].ToString() + "', upload_path = '" + data[54].ToString() + "', no_reo_reg = '" + data[55].ToString() + "', statewide_reg = '" + data[56].ToString() + "', renew_every = '" + data[57].ToString() +
                                       "', renew_on = '" + data[58].ToString() +
                                       "', renew_every_num = '" + data[59].ToString() + "', renew_every_years = '" + data[60].ToString() + "', renew_on_months = '" + data[61].ToString() + "', renew_on_num = '" + data[62].ToString() + "', reo_renew_uploadpath = '" + data[63].ToString() +
                                       "', [Trigger] = '" + data[64].ToString() +"', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set reoSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, reoSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set reoSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, reoSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";

                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regProperty")
            {
                if (existingID == "")
                {
                    query = @"insert into tbl_VPR_Workable_Registration_Property ([state], city, residential,single_family, " +
                        "multi_family,unit2,unit3,unit4,unit5,rental,commercial,condo, " +
                         "townhome,vacant_lot,mobile_home,tag, modified_by, date_modified) values " +
                         "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                         "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                         "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "','" + data[12].ToString() + "','" +
                         data[13].ToString() + "','" + data[14].ToString() + "','" + data[15].ToString() + "','', '" + user.ToString() + "', GETDATE())";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set propTypeSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, propTypeSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set propTypeSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, propTypeSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Registration_Property set
                                    [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() +
                                             "', residential = '" + data[3].ToString() + "', single_family = '" + data[4].ToString() + "', multi_family = '" + data[5].ToString() +
                                             "', unit2 = '" + data[6].ToString() + "', unit3 = '" + data[7].ToString() + "', unit4 = '" + data[8].ToString() + "', unit5 = '" + data[9].ToString() +
                                             "', rental = '" + data[10].ToString() + "', commercial = '" + data[11].ToString() + "', condo = '" + data[12].ToString() +
                                             "', townhome = '" + data[13].ToString() + "', vacant_lot = '" + data[14].ToString() + "', mobile_home = '" + data[15].ToString() +
                                             "', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set propTypeSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, propTypeSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set propTypeSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, propTypeSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }


                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regPropReq")
            {
                if (existingID == "")
                {

                    query = "insert into tbl_VPR_Workable_Registration_PropReq_Bond (one_time_only, bond_amount_escalating, bond_amount, bond_schedule_amount) values " +
                            "('" + data[28].ToString() + "', '" + data[29].ToString() + "', " +
                            "'" + data[30].ToString() + "', '" + data[31].ToString() + "')";

                    query += "insert into tbl_VPR_Workable_Registration_PropReq_Contact ([state], city, lcicompany_name, lcifirst_name, lcilast_name, lcititle, " +
                           "lcibusiness_license_num, lciphone_num1, lciphone_num2, lcibusiness_phone_num1, lcibusiness_phone_num2, lciemergency_phone_num1, " +
                           "lciemergency_phone_num2, lcifax_num1, lcifax_num2, lcicell_num1, lcicell_num2, lciemail, lcistreet, lcistate, lcicity, lcizip, " +
                           "lcihours_from, lcihours_to) values " +
                           "('" + data[54].ToString() + "', '" + data[55].ToString() + "', " +
                           "'" + data[32].ToString() + "', '" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', " +
                           "'" + data[36].ToString() + "', '" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "', " +
                           "'" + data[40].ToString() + "', '" + data[41].ToString() + "', '" + data[42].ToString() + "', '" + data[43].ToString() + "', " +
                           "'" + data[44].ToString() + "', '" + data[45].ToString() + "', '" + data[46].ToString() + "', '" + data[47].ToString() + "', " +
                           "'" + data[48].ToString() + "', '" + data[49].ToString() + "', '" + data[50].ToString() + "', '" + data[51].ToString() + "', " +
                           "'" + data[52].ToString() + "', '" + data[53].ToString() + "')";

                    query += @"insert into tbl_VPR_Workable_Registration_PropReq (state, city, presale_definition, local_contact_required, gse_exclusion, insurance_required,
                             foreclosure_action_info_needed, foreclosure_case_info_needed, foreclosure_deed_required, bond_required, utility_information_required, winterization_required,
                             signature_required, notarization_required, recent_inspection_Date, first_time_vacancy_date, secured_required, additional_signage_required, pictures_required,
                             mobile_vin_number_required, parcel_number_required, legal_description_required, block_lot_number_required, attorney_information_required, broker_information_required_reo,
                             mortgage_contact_name_required, client_tax_number_required, tag, modified_by, date_modified, propReqID, bondID) values " +
                            "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                            "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                            "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                            "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                            "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
                            "'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
                            "'" + data[25].ToString() + "', '" + data[26].ToString() + "', '" + data[27].ToString() + "', '', '" + user.ToString() + "', " +
                            "GETDATE(), (select Max(propReqID) from tbl_VPR_Workable_Registration_PropReq_Contact), (select Max(bondID) from tbl_VPR_Workable_Registration_PropReq_Bond));";

                

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set requirementSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, requirementSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set requirementSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, requirementSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Registration_PropReq set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', presale_definition = '" + data[3].ToString() 
                        + "', local_contact_required = '" + data[4].ToString() + "', gse_exclusion = '" + data[5].ToString() + "', insurance_required = '" + data[6].ToString() + "', foreclosure_action_info_needed = '" + data[7].ToString() 
                        + "', foreclosure_case_info_needed = '" + data[8].ToString() + "', foreclosure_deed_required = '" + data[9].ToString() + "', bond_required = '" + data[10].ToString() 
                        + "', utility_information_required = '" + data[11].ToString() + "', winterization_required = '" + data[12].ToString() + "', signature_required = '" + data[13].ToString() 
                        + "', notarization_required = '" + data[14].ToString() + "', recent_inspection_Date = '" + data[15].ToString() + "', first_time_vacancy_date = '" + data[16].ToString() 
                        + "', secured_required = '" + data[17].ToString() + "', additional_signage_required = '" + data[18].ToString() + "', pictures_required = '" + data[19].ToString() 
                        + "', mobile_vin_number_required = '" + data[20].ToString() + "', parcel_number_required = '" + data[21].ToString() + "', legal_description_required = '" + data[22].ToString() 
                        + "', block_lot_number_required = '" + data[23].ToString() + "', attorney_information_required = '" + data[24].ToString() + "', broker_information_required_reo = '" + data[25].ToString() 
                        + "', mortgage_contact_name_required = '" + data[26].ToString() + "', client_tax_number_required = '" + data[27].ToString() + "', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    query += @"update tbl_VPR_Workable_Registration_PropReq_Bond set one_time_only = '" + data[28].ToString() + "', bond_amount_escalating = '" + data[29].ToString() + "', bond_amount = '" + data[30].ToString()
                        + "', bond_schedule_amount = '" + data[31].ToString() + "' where bondID = (select bondID from tbl_VPR_Workable_Registration_PropReq where id = '" + existingID + "')";

                    query += @"update tbl_VPR_Workable_Registration_PropReq_Contact set [state] = '" + data[54].ToString() + "', city = '" + data[55].ToString() + "', lcicompany_name = '" + data[32].ToString() +
                                       "', lcifirst_name = '" + data[33].ToString() + "', lcilast_name = '" + data[34].ToString() + "', lcititle = '" + data[35].ToString() + "', lcibusiness_license_num = '" + data[36].ToString() +
                                       "', lciphone_num1 = '" + data[37].ToString() + "', lciphone_num2 = '" + data[38].ToString() + "', lcibusiness_phone_num1 = '" + data[39].ToString() + "', lcibusiness_phone_num2 = '" + data[40].ToString() +
                                       "', lciemergency_phone_num1 = '" + data[41].ToString() + "', lciemergency_phone_num2 = '" + data[42].ToString() + "', lcifax_num1 = '" + data[43].ToString() + "', lcifax_num2 = '" + data[44].ToString() +
                                       "', lcicell_num1 = '" + data[45].ToString() + "', lcicell_num2 = '" + data[46].ToString() + "', lciemail = '" + data[47].ToString() + "', lcistreet = '" + data[48].ToString() +
                                       "', lcistate = '" + data[49].ToString() + "', lcicity = '" + data[50].ToString() + "', lcizip = '" + data[51].ToString() + "', lcihours_from = '" + data[52].ToString() +
                                       "', lcihours_to = '" + data[53].ToString() + "' where propReqID = (select propReqID from tbl_VPR_Workable_Registration_PropReq where id = '" + existingID + "')";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set requirementSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, requirementSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set requirementSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, requirementSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCost")
            {
                string dataStoragePFC = "";
                string dataStorageREO = "";
                string dataStorageCommercial = "";
                string dataStorageVacant = "";
                for (int i = 1; i < data.Count; i++)
                {
                    if (data[i] == null || data[i] == "undefined")
                    {
                        data[i] = "";
                    }
                    if (i >= 3 && i <= 48)
                    {
                        dataStoragePFC += "'" + data[i].ToString() + "',";
                    }
                    else if (i >= 49 && i <= 94)
                    {
                        dataStorageREO += "'" + data[i].ToString() + "',";
                    }
                    else if (i >= 95 && i <= 140)
                    {
                        dataStorageCommercial += "'" + data[i].ToString() + "',";
                    }
                    else if (i >= 141 && i <= 186)
                    {
                        dataStorageVacant += "'" + data[i].ToString() + "',";
                    }
                }
                dataStoragePFC = dataStoragePFC.Remove(dataStoragePFC.Length - 1, 1);
                dataStorageREO = dataStorageREO.Remove(dataStorageREO.Length - 1, 1);
                dataStorageCommercial = dataStorageCommercial.Remove(dataStorageCommercial.Length - 1, 1);
                dataStorageVacant = dataStorageVacant.Remove(dataStorageVacant.Length - 1, 1);

                if (existingID == "")
                {
                    query = @"insert into tbl_VPR_Cost_PFC (pfc_reg_cost, pfc_reg_cost_units, pfc_reg_cost_units_amount, pfc_reg_cost_units_check, 
                                pfc_reg_cost_square, pfc_reg_cost_square_feet, pfc_reg_cost_square_amount, pfc_reg_cost_square_check, pfc_reg_cost_amount, 
                                pfc_reg_cost_standard, pfc_renewal_cost_escalating, pfc_renewal_cost_units, pfc_renewal_cost_units_amount, pfc_renewal_cost_units_check, 
                                pfc_renewal_cost_square, pfc_renewal_cost_square_feet, pfc_renewal_cost_square_amount, pfc_renewal_cost_square_check, pfc_renewal_cost_amount, 
                                pfc_escalating_renewal_cost_amount, pfc_escalating_renewal_succeding, pfc_first_escalating_cost_units_amount, pfc_first_escalating_cost_units_check, 
                                pfc_first_escalating_cost_square_feet, pfc_first_escalating_cost_square_amount, pfc_first_escalating_cost_square_check, pfc_second_escalating_cost_units_amount, 
                                pfc_second_escalating_cost_units_check, pfc_second_escalating_cost_square_feet, pfc_second_escalating_cost_square_amount, pfc_second_escalating_cost_square_check, 
                                pfc_third_escalating_cost_units_amount, pfc_third_escalating_cost_units_check, pfc_third_escalating_cost_square_feet, pfc_third_escalating_cost_square_amount, 
                                pfc_third_escalating_cost_square_check, pfc_fourth_escalating_cost_units_amount, pfc_fourth_escalating_cost_units_check, pfc_fourth_escalating_cost_square_feet, 
                                pfc_fourth_escalating_cost_square_amount, pfc_fourth_escalating_cost_square_check, pfc_fifth_escalating_cost_units_amount, pfc_fifth_escalating_cost_units_check, 
                                pfc_fifth_escalating_cost_square_feet, pfc_fifth_escalating_cost_square_amount, pfc_fifth_escalating_cost_square_check) 
                                values(" + dataStoragePFC + ")";

                    query += @"insert into tbl_VPR_Cost_REO (reo_reg_cost, reo_reg_cost_units, reo_reg_cost_units_amount, reo_reg_cost_units_check, 
                                reo_reg_cost_square, reo_reg_cost_square_feet, reo_reg_cost_square_amount, reo_reg_cost_square_check, reo_reg_cost_amount, 
                                reo_reg_cost_standard, reo_renewal_cost_escalating, reo_renewal_cost_units, reo_renewal_cost_units_amount, reo_renewal_cost_units_check, 
                                reo_renewal_cost_square, reo_renewal_cost_square_feet, reo_renewal_cost_square_amount, reo_renewal_cost_square_check, reo_renewal_cost_amount, 
                                reo_escalating_renewal_cost_amount, reo_escalating_renewal_succeding, reo_first_escalating_cost_units_amount, reo_first_escalating_cost_units_check, 
                                reo_first_escalating_cost_square_feet, reo_first_escalating_cost_square_amount, reo_first_escalating_cost_square_check, reo_second_escalating_cost_units_amount, 
                                reo_second_escalating_cost_units_check, reo_second_escalating_cost_square_feet, reo_second_escalating_cost_square_amount, reo_second_escalating_cost_square_check, 
                                reo_third_escalating_cost_units_amount, reo_third_escalating_cost_units_check, reo_third_escalating_cost_square_feet, reo_third_escalating_cost_square_amount, 
                                reo_third_escalating_cost_square_check, reo_fourth_escalating_cost_units_amount, reo_fourth_escalating_cost_units_check, reo_fourth_escalating_cost_square_feet, 
                                reo_fourth_escalating_cost_square_amount, reo_fourth_escalating_cost_square_check, reo_fifth_escalating_cost_units_amount, reo_fifth_escalating_cost_units_check, 
                                reo_fifth_escalating_cost_square_feet, reo_fifth_escalating_cost_square_amount, reo_fifth_escalating_cost_square_check) 
                                values(" + dataStorageREO + ")";

                    query += @"insert into tbl_VPR_Cost_Commercial (commercial_reg_cost, commercial_reg_cost_units, commercial_reg_cost_units_amount, commercial_reg_cost_units_check, 
                                commercial_reg_cost_square, commercial_reg_cost_square_feet, commercial_reg_cost_square_amount, commercial_reg_cost_square_check, commercial_reg_cost_amount, 
                                commercial_reg_cost_standard, commercial_renewal_cost_escalating, commercial_renewal_cost_units, commercial_renewal_cost_units_amount, commercial_renewal_cost_units_check, 
                                commercial_renewal_cost_square, commercial_renewal_cost_square_feet, commercial_renewal_cost_square_amount, commercial_renewal_cost_square_check, commercial_renewal_cost_amount, 
                                commercial_escalating_renewal_cost_amount, commercial_escalating_renewal_succeding, commercial_first_escalating_cost_units_amount, commercial_first_escalating_cost_units_check, 
                                commercial_first_escalating_cost_square_feet, commercial_first_escalating_cost_square_amount, commercial_first_escalating_cost_square_check, commercial_second_escalating_cost_units_amount, 
                                commercial_second_escalating_cost_units_check, commercial_second_escalating_cost_square_feet, commercial_second_escalating_cost_square_amount, commercial_second_escalating_cost_square_check, 
                                commercial_third_escalating_cost_units_amount, commercial_third_escalating_cost_units_check, commercial_third_escalating_cost_square_feet, commercial_third_escalating_cost_square_amount, 
                                commercial_third_escalating_cost_square_check, commercial_fourth_escalating_cost_units_amount, commercial_fourth_escalating_cost_units_check, commercial_fourth_escalating_cost_square_feet, 
                                commercial_fourth_escalating_cost_square_amount, commercial_fourth_escalating_cost_square_check, commercial_fifth_escalating_cost_units_amount, commercial_fifth_escalating_cost_units_check, 
                                commercial_fifth_escalating_cost_square_feet, commercial_fifth_escalating_cost_square_amount, commercial_fifth_escalating_cost_square_check) 
                                values(" + dataStorageCommercial + ")";


                    query += @"insert into tbl_VPR_Cost_Vacant (vacant_reg_cost, vacant_reg_cost_units, vacant_reg_cost_units_amount, vacant_reg_cost_units_check, 
                                vacant_reg_cost_square, vacant_reg_cost_square_feet, vacant_reg_cost_square_amount, vacant_reg_cost_square_check, vacant_reg_cost_amount, 
                                vacant_reg_cost_standard, vacant_renewal_cost_escalating, vacant_renewal_cost_units, vacant_renewal_cost_units_amount, vacant_renewal_cost_units_check, 
                                vacant_renewal_cost_square, vacant_renewal_cost_square_feet, vacant_renewal_cost_square_amount, vacant_renewal_cost_square_check, vacant_renewal_cost_amount, 
                                vacant_escalating_renewal_cost_amount, vacant_escalating_renewal_succeding, vacant_first_escalating_cost_units_amount, vacant_first_escalating_cost_units_check, 
                                vacant_first_escalating_cost_square_feet, vacant_first_escalating_cost_square_amount, vacant_first_escalating_cost_square_check, vacant_second_escalating_cost_units_amount, 
                                vacant_second_escalating_cost_units_check, vacant_second_escalating_cost_square_feet, vacant_second_escalating_cost_square_amount, vacant_second_escalating_cost_square_check, 
                                vacant_third_escalating_cost_units_amount, vacant_third_escalating_cost_units_check, vacant_third_escalating_cost_square_feet, vacant_third_escalating_cost_square_amount, 
                                vacant_third_escalating_cost_square_check, vacant_fourth_escalating_cost_units_amount, vacant_fourth_escalating_cost_units_check, vacant_fourth_escalating_cost_square_feet, 
                                vacant_fourth_escalating_cost_square_amount, vacant_fourth_escalating_cost_square_check, vacant_fifth_escalating_cost_units_amount, vacant_fifth_escalating_cost_units_check, 
                                vacant_fifth_escalating_cost_square_feet, vacant_fifth_escalating_cost_square_amount, vacant_fifth_escalating_cost_square_check) 
                                values(" + dataStorageVacant + ")";

                    query += @"insert into tbl_VPR_Workable_Registration_Cost (state, city, pfc_id, reo_id, commercial_id, vacant_id, date_modified, tag) values
                                ('" + data[1].ToString() + "', '" + data[2].ToString() + "', (select max(pfc_id) from tbl_VPR_Cost_PFC), (select max(reo_id) from tbl_VPR_Cost_REO), (select max(commercial_id) from tbl_VPR_Cost_Commercial), (select max(vacant_id) from tbl_VPR_Cost_Vacant), GETDATE(), '')";


                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set costSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, costSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set costSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, costSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Cost_PFC set pfc_reg_cost = '" + data[3].ToString() + "', pfc_reg_cost_units = '" + data[4].ToString() + "', pfc_reg_cost_units_amount = '" + data[5].ToString()
                        + "', pfc_reg_cost_units_check = '" + data[6].ToString() + "', pfc_reg_cost_square = '" + data[7].ToString() + "', pfc_reg_cost_square_feet = '" + data[8].ToString() + "', pfc_reg_cost_square_amount = '" + data[9].ToString()
                        + "', pfc_reg_cost_square_check = '" + data[10].ToString() + "', pfc_reg_cost_amount = '" + data[11].ToString() + "', pfc_reg_cost_standard = '" + data[12].ToString() + "', pfc_renewal_cost_escalating = '" + data[13].ToString()
                        + "', pfc_renewal_cost_units = '" + data[14].ToString() + "', pfc_renewal_cost_units_amount = '" + data[15].ToString() + "', pfc_renewal_cost_units_check = '" + data[16].ToString() + "', pfc_renewal_cost_square = '" + data[17].ToString()
                        + "', pfc_renewal_cost_square_feet = '" + data[18].ToString() + "', pfc_renewal_cost_square_amount = '" + data[19].ToString() + "', pfc_renewal_cost_square_check = '" + data[20].ToString()
                        + "', pfc_renewal_cost_amount = '" + data[21].ToString() + "', pfc_escalating_renewal_cost_amount = '" + data[22].ToString() + "', pfc_escalating_renewal_succeding = '" + data[23].ToString()
                        + "', pfc_first_escalating_cost_units_amount = '" + data[24].ToString() + "', pfc_first_escalating_cost_units_check = '" + data[25].ToString() + "', pfc_first_escalating_cost_square_feet = '" + data[26].ToString()
                        + "', pfc_first_escalating_cost_square_amount = '" + data[27].ToString() + "', pfc_first_escalating_cost_square_check = '" + data[28].ToString() + "', pfc_second_escalating_cost_units_amount = '" + data[29].ToString()
                        + "', pfc_second_escalating_cost_units_check = '" + data[30].ToString() + "', pfc_second_escalating_cost_square_feet = '" + data[31].ToString() + "', pfc_second_escalating_cost_square_amount = '" + data[32].ToString()
                        + "', pfc_second_escalating_cost_square_check = '" + data[33].ToString() + "', pfc_third_escalating_cost_units_amount = '" + data[34].ToString() + "', pfc_third_escalating_cost_units_check = '" + data[35].ToString()
                        + "', pfc_third_escalating_cost_square_feet = '" + data[36].ToString() + "', pfc_third_escalating_cost_square_amount = '" + data[37].ToString() + "', pfc_third_escalating_cost_square_check = '" + data[38].ToString()
                        + "', pfc_fourth_escalating_cost_units_amount = '" + data[39].ToString() + "', pfc_fourth_escalating_cost_units_check = '" + data[40].ToString() + "', pfc_fourth_escalating_cost_square_feet = '" + data[41].ToString()
                        + "', pfc_fourth_escalating_cost_square_amount = '" + data[42].ToString() + "', pfc_fourth_escalating_cost_square_check = '" + data[43].ToString() + "', pfc_fifth_escalating_cost_units_amount = '" + data[44].ToString()
                        + "', pfc_fifth_escalating_cost_units_check = '" + data[45].ToString() + "', pfc_fifth_escalating_cost_square_feet = '" + data[46].ToString() + "', pfc_fifth_escalating_cost_square_amount = '" + data[47].ToString()
                        + "', pfc_fifth_escalating_cost_square_check = '" + data[48].ToString() + "' where pfc_id = (select pfc_id from tbl_VPR_Workable_Registration_Cost where id = '" + existingID + "')";

                    query += @"update tbl_VPR_Cost_REO set reo_reg_cost = '" + data[49].ToString() + "', reo_reg_cost_units = '" + data[50].ToString() + "', reo_reg_cost_units_amount = '" + data[51].ToString()
                        + "', reo_reg_cost_units_check = '" + data[52].ToString() + "', reo_reg_cost_square = '" + data[53].ToString() + "', reo_reg_cost_square_feet = '" + data[54].ToString() + "', reo_reg_cost_square_amount = '" + data[55].ToString()
                        + "', reo_reg_cost_square_check = '" + data[56].ToString() + "', reo_reg_cost_amount = '" + data[57].ToString() + "', reo_reg_cost_standard = '" + data[58].ToString() + "', reo_renewal_cost_escalating = '" + data[59].ToString()
                        + "', reo_renewal_cost_units = '" + data[60].ToString() + "', reo_renewal_cost_units_amount = '" + data[61].ToString() + "', reo_renewal_cost_units_check = '" + data[62].ToString()
                        + "', reo_renewal_cost_square = '" + data[63].ToString() + "', reo_renewal_cost_square_feet = '" + data[64].ToString() + "', reo_renewal_cost_square_amount = '" + data[65].ToString()
                        + "', reo_renewal_cost_square_check = '" + data[66].ToString() + "', reo_renewal_cost_amount = '" + data[67].ToString() + "', reo_escalating_renewal_cost_amount = '" + data[68].ToString()
                        + "', reo_escalating_renewal_succeding = '" + data[69].ToString() + "', reo_first_escalating_cost_units_amount = '" + data[70].ToString() + "', reo_first_escalating_cost_units_check = '" + data[71].ToString()
                        + "', reo_first_escalating_cost_square_feet = '" + data[72].ToString() + "', reo_first_escalating_cost_square_amount = '" + data[73].ToString() + "', reo_first_escalating_cost_square_check = '" + data[74].ToString()
                        + "', reo_second_escalating_cost_units_amount = '" + data[75].ToString() + "', reo_second_escalating_cost_units_check = '" + data[76].ToString() + "', reo_second_escalating_cost_square_feet = '" + data[77].ToString()
                        + "', reo_second_escalating_cost_square_amount = '" + data[78].ToString() + "', reo_second_escalating_cost_square_check = '" + data[79].ToString() + "', reo_third_escalating_cost_units_amount = '" + data[80].ToString()
                        + "', reo_third_escalating_cost_units_check = '" + data[81].ToString() + "', reo_third_escalating_cost_square_feet = '" + data[82].ToString() + "', reo_third_escalating_cost_square_amount = '" + data[83].ToString()
                        + "', reo_third_escalating_cost_square_check = '" + data[84].ToString() + "', reo_fourth_escalating_cost_units_amount = '" + data[85].ToString() + "', reo_fourth_escalating_cost_units_check = '" + data[86].ToString()
                        + "', reo_fourth_escalating_cost_square_feet = '" + data[87].ToString() + "', reo_fourth_escalating_cost_square_amount = '" + data[88].ToString() + "', reo_fourth_escalating_cost_square_check = '" + data[89].ToString()
                        + "', reo_fifth_escalating_cost_units_amount = '" + data[90].ToString() + "', reo_fifth_escalating_cost_units_check = '" + data[91].ToString() + "', reo_fifth_escalating_cost_square_feet = '" + data[92].ToString()
                        + "', reo_fifth_escalating_cost_square_amount = '" + data[93].ToString() + "', reo_fifth_escalating_cost_square_check = '" + data[94].ToString() + "' where reo_id = (select reo_id from tbl_VPR_Workable_Registration_Cost where id = '" + existingID + "') ";

                    query += @"update tbl_VPR_Cost_Commercial set commercial_reg_cost = '" + data[95].ToString() + "', commercial_reg_cost_units = '" + data[96].ToString() + "', commercial_reg_cost_units_amount = '" + data[97].ToString()
                        + "', commercial_reg_cost_units_check = '" + data[98].ToString() + "', commercial_reg_cost_square = '" + data[99].ToString() + "', commercial_reg_cost_square_feet = '" + data[100].ToString()
                        + "', commercial_reg_cost_square_amount = '" + data[101].ToString() + "', commercial_reg_cost_square_check = '" + data[102].ToString() + "', commercial_reg_cost_amount = '" + data[103].ToString() + "', commercial_reg_cost_standard = '" + data[104].ToString()
                        + "', commercial_renewal_cost_escalating = '" + data[105].ToString() + "', commercial_renewal_cost_units = '" + data[106].ToString() + "', commercial_renewal_cost_units_amount = '" + data[107].ToString()
                        + "', commercial_renewal_cost_units_check = '" + data[108].ToString() + "', commercial_renewal_cost_square = '" + data[109].ToString() + "', commercial_renewal_cost_square_feet = '" + data[110].ToString()
                        + "', commercial_renewal_cost_square_amount = '" + data[111].ToString() + "', commercial_renewal_cost_square_check = '" + data[112].ToString() + "', commercial_renewal_cost_amount = '" + data[113].ToString() + "', commercial_escalating_renewal_cost_amount = '" + data[114].ToString()
                        + "', commercial_escalating_renewal_succeding = '" + data[115].ToString() + "', commercial_first_escalating_cost_units_amount = '" + data[116].ToString() + "', commercial_first_escalating_cost_units_check = '" + data[117].ToString() + "', commercial_first_escalating_cost_square_feet = '" + data[118].ToString()
                        + "', commercial_first_escalating_cost_square_amount = '" + data[119].ToString() + "', commercial_first_escalating_cost_square_check = '" + data[120].ToString() + "', commercial_second_escalating_cost_units_amount = '" + data[121].ToString()
                        + "', commercial_second_escalating_cost_units_check = '" + data[122].ToString() + "', commercial_second_escalating_cost_square_feet = '" + data[123].ToString() + "', commercial_second_escalating_cost_square_amount = '" + data[124].ToString()
                        + "', commercial_second_escalating_cost_square_check = '" + data[125].ToString() + "', commercial_third_escalating_cost_units_amount = '" + data[126].ToString() + "', commercial_third_escalating_cost_units_check = '" + data[127].ToString()
                        + "', commercial_third_escalating_cost_square_feet = '" + data[128].ToString() + "', commercial_third_escalating_cost_square_amount = '" + data[129].ToString() + "', commercial_third_escalating_cost_square_check = '" + data[130].ToString()
                        + "', commercial_fourth_escalating_cost_units_amount = '" + data[131].ToString() + "', commercial_fourth_escalating_cost_units_check = '" + data[132].ToString() + "', commercial_fourth_escalating_cost_square_feet = '" + data[133].ToString()
                        + "', commercial_fourth_escalating_cost_square_amount = '" + data[134].ToString() + "', commercial_fourth_escalating_cost_square_check = '" + data[135].ToString() + "', commercial_fifth_escalating_cost_units_amount = '" + data[136].ToString()
                        + "', commercial_fifth_escalating_cost_units_check = '" + data[137].ToString() + "', commercial_fifth_escalating_cost_square_feet = '" + data[138].ToString() + "', commercial_fifth_escalating_cost_square_amount = '" + data[139].ToString()
                        + "', commercial_fifth_escalating_cost_square_check = '" + data[140].ToString() + "' where commercial_id = (select commercial_id from tbl_VPR_Workable_Registration_Cost where id = '" + existingID + "')";

                    query += @"update tbl_VPR_Cost_Vacant set vacant_reg_cost = '" + data[141].ToString() + "', vacant_reg_cost_units = '" + data[142].ToString() + "', vacant_reg_cost_units_amount = '" + data[143].ToString()
                        + "', vacant_reg_cost_units_check = '" + data[144].ToString() + "', vacant_reg_cost_square = '" + data[145].ToString() + "', vacant_reg_cost_square_feet = '" + data[146].ToString() + "', vacant_reg_cost_square_amount = '" + data[147].ToString()
                        + "', vacant_reg_cost_square_check = '" + data[148].ToString() + "', vacant_reg_cost_amount = '" + data[149].ToString() + "', vacant_reg_cost_standard = '" + data[150].ToString() + "', vacant_renewal_cost_escalating = '" + data[151].ToString()
                        + "', vacant_renewal_cost_units = '" + data[152].ToString() + "', vacant_renewal_cost_units_amount = '" + data[153].ToString() + "', vacant_renewal_cost_units_check = '" + data[154].ToString() + "', vacant_renewal_cost_square = '" + data[155].ToString()
                        + "', vacant_renewal_cost_square_feet = '" + data[156].ToString() + "', vacant_renewal_cost_square_amount = '" + data[157].ToString() + "', vacant_renewal_cost_square_check = '" + data[158].ToString() + "', vacant_renewal_cost_amount = '" + data[159].ToString()
                        + "', vacant_escalating_renewal_cost_amount = '" + data[160].ToString() + "', vacant_escalating_renewal_succeding = '" + data[161].ToString() + "', vacant_first_escalating_cost_units_amount = '" + data[162].ToString() + "', vacant_first_escalating_cost_units_check = '" + data[163].ToString()
                        + "', vacant_first_escalating_cost_square_feet = '" + data[164].ToString() + "', vacant_first_escalating_cost_square_amount = '" + data[165].ToString() + "', vacant_first_escalating_cost_square_check = '" + data[166].ToString()
                        + "', vacant_second_escalating_cost_units_amount = '" + data[167].ToString() + "', vacant_second_escalating_cost_units_check = '" + data[168].ToString() + "', vacant_second_escalating_cost_square_feet = '" + data[169].ToString() + "', vacant_second_escalating_cost_square_amount = '" + data[170].ToString()
                        + "', vacant_second_escalating_cost_square_check = '" + data[171].ToString() + "', vacant_third_escalating_cost_units_amount = '" + data[172].ToString() + "', vacant_third_escalating_cost_units_check = '" + data[173].ToString() + "', vacant_third_escalating_cost_square_feet = '" + data[174].ToString()
                        + "', vacant_third_escalating_cost_square_amount = '" + data[175].ToString() + "', vacant_third_escalating_cost_square_check = '" + data[176].ToString() + "', vacant_fourth_escalating_cost_units_amount = '" + data[177].ToString() + "', vacant_fourth_escalating_cost_units_check = '" + data[178].ToString()
                        + "', vacant_fourth_escalating_cost_square_feet = '" + data[179].ToString() + "', vacant_fourth_escalating_cost_square_amount = '" + data[180].ToString() + "', vacant_fourth_escalating_cost_square_check = '" + data[181].ToString() + "', vacant_fifth_escalating_cost_units_amount = '" + data[182].ToString()
                        + "', vacant_fifth_escalating_cost_units_check = '" + data[183].ToString() + "', vacant_fifth_escalating_cost_square_feet = '" + data[184].ToString() + "', vacant_fifth_escalating_cost_square_amount = '" + data[185].ToString() + "', vacant_fifth_escalating_cost_square_check = '" + data[186].ToString()
                        + "' where vacant_id = (select vacant_id from tbl_VPR_Workable_Registration_Cost where id = '" + existingID + "')";

                    query += @"update tbl_VPR_Workable_Registration_Cost set date_modified = GETDATE(), tag = '' where id = '" + existingID + "'";


                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set costSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, costSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set costSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, costSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCont")
            {
                if (existingID == "")
                {
                    query = "insert into dbo.tbl_VPR_Workable ([state], city, cont_reg, update_reg, tag, modified_by, date_modified) values " +
                            "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "','" + data[4].ToString() + "', '', '" + user.ToString() + "', GETDATE());";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set contRegSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, contRegSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set contRegSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, contRegSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable set
                                    [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', cont_reg = '" + data[3].ToString() + "', update_reg = '" + data[4].ToString() +
                                "', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set contRegSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, contRegSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set contRegSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, contRegSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "inspect")
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i] == null)
                    {
                        data[i] = "";
                    }
                }
                if (existingID == "")
                {
                    query = @"insert into tbl_VPR_Workable_Inspection (state, city, municipal_inspection_fee_required, onetime_only_upon_reg, fee_payment_frequency,
                                payon1, payon2, payon3, payon4, free_escalating, municipal_inspection_fee, escalating_amount, escalating_succeeding, inspection_report_required,
                                inspection_frequency_occupied, inspection_frequency_vacant, how_to_send_inspection_report, tag, modified_by, date_modified) values " +
                            "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                            "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                            "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                            "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                            "'" + data[17].ToString() + "', '', '" + user.ToString() + "', GETDATE());";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set inspectionSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, inspectionSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set inspectionSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, inspectionSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Inspection set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', municipal_inspection_fee_required = '" + data[3].ToString()
                        + "', onetime_only_upon_reg = '" + data[4].ToString() + "', fee_payment_frequency = '" + data[5].ToString() + "', payon1 = '" + data[6].ToString() + "', payon2 = '" + data[7].ToString()
                        + "', payon3 = '" + data[8].ToString() + "', payon4 = '" + data[9].ToString() + "', free_escalating = '" + data[10].ToString() + "', municipal_inspection_fee = '" + data[11].ToString()
                        + "', escalating_amount = '" + data[12].ToString() + "', escalating_succeeding = '" + data[13].ToString() + "', inspection_report_required = '" + data[14].ToString()
                        + "', inspection_frequency_occupied = '" + data[15].ToString() + "', inspection_frequency_vacant = '" + data[16].ToString() + "', how_to_send_inspection_report = '" + data[17].ToString()
                        + "', 	tag = '', modified_by = '" + user.ToString() + "', date_modified = GetDate() where id = '" + existingID + "';";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set inspectionSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, inspectionSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set inspectionSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, inspectionSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }

                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "municipalityInfo")
            {
                if (existingID == "")
                {
                    query = "insert into tbl_VPR_Workable_Municipality ([state], city, municipality_department, municipality_phone_num, " +
                            "municipality_email, municipality_st, municipality_website, municipality_city, municipality_state, municipality_zip, contact_person, title, " +
                            "department, phone_num, email, address, ops_hrs_from, ops_hrs_to, tag, modified_by, date_modified) values " +
                            "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                            "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                            "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                            "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                            "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '', '" + user.ToString() + "', GETDATE())";


                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set municipalitySetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, municipalitySetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                         if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set municipalitySetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, municipalitySetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Municipality set
                                   [state]  = '" + data[1].ToString() + "', city  = '" + data[2].ToString() + "', municipality_department  = '" + data[3].ToString() +
                                "', municipality_phone_num = '" + data[4].ToString() + "', municipality_email  = '" + data[5].ToString() + "', municipality_st  = '" + data[6].ToString() +
                                "', municipality_website = '" + data[7].ToString() + "', municipality_city  = '" + data[8].ToString() + "', municipality_state  = '" + data[9].ToString() + "', municipality_zip  = '" + data[10].ToString() +
                                "', contact_person  = '" + data[11].ToString() + "', title = '" + data[12].ToString() + "', department  = '" + data[13].ToString() + "', phone_num  = '" + data[14].ToString() +
                                "', email  = '" + data[15].ToString() + "', address  = '" + data[16].ToString() + "', ops_hrs_from  = '" + data[17].ToString() + "', ops_hrs_to  = '" + data[18].ToString() +
                                "', tag  = '', modified_by  = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";
                    if (isNull != "1")
                    {
                       if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set municipalitySetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, municipalitySetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set municipalitySetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, municipalitySetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "dereg")
            {
                if (existingID == "")
                {
                    query = "insert into tbl_VPR_Workable_Deregistration ([state], city, dereg_req, conveyed, occupied, how_to_dereg, upload_file, " +
                        "new_owner_info_req, proof_of_conveyance_req, date_of_sale_req, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '', '" + user.ToString() + "', GETDATE())";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set deregistrationSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, deregistrationSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                          if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set deregistrationSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, deregistrationSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                }
                else
                {
                    query = @"update tbl_VPR_Workable_Deregistration set [state]  = '" + data[1].ToString() + "', city  = '" + data[2].ToString() + "', dereg_req  = '" + data[3].ToString() +
                                    "', conveyed = '" + data[4].ToString() + "', occupied  = '" + data[5].ToString() + "', how_to_dereg  = '" + data[6].ToString() +
                                    "', upload_file  = '" + data[7].ToString() + "', new_owner_info_req  = '" + data[8].ToString() + "', proof_of_conveyance_req  = '" + data[9].ToString() +
                                    "', date_of_sale_req  = '" + data[10].ToString() +
                                    "', tag  = '', modified_by  = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + existingID + "'";

                    if (isNull != "1")
                    {
                        if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set deregistrationSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, deregistrationSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '1', GETDATE(), '" + user.ToString() + "')";
                        }
                    }
                    else
                    {
                         if (settingID != "")
                        {
                            query += @"update tbl_VPR_MainSettings set deregistrationSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                        }
                        else
                        {
                            query += @"insert into tbl_VPR_MainSettings ([state], city, deregistrationSetting, dateModified, modified_by) values ('" + data[1].ToString() +
                                "', '" + data[2].ToString() + "', '0', GETDATE(), '" + user.ToString() + "')";
                        }
                    }

                }

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "zip")
            {
                query = "select LEFT(municipality_name, 2) [stateabbr], LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [muniname] ";
                query += "from tbl_VPR_Municipality where municipality_code = '" + data[2].ToString() + "';";

                DataTable dtMuni = cls.GetData(query);

                if (dtMuni.Rows.Count > 0)
                {
                    string stateAbbr = dtMuni.Rows[0][0].ToString();
                    string muniName = dtMuni.Rows[0][1].ToString();

                    query = "update tbl_DB_US_States set " +
                                "isZipActive = " + data[4].ToString() + " " +
                                "where State_Abbr = '" + stateAbbr + "' and " +
                                "(Municipality = '" + muniName + "' OR Municipality = REPLACE(LTRIM(RTRIM('" + muniName + "')),'(2)','')) and " +
                                "ZipCode = '" + data[3].ToString() + "';";
                    try
                    {
                        int exec = cls.ExecuteQuery(query);

                        if (exec > 0)
                        {
                            ArrayList arrPages = new ArrayList();
                            arrPages.Add("Zip Codes");
                            arrPages.Add("");

                            if (data[4].ToString() == "1")
                            {
                                cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
                            }
                            else
                            {
                                cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Inactive", arrPages);
                            }

                            tag = "1";
                        }
                        else
                        {
                            tag = "0";
                        }
                    }
                    catch (Exception)
                    {
                        tag = "0";
                    }
                }

            }

            return tag;
        }

        [WebMethod]
        public static string GetDataREO(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegREO = "", qrySetting = "";

            DataTable dtRegREO = new DataTable();
            DataTable dtSettings = new DataTable();

            qryRegREO = "select top 1 * from tbl_VPR_Workable_Registration_REO (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegREO = cls.GetData(qryRegREO);

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regREO = dtRegREO,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataPropType(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPropType = "", qrySetting = "";

            DataTable dtRegPropType = new DataTable();
            DataTable dtSettings = new DataTable();

            qryRegPropType = "select top 1 * from tbl_VPR_Workable_Registration_Property (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPropType = cls.GetData(qryRegPropType);

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regPropType = dtRegPropType,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataReq(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPropReq, qrySetting = "";

            DataTable dtRegPropReq = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            qryRegPropReq = @"select top 1 a.*, b.*, c.* from tbl_VPR_Workable_Registration_PropReq a (nolock) full join
                                tbl_VPR_Workable_Registration_PropReq_Contact b (nolock) on
                                a.propReqID = b.propReqID 
                                full join  tbl_VPR_Workable_Registration_PropReq_Bond as c  on
                                a.bondID = c.bondID where a.state = '" + state + "' and a.city = '" + city + "' order by a.id desc, date_modified desc";
            dtRegPropReq = cls.GetData(qryRegPropReq);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regPropReq = dtRegPropReq,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataCost(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryCost = "", qrySetting = "";

            DataTable dtCost = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = @"select top 1 * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city
                             + "' order by settingsID desc, dateModified desc";
            dtSettings = cls.GetData(qrySetting);

            qryCost = @"select top 1 * from tbl_VPR_Workable_Registration_Cost cost
                        inner join tbl_VPR_Cost_PFC pfc 
                        on cost.pfc_id = pfc.pfc_id
                        inner join tbl_VPR_Cost_REO reo
                        on cost.reo_id = reo.reo_id
                        inner join tbl_VPR_Cost_Commercial commercial
                        on cost.commercial_id = commercial.commercial_id
                        inner join tbl_VPR_Cost_Vacant vacant
                        on cost.vacant_id = vacant.vacant_id
                        where cost.state = '" + state + "' and cost.city = '" + city + "' order by cost.id desc, cost.date_modified desc";
            dtCost = cls.GetData(qryCost);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regCost = dtCost,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataConReg(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryCont = "", qrySetting = "";

            DataTable dtCont = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            qryCont = "select top 1 * from tbl_VPR_Workable (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtCont = cls.GetData(qryCont);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regCont = dtCont,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataInspection(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryInspect = "", qrySetting = "";

            DataTable dtInspect = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            qryInspect = "select top 1 * from tbl_VPR_Workable_Inspection (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtInspect = cls.GetData(qryInspect);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    inspect = dtInspect,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataMunicipality(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryMunicipal = "", qrySetting = "";

            DataTable dtMunicipal = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            qryMunicipal = "select top 1 * from tbl_VPR_Workable_Municipality (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtMunicipal = cls.GetData(qryMunicipal);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    municipal = dtMunicipal,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetDataDeregistration(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryDereg = "", qrySetting = "";

            DataTable dtDereg = new DataTable();
            DataTable dtSettings = new DataTable();

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            qryDereg = "select top 1 * from tbl_VPR_Workable_Deregistration (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtDereg = cls.GetData(qryDereg);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    dereg = dtDereg,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetOrdinanceSettings(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryOrdinance = "", qrySetting = "";

            DataTable dtOrdinance, dtSettings = new DataTable();

            qryOrdinance = "select top 1 * from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtOrdinance = cls.GetData(qryOrdinance);

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    ordinance = dtOrdinance,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string GetData(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPFC = "", qrySetting = "";

            DataTable dtRegPFC = new DataTable();
            DataTable dtSettings = new DataTable();

            qryRegPFC = "select top 1 * from tbl_VPR_Workable_Registration_PFC (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPFC = cls.GetData(qryRegPFC);

            qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            dtSettings = cls.GetData(qrySetting);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regPFC = dtRegPFC,
                    forSetting = dtSettings
                }
            });
        }

        [WebMethod]
        public static string checkApply(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string query = "";

            DataTable dtCheckApply = new DataTable();

            query = @"select * from tbl_VPR_MainSettings where pfcSetting = '1' and reoSetting = '1' and propTypeSetting = '1' and
                        requirementSetting = '1' and costSetting = '1' and contRegSetting = '1' and inspectionSetting = '1' and
                        municipalitySetting = '1' and deregistrationSetting = '1' and ordinanceSetting = '1'   and  additionalSetting = '1'
                        and state = '" + state + "' and city = '" + city + "'";
            dtCheckApply = cls.GetData(query);

            if (dtCheckApply.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        forApply = dtCheckApply
                    }
                });
            }
            else
            {
                //                query = @"select * from tbl_VPR_MainSettings where pfcSetting = '1' and reoSetting = '1' and propTypeSetting = '1' and
                //                            requirementSetting = '1' and costSetting = '1' and contRegSetting = '1' and inspectionSetting = '1' and
                //                            municipalitySetting = '1' and deregistrationSetting = '1' and ordinanceSetting = 1 and notificationSetting = '1'
                //                            and (state = '" + state + "'  and city = '" + city + "')";

                query = @"select * from tbl_VPR_MainSettings where  (state = '" + state + "'  and city = '" + city + "') ";
                dtCheckApply = cls.GetData(query);

                return JsonConvert.SerializeObject(new
                {
                    Success = false,
                    Message = "Error",
                    data = new
                    {
                        forApply = dtCheckApply
                    }
                });
            }
        }

        [WebMethod]
        public static string updateApply(string state, string city, string settingID, string user, string isError)
        {
            clsConnection cls = new clsConnection();
            string strQuery = "", tag = "";

            if (isError != "1" && isError != "undefined")
            {
                strQuery = "update tbl_VPR_MainSettings set isApply = '1', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                strQuery += "update tbl_VPR_Workable_Registration_PFC set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_REO set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_Property set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_PropReq set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_Cost set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Inspection set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Municipality set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Deregistration set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Ordinance set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Additional_Forms set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";
                //strQuery += "update tbl_VPR_Workable_Notification set tag = 'PENDING FOR REVIEW' where state = '" + state + "' and city = '" + city + "'";

                int exec = cls.ExecuteQuery(strQuery);

                if (exec > 0)
                {
                    tag = "1";
                }
                else
                {
                    tag = "0";
                }
            }
            else
            {

                strQuery = "update tbl_VPR_MainSettings set isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                strQuery += "update tbl_VPR_Workable_Registration_PFC set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_REO set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_Property set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_PropReq set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Registration_Cost set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Inspection set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Municipality set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Deregistration set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Ordinance set tag = '' where state = '" + state + "' and city = '" + city + "'";
                strQuery += "update tbl_VPR_Workable_Additional_Forms set tag = '' where state = '" + state + "' and city = '" + city + "'";
                //strQuery += "update tbl_VPR_Workable_Notification set tag = '' where state = '" + state + "' and city = '" + city + "'";

                int exec = cls.ExecuteQuery(strQuery);

                if (exec > 0)
                {
                    tag = "1";
                }
                else
                {
                    tag = "0";
                }
            }





            return tag;
        }

        public static void ForAuditTrail(DataTable past, DataTable present, ArrayList arrPage, string state, string city)
        {

            clsConnection cls = new clsConnection();

            for (int x = 0; x < present.Columns.Count; x++)
            {
                if (present.Rows[0][x].ToString() != past.Rows[0][x].ToString())
                {
                    cls.FUNC.Audit("Updated " + present.Columns[x].ToString() + " from [" + present.Rows[0][x].ToString() + "] to [" +
                        past.Rows[0][x].ToString() + "] of " + city + ", " + state, arrPage);
                }
            }
        }

        [WebMethod]
        public static string SaveAdditional(List<string> data, string state, string city, string user, string isNull, string settingID)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string tag = "";
            string dataInsert = "";
            dataInsert += "'" + state + "','" + city + "',";
            for (int i = 0; i < data.Count(); i++)
            {
                dataInsert += "'" + data[i].ToString() + "',";
            }

            dataInsert = dataInsert.Remove(dataInsert.Length - 1, 1);

            string qrySelect = "Select * From tbl_VPR_Workable_Additional_Forms where state = '" + state + "' and city = '" + city + "'";
            dt = cls.GetData(qrySelect);
            if (dt.Rows.Count == 0)
            {
                string insertData = @"Insert Into tbl_VPR_Workable_Additional_Forms (state,city,BondForm, BondForm_Check1, BondForm_File,InsuranceCer, InsuranceCer_Check1,InsuranceCer_File,MaintenancePlan, MaintenancePlan_Check1 ,MaintenancePlan_File,
                                    NonOwner, NonOwner_Check1, NonOwner_File,OutOfCountry, OutOfCountry_Check1 ,OutOfCountry_File,ProofOfUtilities, ProofOfUtilities_Check1,ProofOfUtilities_File,SignageForm, SignageForm_Check1 ,SignageForm_File,
                                    Statement, Statement_Check1 ,Statement_File,Structure, Structure_Check1 ,Structure_File,TrespassAffidavit, TrespassAffidavit_Check1 ,TrespassAffidavit_File, tag, date_modified, modified_by) values (" + dataInsert + ", '', GETDATE(), '" + user.ToString() + "')";


                if (isNull != "1")
                {
                    if (settingID != "")
                    {
                        insertData += @"update tbl_VPR_MainSettings set additionalSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        insertData += @"insert into tbl_VPR_MainSettings ([state], city, additionalSetting, dateModified, modified_by) values ('" + state +
                            "', '" + city + "', '1', GETDATE(), '" + user.ToString() + "')";
                    }
                }
                else
                {
                    if (settingID != "")
                    {
                        insertData += @"update tbl_VPR_MainSettings set additionalSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        insertData += @"insert into tbl_VPR_MainSettings ([state], city, additionalSetting, dateModified, modified_by) values ('" + state +
                            "', '" + city + "', '0', GETDATE(), '" + user.ToString() + "')";
                    }
                }

                cls.ExecuteQuery(insertData);
            }
            else
            {

                string qryUpdate = @"UPdate tbl_VPR_Workable_Additional_Forms Set BondForm = '" +  data[0].ToString() + "', BondForm_Check1 = '" +  data[1].ToString() + "', BondForm_File = '" +  data[2].ToString() 
                    + "', InsuranceCer = '" +  data[3].ToString() + "', InsuranceCer_Check1 = '" +  data[4].ToString() + "', InsuranceCer_File = '" +  data[5].ToString() + "', MaintenancePlan = '" +  data[6].ToString() 
                    + "', MaintenancePlan_Check1 = '" +  data[7].ToString() + "', MaintenancePlan_File = '" +  data[8].ToString() + "', NonOwner = '" +  data[9].ToString() + "', NonOwner_Check1 = '" +  data[10].ToString() 
                    + "', NonOwner_File = '" +  data[11].ToString() + "', OutOfCountry = '" +  data[12].ToString() + "', OutOfCountry_Check1 = '" +  data[13].ToString() + "', OutOfCountry_File = '" +  data[14].ToString() 
                    + "', ProofOfUtilities = '" +  data[15].ToString() + "', ProofOfUtilities_Check1 = '" +  data[16].ToString() + "', ProofOfUtilities_File = '" +  data[17].ToString() + "', SignageForm = '" +  data[18].ToString() 
                    + "',  SignageForm_Check1 = '" +  data[19].ToString() + "', SignageForm_File = '" +  data[20].ToString() + "', Statement = '" +  data[21].ToString() + "', Statement_Check1 = '" +  data[22].ToString() 
                    + "', Statement_File = '" +  data[23].ToString() + "', Structure = '" +  data[24].ToString() + "', Structure_Check1 = '" +  data[25].ToString() + "', Structure_File = '" +  data[26].ToString() 
                    + "', TrespassAffidavit = '" +  data[27].ToString() + "', TrespassAffidavit_Check1 = '" +  data[28].ToString() + "', TrespassAffidavit_File = '" +  data[29].ToString() + "',  tag = '', date_modified = GETDATE(), modified_by = '" + user.ToString() + "' where state = '" + state + "' and city = '" + city 
                    + "'";

                if (isNull != "1")
                {
                    if (settingID != "")
                    {
                        qryUpdate += @"update tbl_VPR_MainSettings set additionalSetting = '1', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        qryUpdate += @"insert into tbl_VPR_MainSettings ([state], city, additionalSetting, dateModified, modified_by) values ('" + state +
                            "', '" + city + "', '1', GETDATE(), '" + user.ToString() + "')";
                    }
                }
                else
                {
                    if (settingID != "")
                    {
                        qryUpdate += @"update tbl_VPR_MainSettings set additionalSetting = '0', isApply = '0', dateModified = GETDATE(), modified_by = '" + user.ToString() + "' where settingsID = '" + settingID + "'";
                    }
                    else
                    {
                        qryUpdate += @"insert into tbl_VPR_MainSettings ([state], city, additionalSetting, dateModified, modified_by) values ('" + state +
                            "', '" + city + "', '0', GETDATE(), '" + user.ToString() + "')";
                    }
                }

                cls.ExecuteQuery(qryUpdate);

            }

            return tag;
        }

        [WebMethod]
        public static string GetAdditionalForms(string state, string city)
        {
            clsConnection cls = new clsConnection();



            string qry = @"Select * From tbl_VPR_Workable_Additional_Forms where state = '" + state + "' and city = '" + city + "'";

            string qrySetting = "select distinct * from tbl_VPR_MainSettings where  state = '" + state + "' and city = '" + city + "'";
            DataTable dtSettings = new DataTable();
            dtSettings = cls.GetData(qrySetting);

            DataTable dt = new DataTable();
            dt = cls.GetData(qry);




            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, forSetting = dtSettings } });
        }                                                                                                                           

    }
}