﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class AuditClass
    {
        public string id { get; set; }
        public string CompleteAction { get; set; }
        public string Municipality { get; set; }
        public string Page { get; set; }
        public string MainTab { get; set; } 
        public string SubTab { get; set; }
        public string Field { get; set; }
        public string Action { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string ActionBy { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }

    public class AuditList
    {
        public List<AuditClass> tblAuditList { get; set; }
    }

    public class AuditParam
    {
        public string timeFrame { get; set; }
        public string by { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}