﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class UserSettings
    {
    }
    public class AccessRole
    {
        public string id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
    }
    public class AccessFunc
    {
        public string id { get; set; }
        public string name { get; set; }
        public string isOn { get; set; }
    }

    public class UserDetails
    {
        public string id { get; set; }
        public string empname { get; set; }
        public string emplevel { get; set; }
        public string immsupp { get; set; }
        public string role_id { get; set; }
        public string skill_id { get; set; }
    }

    public class UserList
    {
        public List<UserDetails> tblUserList { get; set; }
    }

    public class RoleSkill
    {
        public string id { get; set; }
        public string rsname { get; set; }
        public string rstype { get; set; }
    }
}

