﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class BasicFunction
    {
        private static String sCryptoKey = "Alt1S0urc3HRMSProject";
        public DateTime getenddate(string myenddate)
        {
            DateTime Endate = new DateTime();
            Endate = Convert.ToDateTime(myenddate).AddDays(1).AddSeconds(-1);
            return Endate;
        }
        public string NullDateTime(string myenddate)
        {
            if (myenddate != "")
            {
                return Convert.ToDateTime(myenddate).ToString("hh:mm tt");
            }
            else
            {
                return "Pending";
            }

        }
        public string Decimal102(string myvalue)
        {
            if (myvalue != "")
            {
                return Convert.ToDecimal(myvalue).ToString("#.##");
            }
            else
            {
                return "";
            }

        }
        public string Encrypt(string sInput)
        {
            string strDest = "";
            int intLenSrce;
            int intPadCnt;
            if (sInput.Length < 1)
            {
                return strDest;
            }
            else
            {
                intLenSrce = sInput.Length;
                intPadCnt = 15 - (intLenSrce % 16);
                strDest = stringReverse(sInput);


                if (intPadCnt > 0)
                {
                    strDest = strDest + sCryptoKey.Substring(intLenSrce, intPadCnt);
                    strDest = ConvStrBStr(strDest, intLenSrce);
                    if (intLenSrce < 16)
                    {
                        strDest = ConvBStrHex(strDest);
                        strDest = strDest + "0" + intLenSrce.ToString("X");
                    }
                    else
                    {
                        strDest = ConvBStrHex(strDest);
                        strDest = strDest + "" + intLenSrce.ToString("X");
                    }



                }
            }
            return strDest;
        }
        public string Decrypt(string sInput)
        {
            string strDest = "";
            int intLenSrce;
            string Decrypt = "";
            if (sInput.Length < 1)
            {
                return strDest;
            }
            else
            {
                var mti = sInput.Substring(sInput.Length - 2);
                intLenSrce = Convert.ToChar(Convert.ToByte(mti, 16));
                strDest = ConvHexBStr(sInput.Substring(0, sInput.Length - 2));
                strDest = ConvBStrStr(strDest, intLenSrce);
                strDest = stringReverse(strDest).Substring(0, intLenSrce);
            }
            Decrypt = strDest;
            return Decrypt;
        }
        public string stringReverse(string str)
        {
            char[] inputarray = str.ToCharArray();
            Array.Reverse(inputarray);
            string output = new string(inputarray);
            return output;
        }
        public string ConvStrBStr(string strInput, int nShftCnt)
        {
            int intCtr;
            string strDest = "", ConvStrBStr;
            intCtr = 1;
            for (intCtr = 0; intCtr < strInput.Length; intCtr++)
            {
                var sample = Convert.ToInt32(Convert.ToChar(strInput.Substring(intCtr, 1)));
                var left = sample.ToString("X").Substring(0, 1);
                var right = sample.ToString("X").Substring(sample.ToString("X").Length - 1);
                strDest = strDest + ConvHexBin(left) + ConvHexBin(right);
                //strDest = strDest + ConvHexBin(Convert.ToInt32(Convert.ToChar(strInput.Substring(intCtr, 1))).ToString("X").Substring(0, 1)) + ConvHexBin(Convert.ToInt32(Convert.ToChar(strInput.Substring(intCtr, 1))).ToString("X").Substring(Convert.ToInt32(strInput.Substring(intCtr, 1)).ToString("X").Length - 1)); ;

            }
            ConvStrBStr = strDest.Substring(nShftCnt) + strDest.Substring(0, nShftCnt);
            return ConvStrBStr;
        }
        public string ConvBStrHex(string strInput)
        {
            int intCtr;
            string strDest = "", ConvBStrHex;
            intCtr = 1;
            for (intCtr = 0; intCtr < strInput.Length; intCtr += 4)
            {
                strDest = strDest + ConvBinHex(strInput.Substring(intCtr, 4));

            }
            ConvBStrHex = strDest;
            return ConvBStrHex;

        }
        public string ConvBStrStr(string strInput, int nShftCnt)
        {
            int intCtr;
            string strDest = "", ConvBStrStr, strLByte = "", strRByte = "";
            intCtr = 1;
            strInput = strInput.Substring(strInput.Length - nShftCnt) + strInput.Substring(0, (strInput.Length - nShftCnt));
            for (intCtr = 0; intCtr < strInput.Length; intCtr += 8)
            {
                strLByte = strInput.Substring(intCtr, 4);
                strRByte = strInput.Substring(intCtr + 4, 4);
                Char mychar = Convert.ToChar(Convert.ToByte(ConvBinHex(strLByte) + ConvBinHex(strRByte), 16));
                strDest = strDest + mychar;

            }
            ConvBStrStr = strDest.Substring(nShftCnt + 1) + strDest.Substring(0, nShftCnt);
            return ConvBStrStr;

        }
        public string ConvHexBStr(string strInput)
        {
            int intCtr;
            string strDest = "", ConvHexBStr;
            for (intCtr = 0; intCtr < strInput.Length; intCtr++)
            {
                strDest = strDest + ConvHexBin(strInput.Substring(intCtr, 1));
            }
            ConvHexBStr = strDest;
            return strDest;

        }
        public string ConvHexBin(string hInput)
        {
            string ConvHexBin;
            switch (hInput)
            {
                case "0":
                    ConvHexBin = "0000";
                    break;
                case "1":
                    ConvHexBin = "0001";
                    break;
                case "2":
                    ConvHexBin = "0010";
                    break;
                case "3":
                    ConvHexBin = "0011";
                    break;
                case "4":
                    ConvHexBin = "0100";
                    break;
                case "5":
                    ConvHexBin = "0101";
                    break;
                case "6":
                    ConvHexBin = "0110";
                    break;
                case "7":
                    ConvHexBin = "0111";
                    break;
                case "8":
                    ConvHexBin = "1000";
                    break;
                case "9":
                    ConvHexBin = "1001";
                    break;
                case "A":
                    ConvHexBin = "1010";
                    break;
                case "B":
                    ConvHexBin = "1011";
                    break;
                case "C":
                    ConvHexBin = "1100";
                    break;
                case "D":
                    ConvHexBin = "1101";
                    break;
                case "E":
                    ConvHexBin = "1110";
                    break;
                case "F":
                    ConvHexBin = "1111";
                    break;
                default:
                    ConvHexBin = "";
                    break;
            }
            return ConvHexBin;
        }
        public string ConvBinHex(string hInput)
        {
            string ConvBinHex;
            switch (hInput)
            {
                case "0000":
                    ConvBinHex = "0";
                    break;
                case "0001":
                    ConvBinHex = "1";
                    break;
                case "0010":
                    ConvBinHex = "2";
                    break;
                case "0011":
                    ConvBinHex = "3";
                    break;
                case "0100":
                    ConvBinHex = "4";
                    break;
                case "0101":
                    ConvBinHex = "5";
                    break;
                case "0110":
                    ConvBinHex = "6";
                    break;
                case "0111":
                    ConvBinHex = "7";
                    break;
                case "1000":
                    ConvBinHex = "8";
                    break;
                case "1001":
                    ConvBinHex = "9";
                    break;
                case "1010":
                    ConvBinHex = "A";
                    break;
                case "1011":
                    ConvBinHex = "B";
                    break;
                case "1100":
                    ConvBinHex = "C";
                    break;
                case "1101":
                    ConvBinHex = "D";
                    break;
                case "1110":
                    ConvBinHex = "E";
                    break;
                case "1111":
                    ConvBinHex = "F";
                    break;
                default:
                    ConvBinHex = "";
                    break;
            }
            return ConvBinHex;
        }

    }
}