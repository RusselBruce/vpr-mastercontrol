﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class MappingConditions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetList()
        {
            clsConnection cls = new clsConnection();
            string getList = "select id, field_name, ";
            getList += "CASE WHEN column_name is null then '' else column_name end + ',' + ";
            getList += "CASE WHEN column_name2 is null then '' else column_name2 end + ',' + ";
            getList += "CASE WHEN column_name3 is null then '' else column_name3 end [column_name], ";
            getList += "CASE WHEN logical_operator is null then '' else logical_operator end + ',' + ";
            getList += "CASE WHEN logical_operator2 is null then '' else logical_operator2 end + ',' + ";
            getList += "CASE WHEN logical_operator3 is null then '' else logical_operator3 end [logical_operator], ";
            getList += "CASE WHEN column_val is null then '' else column_val end + ',' + ";
            getList += "CASE WHEN column_val2 is null then '' else column_val2 end + ',' + ";
            getList += "CASE WHEN column_val3 is null then '' else column_val3 end [column_val], ";
            getList += "CASE WHEN and_or_val is null then '' else and_or_val end + ',' + ";
            getList += "CASE WHEN and_or_val2 is null then '' else and_or_val2 end + ',' + ";
            getList += "CASE WHEN and_or_val3 is null then '' else and_or_val3 end [and_or_val], ";
            getList += "add_cond, text_val, yes_no_val, chkbox_val, modified_by, modified_date ";
            getList += "from tbl_VPR_Mapping_Conditions where deleted_date is null";
            DataTable dt = cls.GetData(getList);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string GetColumns()
        {
            clsConnection cls = new clsConnection();
            string getColumns = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tbl_VPR_CreatedFile'";
            DataTable dt = cls.GetData(getColumns);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string GetVal(string cfVal)
        {
            clsConnection cls = new clsConnection();
            string qry = "";
            if (cfVal == "Occupancy Status" || cfVal == "Property Type")
            {
                if (cfVal == "Occupancy Status")
                {
                    qry = "select normalize [Occupancy Status] from tbl_VPR_OccupancyStatus_Normalize group by normalize order by normalize";
                }
                else
                {
                    qry = "select normalize [Property Type] from tbl_VPR_PropertyType_Normalize group by normalize order by normalize";
                }
            }
            else
            {
                qry = "select [" + cfVal + "] from view_VPR_Inflow group by [" + cfVal + "] order by [" + cfVal + "]";
            }

            //qry = "select [" + cfVal + "] from view_VPR_Inflow group by [" + cfVal + "] order by [" + cfVal + "]";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static void SaveConditions(ArrayList myData, string usr)
        {
            clsConnection cls = new clsConnection();

            string qry = "insert into tbl_VPR_Mapping_Conditions (field_name, column_name, logical_operator, column_val, and_or_val, ";
            qry += "column_name2, logical_operator2, column_val2, and_or_val2, column_name3, logical_operator3, column_val3, and_or_val3, ";
            qry += "add_cond, text_val, yes_no_val, chkbox_val, modified_by, modified_date) values ";
            qry += "('" + myData[0] + "','" + myData[1] + "','" + myData[2] + "','" + myData[3] + "', ";
            qry += "'" + myData[4] + "','" + myData[5] + "','" + myData[6] + "','" + myData[7] + "', ";
            qry += "'" + myData[8] + "','" + myData[9] + "','" + myData[10] + "','" + myData[11] + "', ";
            qry += "'" + myData[12] + "','" + myData[13] + "','" + myData[14] + "','" + myData[15] + "', ";
            qry += "'" + myData[16] + "','" + usr + "', GETDATE())";

            cls.ExecuteQuery(qry);

        }

        [WebMethod]
        public static void UpdtConditions(ArrayList myData, string dataid, string usr)
        {
            clsConnection cls = new clsConnection();

            string qry = "update tbl_VPR_Mapping_conditions set ";
            qry += "field_name = '" + myData[0] + "', column_name = '" + myData[1] + "', logical_operator = '" + myData[2] + "', ";
            qry += "column_val = '" + myData[3] + "', and_or_val = '" + myData[4] + "', column_name2 = '" + myData[5] + "', ";
            qry += "logical_operator2 = '" + myData[6] + "', column_val2 = '" + myData[7] + "', and_or_val2 = '" + myData[8] + "', ";
            qry += "column_name3 = '" + myData[9] + "', logical_operator3 = '" + myData[10] + "', column_val3 = '" + myData[11] + "', ";
            qry += "and_or_val3 = '" + myData[12] + "', add_cond = '" + myData[13] + "', text_val = '" + myData[14] + "', ";
            qry += "yes_no_val = '" + myData[15] + "', chkbox_val = '" + myData[16] + "', modified_by = '" + usr + "', ";
            qry += "modified_date = GETDATE() where id = " + dataid;

            cls.ExecuteQuery(qry);

        }

        [WebMethod]
        public static void DeleteRow(string id, string usr)
        {
            clsConnection cls = new clsConnection();

            string qry = "update tbl_VPR_Mapping_Conditions set deleted_by = '" + usr + "', deleted_date = GETDATE() where id = " + id;

            cls.ExecuteQuery(qry);
        }

        [WebMethod]
        public static string UpdateRow(string dataid)
        {
            clsConnection cls = new clsConnection();

            string qry = "select * from tbl_VPR_Mapping_Conditions where id = " + dataid;
            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }
    }
}