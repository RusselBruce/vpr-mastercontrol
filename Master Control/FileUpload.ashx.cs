﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace Master_Control
{
	/// <summary>
	/// Summary description for FileUpload
	/// </summary>
	public class FileUpload : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{

			clsConnection cls = new clsConnection();
          
            //string query = "select id, [filename] from tbl_VPR_Mapping_PDF order by id desc";

            //DataTable dt = new DataTable();
            //dt = cls.GetData(query);

            //string fn = dt.Rows[0][1].ToString();

			if (context.Request.Files.Count > 0)
			{

				HttpFileCollection files = context.Request.Files;
                string fname = files.AllKeys[0].ToString();
                string IO2 = fname.Substring(0,12);
                string mark = "City Website";
                string [] folder = fname.Split('_');
                string Initial = "Initial";
                string Renewal = "Renewal";
              

                if (IO2 == mark)
                {
                    string pathonline = HttpContext.Current.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/');
                    string Initialfolder = HttpContext.Current.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Initial + '/');
                    string Renewalfolder = HttpContext.Current.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Renewal + '/');
                    string marksIO = "OI";
                  fname = fname.Remove(0, 13);


                  if (Directory.Exists(pathonline))
                  {
                      if (folder[1].ToString() == marksIO)
                     {
                  for (int i = 0; i < files.Count; i++)
                 {
                     HttpPostedFile file = files[i];

                     string path = Path.Combine(context.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Initial+'/'), fname + ".pdf");

                     //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");


                     file.SaveAs(path);

                     ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                     //try
                     //{
                     //	cls.ExecuteQuery(update);
                     //}
                     //catch (Exception) { }

                 }
                     }
                     else
                      {


                          for (int i = 0; i < files.Count; i++)
                          {
                              HttpPostedFile file = files[i];

                              string path = Path.Combine(context.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Renewal + '/'), fname + ".pdf");

                              //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");


                              file.SaveAs(path);

                              ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                              //try
                              //{
                              //	cls.ExecuteQuery(update);
                              //}
                              //catch (Exception) { }

                          }


                      }

                  }
                  else
                  {


                 DirectoryInfo di = Directory.CreateDirectory(pathonline);
                 DirectoryInfo di2 = Directory.CreateDirectory(Initialfolder);
                 DirectoryInfo di4 = Directory.CreateDirectory(Renewalfolder);

                    
                     if (folder[1].ToString() == marksIO)
                     {
                  for (int i = 0; i < files.Count; i++)
                 {
                     HttpPostedFile file = files[i];

                     string path = Path.Combine(context.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Initial+'/'), fname + ".pdf");

                     //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");


                     file.SaveAs(path);

                     ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                     //try
                     //{
                     //	cls.ExecuteQuery(update);
                     //}
                     //catch (Exception) { }

                 }
                     }
                     else
                     {

                    
                     for (int i = 0; i < files.Count; i++)
                     {
                         HttpPostedFile file = files[i];

                         string path = Path.Combine(context.Server.MapPath("~/Files/Online Files/" + folder[4].ToString() + '/' + Renewal + '/'), fname + ".pdf");

                         //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");


                         file.SaveAs(path);

                         ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

                         //try
                         //{
                         //	cls.ExecuteQuery(update);
                         //}
                         //catch (Exception) { }

                     }

                     }
                  }
                }
                else
                {

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        string path = "";

                        if(!System.IO.Directory.Exists(context.Server.MapPath("~/Files/Additional Forms/")))
                        {
                            System.IO.Directory.CreateDirectory(context.Server.MapPath("~/Files/Additional Forms/"));
                            path = Path.Combine(context.Server.MapPath("~/Files/Additional Forms/"), fname + ".pdf");
                            file.SaveAs(path);
                        }
                        else
                        {
                            path = Path.Combine(context.Server.MapPath("~/Files/Additional Forms/"), fname + ".pdf");
                            file.SaveAs(path);
                        }
                        
                        //string path = Path.Combine(context.Server.MapPath("~/Files/Additional Forms/"), fname + ".pdf");
                        //string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");
                        //file.SaveAs(path);
                         ////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();
                        //try
                        //{
                        //	cls.ExecuteQuery(update);
                        //}
                        //catch (Exception) { }
                    }
                }
			}
			context.Response.ContentType = "text/plain";
			context.Response.Write("File Uploaded Successfully!");
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}