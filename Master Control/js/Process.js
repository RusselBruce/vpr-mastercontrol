﻿$(document).ready(function () {

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    loadState();

});

function loadState() {
    $.ajax({
        type: 'POST',
        url: 'Process.aspx/popState',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            $('#lstState').empty();
            for (var i = 0; i < obj.length; i++) {
                $('#lstState').append('<li><button type="button" class="btn btn-link stateLink noPadMar city" id="' + obj[i].stateName + '">' + obj[i].stateName + '</button></li>');
            }

            $('.city').click(function () {
            	var id = $(this).attr('id');
            	$('#lstState li').each(function () {
            		$('#lstState li').removeClass('activeList');
            	});

            	$(this).closest('li').addClass('activeList');
                loadCity(id);
            });

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}


function loadCity(state) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Process.aspx/popFunction',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var obj = data.d;
            
            var ddl = "<select>";
            for (var j = 0; j < obj.length; j++)
            {
                ddl += "<option value='" + obj[j].funcName + "'>" + obj[j].funcName + "</option>"
            }

            ddl += "</select>"
            

            $.ajax({
                type: 'POST',
                url: 'Process.aspx/popCity',
                data: '{"state" : "' + state + '"}',
                contentType: 'application/json; charset=utf-8',
                success: function (res) {
                    var fl = res.d;
                    $('#cityBody').empty();
                    console.log(fl);
                    for (var i = 0; i < fl.length; i++) {
                        var ta = " <tr>";
                        var ck = "";
                        
                        ta += "<td><input type='checkbox' id='" + fl[i].id + "' data-id='" + fl[i].cityName + "' class='cityChk'></td>";
                        
                        ta += "<td>If the city " + fl[i].cityName + " in use</td>";
                        ta += "<td>"+ddl+"</td>"
                        ta += "<tr>";
                        $('#cityBody').append(ta);

                        if (fl[i].isActive == "True") {
                            //console.log($('#city_' + fl[i].id).attr('class'));
                            $("#" + fl[i].id).prop('checked', true);
                        }
                        
                        
                        $("#" + fl[i].id).closest('tr').find('td select').val(fl[i].setting);
                        
                    }
                    $('#modalLoading').modal('hide');

                },
                error: function (response) {
                    console.log(response.responseText);
                },
                failure: function (response) {
                    console.log(response.responseText);
                }
            });


        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#saveBtn').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var mainArr = [];
    var subArr = {};
    $('#cityBody tr:has(td)').each(function ()
    {
        var cellChk = "";
        if($(this).find("td .cityChk").is(':checked'))
        {
            cellChk = "1";
        }
        else
        {
            cellChk = "0";
        }
        
        var cellId = $(this).find("td .cityChk").attr('data-id');
        var cellPro = $(this).find("td select option:selected").val();
        
        subArr = {
            
            'cityName' : cellId,
            'isActive': cellChk,
            'setting' : cellPro
        }
        mainArr.push(subArr);
    });
    console.log(mainArr);

    $.ajax({
        type: 'POST',
        url: 'Process.aspx/updateProcess',
        data: '{pl : '+ JSON.stringify(mainArr) +'}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal('hide');
            loadCity(data.d);

        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
});