﻿$(document).ready(function () {
	var myurl = "Datamapping.aspx/"

	$('.text-date').datepicker({
		autoclose: true
	});
	function ShowHeaders(data) {
		$.each(data, function (record, recordnumber) {
			$("#listB").append("<div id='B1' class='slot col-md-2 origin'> <div id='a1' class='item' myval='" + recordnumber.FieldName + "' mysample='" + recordnumber.SampleData + "' current>" + recordnumber.FieldName + " </div></div>");
			$(".divSAMPLEDATA .slot").empty();
		});

		$("#divLOADER").hide();
		var dragOption = {
			delay: 10,
			distance: 5,
			opacity: 0.45,
			revert: "invalid",
			start: function (event, ui) {
				$(".ui-selected").each(function () {
					$(this).data("original", $(this).position());
				});
			},
			drag: function (event, ui) {
				var offset = ui.position;
				//console.log(ui, this)
				$(".ui-selected").not(this).each(function () {
					var current = $(this).offset(),
						targetLeft = document.elementFromPoint(current.left - 1, current.top),
						targetRight = document.elementFromPoint(current.left + $(this).width() + 1, current.top);
					$(this).css({
						position: "relative",
						left: offset.left,
						top: offset.top
					}).data("target", $.unique([targetLeft, targetRight]));

					//console.log($.unique([targetLeft, targetRight]));
				});
			},
			stop: function (event, ui) {
				validate($(".ui-selected").not(ui.draggable));
			}
		}, dropOption = {
			accept: '.item',
			activeClass: "green3",
			greedy: true,
			drop: function (event, ui) {
				console.log("drop")
				if ($(this).is(".slot")) {
					console.log("appending")
					$(this).append(ui.draggable.css({
						top: '5px',
						left: '5px'
					}));
					var standard = $(this).attr("myval");
					var newsample = $(this).find(".item").attr("mysample");
					$(this).find(".item").attr("current", standard);
					$(this).removeClass("empty");
					if ($(this).hasClass("origin")) {
						$(".divSAMPLEDATA .slot[data-val='" + newsample + "']").remove();
						$(".divSTANDARD .slot[data-val='" + newsample + "']").remove();
						return;
					} else {

						if ($(this).closest(".divSTANDARD").find(".empty").length == 0) {
							//$(this).addClass('empty');
							$(this).attr("data-val", newsample);
							$(this).closest(".multiplier").find(".divSAMPLEDATA .slot:last-child").attr("data-val", newsample);
							$(this).closest(".multiplier").find(".divSAMPLEDATA .slot:last-child").html(newsample);
							$(this).closest(".divSTANDARD").append('<div id="B1" class="slot col-md-2 ui-droppable empty" data-val=""> </div>');
							$(this).closest(".multiplier").find(".divSAMPLEDATA").append('<div id="B1" class="slot col-md-2 ui-droppable" data-val=""></div>');
							$(".slot").droppable(dropOption);
						}
					}
				} else {
					console.log("reverting")
					ui.draggable.animate({
						top: 0,
						left: 0
					}, "slow");

				}
				validate($(".ui-selected").not(ui.draggable));
			}
		}

		$(".divbox").selectable({
			filter: ".item",
			start: function (event, ui) {
				$(".ui-selected").draggable("destroy");

			},
			stop: function (event, ui) {
				$(".ui-selected").draggable(dragOption)
			}
		});
		$(".slot").droppable(dropOption);

		function validate($draggables) {

			$draggables.each(function () {
				console.log($($(this).data("target")));
				var $target = $($(this).data("target")).filter(function (i, elm) {

					console.log($(this).is(".slot") && !$(this).has(".item").length);
					return $(this).is(".slot") && !$(this).has(".item").length;
				});
				console.log($target);
				if ($target.length) {
					$target.append($(this).css({
						top: '3px',
						left: '3px'
					}))
				} else {
					$(this).animate({
						top: 0,
						left: 0
					}, "slow");
				}

			});
			$(".ui-selected").data("original", null)
				.data("target", null)
				.removeClass("ui-selected");
		}

		$(function () {
			$(".item").draggable({ revert: "invalid" });
		});
		$("#divUPLOADER").fadeOut("slow", function () {

			$("#divCONTENT").fadeIn();
		});
	}
	$(".dropzone").dropzone({
		url: "DatamappingHandler.ashx",
		addRemoveLinks: true,
		maxFiles: 1,
		acceptedFiles: ".xlsx, .xls, xlsb",
		success: function (file, response) {
			$("#divLOADER").show();
			$("#listB").empty();
			$("#divSTANDARD .ui-draggable").remove();
			var myresult;

			myresult = response;
			var imgName = myresult[0];
			file.previewElement.classList.add("dz-success");
			file.previewElement.classList.add("newupload");
			$(".newupload").find(".dz-filename span").text(myresult);
			file.previewElement.classList.remove("newupload");
			x = 0;

			var arr = myresult.split('*');
			$('#lblFN').text(arr[0]);
			$('#lblFN').attr('data-id', arr[1]);

			$("#divLOADER").hide();
		},
		error: function (file, response) {
			if (!file.accepted) this.rsssssemoveFile(file);
			alertify.error('You can not upload any more files.');
		}
	});
	$('#btnGetHead').click(function () {
		var filename = $('#lblFN').attr('data-id');
		var sheetname = $('#inSN').val();

		var newdata = "{'filename': " + JSON.stringify(filename) + ", 'sheetname': " + JSON.stringify(sheetname) + "}";
		busconnect("Datamapping.aspx/", newdata, 'getUPLOADEDDATA', ShowHeaders);
	});
	$("#btnBACK").click(function () {
		$("#divCONTENT").fadeOut("slow", function () {
			$("#divUPLOADER").fadeIn();
		});
	});
	$("#btnSUBMIT").click(function () {
		var obj = new Array();
		$("#divLOADER").show();

		$('.divSTANDARD div.slot .item').each(function (i, row) {
			var newdata = {
				"FieldName": $(this).attr("myval"),
				"SampleData": $(this).attr("mysample"),
				"GroupName": $.trim($(this).closest(".multiplier").find(".txtGROUPNAME").val())
			}
			obj.push(newdata);
		});

		var Grouping = [];
		Grouping = {
			Fields: obj,
			GroupName: $.trim($("#txtGROUPNAME").val()),
			Name: $.trim($("#txtNAME").val()),
			Source: $.trim($("#txtSOURCE").val()),
			Location: $.trim($("#txtLOCATION").val()),
			Description: $.trim($("#txtDESCRIPTION").val()),
			Occurence: $.trim($("#optOCCURENCE option:selected").val()),
			StartTime: $.trim($("#txtSTARTTIME").val()),
			EndTime: $.trim($("#txtENDTIME").val()),
			StartDate: $.trim($("#txtSTARTDATE").val()),
			EndDate: $.trim($("#txtENDDATE").val())
		}

		var originalFile = $('#lblFN').text();
		var generatedFile = $('#lblFN').attr('data-id');
		var sheetname = $('#inSN').val();

		var GroupingSetting;
		GroupingSetting = "{'originalFile' : '" + originalFile + "', 'generatedFile' : '" + generatedFile + "', 'sheetname' : '" + sheetname + "', 'GroupSetting' : " + JSON.stringify(Grouping) + "}";
		busconnect('Datamapping.aspx/', GroupingSetting, "SaveGroup", matchHEADER);
		function matchHEADER(mydata) {
			$("#divLOADER").hide();
			alertify.success("Successfully Created Grouping");
			$("#mdlMAPPING").modal("hide");
		}
	});
	$("#divMAIN").on("click", ".btnADD", function () {
		$('.btnRMV').show();
		$(this).hide();
		$(".clones .multiplier").clone().appendTo("#divMAIN");
		function validate($draggables) {

			$draggables.each(function () {
				console.log($($(this).data("target")));
				var $target = $($(this).data("target")).filter(function (i, elm) {

					console.log($(this).is(".slot") && !$(this).has(".item").length);
					return $(this).is(".slot") && !$(this).has(".item").length;
				});
				console.log($target);
				if ($target.length) {
					$target.append($(this).css({
						top: '3px',
						left: '3px'
					}))
				} else {
					$(this).animate({
						top: 0,
						left: 0
					}, "slow");
				}

			});
			$(".ui-selected").data("original", null)
				.data("target", null)
				.removeClass("ui-selected");
		}
		var dragOption = {
			delay: 10,
			distance: 5,
			opacity: 0.45,
			revert: "invalid",
			start: function (event, ui) {
				$(".ui-selected").each(function () {
					$(this).data("original", $(this).position());
				});
			},
			drag: function (event, ui) {
				var offset = ui.position;
				//console.log(ui, this)
				$(".ui-selected").not(this).each(function () {
					var current = $(this).offset(),
						targetLeft = document.elementFromPoint(current.left - 1, current.top),
						targetRight = document.elementFromPoint(current.left + $(this).width() + 1, current.top);
					$(this).css({
						position: "relative",
						left: offset.left,
						top: offset.top
					}).data("target", $.unique([targetLeft, targetRight]));

					//console.log($.unique([targetLeft, targetRight]));
				});
			},
			stop: function (event, ui) {
				validate($(".ui-selected").not(ui.draggable));
			}
		}, dropOption = {
			accept: '.item',
			activeClass: "green3",
			greedy: true,
			drop: function (event, ui) {
				console.log("drop")
				if ($(this).is(".slot")) {
					console.log("appending")
					$(this).append(ui.draggable.css({
						top: '5px',
						left: '5px'
					}));
					var standard = $(this).attr("myval");
					var newsample = $(this).find(".item").attr("mysample");
					$(this).find(".item").attr("current", standard);
					$(this).removeClass("empty");
					if ($(this).hasClass("origin")) {
						$(".divSAMPLEDATA .slot[data-val='" + newsample + "']").remove();
						$(".divSTANDARD .slot[data-val='" + newsample + "']").remove();
						return;
					} else {

						if ($(this).closest(".divSTANDARD .empty").length == 0) {
							$(this).attr("data-val", newsample);
							$(this).closest(".multiplier").find(".divSAMPLEDATA .slot:last-child").attr("data-val", newsample);
							$(this).closest(".multiplier").find(".divSAMPLEDATA .slot:last-child").html(newsample);
							$(this).closest(".divSTANDARD").append('<div id="B1" class="slot col-md-2 ui-droppable empty" data-val=""> </div>');
							$(this).closest(".multiplier").find(".divSAMPLEDATA").append('<div id="B1" class="slot col-md-2 ui-droppable" data-val=""></div>');
							$(".slot").droppable(dropOption);
						}
					}
				} else {
					console.log("reverting")
					ui.draggable.animate({
						top: 0,
						left: 0
					}, "slow");

				}
				validate($(".ui-selected").not(ui.draggable));


			}
		}
		$(".slot").droppable(dropOption);

	});

	$("#divMAIN").on("click", ".btnRMV", function () {

		if ($(this).closest('#divMAIN .multiplier').is(':last-child') == true) {
			var len = $('#divMAIN .multiplier').length;
			$('#divMAIN .multiplier .btnADD:eq(' + (len - 2) + ')').show();
		}

		$(this).closest('.panel').remove();

		if ($('#divMAIN .multiplier').length == 1) {
			$('.btnRMV').hide();
		}
	});
});