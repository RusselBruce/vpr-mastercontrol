﻿$(document).ready(function () {
    //getVal();
    //getList();
});

$('#slct').change(function () {

    var val = $('#slct').val();
    var  standardLabel = [];
    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/GetList',
        data: '{header: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                if (val == "occupancy") {
                    $('.prop').hide();
                    $('.occu').show();

                    $('#tblOccupancy tbody').empty();
                    $.each(records, function (idx, val) {
                        standardLabel.push(val.id + ',' + val.normalize1 );
                        $('#tblOccupancy tbody').append(
                            '<tr>' +
                                '<td><label class="lblOcc">' + val.Normalize + '</label><input type="text" class="form-control input-sm tbOcc" style="display:none;" value="'+val.Normalize+'" /></td>' +
                                '<td>' + dropDown3(val.id) + '</td>' +
                                '<td>' + deleteRow(val.id) + '</td>' +
                            '</tr>'
                        );
                    });                    

                } else if (val == "property") {
                    $('.prop').show();
                    $('.occu').hide();

                    $('#tblProperty tbody').empty();
                    $.each(records, function (idx, val) {
                       
                        var test = val.id;
                        standardLabel.push(val.id + ',' + val.normalize1 + ',' + val.col_name);
                        $('#tblProperty tbody').append(
                            '<tr>' +
                                '<td><label class="lblProp">' + val.Normalize + '</label><input type="text" class="form-control input-sm tbProp" style="display:none;" value="' + val.Normalize + '" /></td>' +
                                '<td>' + dropDown1(val.id) + '</td>' +
                                '<td>' + dropDown2(val.id) + '</td>' +
                                '<td>' + deleteRow(val.id) + '</td>' +
                            '</tr>'
                        );
                    });
                    
                } else {
                    $('.occu').hide();
                    $('.prop').hide();
                }
                
                getLabels(val, standardLabel);

                if (val == "property")
                {
                    for (var i = 0; i < standardLabel.length; i++) {
                        var stringval = standardLabel[i];
                        var splitVal = stringval.split(',');

                        var IDddl = 'ddlRegis' + splitVal[0];

                        if (splitVal[2] == 'Commercial') {
                            document.getElementById(IDddl).selectedIndex = 0;
                        }
                        else if (splitVal[2] == 'Condo') {
                            document.getElementById(IDddl).selectedIndex = 1;
                        }
                        else if (splitVal[2] == 'Mobile Home') {
                            document.getElementById(IDddl).selectedIndex = 2;
                        }
                        else if (splitVal[2] == 'Residential') {
                            document.getElementById(IDddl).selectedIndex = 3;
                        }
                        else if (splitVal[2] == 'Townhome') {
                            document.getElementById(IDddl).selectedIndex = 4;
                        }
                        else if (splitVal[2] == 'Vacant Lot') {
                            document.getElementById(IDddl).selectedIndex = 5;
                        }
                        else {
                        }

                    }
                }
                
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
});

function dropDown1(val) {

    var select = '<select id="ddlStan' + val +  '" class="form-control input-sm noPadMar slctLabel"></select>';

    return select;
}
function dropDown3(val) {

    var select = '<select id="ddlStanOccu' + val + '" class="form-control input-sm noPadMar slctLabel"></select>';

    return select;
}

function getLabels(val, standardLabel) {
    var standardLabelPass = standardLabel;
    var idVal = [];
    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/GetNormalize',
        data: '{normalize: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('.slctLabel').empty();
               

                $.each(records, function (idx, val) {
                    idVal.push(val.id + ',' + val.Normalize);
                    $('.slctLabel').append('<option value="' + val.Normalize + '">' + val.Normalize + '</option>');
                });

                if (val == "property")
                {
                    for (var i = 0; i < standardLabelPass.length; i++) {
                        var stringval = standardLabelPass[i];
                        var splitVal = stringval.split(',');


                        var IDddl = 'ddlStan' + splitVal[0];

                        for (var ii = 0; ii < idVal.length; ii++) {
                            var idSplit = idVal[ii];
                            var idName = idSplit.split(',');
                            if (idSplit.includes(splitVal[1])) {
                                var first = idName[0];
                                var total = first - 1;
                                var element = document.getElementById(IDddl);
                                element.value = idName[1];
                            }
                        }
                    }
                }
                else if ("occupancy")
                {
                    for (var i = 0; i < standardLabelPass.length; i++) {
                        var stringval = standardLabelPass[i];
                        var splitVal = stringval.split(',');


                        var IDddl = 'ddlStanOccu' + splitVal[0];

                        for (var ii = 0; ii < idVal.length; ii++) {
                            var idSplit = idVal[ii];
                            var idName = idSplit.split(',');
                            if (idSplit.includes(splitVal[1])) {
                                var first = idName[0];
                                var total = first - 1;
                                var element = document.getElementById(IDddl);
                                element.value = idName[1];
                            }
                        }
                    }
                }
                else
                {

                }
                





            }
        }

    });
}







function dropDown2(val) {
    //var select1 = '<select id="ddlRegis' + val + '" class="form-control input-sm noPadMar slctLabel2">';
    //select1 += '<option values="Commercial">Commercial</option>';
    //select1 += '<option values="Condo">Condo</option>';
    //select1 += '<option values="Mobile Home">Mobile Home</option>';
    //select1 += '<option values="Residential">Residential</option>';
    //select1 += '<option values="Townhome">Townhome</option>';
    //select1 += '<option values="Vacant Lot">Vacant Lot</option>';
    //select1 += '</select>';

    var select1 = '<select id="ddlRegis' + val + '" class="form-control input-sm noPadMar slctLabel2">';
    select1 += '<option values="1">Commercial</option>';
    select1 += '<option values="2">Condo</option>';
    select1 += '<option values="3">Mobile Home</option>';
    select1 += '<option values="4">Residential</option>';
    select1 += '<option values="5">Townhome</option>';
    select1 += '<option values="6">Vacant Lot</option>';
    select1 += '</select>';


    return select1;
}


//function getSelect() {

//    $.ajax({
//        type: "POST",
//        url: "Standardization.aspx/GetNormalize",
//        contentType: "application/json; charset=utf-8",
//        success: function (data) {
//            var d = $.parseJSON(data.d);

//            if (d.Success) {
//                var records = d.data.record;

//                $.each(records, function (idx, val) {
//                    $('#municipality').append(
//                        '<option value="' + val.Normalize + '">' + val.Normalize + '</option>'
//                    );
//                });
//            }

//        }
//    });
//}

//function saveRow() {

//}

function deleteRow(val) {
    var updt = '<button type="button" class="btn btn-link btnEdit" data-id="' + val + '" onclick="updtRow($(this))"><i class="fa fa-pencil-square-o"></i></button>';
    var cancel = '<button type="button" class="btn btn-link text-red btncancel" data-id="' + val + '" onclick="cancel($(this))" style = "display: none;"><i class="fa fa-ban"></i></button>';
    var del = '<button type="button" class="btn btn-link text-red" data-id="' + val + '" onclick="delRow($(this))"><i class="fa fa-trash"></i></button>';
    var save = '<button type="button" class="btn btn-link text-green" data-id="' + val + '" onclick="save($(this))"><i class="fa fa-save"></i></button>';

    return updt +cancel+ save;
}

function updtRow(val) {


    $(val).closest('tr').find('td:eq(0) .lblOcc').hide();
    $(val).closest('tr').find('td:eq(0) .tbOcc').show();

    $(val).closest('tr').find('td:eq(0) .lblProp').hide();
    $(val).closest('tr').find('td:eq(0) .tbProp').show();

    $(val).hide();
    $(val).next().show();
}

function cancel(val) {
    $(val).closest('tr').find('td:eq(0) .lblOcc').show();
    $(val).closest('tr').find('td:eq(0) .tbOcc').hide();

    $(val).closest('tr').find('td:eq(0) .lblProp').show();
    $(val).closest('tr').find('td:eq(0) .tbProp').hide();

    $(val).hide();
    $(val).prev().show();
}

function save(val) {
    var tbOcc = $(val).closest('tr').find('td:eq(0) .tbOcc').val();
    var lblOcc = $(val).closest('tr').find('td:eq(0) .lblOcc');

    var tbProp = $(val).closest('tr').find('td:eq(0) .tbProp').val();
    var lblProp = $(val).closest('tr').find('td:eq(0) .lblProp');

    lblProp.text(tbProp);
    $(val).closest('tr').find('td:eq(0) .tbProp').hide();
    $(val).closest('tr').find('td:eq(0) .lblProp').show();

    lblOcc.text(tbOcc);
    $(val).closest('tr').find('td:eq(0) .tbOcc').hide();
    $(val).closest('tr').find('td:eq(0) .lblOcc').show();

    $(val).prevAll('.btncancel').hide();

    $(val).prevAll('.btnEdit').show();

    var slctHdr = $('#slct').val();
    var dataid = $(val).attr('data-id');
    var occNorm = '', stLbl = '', regCondClass = '';
    var usr = $('#usr').text();

    var myData = [];

    if (slctHdr == 'occupancy') {
        occNorm = $(val).closest('tr').find('td:eq(1) select').val();

        myData = [dataid, tbOcc, occNorm];
    } else {
        stLbl = $(val).closest('tr').find('td:eq(1) select').val();
        regCondClass = $(val).closest('tr').find('td:eq(2) select').val();

        myData = [dataid, tbProp, stLbl, regCondClass]
    }

    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/Save',
        data: '{myData: ' + JSON.stringify(myData) + ', slctHdr: "' + slctHdr + '", usr: "' + usr + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
        }
    });
}

//function fetch(val) {

//    $.ajax({


//    });
//}