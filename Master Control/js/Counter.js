﻿$(document).ready(function () {

    getCheckCount();
    getApproveCount();

});


function getCheckCount() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCheckerCount',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                $('#checkerCount').text(records[0].CountNum);

            }

        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getApproveCount() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetApproverCount',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                $('#approverCount').text(records[0].CountNum);

            }

        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}