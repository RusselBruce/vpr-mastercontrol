﻿$(document).ready(function () {
    getEmp();

});

function getEmp() {

    $.ajax({
        type: 'POST',
        url: 'AuthorizedSignatory.aspx/GetEmp',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#slctAuthSig, #slctAlter').empty();
                $('#slctAuthSig, #slctAlter').append('<option value="" selected="selected">--Select One--</option>');

                $.each(records, function (idx, val) {
                    $('#slctAuthSig, #slctAlter').append(
                        '<option value="' + val.NTID + '">' + val.EmpName + '</option>'
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}