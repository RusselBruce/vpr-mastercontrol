﻿function getDate(date) {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = new Date(date);
    var month = d.getMonth();
    var day = d.getDate();
    day = (d.toString().length == 1) ? '0' + day : day;
    var year = d.getFullYear();

    return StrMonths[month] + ' ' + day + ', ' + year;
}

function getDateTime(date) {
    if (date != null) {
        var strDate = date.replace('T', ' ');

        var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var d = new Date(strDate);
        var month = d.getMonth();
        var day = d.getDate();
        day = (day.toString().length == 1) ? '0' + day : day;
        var year = d.getFullYear();


        var hours = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        var minutes = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
        var seconds = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();

        return StrMonths[month] + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes + ':' + seconds;
    } else {
        return '-';
    }
}

function inGetDate(date) {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = new Date(date);
    var month = d.getMonth();
    var day = d.getDate();
    day = (d.toString().length == 1) ? '0' + day : day;
    var year = d.getFullYear();

    var val = StrMonths[month] + ' ' + day + ', ' + year;

    return '<input type="text" class="form-control input-sm" value="' + val + '" />';
}

function inGetDateTime(date) {
    if (date != null) {
        var strDate = date.replace('T', ' ');

        var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var d = new Date(strDate);
        var month = d.getMonth();
        var day = d.getDate();
        day = (day.toString().length == 1) ? '0' + day : day;
        var year = d.getFullYear();


        var hours = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        var minutes = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
        var seconds = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();

        var val = StrMonths[month] + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes + ':' + seconds;

        return '<input type="text" class="form-control input-sm" value="' + val + '" />';
    } else {
        return '';
    }
}

function getMonths() {
    var StrMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return StrMonths;
}