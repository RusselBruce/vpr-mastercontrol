﻿var lcCity = "", lcZip = "", lcState = "";

var existingSettingforPFC = "", existingSettingforREO = "", existingSettingPropType = "",
    existingSettingforReq, existingSettingforCost = "", existingSettingforContReg = "";
var existingSettingforInspection = "", existingSettingforMunicipality = "", existingSettingforDeregistration = "",
     existingSettingforOrdinance = "", existingSettingforAddForms = "";

$(document).ready(function () {
    getCheckCount();
    getApproveCount();
    getState();
    
    for(var i = 1; i <= 10 ; i++){
        $("#UploadFile"+i).fileinput({
            browseLabel: "Browse",
        });
    }

    //$("#UploadFile1").fileinput({
    //    browseLabel: "Browse",
    //});

    $('.datepicker').datepicker({
        autoclose: true
    });

    $(".timepicker").timepicker({
        showInputs: false
    });

    $('#slctAddnInfo').multiselect({
        numberDisplayed: 0,
        includeSelectAllOption: true,
    });

    $('#tabPFC, #tabREO, #tabPropType, #tabCost, #tabContReg, #tabPropReq, #lci, ' +
        '#tabInspection, #tabMunicipality, #tabDeregistration').find('div.audit input').prop('disabled', true);

    $('#tabPFC, #tabREO, #tabPropType, #tabCost, #tabContReg, #tabPropReq, #lci, ' +
        '#tabInspection, #tabMunicipality, #tabDeregistration').find('div.audit select').prop('disabled', true);

    $('#tabPFC .file-input, #tabDeregistration .file-input').prop('disabled', true);


    $("input[type=number]").keypress(function (event) {
        if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
            return false;
        }
    });


    $('#slctRenewEveryNum, #slctREORenewEveryNum').empty();
    $('#slctRenewEveryNum, #slctREORenewEveryNum').append('<option value="" selected="selected">N/A</option>');
    for (x = 1; x <= 100; x++) {
        $('#slctRenewEveryNum, #slctREORenewEveryNum').append('<option value="' + x + '">' + x + '</option>');
    }

    var months = getMonths();

    $('#slctRenewOnMonths, #slctREORenewOnMonths').empty();
    $('#slctRenewOnMonths, #slctREORenewOnMonths').append('<option value="" selected="selected">N/A</option>');
    $.each(months, function (idx, val) {
        $('#slctRenewOnMonths, #slctREORenewOnMonths').append('<option value="' + val + '">' + val + '</option>');
    });

    var x;

    $('#slctRenewOnNum, #slctREORenewOnNum').empty();
    $('#slctRenewOnNum, #slctREORenewOnNum').append('<option value="" selected="selected">N/A</option>');
    for (x = 1; x <= 31; x++) {
        $('#slctRenewOnNum, #slctREORenewOnNum').append('<option value="' + x + '">' + x + '</option>');
    }

    var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
    var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
    var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
    var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

    rdRenewal1.prop('checked', true);
    divRenewOn1.prop('disabled', true);
    divRenewOn2.prop('disabled', true);



    var rdREORenewal1 = $('input[name=rdREORenewal]:eq(0)');
    var rdREORenewal2 = $('input[name=rdREORenewal]:eq(1)');
    var divREORenewOn1 = rdREORenewal2.closest('div').next().find('select');
    var divREORenewOn2 = rdREORenewal2.closest('div').next().next().find('select');

    rdREORenewal1.prop('checked', true);
    divREORenewOn1.prop('disabled', true);
    divREORenewOn2.prop('disabled', true);


    $("#RDEVERY").click(function () {
        $('#slctRenewOnMonths').prop('disabled', true);
        $('#slctRenewOnNum').prop('disabled', true);
        $('#slctRenewEveryNum').prop('disabled', false);
        $('#slctRenewEveryYears').prop('disabled', false);

        $('#slctRenewOnMonths').val('');
        $('#slctRenewOnNum').val('');
    });

    $("#RDON").click(function () {

        $('#slctRenewOnMonths').prop('disabled', false);
        $('#slctRenewOnNum').prop('disabled', false);
        $('#slctRenewEveryNum').prop('disabled', true);
        $('#slctRenewEveryYears').prop('disabled', true);

        $('#slctRenewEveryNum').val('');
        $('#slctRenewEveryYears').val('');
    });
    $('.renewYes').hide();
    $('.renewREOYes').hide();
    $('.select2').select2();
    $('#btnSavePDFRenew').hide();
    $('#btnSavePDF2').hide();
    $('#btnSavePDFRenewREO').hide();
});

$("#Reordvery").click(function () {

    $('#slctREORenewOnMonths').prop('disabled', true);
    $('#slctREORenewOnNum').prop('disabled', true);



    $('#slctREORenewEveryNum').prop('disabled', false);
    $('#slctREORenewEveryYears').prop('disabled', false);

    $('#slctREORenewOnMonths').val('');
    $('#slctREORenewOnNum').val('');
});

$("#Reordon").click(function () {

    $('#slctREORenewOnMonths').prop('disabled', false);
    $('#slctREORenewOnNum').prop('disabled', false);



    $('#slctREORenewEveryNum').prop('disabled', true);
    $('#slctREORenewEveryYears').prop('disabled', true);

    $('#slctREORenewEveryNum').val('');
    $('#slctREORenewEveryYears').val('');
});

$("#RDEVERY").click(function () {

    $('#slctRenewOnMonths').prop('disabled', true);
    $('#slctRenewOnNum').prop('disabled', true);



    $('#slctRenewEveryNum').prop('disabled', false);
    $('#slctRenewEveryYears').prop('disabled', false);

    $('#slctRenewOnMonths').val('');
    $('#slctRenewOnNum').val('');
});

$("#RDON").click(function () {

    $('#slctRenewOnMonths').prop('disabled', false);
    $('#slctRenewOnNum').prop('disabled', false);



    $('#slctRenewEveryNum').prop('disabled', true);
    $('#slctRenewEveryYears').prop('disabled', true);

    $('#slctRenewEveryNum').val('');
    $('#slctRenewEveryYears').val('');
});

function check(val) {
    $(val).hide();
    $(val).closest('div.col-xs-12').find('div.audit input').prop('disabled', true);
    $(val).closest('div.col-xs-12').find('div.audit select').prop('disabled', true);
    $(val).closest('div.col-xs-12').find('div.audit button').prop('disabled', true);
    $(val).closest('div.col-xs-12').find('div.audit textarea').prop('disabled', true);
    //$.each($('div.audit .file-input *'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
    //$.each($('div.audit .btn-group button'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
}
function cross(val) {

    $(val).prev().show();
    $(val).closest('div.col-xs-12').find('div.audit input').prop('disabled', false);
    $(val).closest('div.col-xs-12').find('div.audit select').prop('disabled', false);
    $(val).closest('div.col-xs-12').find('div.audit button').prop('disabled', false);
    $(val).closest('div.col-xs-12').find('div.audit textarea').prop('disabled', false);
    //if ($(val).closest('div').prev().find('div.file-input').hasClass('file-input')) {
    //    $.each($('div.audit .file-input *'), function (idx, val) {
    //        $(this).prop('disabled', false);
    //        $(this).attr('disabled', false);
    //        if ($(this).hasClass('disable')) {
    //            $(this).removeClass('disable');
    //        }
    //        if ($(this).hasClass('file-caption-disabled')) {
    //            $(this).removeClass('file-caption-disabled');
    //        }
    //    });
    //}

    //if ($(val).closest('div').prev().find('div.btn-group').hasClass('btn-group')) {
    //    $.each($('div.audit button'), function (idx, val) {
    //        $(this).prop('disabled', false);
    //        $(this).attr('disabled', false);
    //        if ($(this).hasClass('disable')) {
    //            $(this).removeClass('disable');
    //        }
    //        if ($(this).hasClass('dropdown-toggle')) {
    //            $(this).removeClass('disabled');
    //        }
    //    });
    //}
}

function checkReq(val) {
    $(val).hide();
    $(val).closest('div.col-xs-6').find('div.auditReq input').prop('disabled', true);
    $(val).closest('div.col-xs-6').find('div.auditReq select').prop('disabled', true);
    $(val).closest('div.col-xs-6').find('div.auditReq button').prop('disabled', true);
    //$.each($('div.audit .file-input *'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
    //$.each($('div.audit .btn-group button'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
}
function crossReq(val) {
    $(val).prev().show();
    $(val).closest('div.col-xs-6').find('div.auditReq input').prop('disabled', false);
    $(val).closest('div.col-xs-6').find('div.auditReq select').prop('disabled', false);
    $(val).closest('div.col-xs-6').find('div.auditReq button').prop('disabled', false);
    //$.each($('div.audit .file-input *'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
    //$.each($('div.audit .btn-group button'), function (idx, val) {
    //    $(this).prop('disabled', true);
    //    $(this).attr('disabled', true);
    //});
}



function checkInspect(val) {
    $(val).hide();
    $(val).closest('div.grp').find('div.audit input').prop('disabled', true);
    $(val).closest('div.grp').find('div.audit select').prop('disabled', true);
}

function crossInspect(val) {
    $(val).prev().show();
    $(val).closest('div.grp').find('div.audit input').prop('disabled', false);
    $(val).closest('div.grp').find('div.audit select').prop('disabled', false);
}

function getState() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblState tbody').empty();
                //$('#slctMuniMailSta').empty();
                $.each(records, function (idx, val) {
                    $('#tblState tbody').append(
						'<tr class="rowHover" onclick="getMunicipality(this);">' +
							'<td data-id="' + val.State_Abbr + '">' + val.State + '</td>' +
						'</tr>'
					);

                    //$('#slctMuniMailSta').append('<option value="' + val.State + '">' + val.State + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMunicipality(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $(val).find('td').text();
    var state = $(val).find('td').attr('data-id');
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetMunicipality',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblMunicipality tbody').empty();
                $('#tblZip tbody').empty();
                $.each(records, function (idx, val) {
                    $('#tblMunicipality tbody').append(
						'<tr class="rowHover" onclick="getZip(this);">' +
							'<td data-id="' + val.municipality_code + '" >' + val.municipality_name + '</td>' +
						'</tr>'
					);
                });
            }
            $('#divSettings').hide();

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

var state, city;
function getZip(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //state = $('#tblState tbody tr.activeRow').text();
    state = $('#tblState tbody tr.activeRow td').attr('data-id');
    city = $(val).find('td').attr('data-id');
     //city = $(val).find('td').text();
    $.each($('#tblMunicipality tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');

    $('#divSettings input[type=text]').val('');
    $('#divSettings input[type=number]').val('');
    $('#modalEdit input[type=text]').val('');
    $('#divSettings select').each(function () {
        $(this).find('option:first-child').prop('selected', true);
    });

    getDataPFC(state, city);

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetZip',
        data: '{city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblZip tbody').empty();

                $.each(records, function (idx, val) {
                    var checked = '';
                    if (val.isZipActive) {
                        checked = 'checked';
                    }

                    $('#tblZip tbody').append(
						'<tr>' +
							'<td style="width: 10%;"><input data-id="' + val.ZipCode + '" type="checkbox" class="toggle" ' + checked + ' /></td>' +
							'<td><label>' + val.ZipCode + '</label></td>' +
						'</tr>'
					);

                    //$('#slctMuniMailZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });
                $('#modalLoading').modal('hide');
            }

            //getOrdinance();

            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });

            $('#divSettings').show();

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

//UPLOAD PDF
$('#btnSavePDF').click(function () {

    if ($('#inUploadPath').val() == '') {

        alertify.alert('Error', 'Please upload registration form!');

    } else {
        //$('#exampleModal').hide();
        //var usr = $('#usr').text();
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        serv = "Property Registration";
        var name = $('#inUploadPath').val().replace('C:\\fakepath\\', '');
        $('#linkPDF').text(name);
        var client = 'PFC';
        var myData = [name, client, serv, $("#inUploadPath").get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDFRenew').click(function () {

    if ($('#inUploadPath1').val() == '') {

        alertify.alert('Error', 'Please upload registration form!');

    } else {
        //$('#exampleModal').hide();
        //var usr = $('#usr').text();
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        serv = "Renewal";
        var name = $('#inUploadPath1').val().replace('C:\\fakepath\\', '');
        $('#linkPDFRenewal').text(name);
        var client = 'PFC';
        var myData = [name, client, serv, $('#inUploadPath1').get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDF2').click(function () {
    if ($('#inREOUploadPath').val() == '') {

        alertify.alert('Error', 'Please upload registration form!');

    } else {
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        //var usr = $('#usr').text();
        serv = "Property Registration";
        var name = $('#inREOUploadPath').val().replace('C:\\fakepath\\', '');
        $('#linkPDFREO').text(name);
        var client = 'REO';
        var myData = [name, client, serv, $('#inREOUploadPath').get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDFRenewREO').click(function () {

    if ($('#Reoupload1').val() == '') {

        alertify.alert('Error', 'Please upload registration form!');

    } else {
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        //var usr = $('#usr').text();
        serv = "Renewal";
        var name = $('#Reoupload1').val().replace('C:\\fakepath\\', '');
        $('#linkPDFRenewalREO').text(name);
        var client = 'REO';
        var myData = [name, client, serv, $('#Reoupload1').get(0)]

        pdfUpload(myData);
    }
})

$('#debtnSavePDF').click(function () {

    if ($('#inUploadPathD').val() == '') {

        alertify.alert('Error', 'Please upload registration form!');

    } else {
        //$('#exampleModal').hide();
        //var usr = $('#usr').text();
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        //  //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        serv = "Delist";
        var name = $('#inUploadPathD').val().replace('C:\\fakepath\\', '');
        $('#delinkPDF').text(name);
        var client = '';
        var myData = [name, client, serv, $("#inUploadPathD").get(0)]

        pdfUpload(myData);
    }
})

function pdfUpload(myData) {

    var client, regType = '';

    if (myData[1] == 'PFC') {
        client = 'P';
    } else if (myData[1] == 'REO') {
        client = 'R';
    } else if (myData[1] == 'RESI') {
        client = 'RE';
    }

    if (myData[2] == 'Property Registration') {
        regType = 'I';
    } else if (myData[2] == 'Delist') {
        regType = 'D';
    } else if (myData[2] == "Renewal") {
        regType = 'R';
    }

    var filename = myData[0];
    var fileUpload = myData[3];

    //if (myData[1] == 'PFC') {
    //    if (regType == "P") {
    //        fileUpload = $("#inUploadPath").get(0);
    //    } else if (regType == "R") {
    //        fileUpload = $("#inUploadPath1").get(0);
    //    }


    //} else if (myData[1] == 'REO') {
    //    if(regType == "")
    //    fileUpload = $("#inREOUploadPath").get(0);
    //}

    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filename, files[i]);
    }
    $.ajax({
        url: "FileUpload1.ashx",
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {

            alertify.alert('Success',result);
            $('#modalLoading').modal('hide');
        },
        error: function (result) {
            alertify.alert('Success', result);
            $('#modalLoading').modal('hide');
        }
    });
}

function openPDF(val) {
    $('#modalPDF').modal({ backdrop: 'static', keyboard: false });
    $('#lblPDF').text('PDF Name: ' + $(val).text());

    $('#pdf').replaceWith('<iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>');
    $('#pdf').attr('src', '../Files/MC Official PDF Files/' + $(val).text());

    $('#modalOrdinanceHist').modal('hide');
}
//UPLOAD PDF

$('#btntabREO').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataREO',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regREO = d.data.regREO;
                var look1 = d.data.check;

               
                if (regREO.length > 0) {
                    if (regREO[0].tag == 'CURRENTLY UNDER REVIEW' || regREO[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || regREO[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabREO div.tag').remove();
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforREO = regREO[0].id;
                        console.log(existingSettingforREO);

                        //console.log(regREO[0].id);

                        //BANKED-OWED
                        $('#slctReoBankOwed').val('' + regREO[0].reo_bank_owed + '');
                        $('#Drpreobanktime').val(regREO[0].reo_bank_owed_timeline1);
                        $('#inReoBankOwed1').val(regREO[0].reo_bank_owed_timeline2);
                        $('#slctReoBankOwed2').val(regREO[0].reo_bank_owed_timeline3);
                        $('#slctReoBankOwed3').val(regREO[0].reo_bank_owed_timeline4);
                        $('#slctReoBankOwed4').val(regREO[0].reo_bank_owed_timeline5);

                        //VACANT
                        $('#slctReoVacant').val('' + regREO[0].reo_vacant + '');
                        $('#slctReowith').val(regREO[0].reo_vacant_timeline1);
                        $('#inReoVacantTimeline1').val(regREO[0].reo_vacant_timeline2);
                        $('#slctReoVacantTimeline2').val(regREO[0].reo_vacant_timeline3);
                        $('#slctReoVacantTimeline3').val(regREO[0].reo_vacant_timeline4);
                        $('#slctReoVacantTimeline4').val(regREO[0].reo_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctReoCityNotice').val('' + regREO[0].reo_city_notice + '');
                        $('#slctReocitynoticewith').val(regREO[0].reo_city_notice_timeline1);
                        $('#slctReocitytimeline1').val(regREO[0].reo_city_notice_timeline2);
                        $('#slctReoBusinesstimeline2').val(regREO[0].reo_city_notice_timeline3);
                        $('#slctReodaystimeline3').val(regREO[0].reo_city_notice_timeline4);
                        $('#slctReovacancytimeline4').val(regREO[0].reo_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctReoCodeViolation').val('' + regREO[0].reo_code_violation + '');
                        $('#slctReoviolationwith').val(regREO[0].reo_code_violation_timeline1);
                        $('#slctReoViolationtimeline1').val(regREO[0].reo_code_violation_timeline2);
                        $('#slctReoviolationtimeline2').val(regREO[0].reo_code_violation_timeline3);
                        $('#slctReoviolationdaystimeline3').val(regREO[0].reo_code_violation_timeline4);
                        $('#slctReoviolationvacancytimeline4').val(regREO[0].reo_code_violation_timeline5);

                        //BOARDED ONLY
                        $('#slctReoBoardedOnly').val('' + regREO[0].reo_boarded_only + '');
                        $('#slctReoBoardedwith').val(regREO[0].reo_boarded_only_timeline1);
                        $('#slctReoBoardedtimeline1').val(regREO[0].reo_boarded_only_timeline2);
                        $('#slctReoBoardedtimeline2').val(regREO[0].reo_boarded_only_timeline3);
                        $('#slctReoBoardeddaystimeline3').val(regREO[0].reo_boarded_only_timeline4);
                        $('#slctReoBoardedtimeline4').val(regREO[0].reo_boarded_only_timeline5);

                        //DISTRESSED ABANDONED
                        $('#slctReoDistressedAbandoned').val('' + regREO[0].reo_distressed_abandoned + '');
                        $('#DrpReoAbandoned').val(regREO[0].reo_distressed_abandoned1);
                        $('#inReoDistressedAbandoned1').val(regREO[0].reo_distressed_abandoned2);
                        $('#slctReoDistressedAbandoned2').val(regREO[0].reo_distressed_abandoned3);
                        $('#slctReoDistressedAbandoned3').val(regREO[0].reo_distressed_abandoned4);
                        $('#slctReoDistressedAbandoned4').val(regREO[0].reo_distressed_abandoned5);

                        //RENTAL REGISTRATION
                        $('#slctRentalRegistration').val('' + regREO[0].rental_registration + '');
                        $('#slctRentalFormwithin').val(regREO[0].rental_registration_timeline1);
                        $('#slctReorentaltimeline1').val(regREO[0].rental_registration_timeline2);
                        $('#slctReorentaltimeline2').val(regREO[0].rental_registration_timeline3);
                        $('#slctReorentaltimeline3').val(regREO[0].rental_registration_timeline4);
                        $('#slctReorentaltimeline4').val(regREO[0].rental_registration_timeline5);

                        //OTHERS
                        $('#slctReoOther').val(regREO[0].reo_other);
                        $('#slctreootherwithin').val(regREO[0].reo_other_timeline1);
                        $('#inReoOtherTimeline1').val(regREO[0].reo_other_timeline2);
                        $('#slctReoOtherTimeline2').val(regREO[0].reo_other_timeline3);
                        $('#slctReoOtherTimeline3').val(regREO[0].reo_other_timeline4);
                        $('#slctReoOtherTimeline4').val(regREO[0].reo_other_timeline5);

                        $('#slctREOPaymentType').val(regREO[0].payment_type);
                        $('#slctREOTypeOfRegistration').val(regREO[0].type_of_registration);
                        $('#slctREOVmsRenewal').val('' + regREO[0].vms_renewal + '');

                        $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                        $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                        $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                        $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);


                        if (regREO[0].vms_renewal == 'false' || regREO[0].vms_renewal == '' || $('#slctREOVmsRenewal').val() == '') {
                            $('.renewREOYes').hide();
                            $('#btnSavePDFRenewREO').hide();
                        } else {
                            $('.renewREOYes').show();
                            $('#btnSavePDFRenewREO').show();
                            if (regREO[0].renew_every == 'false') {

                                $('#slctREORenewOnMonths').prop('disabled', false);
                                $('#slctREORenewOnNum').prop('disabled', false);

                                $('#Reordon').prop('checked', true);
                                $('#Reordvery').prop('checked', false);

                                $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                                $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                                $('#slctREORenewEveryNum').val('');
                                $('#slctREORenewEveryYears').val('');

                                $('#slctREORenewEveryNum').prop('disabled', true);
                                $('#slctREORenewEveryYears').prop('disabled', true);

                            } else {
                                $('#slctREORenewEveryNum').prop('disabled', false);
                                $('#slctREORenewEveryYears').prop('disabled', false);

                                $('#Reordvery').prop('checked', true);
                                $('#Reordon').prop('checked', false);

                                $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                                $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);
                                $('#slctREORenewOnMonths').val('');
                                $('#slctREORenewOnNum').val('');

                                $('#slctREORenewOnMonths').prop('disabled', true);
                                $('#slctREORenewOnNum').prop('disabled', true);
                            }
                        }

                        if ($('#slctREOTypeOfRegistration').val() == 'PDF') {
                            var fn = regREO[0].upload_path;

                            $('#slctREOTypeOfRegistration').closest('div').next().show();

                            $('#linkPDFREO').text(fn);
                            $('#btnSavePDF2').show();

                            if ($('#slctREOVmsRenewal').val() == 'true' || regREO.vms_renewal == 'true') {
                                var fname = regREO[0].reo_renew_uploadpath;
                                $('#linkPDFRenewalREO').text(fname);
                                $('.renewREOYes').show();
                                $('.renewUploadREO').show();
                                $('#btnSavePDFRenewREO').show();

                            } else if ($('#slctREOVmsRenewal').val() == 'false' || regREO.vms_renewal == 'false') {
                                $('.renewREOYes').hide();
                                $('.renewUploadREO').hide();
                                $('#btnSavePDFRenewREO').hide();
                            }
                            else {
                                $('.renewREOYes').hide();
                                $('.renewUploadREO').hide();
                                $('#btnSavePDFRenewREO').hide();
                            }
                        } else {
                            $('#slctREOTypeOfRegistration').closest('div').next().hide();
                            $('.renewREOYes').hide();
                            $('.renewUploadREO').hide();
                            $('#btnSavePDFRenewREO').hide();
                            $('#btnSavePDF2').hide();

                            if ($('#slctREOVmsRenewal').val() == 'true' || regREO.vms_renewal == 'true') {
                                $('.renewREOYes').show();

                            } else if ($('#slctREOVmsRenewal').val() == 'false' || regREO.vms_renewal == 'false') {
                                $('.renewREOYes').hide();
                            }
                            else {
                                $('.renewREOYes').hide();
                            }
                        }

                        if (regREO[0].no_reo_reg == true) {
                            $('#reoReg').prop('checked', true);
                            $('#tabREO select').prop('disabled', true);
                            $('#tabREO input[type=number]').prop('disabled', true);
                            $('#tabREO input[type=text]').prop('disabled', true);
                            $('#tabREO select').val('');
                            $('#tabREO input[type=number]').val('');
                            $('#tabREO input[type=text]').val('');
                        } else {
                            $('#reoReg').prop('checked', false);
                            $('#tabREO select').prop('disabled', false);
                            $('#tabREO input[type=number]').prop('disabled', false);
                            $('#tabREO input[type=text]').prop('disabled', false);
                        }

                        if (regREO[0].statewide_reg == true) {
                            $('#reoStatewideReg').prop('checked', true);
                        } else {
                            $('#reoStatewideReg').prop('checked', false);
                        }

                        if (regREO[0].tag == 'PENDING FOR APPROVAL') {
                            $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                            $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();

                            $('#tabREO div.tag').remove();
                            $('#tabREO').prepend(
                                '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                    '<label class="text-green">' + regREO[0].tag + '!</label>' +
                                '</div>'
                            );

                            $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);

                        }
                        //else {
                        //    //$('#tabPFC div.tag').remove();
                        //    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                        //    $('#tabREO div.tag').remove();
                        //    //$('#tabPFC').prepend(
                        //    //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //    //        '<label class="text-green">CURRENTLY ' + regPFC[0].tag + '!</label>' +
                        //    //    '</div>'
                        //    //);
                        //    $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        //    $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                        //}
                    }
                    //else {
                    //    //$('#tabREO div.tag').remove();
                    //    //$('#tabREO').prepend(
                    //    //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                    //    //        '<label class="text-red">Pending ' + regREO[0].tag + '!</label>' +
                    //    //    '</div>'
                    //    //);
                    //    //$('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    //    $('#tabREO div.tag').remove();
                    //    $('#tabREO').prepend(
                    //        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                    //            '<label class="text-red">PENDING ' + regREO[0].tag + '!</label>' +
                    //        '</div>'
                    //    );
                    //    //$('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    //    $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    //    $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //}

                    if ($('#Reordvery').is(':checked')) {
                        $('#slctREORenewEveryNum').prop('disabled', false);
                        $('#slctREORenewEveryYears').prop('disabled', false);
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);

                    } else if ($('#Reordon').is(':checked')) {
                        $('#slctREORenewOnMonths').prop('disabled', false);
                        $('#slctREORenewOnNum').prop('disabled', false);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    }

                    $('#tabREO').find('div.audit input').prop('disabled', true);
                    $('#tabREO').find('div.audit select').prop('disabled', true);
                    $('#tabREO').find('div.audit button').prop('disabled', true);

                    $('#modalLoading').modal('hide');


                } else {
                    //$('#tabREO div.tag').remove();
                    //$('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                    $('#tabREO div.valid').remove();
                    $('#tabREO').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    //$('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    $('#modalLoading').modal('hide');
                }

                if (look1 != undefined) {
                    if (look1[0].reoSetting == '2') {
                        //$('#tabPFC div.tag').remove();
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabREO div.tag').remove();
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //        '<label class="text-green">CURRENTLY ' + regPFC[0].tag + '!</label>' +
                        //    '</div>'
                        //);
                    //    $('#tabREO').prepend(
                    //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                    //        '<label class="text-green">' + regREO[0].tag + '!</label>' +
                    //    '</div>'
                    //);
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabREO').prepend(
                                '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                    '<label class="text-green">' + regREO[0].tag + '!</label>' +
                                '</div>'
                            );

                    }
                }
                if (regREO[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabREO div.tag').remove();
                    $('#tabREO').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + regREO[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }
                CheckApplyButton();
                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#btntabProperty').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataPropType',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regPropType = d.data.regPropType;
                var look2 = d.data.check;

                CheckApplyButton();
                if (regPropType.length > 0) {
                    if (regPropType[0].tag == 'CURRENTLY UNDER REVIEW' || regPropType[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || regPropType[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingPropType = regPropType[0].id;
                        console.log(existingSettingPropType);
                        //console.log([regPropType[0].id]);

                        $('#slctPropResidential').val('' + regPropType[0].residential + '');
                        $('#slctSingleFamily').val('' + regPropType[0].single_family + '');
                        $('#slctMultiFamily').val('' + regPropType[0].multi_family + '');
                        $('#slct2units').val('' + regPropType[0].unit2 + '');
                        $('#slct3units').val('' + regPropType[0].unit3 + '');
                        $('#slct4units').val('' + regPropType[0].unit4 + '');
                        $('#slct5units').val('' + regPropType[0].unit5 + '');
                        $('#slctPropRental').val('' + regPropType[0].rental + '');
                        $('#slctPropCommercial').val('' + regPropType[0].commercial + '');
                        $('#slctPropCondo').val('' + regPropType[0].condo + '');
                        $('#slctPropTownhome').val('' + regPropType[0].townhome + '');
                        $('#slctPropVacantLot').val('' + regPropType[0].vacant_lot + '');
                        $('#slctPropMobilehome').val('' + regPropType[0].mobile_home + '');
                    }
                    $('#tabPropType').find('div.audit input').prop('disabled', true);

                    $('#tabPropType').find('div.audit select').prop('disabled', true);
                    $('#modalLoading').modal('hide');
                } else {
                    //$('#tabPropType div.tag').remove();
                    //$('#tabPropType div:last button:eq(0)').prop('disabled', false);
                    $('#tabPropType div.valid').remove();
                    $('#tabPropType').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    //$('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }


                if (look2 != undefined) {
                    if (look2[0].propTypeSetting == '2') {
                        //$('#tabPFC div.tag').remove();
                        $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropType div.tag').remove();
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //        '<label class="text-green">CURRENTLY ' + regPFC[0].tag + '!</label>' +
                        //    '</div>'
                        //);
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + regPropType[0].tag + '!</label>' +
                            '</div>'
                        );


                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    

                }
                if (regPropType[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPropType div.tag').remove();
                    $('#tabPropType').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + regPropType[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }
                


                $('#modalLoading').modal('hide');
            }
        }
    });

});

function defaultMuni() {
    $('#inMuniDept').val('');
    $('#inMuniPhone').val('');
    $('#inMuniEmail').val('');
    $('#inMuniMailStr').val('');
    $('#inWebsite').val('');
    $('#inTitle').val('');
    $('#inContact').val('');
    $('#inDept').val('');
    $('#inPhone').val('');
    $('#inEmail').val('');
    $('#inMailing').val('');
    $('#inHrsFrom').val('');
    $('#inHrsTo').val('');
    $('#slctMuniMailCt').empty();
    $('#slctMuniMailZip').empty();
}

var cityVal, stateVal, zipVal;
$('#idMunicipality').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    defaultMuni();
    getMuniState();

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataMunicipality',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var municipal = d.data.municipal;

                var municipalLook = d.data.check;
                
                if (municipal.length > 0) {
                    if (municipal[0].tag == 'CURRENTLY UNDER REVIEW' || municipal[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || municipal[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabMunicipality').find('div input').prop('disabled', false);
                        $('#tabMunicipality').find('div select').prop('disabled', false);
                        $('#tabMunicipality').find('div button').prop('disabled', false);


                        existingSettingforMunicipality = municipal[0].id;
                        console.log(existingSettingforMunicipality);

                        $('#inMuniDept').val(municipal[0].municipality_department);
                        $('#inMuniPhone').val(municipal[0].municipality_phone_num);
                        $('#inMuniEmail').val(municipal[0].municipality_email);
                        $('#inMuniMailStr').val(municipal[0].municipality_st);
                        $('#inWebsite').val(municipal[0].municipality_website);

                        cityVal = municipal[0].municipality_city;
                        stateVal = municipal[0].municipality_state;
                        zipVal = municipal[0].municipality_zip;



                        //$('#slctMuniMailSta').val(municipal[0].municipality_state);


                        getMuniCitySet(municipal[0].municipality_state, municipal[0].municipality_city);
                        getMuniZipSet(municipal[0].municipality_state, municipal[0].municipality_city, municipal[0].municipality_zip);

                        //sessionStorage.setItem('stateVal', municipal[0].municipality_state);

                        if (stateVal != '' && stateVal != undefined && stateVal != null) {
                            $('#slctMuniMailSta').val(municipal[0].municipality_state);
                        }

                        if (cityVal != '' && cityVal != undefined && cityVal != null) {
                            $('#slctMuniMailCt').val(municipal[0].municipality_city)
                        }

                        if (zipVal != '' && zipVal != undefined && zipVal != null) {
                            $('#slctMuniMailZip').val(municipal[0].municipality_zip);
                        }

                        $('#inContact').val(municipal[0].contact_person);
                        $('#inTitle').val(municipal[0].title);
                        $('#inDept').val(municipal[0].department);
                        $('#inPhone').val(municipal[0].phone_num);
                        $('#inEmail').val(municipal[0].email);
                        $('#inMailing').val(municipal[0].address);
                        $('#inHrsFrom').val(municipal[0].ops_hrs_from);
                        $('#inHrsTo').val(municipal[0].ops_hrs_to);

                        $('#modalLoading').modal('hide');

                    }

                    

                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabMunicipality div.valid').remove();
                    $('#tabMunicipality').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                }

                if (municipalLook != undefined) {
                    if (municipalLook[0].municipalitySetting == '2') {
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabMunicipality div.tag').remove();

                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + municipal[0].tag + '!</label>' +
                            '</div>'
                        );

                    }
                   
                }
                if (municipal[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabMunicipality div.tag').remove();
                    $('#tabMunicipality').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + municipal[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }
                
                CheckApplyButton();

                $('#tabMunicipality').find('div.audit input').prop('disabled', true);
                $('#tabMunicipality').find('div.audit select').prop('disabled', true);
                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#btntabRequirements').click(function () {
    $('#lciState').empty();
    $('#lciCity').empty();
    $('#lciZip').empty();
    getLCIState();

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataReq',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var regPropReq = d.data.regPropReq;
                var lookrePropeReq = d.data.check;
                
                if (regPropReq.length > 0) {
                    if (regPropReq[0].tag == 'CURRENTLY UNDER REVIEW' || regPropReq[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || regPropReq[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforReq = regPropReq[0].id;
                        console.log(existingSettingforReq);

                        //var add_info = '';
                        //if (regPropReq[0].addn_info != '') {
                        //    var split = regPropReq[0].addn_info.split(',');
                        //    $('#slctAddnInfo').multiselect('select', split);
                        //}

                        $('#slctPresaleDefinition').val('' + regPropReq[0].presale_definition + '');
                        $('#slctLocalContactRequired').val('' + regPropReq[0].local_contact_required + '');
                        $('#slctGseExclusion').val('' + regPropReq[0].gse_exclusion + '');
                        $('#slctInsuranceReq').val('' + regPropReq[0].insurance_required + '');
                        $('#slctForeclosureActInfo').val('' + regPropReq[0].foreclosure_action_info_needed + '');
                        $('#slctForeclosureCaseInfo').val('' + regPropReq[0].foreclosure_case_info_needed + '');
                        $('#slctForeclosureDeedReq').val('' + regPropReq[0].foreclosure_deed_required + '');
                        $('#slctBondReq').val('' + regPropReq[0].bond_required + '');
                        $('#slctUtilityInfoReq').val('' + regPropReq[0].utility_information_required + '');
                        $('#slctWinterReq').val('' + regPropReq[0].winterization_required + '');
                        $('#slctSignReq').val('' + regPropReq[0].signature_required + '');
                        $('#slctNotarizationReq').val('' + regPropReq[0].notarization_required + '');
                        $('#slctRecInsDate').val('' + regPropReq[0].recent_inspection_Date + '');
                        $('#slctFirstTimeVacancyDate').val('' + regPropReq[0].first_time_vacancy_date + '');
                        $('#slctSecuredRequired').val('' + regPropReq[0].secured_required + '');
                        $('#slctAddSignReq').val('' + regPropReq[0].additional_signage_required + '');
                        $('#slctPicReq').val('' + regPropReq[0].pictures_required + '');
                        $('#slctMobileVINReq').val('' + regPropReq[0].mobile_vin_number_required + '');
                        $('#slctParcelReq').val('' + regPropReq[0].parcel_number_required + '');
                        $('#slctLegalDescReq').val('' + regPropReq[0].legal_description_required + '');
                        $('#slctBlockLotReq').val('' + regPropReq[0].block_lot_number_required + '');
                        $('#slctAttyInfoReq').val('' + regPropReq[0].attorney_information_required + '');
                        $('#slctBrkInfoReq').val('' + regPropReq[0].broker_information_required_reo + '');
                        $('#slctMortContactNameReq').val('' + regPropReq[0].mortgage_contact_name_required + '');
                        $('#slctClientTaxReq').val('' + regPropReq[0].client_tax_number_required + '');

                        $('#slctOneTime').val('' + regPropReq[0].one_time_only + '');

                        btnslctBondReq();

                        if (regPropReq[0].bond_schedule_amount != undefined || regPropReq[0].bond_schedule_amount != null) {
                            $('#chkPayAgainWhenREO').prop('checked', true);
                        }
                        else {
                            $('#chkPayUponRenewal').prop('checked', true);
                        }
                        btnslctOneTime();
                        btnchkPayAgainWhenREO();



                        //$('#slctOneTime').val();
                        //$('#inBondAmount').val();
                        //$('#slctBondAmountUSP').val();


                        var bond_schedule_amount = regPropReq[0].bond_schedule_amount.split(',');
                        var bondctr = 1;
                        for (ii = 0; ii <= bond_schedule_amount.length - 1; ii++) {
                            if (bond_schedule_amount.length > 0 && bond_schedule_amount != "") {
                                $('#txtBond1').val('' + bond_schedule_amount[0] + '');

                                if (ii >= 1) {
                                    data = '<div class="col-xs-12 " id="dvBond' + bondctr + '" style="padding-top:5px">'
                                    data += '<div class="col-xs-1">' + CountSt(bondctr) + '</div>'
                                    data += '<div class="col-xs-3">'
                                    data += '<input id="txtBond' + bondctr + '" type="text" value="' + bond_schedule_amount[ii] + '" class="form-control input-sm" />'
                                    data += '</div>'
                                    data += '<div class="col-xs-3">'
                                    data += '<select id="slcBond' + bondctr + '" class="form-control input-sm">'
                                    data += '<option value="" selected="selected">USD</option>'
                                    data += '</select>'
                                    data += '</div>'
                                    data += '</div>';
                                    $("#dvappendBond").append(data);
                                }

                            } else {
                                //$('#divEscalPFCCost1').hide();
                                $('#txtBond1').val('');
                                for (iii = 2; iii <= 100; iii++) {
                                    $('#dvBond' + iii).remove();
                                }
                            }

                            bondctr++;

                        }



                        $('#lciCompany').val(regPropReq[0].lcicompany_name);
                        $('#lciFirstName').val(regPropReq[0].lcifirst_name);
                        $('#lciLastName').val(regPropReq[0].lcilast_name);
                        $('#lciTitle').val(regPropReq[0].lcititle);
                        $('#lciBusinessLicenseNum').val(regPropReq[0].lcibusiness_license_num);
                        $('#lciPhoneNum1').val(regPropReq[0].lciphone_num1);
                        $('#lciPhoneNum2').val(regPropReq[0].lciphone_num2);
                        $('#lciBusinessPhoneNum1').val(regPropReq[0].lcibusiness_phone_num1);
                        $('#lciBusinessPhoneNum2').val(regPropReq[0].lcibusiness_phone_num2);
                        $('#lciEmrPhone1').val(regPropReq[0].lciemergency_phone_num1);
                        $('#lciEmrPhone2').val(regPropReq[0].lciemergency_phone_num2);
                        $('#lciFaxNum1').val(regPropReq[0].lcifax_num1);
                        $('#lciFaxNum2').val(regPropReq[0].lcifax_num2);
                        $('#lciCellNum1').val(regPropReq[0].lcicell_num1);
                        $('#lciCellNum2').val(regPropReq[0].lcicell_num2);
                        $('#lciEmail').val(regPropReq[0].lciemail);
                        $('#lciStreet').val(regPropReq[0].lcistreet);

                        $('#lciState :selected').text(regPropReq[0].lcistate);

                        getLCICitySet(regPropReq[0].lcistate, regPropReq[0].lcicity);
                        getLCIZipSet(regPropReq[0].lcistate, regPropReq[0].lcicity, regPropReq[0].lcizip);

                        sessionStorage.setItem('LCstateVal', regPropReq[0].lcistate);

                        $('#lciHrsFrom').val(regPropReq[0].lcihours_from);
                        $('#lciHrsTo').val(regPropReq[0].lcihours_to);

                    }

                    

                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabPropReq div.valid').remove();
                    $('#tabPropReq').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    $('#modalLoading').modal('hide');
                }

                if (lookrePropeReq != undefined) {
                    if (lookrePropeReq[0].requirementSetting == '2') {
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + regPropReq[0].tag + '!</label>' +
                            '</div>'
                        );
                    }
                    

                }
                if (regPropReq[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPropReq div.tag').remove();
                    $('#tabPropReq').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + regPropReq[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                CheckApplyButton();

               
                $('#tabPropReq').find('div.auditReq input').prop('disabled', true);
                $('#tabPropReq').find('div.auditReq select').prop('disabled', true);
                $('#tabPropReq').find('div.auditReq button').prop('disabled', true);

                $('#modalLoading').modal('hide');
            }
        }
    });

});

$('#idInspection').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataInspection',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var inspect = d.data.inspect;
                var lookInspect = d.data.check;
                
                if (inspect.length > 0) {
                    if (inspect[0].tag == 'CURRENTLY UNDER REVIEW' || inspect[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || inspect[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabInspection').find('div input').prop('disabled', false);
                        $('#tabInspection').find('div select').prop('disabled', false);
                        $('#tabInspection').find('div button').prop('disabled', false);

                        if (inspect[0].tag == 'PENDING FOR REVIEW') {
                            $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + inspect[0].tag + '!</label>' +
                            '</div>'
                        );
                        }

                        existingSettingforInspection = inspect[0].id;
                        console.log(existingSettingforInspection);

                        $('#slctMuniInsReq').val('' + inspect[0].municipal_inspection_fee_required + '');
                        $('#slctOneTimeUponReg').val('' + inspect[0].onetime_only_upon_reg + '');
                        $('#slctFeePaymentFreq').val(inspect[0].fee_payment_frequency);
                        $('#inPayon1').val('' + inspect[0].payon1 + '');
                        $('#inPayon2').val('' + inspect[0].payon2 + '');
                        $('#inPayon3').val('' + inspect[0].payon3 + '');
                        $('#inPayon4').val('' + inspect[0].payon4 + '');
                        $('#slctFreeEscalating').val('' + inspect[0].free_escalating + '');
                        $('#inMunicipalInsFee').val(inspect[0].municipal_inspection_fee);
                        //$('#inMunicpalInsFeeCurr').val(inspect[0].municipal_inspection_fee_curr);


                        var escalating_amount = "";

                        var escalating_succeeding = "";


                        if (inspect[0].escalating_amount != '' || inspect[0].escalating_amount != null) {
                            escalating_amount = inspect[0].escalating_amount.split(',');
                        }

                        if (inspect[0].escalating_succeeding != '' || inspect[0].escalating_succeeding != null) {
                            escalating_succeeding = inspect[0].escalating_succeeding.split(',');
                        }

                        var ctrInspection = 1;
                        for (ii = 0; ii <= escalating_amount.length - 1; ii++) {
                            if (escalating_amount.length > 0) {
                                $('#inputIns1').val('' + escalating_amount[0] + '');
                                if (ii >= 1) {
                                    $('.InsescalInput' + ctrInspection + '').show();
                                    $('#inputIns' + ctrInspection).val('' + escalating_amount[ii] + '');
                                    escalating_succeeding[ii] == 'true' ? $('#inspectCheck' + ctrInspection + '').prop('checked', true) : $('#inspectCheck' + ctrInspection + '').prop('checked', false);
                                }
                                if ($('#inputIns' + ctrInspection + '').val() == undefined || $('#inputIns' + ctrInspection + '').val() == '') {
                                    $('.PFChideCheck' + ctrInspection + '').hide();
                                }
                                ctrInspection++;
                            } else {
                                $('#inputIns1').val('');
                                for (iii = 2; iii <= 5; iii++) {
                                    $('#inspectCheck' + ctrInspection + '').prop('checked', false);
                                }
                            }
                        }


                        $('#slctInsUpReq').val('' + inspect[0].inspection_report_required + '');
                        $('#slctInsCriOccc').val(inspect[0].inspection_frequency_occupied);
                        $('#slctInsCriVac').val(inspect[0].inspection_frequency_vacant);
                        $('#slctInsFeePay').val('' + inspect[0].how_to_send_inspection_report + '');

                        inspectionValue();


                        $('#tabInspection').find('div.audit input').prop('disabled', true);
                        $('#tabInspection').find('div.audit select').prop('disabled', true);
                        $('#tabInspection').find('div.audit button').prop('disabled', true);
                        $('#modalLoading').modal('hide');

                    } else {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">PENDING ' + inspect[0].tag + '!</label>' +
                            '</div>'
                        );
                        //$('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();

                        $('#modalLoading').modal('hide');
                    }
                } else {
                    $('#tabInspection div.valid').remove();
                    $('#tabInspection').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                if (lookInspect != undefined) {
                    if (lookInspect[0].inspectionSetting == '2') {
                        $('#tabPFC div.tag').remove();
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabInspection div.tag').remove();
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + inspect[0].tag + '!</label>' +
                            '</div>'
                        );
                    }
                }
                if (inspect[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabInspection div.tag').remove();
                    $('#tabInspection').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + inspect[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }

                CheckApplyButton();

                $('#tabInspection').find('div.audit input').prop('disabled', true);
                $('#tabInspection').find('div.audit select').prop('disabled', true);
                $('#tabInspection').find('div.audit button').prop('disabled', true);

                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#ContReg').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataConReg',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regCont = d.data.regCont;
                var lookregCont = d.data.check;
                
                //$('#slctContReg').val(regCont[0].cont_reg);
                //$('#slctUpdateReg').val(regCont[0].update_reg);

                if (regCont.length > 0) {
                    if (regCont[0].tag == 'CURRENTLY UNDER REVIEW' || regCont[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || regCont[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforContReg = regCont[0].id;
                        console.log(existingSettingforContReg);
                        //console.log(regCont[0].id);

                        $('#slctContReg').val(regCont[0].cont_reg);
                        $('#slctUpdateReg').val(regCont[0].update_reg);
                    }


                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabContReg div.valid').remove();
                    $('#tabContReg').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                if (lookregCont != undefined) {
                    if (lookregCont[0].contRegSetting == '2') {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg').prepend(
                                    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                        '<label class="text-green">' + regCont[0].tag + '!</label>' +
                                    '</div>'
                                );
                    }
                }
                if (regCont[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabContReg div.tag').remove();
                    $('#tabContReg').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + regCont[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }
                
                CheckApplyButton();
                $('#tabContReg').find('div.audit input').prop('disabled', true);
                $('#tabContReg').find('div.audit select').prop('disabled', true);

                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#idDeregistration').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetDataDeregistration',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var dereg = d.data.dereg;
                var deregLook = d.data.check;

                if (dereg.length > 0) {
                    if (dereg[0].tag == 'CURRENTLY UNDER REVIEW' || dereg[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || dereg[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);

                        //console.log(dereg[0].id);
                        existingSettingforDeregistration = dereg[0].id;
                        console.log(existingSettingforDeregistration);

                        $('#slctDeregRequired').val('' + dereg[0].dereg_req + '');
                        $('#slctConveyed').val('' + dereg[0].conveyed + '');
                        $('#slctOccupied').val('' + dereg[0].occupied + '');
                        $('#slctHowToDereg').val(dereg[0].how_to_dereg);
                        //$('#inUploadPathD').val(dereg[0].upload_file);
                        $('#slctNewOwnerInfoReq').val('' + dereg[0].new_owner_info_req + '');
                        $('#slctProofOfConveyReq').val('' + dereg[0].proof_of_conveyance_req + '');
                        $('#slctDateOfSaleReq').val('' + dereg[0].date_of_sale_req + '');


                        if (dereg[0].how_to_dereg == 'PDF') {
                            var fn = dereg[0].upload_file;
                            $('#delinkPDF').text(fn);
                            $('#slctHowToDereg').closest('div').next().show();
                            $('#debtnSavePDF').show();
                        } else {
                            $('#slctHowToDereg').closest('div').next().hide();
                            $('#debtnSavePDF').hide();
                        }
                        $('#modalLoading').modal('hide');
                    }
                } else {
                    $('#tabDeregistration div.valid').remove();

                    $('#tabDeregistration').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    //$('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();

                    $('#modalLoading').modal('hide');
                }

                if (deregLook != undefined) {
                    if (deregLook[0].deregistrationSetting == '2') {
                        //$('#tabPFC div.tag').remove();
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabDeregistration div.tag').remove();
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //        '<label class="text-green">CURRENTLY ' + regPFC[0].tag + '!</label>' +
                        //    '</div>'
                        //);
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + dereg[0].tag + '!</label>' +
                            '</div>'
                        );
                    }
                }
                if (dereg[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabDeregistration div.tag').remove();
                    $('#tabDeregistration').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + dereg[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }
                $('#tabDeregistration').find('div.audit input').prop('disabled', true);
                $('#tabDeregistration').find('div.audit select').prop('disabled', true);
                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#idOrdinance').click(function () {
    //getOrdinance('');
    GetOrdi();
});

function CheckApplyButton() {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/checkApply',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (data.d == '1')  {

                    $('.btnApply').prop('disabled', false);

            }
            else {
                    $('.btnApply').prop('disabled', true);
            }

            if (sessionStorage.getItem('finalReview') == 'true') {
                $('.btnApply').prop('disabled', true)
            }
            
            //$('#modalLoading').modal('hide');
        }, error: function (response) {
            alertify.error('Error', 'Error Saving');
            console.log(response.responseText);
            //$('#modalLoading').modal('hide');
        }
    });
}

$('.btnApply').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var saveName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();

    var user = $('#usr').text();

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/isApply',
        data: '{state: "' + state + '", city: "' + city + '", name: "' + saveName + '", user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                alertify.alert('Success', 'The settings for ' + saveName + ' have been sent to the approver for approval.');

                
                $('#tabPFC, #tabREO, #tabPropType, #tabCost, #tabContReg, #tabPropReq,' +
                '#tabInspection, #tabMunicipality, #tabDeregistration').prepend(
                '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                    '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                                '</div>'
                );
               

            } else {
                alertify.error('Error', 'There is no saved data in Ordinance Settings');
            }

            $('#modalLoading').modal('hide');

        }, error: function (response) {
            alertify.error('Error', 'Error in database');
            console.log(response.responseText);
            $('#modalLoading').modal('hide');
        }
    });
});


function getDataPFC(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetData',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
           

            if (d.Success) {

                // PFC
                var regPFC = d.data.regPFC;
                var look = d.data.check;

                if (regPFC.length > 0) {
                    if (regPFC[0].tag == 'CURRENTLY UNDER REVIEW' || regPFC[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || regPFC[0].tag == 'PENDING FOR APPROVAL') {

                        console.log(regPFC[0].id);
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforPFC = regPFC[0].id;
                        console.log(existingSettingforPFC);

                        //PFC DEFAULT
                        $('#slctPfcDefault').val('' + regPFC[0].pfc_default + '');
                        $('#inDefRegTimeline1').val(regPFC[0].def_reg_timeline1);
                        $('#slctDefRegTimeline2').val(regPFC[0].def_reg_timeline2);
                        $('#slctDefRegTimeline3').val(regPFC[0].def_reg_timeline3);
                        $('#slctDefRegTimeline4').val(regPFC[0].def_reg_timeline4);
                        $('#slctDefRegTimeline5').val(regPFC[0].def_reg_timeline5);

                        //PFC VACANT
                        $('#slctPfcVacantTimeline').val('' + regPFC[0].pfc_vacant + '');
                        $('#inPfcVacantTimeline1').val('' + regPFC[0].pfc_vacant_timeline1);
                        $('#slctPfcVacantTimeline2').val('' + regPFC[0].pfc_vacant_timeline2);
                        $('#slctPfcVacantTimeline3').val('' + regPFC[0].pfc_vacant_timeline3);
                        $('#slctPfcVacantTimeline4').val('' + regPFC[0].pfc_vacant_timeline4);
                        $('#slctPfcVacantTimeline5').val('' + regPFC[0].pfc_vacant_timeline5);

                        //PFC FORECLOSURE
                        $('#slctPfcForeclosure').val('' + regPFC[0].pfc_foreclosure + '');
                        $('#inPfcDefForclosureTimeline1').val(regPFC[0].pfc_def_foreclosure_timeline1);
                        $('#slctPfcDefForclosureTimeline2').val(regPFC[0].pfc_def_foreclosure_timeline2);
                        $('#slctPfcDefForclosureTimeline3').val(regPFC[0].pfc_def_foreclosure_timeline3);
                        $('#slctPfcDefForclosureTimeline4').val(regPFC[0].pfc_def_foreclosure_timeline4);
                        $('#slctPfcDefForclosureTimeline5').val(regPFC[0].pfc_def_foreclosure_timeline5);

                        //FORECLOSURE AND VACANT
                        $('#slctPfcForeclosureVacant').val('' + regPFC[0].pfc_foreclosure_vacant + '');
                        $('#inPfcForeclosureVacantTimeline1').val(regPFC[0].pfc_foreclosure_vacant_timeline1);
                        $('#slctPfcForeclosureVacantTimeline2').val(regPFC[0].pfc_foreclosure_vacant_timeline2);
                        $('#slctPfcForeclosureVacantTimeline3').val(regPFC[0].pfc_foreclosure_vacant_timeline3);
                        $('#slctPfcForeclosureVacantTimeline4').val(regPFC[0].pfc_foreclosure_vacant_timeline4);
                        $('#slctPfcForeclosureVacantTimeline5').val(regPFC[0].pfc_foreclosure_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctPfcCityNotice').val('' + regPFC[0].pfc_city_notice + '');
                        $('#inPfcCityNoticeTimeline1').val(regPFC[0].pfc_city_notice_timeline1);
                        $('#slctPfcCityNoticeTimeline2').val(regPFC[0].pfc_city_notice_timeline2);
                        $('#slctPfcCityNoticeTimeline3').val(regPFC[0].pfc_city_notice_timeline3);
                        $('#slctPfcCityNoticeTimeline4').val(regPFC[0].pfc_city_notice_timeline4);
                        $('#slctPfcCityNoticeTimeline5').val(regPFC[0].pfc_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctPfcCodeViolation').val('' + regPFC[0].pfc_code_violation + '');
                        $('#inPfcCodeViolationTimeline1').val(regPFC[0].pfc_code_violation_timeline1);
                        $('#slctPfcCodeViolationTimeline2').val(regPFC[0].pfc_code_violation_timeline2);
                        $('#slctPfcCodeViolationTimeline3').val(regPFC[0].pfc_code_violation_timeline3);
                        $('#slctPfcCodeViolationTimeline4').val(regPFC[0].pfc_code_violation_timeline4);
                        $('#slctPfcCodeViolationTimeline5').val(regPFC[0].pfc_code_violation_timeline5);

                        //BOARDED
                        $('#slctPfcBoarded').val('' + regPFC[0].pfc_boarded + '');
                        $('#inPfcBoardedTimeline1').val(regPFC[0].pfc_boarded_timeline1);
                        $('#slctPfcBoardedTimeline2').val(regPFC[0].pfc_boarded_timeline2);
                        $('#slctPfcBoardedTimeline3').val(regPFC[0].pfc_boarded_timeline3);
                        $('#slctPfcBoardedTimeline4').val(regPFC[0].pfc_boarded_timeline4);
                        $('#slctPfcBoardedTimeline5').val(regPFC[0].pfc_boarded_timeline5);

                        //OTHERS
                        $('#slctPfcOther').val('' + regPFC[0].pfc_other + '');
                        $('#inPfcOtherTimeline1').val(regPFC[0].pfc_other_timeline1);
                        $('#slctPfcOtherTimeline2').val(regPFC[0].pfc_other_timeline2);
                        $('#slctPfcOtherTimeline3').val(regPFC[0].pfc_other_timeline3);
                        $('#slctPfcOtherTimeline4').val(regPFC[0].pfc_other_timeline4);
                        $('#slctPfcOtherTimeline5').val(regPFC[0].pfc_other_timeline5);

                        

                        //$('#slctSpcReq').val(regPFC[0].special_requirements);
                        $('#slctPaymentType').val(regPFC[0].payment_type);
                        $('#slctTypeOfRegistration').val(regPFC[0].type_of_registration);
                        $('#slctVmsRenewal').val('' + regPFC[0].vms_renewal + '');

                        

                        if (regPFC[0].vms_renewal == 'false' || regPFC[0].vms_renewal == '' || $('#slctVmsRenewal').val() == '') {

                            $('.renewYes').hide();
                            $('#btnSavePDFRenew').hide();

                        } else {

                            $('.renewYes').show();
                            $('#btnSavePDFRenew').hide();
                            if (regPFC[0].renew_every == 'false') {

                                $('#slctRenewOnMonths').prop('disabled', false);
                                $('#slctRenewOnNum').prop('disabled', false);

                                $('#RDON').prop('checked', true);
                                $('#RDEVERY').prop('checked', false);


                                $('#slctRenewOnMonths').val(regPFC[0].renew_on_months);
                                $('#slctRenewOnNum').val(regPFC[0].renew_on_num);
                                $('#slctRenewEveryNum').val('');
                                $('#slctRenewEveryYears').val('');

                                $('#slctRenewEveryNum').prop('disabled', true);
                                $('#slctRenewEveryYears').prop('disabled', true);
                            } else {
                                $('#slctRenewEveryNum').prop('disabled', false);
                                $('#slctRenewEveryYears').prop('disabled', false);

                                $('#RDEVERY').prop('checked', true);
                                $('#RDON').prop('checked', false);

                                $('#slctRenewOnMonths').val('');
                                $('#slctRenewOnNum').val('');
                                $('#slctRenewEveryNum').val(regPFC[0].renew_every_num);
                                $('#slctRenewEveryYears').val(regPFC[0].renew_every_years);

                                $('#btnSavePDFRenew').hide();
                                $('#slctRenewOnMonths').prop('disabled', true);
                                $('#slctRenewOnNum').prop('disabled', true);
                            }

                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if (regPFC[0].type_of_registration == 'PDF') {
                            var fn = regPFC[0].upload_path;
                            $('#linkPDF').text(fn);
                            //$('#linkButton').show();
                            $('#slctTypeOfRegistration').closest('div').next().show();
                            $('#btnSavePDF').show();
                            if ($('#slctVmsRenewal').val() == 'true' || regPFC[0].vms_renewal == 'true') {
                                var fname = regPFC[0].renew_upload_path;
                                $('#linkPDFRenewal').text(fname);
                                $('.renewYes').show();
                                $('#btnSavePDFRenew').show();
                                $('.renewUploadPFC').show();
                            } else if ($('#slctVmsRenewal').val() == 'true' || regPFC[0].vms_renewal == 'true') {
                                $('.renewYes').hide();
                                $('#btnSavePDFRenew').hide();
                                $('.renewUploadPFC').hide();
                            }
                            else {
                                $('.renewYes').hide();
                                $('#btnSavePDFRenew').hide();
                                $('.renewUploadPFC').hide();
                            }

                        } else {
                            $('#slctTypeOfRegistration').closest('div').next().hide();
                            $('#btnSavePDF').hide();
                            $('.renewUploadPFC').hide();
                            $('#btnSavePDFRenew').hide();

                            if ($('#slctVmsRenewal').val() == 'true' || regPFC[0].vms_renewal == 'true') {
                                $('.renewYes').show();
                            } else if ($('#slctVmsRenewal').val() == 'true' || regPFC[0].vms_renewal == 'true') {
                                $('.renewYes').hide();
                            }
                            else {
                                $('.renewYes').hide();
                            }
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (regPFC[0].no_pfc_reg == true) {
                            $('#pfcReg').prop('checked', true);
                            $('#tabPFC select').prop('disabled', true);
                            $('#tabPFC input[type=number]').prop('disabled', true);
                            $('#tabPFC input[type=text]').prop('disabled', true);
                            $('#tabPFC select').val('');
                            $('#tabPFC input[type=number]').val('');
                            $('#tabPFC input[type=text]').val('');

                            var no_pfc_reg = 'Checked';
                        } else {
                            $('#pfcReg').prop('checked', false);
                            $('#tabPFC select').prop('disabled', false);
                            $('#tabPFC input[type=number]').prop('disabled', false);
                            $('#tabPFC input[type=text]').prop('disabled', false);

                            var no_pfc_reg = 'Unchecked';
                        }

                        

                        if (regPFC[0].statewide_reg == true) {
                            $('#pfcStatewideReg').prop('checked', true);
                            var statewide_reg = 'Checked';
                        } else {
                            $('#pfcStatewideReg').prop('checked', false);
                            var statewide_reg = 'Unchecked';
                        }
                    }

                    if ($('#RDEVERY').is(':checked')) {
                        $('#slctRenewEveryNum').prop('disabled', false);
                        $('#slctRenewEveryYears').prop('disabled', false);
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);

                    } else if ($('#RDON').is(':checked')) {
                        $('#slctRenewOnMonths').prop('disabled', false);
                        $('#slctRenewOnNum').prop('disabled', false);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    }

                    $('#tabPFC').find('div.audit input').prop('disabled', true);

                    $('#tabPFC').find('div.audit select').prop('disabled', true);
                    $('#tabPFC').find('div.audit button').prop('disabled', true);

                    //$('#tabPFC').find('div.audit button').prop('disabled', true);

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabPFC div.tag').remove();
                    $('#tabPFC').prepend(
                    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                    '</div>'
                );
                    //$('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                if (look != undefined) {
                    if (look[0].pfcSetting == '2') {
                        //$('#tabPFC div.tag').remove();
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                        //$('#tabPFC div.tag').remove();
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //        '<label class="text-green">' + regPFC[0].tag + '!</label>' +
                        //    '</div>'
                        //);
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (look[0].isApply == '2') {
                        sessionStorage.setItem('applyThree', 'true');
                        sessionStorage.setItem('finalReview', 'false');
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + regPFC[0].tag + '!</label>' +
                            '</div>'
                        );
                    }
                }
                if (regPFC[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);

                    $('#tabPFC div.tag').remove();
                    $('#tabPFC').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + regPFC[0].tag + '!</label>' +
                        '</div>'
                    );
                    sessionStorage.setItem('finalReview', 'true');
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }
                
                CheckApplyButton();

            }
            $('#modalLoading').modal('hide');
        }
    });
}

function getOrdinance(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetOrdinance',

        data: '{state: "' + state + '", city: "' + city + '", hist: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            var d = $.parseJSON(data.d);
            
            if (d.Success) {
                var records = d.data.record;

                //console.log([records[0].id]);

                if (val == "" || val == undefined) {
                    //$('#tblOrdinance').bootstrapTable('destroy');

                    //$('#tblOrdinance').bootstrapTable({
                    //    data: records
                    //});

                    //$('#tblOrdinance thead tr th:eq(2) div:eq(0)').attr('style', 'width: 500px');



                } else {
                    $('#tblOrdinanceHist').bootstrapTable('destroy');

                    $('#tblOrdinanceHist').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinanceHist thead tr th:eq(3) div:eq(0)').attr('style', 'width: 500px');
                }
                    $('#btnAddOrdinance').prop('disabled', false);
            

                if (sessionStorage.getItem('finalReview') == 'true') {
                    $('#tabOrdinance div.tag').remove();
                    $('#tabOrdinance').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red"> CURRENTLY ON FINAL REVIEW!</label>' +
                        '</div>'
                    );

                    $('#btnAddOrdinance').prop('disabled', true);
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }
                if (sessionStorage.getItem('finalReview') == 'false') {
                    $('#tabOrdinance div.tag').remove();
                    $('#tabOrdinance').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-green"> PENDING FOR APPROVAL!</label>' +
                        '</div>'
                    );

                    $('#btnAddOrdinance').prop('disabled', true);
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                }



                CheckApplyButton();
                


                $('#modalLoading').modal('hide');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function action(val) {
    return '<div class="col-xs-2"><button type="button" class="btn btn-success " data-id="' + val + '" onclick="editOrd(this);"><i class="fa fa-pencil-square"></i></button></div><div class="col-xs-2" style="margin-left: 10%"><button data-id="' + val + '" type="button" class="btn btn-danger " onclick="deleteOrd(this);"><i class="fa fa-trash-o"></i></button></div>';
}

$('#btnAddOrdinance').click(function () {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });

    $('.datepicker').datepicker({
        autoclose: true
    });

    $('.file').fileinput({
        'showUpload': false
    });
});

function deleteOrd(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var ordID = $(val).attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/DeleteOrdinance',
        data: '{id: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            getOrdinance('');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function editOrd(val) {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var ordID = $(val).attr('data-id');
    //alert(ordID);
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetOrdinance',

        data: '{state: "' + state + '", city: "' + city + '", hist: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                $('#ordID').text(records[0].id)
                $('#tblAddOrd input:eq(0)').val(records[0].ordinance_num);
                $('#tblAddOrd input:eq(1)').val(records[0].ordinance_name);
                $('#tblAddOrd textarea:eq(0)').val(records[0].description);
                $('#tblAddOrd input:eq(2)').val(records[0].section);
                $('#tblAddOrd input:eq(3)').val(records[0].source);
                $('#tblAddOrd input:eq(4)').val(records[0].enacted_date);
                $('#tblAddOrd input:eq(5)').val(records[0].revision_date);

                $('.file').fileinput({
                    'showUpload': false
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });


}

$('#btnApplyOrdinance').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var ordNum = $('#tblAddOrd tbody tr td input:eq(0)').val();
    var ordName = $('#tblAddOrd tbody tr td input:eq(1)').val();
    var desc = $('#tblAddOrd tbody tr td textarea:eq(0)').val();
    var sect = $('#tblAddOrd tbody tr td input:eq(2)').val();
    var src = $('#tblAddOrd tbody tr td input:eq(3)').val();
    var enDate = $('#tblAddOrd tbody tr td input:eq(4)').val();
    var revDate = $('#tblAddOrd tbody tr td input:eq(5)').val();
    var file = $('#tblAddOrd tbody tr td input:eq(6)').val();
    var ordId = $('#ordID').text();

    var myData = [];

    if (ordId == '') {
        myData = ['ordinance', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file];
    } else {
        myData = ['ordinance2', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file, ordId];
    }

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            getOrdinance('');
            $('#modalAddOrdinance').modal('hide');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });

});



$('#btnHistory').click(function () {
    $('#modalOrdinanceHist').modal({ backdrop: 'static', keyboard: false });
    getHistoryOrdinance();
    //getOrdinance('hist');
});

function isHidden(el) {
    var bool = true;
    if ($(el).attr('style') == 'display: none;') {
        bool = false;
    }
    return bool;
}

$('#btnSaveRegPFC').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var saveName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();

    //PFC DEFAULT
    var pfc_default = $('#slctPfcDefault').val();
    var pfc_default_timeline1 = $('#inDefRegTimeline1').val();
    var pfc_default_timeline2 = $('#slctDefRegTimeline2').val();
    var pfc_default_timeline3 = $('#slctDefRegTimeline3').val();
    var pfc_default_timeline4 = $('#slctDefRegTimeline4').val();
    var pfc_default_timeline5 = $('#slctDefRegTimeline5').val();

    //PFC VACANT
    var pfc_vacant = $('#slctPfcVacantTimeline').val();
    var pfc_vacant_timeline1 = $('#inPfcVacantTimeline1').val();
    var pfc_vacant_timeline2 = $('#slctPfcVacantTimeline2').val();
    var pfc_vacant_timeline3 = $('#slctPfcVacantTimeline3').val();
    var pfc_vacant_timeline4 = $('#slctPfcVacantTimeline4').val();
    var pfc_vacant_timeline5 = $('#slctPfcVacantTimeline5').val();

    //PFC FORECLOSURE
    var pfc_foreclosure = $('#slctPfcForeclosure').val();
    var pfc_foreclosure_timeline1 = $('#inPfcDefForclosureTimeline1').val();
    var pfc_foreclosure_timeline2 = $('#slctPfcDefForclosureTimeline2').val();
    var pfc_foreclosure_timeline3 = $('#slctPfcDefForclosureTimeline3').val();
    var pfc_foreclosure_timeline4 = $('#slctPfcDefForclosureTimeline4').val();
    var pfc_foreclosure_timeline5 = $('#slctPfcDefForclosureTimeline5').val();

    //PFC FORECLOSURE AND VACANT
    var pfc_foreclosure_vacant = $('#slctPfcForeclosureVacant').val();
    var pfc_foreclosure_vacant_timeline1 = $('#inPfcForeclosureVacantTimeline1').val();
    var pfc_foreclosure_vacant_timeline2 = $('#slctPfcForeclosureVacantTimeline2').val();
    var pfc_foreclosure_vacant_timeline3 = $('#slctPfcForeclosureVacantTimeline3').val();
    var pfc_foreclosure_vacant_timeline4 = $('#slctPfcForeclosureVacantTimeline4').val();
    var pfc_foreclosure_vacant_timeline5 = $('#slctPfcForeclosureVacantTimeline5').val();

    //PFC CITY NOTICE
    var pfc_city_notice = $('#slctPfcCityNotice').val();
    var pfc_city_notice_timeline1 = $('#inPfcCityNoticeTimeline1').val();
    var pfc_city_notice_timeline2 = $('#slctPfcCityNoticeTimeline2').val();
    var pfc_city_notice_timeline3 = $('#slctPfcCityNoticeTimeline3').val();
    var pfc_city_notice_timeline4 = $('#slctPfcCityNoticeTimeline4').val();
    var pfc_city_notice_timeline5 = $('#slctPfcCityNoticeTimeline5').val();

    //PFC CODE VIOLATION
    var pfc_code_violation = $('#slctPfcCodeViolation').val();
    var pfc_code_violation_timeline1 = $('#inPfcCodeViolationTimeline1').val();
    var pfc_code_violation_timeline2 = $('#slctPfcCodeViolationTimeline2').val();
    var pfc_code_violation_timeline3 = $('#slctPfcCodeViolationTimeline3').val();
    var pfc_code_violation_timeline4 = $('#slctPfcCodeViolationTimeline4').val();
    var pfc_code_violation_timeline5 = $('#slctPfcCodeViolationTimeline5').val();

    //PFC BOARDED
    var pfc_boarded = $('#slctPfcBoarded').val();
    var pfc_boarded_timeline1 = $('#inPfcBoardedTimeline1').val();
    var pfc_boarded_timeline2 = $('#slctPfcBoardedTimeline2').val();
    var pfc_boarded_timeline3 = $('#slctPfcBoardedTimeline3').val();
    var pfc_boarded_timeline4 = $('#slctPfcBoardedTimeline4').val();
    var pfc_boarded_timeline5 = $('#slctPfcBoardedTimeline5').val();

    //PFC OTHER
    var pfc_other = $('#slctPfcOther').val();
    var pfc_other_timeline1 = $('#inPfcOtherTimeline1').val();
    var pfc_other_timeline2 = $('#slctPfcOtherTimeline2').val();
    var pfc_other_timeline3 = $('#slctPfcOtherTimeline3').val();
    var pfc_other_timeline4 = $('#slctPfcOtherTimeline4').val();
    var pfc_other_timeline5 = $('#slctPfcOtherTimeline5').val();



    //var special_requirements = $('#slctSpcReq').val();
    var payment_type = $('#slctPaymentType').val();
    var type_of_registration = $('#slctTypeOfRegistration').val();
    var vms_renewal = $('#slctVmsRenewal').val();


    var upload_path = $('#linkPDF').text();
    //if ($('#tabPFC div.file-caption-name').attr('title') != undefined) {
    //    //upload_path = $('#tabPFC div.file-caption-name').attr('title');
    //    upload_path = $('#inUploadPath').val().replace('C:\\fakepath\\', '');
    //}
    var no_pfc_reg = '';
    if ($('#pfcReg').is(':checked')) {
        no_pfc_reg = 'true';
    } else {
        no_pfc_reg = 'false';
    }

    var statewide_reg = '';
    if ($('#pfcStatewideReg').is(':checked')) {
        statewide_reg = 'true';
    } else {
        statewide_reg = 'false';
    } 

    var renew_every = '';
    var renew_on = '';
    //if ($('input[name=rdRenewal]').is(':checked')) {
    //    if ($(this).val() == 'Every') {
    //        renew_every = 'true';
    //        renew_on = 'false';
    //    } else {
    //        renew_every = 'false';
    //        renew_on = 'true';
    //    }

    //}

    if ($("#RDEVERY").prop("checked")) {
        renew_every = 'true';
        renew_on = 'false';
        // do something
    }
    else if ($("#RDON").prop("checked")) {
        renew_every = 'false';
        renew_on = 'true';
    }
    else {
        renew_every = '';
        renew_on = '';
    }

    var renew_every_num = $('#slctRenewEveryNum').val();
    var renew_every_years = $('#slctRenewEveryYears').val();

    var renew_on_months = $('#slctRenewOnMonths').val();
    var renew_on_num = $('#slctRenewOnNum').val();

    var Renewupload_path = $('#linkPDFRenewal').text();

    var myData = [
       'regPFC',
       state,
       city,
       //PFC DEFAULT
       pfc_default,
       pfc_default_timeline1,
       pfc_default_timeline2,
       pfc_default_timeline3,
       pfc_default_timeline4,
       pfc_default_timeline5,

       //PFC VACANT
       pfc_vacant,
       pfc_vacant_timeline1,
       pfc_vacant_timeline2,
       pfc_vacant_timeline3,
       pfc_vacant_timeline4,
       pfc_vacant_timeline5,

       //PFC FORECLOSURE
       pfc_foreclosure,
       pfc_foreclosure_timeline1,
       pfc_foreclosure_timeline2,
       pfc_foreclosure_timeline3,
       pfc_foreclosure_timeline4,
       pfc_foreclosure_timeline5,

       //PFC FORECLOSURE AND VACANT
       pfc_foreclosure_vacant,
       pfc_foreclosure_vacant_timeline1,
       pfc_foreclosure_vacant_timeline2,
       pfc_foreclosure_vacant_timeline3,
       pfc_foreclosure_vacant_timeline4,
       pfc_foreclosure_vacant_timeline5,

       //PFC CITY NOTICE
       pfc_city_notice,
       pfc_city_notice_timeline1,
       pfc_city_notice_timeline2,
       pfc_city_notice_timeline3,
       pfc_city_notice_timeline4,
       pfc_city_notice_timeline5,

       //PFC CODE VIOLATION
       pfc_code_violation,
       pfc_code_violation_timeline1,
       pfc_code_violation_timeline2,
       pfc_code_violation_timeline3,
       pfc_code_violation_timeline4,
       pfc_code_violation_timeline5,

       //PFC BOARDED
       pfc_boarded,
       pfc_boarded_timeline1,
       pfc_boarded_timeline2,
       pfc_boarded_timeline3,
       pfc_boarded_timeline4,
       pfc_boarded_timeline5,

       //PFC OTHER
       pfc_other,
       pfc_other_timeline1,
       pfc_other_timeline2,
       pfc_other_timeline3,
       pfc_other_timeline4,
       pfc_other_timeline5,

       payment_type,
       type_of_registration,
       vms_renewal,
       upload_path,
       no_pfc_reg,
       statewide_reg,
       renew_every,
       renew_on,
       renew_every_num,
       renew_every_years,
       renew_on_months,
       renew_on_num,
       Renewupload_path,
    ];


    var btns = $('#tabPFC .fa-check').closest('button');
    var count = 0;

    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    var user = $('#usr').text();
    if (count == 0) {
        pfcAudit();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforPFC + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfuly!', function () {
                        
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        //$('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });

                    //CheckApplyButton();

                } else {
                    alertify.alert('Error','Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegREO').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var reo_bank_owed = $('#slctReoBankOwed').val();
    var reo_bank_owed_timeline1 = $('#Drpreobanktime').val();
    var reo_bank_owed_timeline2 = $('#inReoBankOwed1').val();
    var reo_bank_owed_timeline3 = $('#slctReoBankOwed2').val();
    var reo_bank_owed_timeline4 = $('#slctReoBankOwed3').val();
    var reo_bank_owed_timeline5 = $('#slctReoBankOwed4').val();

    //Vacant
    var reo_vacant = $('#slctReoVacant').val();
    var reo_vacant_timeline1 = $('#slctReowith').val();
    var reo_vacant_timeline2 = $('#inReoVacantTimeline1').val();
    var reo_vacant_timeline3 = $('#slctReoVacantTimeline2').val();
    var reo_vacant_timeline4 = $('#slctReoVacantTimeline3').val();
    var reo_vacant_timeline5 = $('#slctReoVacantTimeline4').val();

    //CITY NOTICE
    var reo_city_notice = $('#slctReoCityNotice').val();
    var reo_city_notice_timeline1 = $('#slctReocitynoticewith').val();
    var reo_city_notice_timeline2 = $('#slctReocitytimeline1').val();
    var reo_city_notice_timeline3 = $('#slctReoBusinesstimeline2').val();
    var reo_city_notice_timeline4 = $('#slctReodaystimeline3').val();
    var reo_city_notice_timeline5 = $('#slctReovacancytimeline4').val();

    //CODE VIOLATION
    var reo_code_violation = $('#slctReoCodeViolation').val();
    var reo_code_violation_timeline1 = $('#slctReoviolationwith').val();
    var reo_code_violation_timeline2 = $('#slctReoViolationtimeline1').val();
    var reo_code_violation_timeline3 = $('#slctReoviolationtimeline2').val();
    var reo_code_violation_timeline4 = $('#slctReoviolationdaystimeline3').val();
    var reo_code_violation_timeline5 = $('#slctReoviolationvacancytimeline4').val();

    //BOARDED
    var reo_boarded_only = $('#slctReoBoardedOnly').val();
    var reo_boarded_only_timeline1 = $('#slctReoBoardedwith').val();
    var reo_boarded_only_timeline2 = $('#slctReoBoardedtimeline1').val();
    var reo_boarded_only_timeline3 = $('#slctReoBoardedtimeline2').val();
    var reo_boarded_only_timeline4 = $('#slctReoBoardeddaystimeline3').val();
    var reo_boarded_only_timeline5 = $('#slctReoBoardedtimeline4').val();

    //DISTRESSED ABANDONED
    var reo_distressed_abandoned = $('#slctReoDistressedAbandoned').val();
    var reo_distressed_abandoned1 = $('#DrpReoAbandoned').val();
    var reo_distressed_abandoned2 = $('#inReoDistressedAbandoned1').val();
    var reo_distressed_abandoned3 = $('#slctReoDistressedAbandoned2').val();
    var reo_distressed_abandoned4 = $('#slctReoDistressedAbandoned3').val();
    var reo_distressed_abandoned5 = $('#slctReoDistressedAbandoned4').val();

    //RENTAL REGISTRATION
    var rental_registration = $('#slctRentalRegistration').val();
    var rental_registration_timeline1 = $('#slctRentalFormwithin').val();
    var rental_registration_timeline2 = $('#slctReorentaltimeline1').val();
    var rental_registration_timeline3 = $('#slctReorentaltimeline2').val();
    var rental_registration_timeline4 = $('#slctReorentaltimeline3').val();
    var rental_registration_timeline5 = $('#slctReorentaltimeline4').val();

    //OTHERS
    var reo_other = $('#slctReoOther').val();
    var reo_other_timeline1 = $('#slctreootherwithin').val();
    var reo_other_timeline2 = $('#inReoOtherTimeline1').val();
    var reo_other_timeline3 = $('#slctReoOtherTimeline2').val();
    var reo_other_timeline4 = $('#slctReoOtherTimeline3').val();
    var reo_other_timeline5 = $('#slctReoOtherTimeline4').val();


    //var rental_form = $('#slctRentalForm').val()

    //var special_requirements = $('#slctREOSpcReq').val();
    var payment_type = $('#slctREOPaymentType').val();
    var type_of_registration = $('#slctREOTypeOfRegistration').val();
    var vms_renewal = $('#slctREOVmsRenewal').val();
    var upload_path = $('#linkPDFREO').text();

    var no_reo_reg = '';
    if ($('#reoReg').is(':checked')) {
        no_reo_reg = 'true';
    } else {
        no_reo_reg = 'false';
    }

    var statewide_reg = '';
    if ($('#reoStatewideReg').is(':checked')) {
        statewide_reg = 'true';
    } else {
        statewide_reg = 'false';
    }

    var renew_every = '';
    var renew_on = '';
    //if ($('input[name=rdREORenewal]').is(':checked')) {
    //    if ($(this).val() == 'Every') {
    //        renew_every = 'true';
    //        renew_on = 'false';
    //    } else {
    //        renew_every = 'false';
    //        renew_on = 'true';
    //    }
    //}

    if ($("#Reordvery").prop("checked")) {
        renew_every = 'true';
        renew_on = 'false';
        // do something
    }
    else if ($("#Reordon").prop("checked")) {
        renew_every = 'false';
        renew_on = 'true';
    }
    else {
        renew_every = '';
        renew_on = '';
    }

    var renew_every_num = $('#slctREORenewEveryNum').val();
    var renew_every_years = $('#slctREORenewEveryYears').val();

    var renew_on_months = $('#slctREORenewOnMonths').val();
    var renew_on_num = $('#slctREORenewOnNum').val();

    var Renewupload_path = $('#linkPDFRenewalREO').text();

    var myData = [
        'regREO',
        state,
        city,

        //BANKED-OWED
        reo_bank_owed,
        reo_bank_owed_timeline1,
        reo_bank_owed_timeline2,
        reo_bank_owed_timeline3,
        reo_bank_owed_timeline4,
        reo_bank_owed_timeline5,

        //VACANT
        reo_vacant,
        reo_vacant_timeline1,
        reo_vacant_timeline2,
        reo_vacant_timeline3,
        reo_vacant_timeline4,
        reo_vacant_timeline5,

        //CITY NOTICE
        reo_city_notice,
        reo_city_notice_timeline1,
        reo_city_notice_timeline2,
        reo_city_notice_timeline3,
        reo_city_notice_timeline4,
        reo_city_notice_timeline5,

        //CODE VIOLATION
        reo_code_violation,
        reo_code_violation_timeline1,
        reo_code_violation_timeline2,
        reo_code_violation_timeline3,
        reo_code_violation_timeline4,
        reo_code_violation_timeline5,

        //BOARDED
        reo_boarded_only,
        reo_boarded_only_timeline1,
        reo_boarded_only_timeline2,
        reo_boarded_only_timeline3,
        reo_boarded_only_timeline4,
        reo_boarded_only_timeline5,

        //DISTRESSED ABANDONED
        reo_distressed_abandoned,
        reo_distressed_abandoned1,
        reo_distressed_abandoned2,
        reo_distressed_abandoned3,
        reo_distressed_abandoned4,
        reo_distressed_abandoned5,

        //RENTAL REGISTRATION
        rental_registration,
        rental_registration_timeline1,
        rental_registration_timeline2,
        rental_registration_timeline3,
        rental_registration_timeline4,
        rental_registration_timeline5,

        //OTHERS
        reo_other,
        reo_other_timeline1,
        reo_other_timeline2,
        reo_other_timeline3,
        reo_other_timeline4,
        reo_other_timeline5,

        //rental_form,
        //special_requirements,
        payment_type,
        type_of_registration,
        vms_renewal,
        upload_path,
        no_reo_reg,
        statewide_reg,
        renew_every,
        renew_on,
        renew_every_num,
        renew_every_years,
        renew_on_months,
        renew_on_num,
        Renewupload_path
    ];

    var btns = $('#tabREO .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();
    if (count == 0) {
        
        reoAudit();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforREO + '" }',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success','Settings saved successfully', function () {
                        
                        //$('#tabREO').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        //$('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                    //CheckApplyButton(state, city);
                } else {
                    alertify.alert('Error','Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegProperty').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var residential = $('#tabPropType select:eq(0)').val();
    var single_family = $('#tabPropType select:eq(1)').val();
    var multi_family = $('#tabPropType select:eq(2)').val();
    var unit2 = $('#tabPropType select:eq(3)').val();
    var unit3 = $('#tabPropType select:eq(4)').val();
    var unit4 = $('#tabPropType select:eq(5)').val();
    var unit5 = $('#tabPropType select:eq(6)').val();
    var rental = $('#tabPropType select:eq(7)').val();
    var commercial = $('#tabPropType select:eq(8)').val();
    var condo = $('#tabPropType select:eq(9)').val();
    var townhome = $('#tabPropType select:eq(10)').val();
    var vacant_lot = $('#tabPropType select:eq(11)').val();
    var mobile_home = $('#tabPropType select:eq(12)').val();

    var myData = [
        'regProperty',
        state,
        city,
        residential,
        single_family,
        multi_family,
        unit2,
        unit3,
        unit4,
        unit5,
        rental,
        commercial,
        condo,
        townhome,
        vacant_lot,
        mobile_home
    ];


    var btns = $('#tabPropType .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();
    if (count == 0) {
        
        propertyTypeAudit();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingPropType + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Settings saved successfully', function () {
                        
                        //$('#tabPropType').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        ////$('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                    
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegPropReq').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');


    var ReqOwner = $('#slctPresaleDefinition').val();
    var ReqLOcalContactReq = $('#slctLocalContactRequired').val();
    var ReqGSE = $('#slctGseExclusion').val();
    var ReqInsurance = $('#slctInsuranceReq').val();
    var ReqForeClosure = $('#slctForeclosureActInfo').val();
    var ReqForeClosureCase = $('#slctForeclosureCaseInfo').val();
    var ReqForeClosureDeed = $('#slctForeclosureDeedReq').val();
    var ReqBond = $('#slctBondReq').val();
    var ReqUtilityInfo = $('#slctUtilityInfoReq').val();
    var ReqWinterrization = $('#slctWinterReq').val();
    var ReqSignatureReq = $('#slctSignReq').val();
    var ReqNotarization = $('#slctNotarizationReq').val();
    var ReqRecent = $('#slctRecInsDate').val();
    var ReqFirstTime = $('#slctFirstTimeVacancyDate').val();
    var ReqSecured = $('#slctSecuredRequired').val();
    var ReqAdditional = $('#slctAddSignReq').val();
    var ReqPictureReq = $('#slctPicReq').val();
    var ReqMobileVIN = $('#slctMobileVINReq').val();
    var ReqParcel = $('#slctParcelReq').val();
    var ReqLegal = $('#slctLegalDescReq').val();
    var ReqBlock = $('#slctBlockLotReq').val();
    var ReqAttorney = $('#slctAttyInfoReq').val();
    var ReqBroker = $('#slctBrkInfoReq').val();
    var ReqMortgage = $('#slctMortContactNameReq').val();
    var ReqClient = $('#slctClientTaxReq').val();

    var ReqEscalatingRenewal = "";
    var ReqOnetime = "";
    var ReqBondAmount = "";
    var ReqInBOnd = "";

    if ($('#slctBondReq').val() == 'true') {

        ReqOnetime = $('#slctOneTime').val();
        ReqInBOnd = $('#inBondAmount').val();
        ReqBondAmount = $('#slctBondAmountUSP').val();


        ReqEscalatingRenewal = $('#txtBond1').val() + ',';
        for (i = 2; i < countBond; i++) {
            if ($('#dvBond' + i).is(':visible')) {
                ReqEscalatingRenewal += $('#txtBond' + i).val() + ',';
            }
        }
        ReqEscalatingRenewal = ReqEscalatingRenewal.slice(0, -1);
    }

    //Local Contact
    var lcicompany_name = $('#lciCompany').val();
    var lcifirst_name = $('#lciFirstName').val();
    var lcilast_name = $('#lciLastName').val();
    var lcititle = $('#lciTitle').val();
    var lcibusiness_license_num = $('#lciBusinessLicenseNum').val();
    var lciphone_num1 = $('#lciPhoneNum1').val();
    var lciphone_num2 = $('#lciPhoneNum2').val();
    var lcibusiness_phone_num1 = $('#lciBusinessPhoneNum1').val();
    var lcibusiness_phone_num2 = $('#lciBusinessPhoneNum2').val();
    var lciemergency_phone_num1 = $('#lciEmrPhone1').val();
    var lciemergency_phone_num2 = $('#lciEmrPhone2').val();
    var lcifax_num1 = $('#lciFaxNum1').val();
    var lcifax_num2 = $('#lciFaxNum2').val();
    var lcicell_num1 = $('#lciCellNum1').val();
    var lcicell_num2 = $('#lciCellNum2').val();
    var lciemail = $('#lciEmail').val();
    var lcistreet = $('#lciStreet').val();

    var lcistate = '';


    if (lcState == "") {


        if ($('#lciState').val() != undefined) {
            lcistate = $('#lciState').val();
        }
    } else {
        lcistate = lcState;
    }

    var lcicity = '';
    if (lcCity == "") {
        if ($('#lciCity').val() != undefined) {
            lcicity = $('#lciCity').val();
        }
    } else {


        lcicity = lcCity;
    }

    var lcizip = '';
    if (lcZip == "") {
        if ($('#lciZip').val() != undefined) {
            lcizip = $('#lciZip').val();
        }
    } else {


        lcizip = lcZip;
    }

    var lcihours_from = $('#lciHrsFrom').val();
    var lcihours_to = $('#lciHrsTo').val();

    var contactState = state;
    var contactCity = city;

    var myData = [
        'regPropReq',
        state,
        city,
        ReqOwner,
        ReqLOcalContactReq,
        ReqGSE,
        ReqInsurance,
        ReqForeClosure,
        ReqForeClosureCase,
        ReqForeClosureDeed,
        ReqBond,
        ReqUtilityInfo,
        ReqWinterrization,
        ReqSignatureReq,
        ReqNotarization,
        ReqRecent,
        ReqFirstTime,
        ReqSecured,
        ReqAdditional,
        ReqPictureReq,
        ReqMobileVIN,
        ReqParcel,
        ReqLegal,
        ReqBlock,
        ReqAttorney,
        ReqBroker,
        ReqMortgage,
        ReqClient,

        //2nd Data
        ReqOnetime,
        ReqInBOnd,
        ReqBondAmount,
        ReqEscalatingRenewal,

       //Local Contact
        lcicompany_name,
        lcifirst_name,
        lcilast_name,
        lcititle,
        lcibusiness_license_num,
        lciphone_num1,
        lciphone_num2,
        lcibusiness_phone_num1,
        lcibusiness_phone_num2,
        lciemergency_phone_num1,
        lciemergency_phone_num2,
        lcifax_num1,
        lcifax_num2,
        lcicell_num1,
        lcicell_num2,
        lciemail,
        lcistreet,
        lcistate,
        lcicity,
        lcizip,
        lcihours_from,
        lcihours_to,
        contactState,
        contactCity
    ];

    var btns = $('#tabPropReq .fa-check').closest('button');
    var lcibtns = $('#lci .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    $.each(lcibtns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforReq + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfully', function () {
                        
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});


$('#btnSaveRegCont').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var cont_reg = $('#slctContReg').val();
    var update_reg = $('#slctUpdateReg').val();

    var myData = [
        'regCont',
        state,
        city,
        cont_reg,
        update_reg
    ];

    var btns = $('#tabContReg .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();
    if (count == 0) {
        contReg();
        

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforContReg + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfully', function () {
                        
                        //$('#tabContReg').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        ////$('#tabContReg div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error','Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveInspection').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    
    var municipal_inspection_fee_required = $('#slctMuniInsReq').val();
    var onetime_only_upon_reg = $('#slctOneTimeUponReg').val();
    var fee_payment_frequency = $('#slctFeePaymentFreq').val();
    var payon1 = $('#inPayon1').val();
    var payon2 = $('#inPayon2').val();
    var payon3 = $('#inPayon3').val();
    var payon4 = $('#inPayon4').val();
    var free_escalating = $('#slctFreeEscalating').val();
    var municipal_inspection_fee = $('#inMunicipalInsFee').val();
    //var municipal_inspection_fee_curr = $('#inMunicpalInsFeeCurr').val();


    var escalating_amount = "";
    var escalating_succeeding = "false,";


    if (municipal_inspection_fee_required == 'false' || $('#slctFreeEscalating').val() == 'false') {
        escalating_amount = "";
        escalating_succeeding = "false";
    } else {
        for (ii = 1; ii <= 5; ii++) {
            escalating_amount += $('#inputIns' + ii).val() != '' && $('#inputIns' + ii).val() != 0 ?
                 $('#inputIns' + ii).val() + ',' : '';
        }
        escalating_amount = escalating_amount.slice(0, -1);

        for (ii = 2; ii <= 5; ii++) {
            escalating_succeeding += $('#inspectCheck' + ii).is(':checked') ? "true," : "false,";
        }
        escalating_succeeding = escalating_succeeding.slice(0, -1);

    }

    var inspection_report_required = $('#slctInsUpReq').val();
    var inspection_frequency_occupied = $('#slctInsCriOccc').val();
    var inspection_frequency_vacant = $('#slctInsCriVac').val();
    var how_to_send_inspection_report = $('#slctInsFeePay').val();

    //-------------------------------------------------------------------------------------

    var myData = [
        'inspect',
        state,
        city,
        municipal_inspection_fee_required,
        onetime_only_upon_reg,
        fee_payment_frequency,
        payon1,
        payon2,
        payon3,
        payon4,
        free_escalating,
        municipal_inspection_fee,
        escalating_amount,
        escalating_succeeding,
        inspection_report_required,
        inspection_frequency_occupied,
        inspection_frequency_vacant,
        how_to_send_inspection_report
    ];


    var btns = $('#tabInspection .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforInspection + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfully', function () {
                        
                        //$('#tabInspection').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        ////$('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveMunicipalInfo').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var municipality_department = $('#inMuniDept').val();
    var municipality_phone_num = $('#inMuniPhone').val();
    var municipality_email = $('#inMuniEmail').val();
    var municipality_st = $('#inMuniMailStr').val();
    var municipality_website = $('#inWebsite').val();


    var municipality_state;
    if (stateVal != '' && stateVal != undefined && stateVal != null) {
        municipality_state = stateVal;
    } else {
        municipality_state = $('#slctMuniMailSta').val();
    }

    var municipality_city
    if (cityVal != '' && cityVal != undefined && cityVal != null) {
        municipality_city = cityVal;
    } else {
        municipality_city = $('#slctMuniMailCt').val();
    }

    var municipality_zip
    if (zipVal != '' && zipVal != undefined && zipVal != null) {
        municipality_zip = zipVal;
    } else {
        municipality_zip = $('#slctMuniMailZip').val();
    }


    var contact_person = $('#inContact').val();
    var title = $('#inTitle').val();
    var department = $('#inDept').val();
    var phone_num = $('#inPhone').val();
    var email = $('#inEmail').val();
    var address = $('#inMailing').val();
    var ops_hrs_from = $('#inHrsFrom').val();
    var ops_hrs_to = $('#inHrsTo').val();


    var myData = [
        'municipalityInfo',
        state,
        city,
        municipality_department,
        municipality_phone_num,
        municipality_email,
        municipality_st,
        municipality_website,
        municipality_city,
        municipality_state,
        municipality_zip,
        contact_person,
        title,
        department,
        phone_num,
        email,
        address,
        ops_hrs_from,
        ops_hrs_to
    ];

    //$('#slctMuniMailSta').val(stateVal);

    //cityVal = "";
    //stateVal = "";
    //zipVal = "";

    var btns = $('#tabMunicipality .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();

    if (count == 0) {

        municipalityInfo();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforMunicipality + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfully', function () {

                        //$('#tabMunicipality').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        ////$('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                    $('#modalLoading').modal('hide');
                    existingSettingforMunicipality = "";
                    cityVal = "";
                    stateVal = "";
                    zipVal = "";
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveDeregistration').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var dereg_req = $('#slctDeregRequired').val();
    var conveyed = $('#slctConveyed').val();
    var occupied = $('#slctOccupied').val();
    var how_to_dereg = $('#slctHowToDereg').val();
    var upload_file = $('#delinkPDF').text();
    //var upload_file = $('#inUploadPathD').val();
    var new_owner_info_req = $('#slctNewOwnerInfoReq').val();
    var proof_of_conveyance_req = $('#slctProofOfConveyReq').val();
    var date_of_sale_req = $('#slctDateOfSaleReq').val();

    var myData = [
        'dereg',
        state,
        city,
        dereg_req,
        conveyed,
        occupied,
        how_to_dereg,
        upload_file,
        new_owner_info_req,
        proof_of_conveyance_req,
        date_of_sale_req
    ];

    var btns = $('#tabDeregistration .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();

    if (count == 0) {

        deregistration();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + existingSettingforDeregistration + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfully', function () {
                        
                        //$('#tabDeregistration').prepend(
                        //    '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                        //        '<label class="text-green">PENDING FOR APPROVAL!</label>' +
                        //    '</div>'
                        //);

                        ////$('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                        //$('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveZip').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var myData;

    var saved;
    
    var id = 1;

    $.each($('#tblZip tbody tr'), function (idx, val) {
        var zip = $(this).find('td:eq(1)').text();
        var isActive;

        if ($(this).find('td:eq(0) div').hasClass('off')) {
            isActive = '0';
        } else {
            isActive = '1';
        }

        myData = [
            'zip',
            state,
            city,
            zip,
            isActive
        ];

        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "'+id+'"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.d == '1') {
                    alertify.alert('Settings saved successfully');
                } else {
                    alertify.alert('Error Saving!');
                }
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    });
    $('#modalLoading').modal('hide');
});


$('.btnCancel').click(function () {
    $('#divSettings input, #divSettings select').val('');
    $('#divSettings').hide();
    $('#tblMunicipality tbody').empty();
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    });
});

$('#inRegCost').keyup(function () {
    if ($('#slctRegCostStandard').val() == 'true') {
        $('#inRenewAmt').val($(this).val());
    }
});

$('#btnAddNew1').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#btnAddNew1REO').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#btnAddNew2Comm').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});
$('#btnAddNew1Vacant').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#inComRegFee').keyup(function () {
    if ($('#slctComFeeStandard').val() == 'true') {
        $('#inRenewAmt2').val($(this).val());
    }
});

$('#btnAddNew2').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);
        
        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#navReg li button').click(function () {
    $.each($('#navReg li'), function () {
        if ($(this).find('button').hasClass('btn-primary')) {
            $(this).find('button').removeClass('btn-primary');
            $(this).find('button').addClass('btn-default');
        }
    });

    $(this).addClass('btn-primary');
    $(this).focus();
});

function cellStyle(value, row, index, field) {
    return {
        classes: 'text-justify'
    };
}

$('.criteriaEdit').click(function () {
    $('#modalEdit').modal({ backdrop: 'static', keyboard: false });

    $(".timepicker").timepicker({
        showInputs: false
    });

    var fieldText = $(this).parent().parent().find('div:eq(0)').text();
    var btnData = $(this).attr('data-id');
    $('#lblEdit').text('Enter ' + fieldText + ' ');

    $.each($('#modalEdit div.panel-body'), function () {
        $(this).css('display', 'none');
    });

    $('#' + btnData).removeAttr('style');

    //getLCIState();
    //getLCICity('');
    //getLCIZip('');
    //$.when(getLCICity('')).then();

    //$('#lciState').change(function () {
    //    var state = $(this).val();

    //    getLCICity(state);
    //});

    //$('#lciCity').change(function () {
    //    var city = $(this).val();

    //    getLCIZip(city);
    //});
});

var stateSelect;
$('#lciState').change(function () {
    stateSelect = $(this).val();

    getLCICity(stateSelect);
});

$('#lciCity').change(function () {
    var city = $(this).val();

    if (sessionStorage.getItem('LCcityVal') != '') {
        //getLCIZip(sessionStorage.getItem('LCstateVal'), city);

        getLCIZip($('#lciState :selected').text(), city);
        sessionStorage.setItem('LCcityVal', 'Null');
    }
    else if (sessionStorage.getItem('LCcityVal') == 'Null') {
        getLCIZip(sessionStorage.getItem('LCstateVal'), city);
        sessionStorage.setItem('LCcityVal', 'Null');
    }
    else {
        getLCIZip(stateSelect, city);
    }


});

var muniState;
$('#slctMuniMailSta').change(function () {
    muniState = $(this).val();

    getMuniCity(muniState);
});

$('#slctMuniMailCt').change(function () {
    var city = $(this).val();


    //sessionStorage.setItem('cityVal', municipal[0].municipality_city);
    //sessionStorage.setItem('zipVal', municipal[0].municipality_zip);

    if (sessionStorage.getItem('cityVal') != '') {
        //getMuniZip(sessionStorage.getItem('stateVal'), city);
        getMuniZip($('#slctMuniMailSta').val(), city);
        sessionStorage.setItem('cityVal', 'Null');
    }
    else if (sessionStorage.getItem('cityVal') == 'Null') {
        getMuniZip(sessionStorage.getItem('stateVal'), city);
        sessionStorage.setItem('cityVal', 'Null');
    }
    else {
        getMuniZip(muniState, city);
    }
});

function getLCIState() {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetMuniState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var states = d.data.record;

                $('#lciState').empty();
                $('#lciState').append('<option value="" selected="selected">--Select State--</option>');
                $.each(states, function (idx, val) {
                    $('#lciState').append('<option value="' + val.State + '">' + val.State + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCICity(state) {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#lciCity').empty();
                $('#lciCity').append('<option value="" selected="selected">--Select City--</option>');
                $.each(cities, function (idx, val) {
                    $('#lciCity').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCICitySet(state, city) {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#lciCity').empty();
                $('#lciCity').append('<option value="">--Select City--</option>');
                $.each(cities, function (idx, val) {
                    $('#lciCity').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                $('#lciCity :selected').text(city);
                //sessionStorage.setItem('LCcityVal', city);

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCIZip(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/getContactZip',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.contactZip;

                $('#lciZip').empty();
                $('#lciZip').append('<option value="" selected="selected">--Select Zip--</option>');
                $.each(zips, function (idx, val) {
                    $('#lciZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCIZipSet(state, city, code) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/getContactZip',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.contactZip;

                $('#lciZip').empty();
                $('#lciZip').append('<option value="">--Select Zip--</option>');
                $.each(zips, function (idx, val) {
                    $('#lciZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });
                $('#lciZip :selected').text(code)
                $('#modalLoading').modal('hide');

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMuniState() {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetMuniState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var states = d.data.record;

                $('#slctMuniMailSta').empty();
                $('#slctMuniMailSta').append('<option value="" selected="selected">--Select State--</option>');
                $.each(states, function (idx, val) {
                    $('#slctMuniMailSta').append('<option value="' + val.State + '">' + val.State + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMuniCity(state) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#slctMuniMailCt').empty();
                $('#slctMuniMailCt').append('<option value="" selected="selected">--Select City--</option>');
                $.each(cities, function (idx, val) {
                    $('#slctMuniMailCt').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMuniCitySet(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#slctMuniMailCt').empty();
                $('#slctMuniMailCt').append('<option value="" >--Select City--</option>');
                $.each(cities, function (idx, val) {

                    $('#slctMuniMailCt').append('<option value="' + val.City + '">' + val.City + '</option>');


                });
                $('#slctMuniMailCt :selected').text(city);
                //sessionStorage.setItem('cityVal', city);
                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMuniZipSet(state, city, code) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/getMuniZip',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.muniZip;

                $('#slctMuniMailZip').empty();
                $('#slctMuniMailZip').append('<option value="">--Select Zip--</option>');
                $.each(zips, function (idx, val) {
                    $('#slctMuniMailZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });
                $('#slctMuniMailZip :selected').text(code)
                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMuniZip(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/getMuniZip',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.muniZip;

                $('#slctMuniMailZip').empty();
                $('#slctMuniMailZip').append('<option value="" selected="selected">--Select Zip--</option>');
                $.each(zips, function (idx, val) {
                    $('#slctMuniMailZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
//<Muni Zip

$('.number').keypress(function (event) {
    var charCode = (event.which) ? event.which : event.keyCode

    if (
      (charCode != 46 || $(this).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
      (charCode < 48 || charCode > 57))
        return false;

    return true;

});

$('#slctLocalContactRequired').change(function () {
    if ($('#slctLocalContactRequired').val() == 'true') {
        $('#localContactEdit').prop("disabled", false);
    }
    else {
        $('#localContactEdit').prop("disabled", true);
    }
});

$('#slctPfcCityNotice').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcCityNoticeTimeline1').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcCityNoticeTimeline1').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcCityNoticeTimeline1').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', true);
    }
});

$('#slctPfcCodeViolation').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcCodeViolationTimeline1').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcCodeViolationTimeline1').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcCodeViolationTimeline1').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', true);
    }
});

$('#slctPfcBoarded').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcBoardedTimeline1').prop('disabled', false);
        $('#slctPfcBoardedTimeline2').prop('disabled', false);
        $('#slctPfcBoardedTimeline3').prop('disabled', false);
        $('#slctPfcBoardedTimeline4').prop('disabled', false);
        $('#slctPfcBoardedTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcBoardedTimeline1').prop('disabled', true);
        $('#slctPfcBoardedTimeline2').prop('disabled', true);
        $('#slctPfcBoardedTimeline3').prop('disabled', true);
        $('#slctPfcBoardedTimeline4').prop('disabled', true);
        $('#slctPfcBoardedTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcBoardedTimeline1').prop('disabled', true);
        $('#slctPfcBoardedTimeline2').prop('disabled', true);
        $('#slctPfcBoardedTimeline3').prop('disabled', true);
        $('#slctPfcBoardedTimeline4').prop('disabled', true);
        $('#slctPfcBoardedTimeline5').prop('disabled', true);
    }
});

$('#slctPfcOther').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcOtherTimeline1').prop('disabled', false);
        $('#slctPfcOtherTimeline2').prop('disabled', false);
        $('#slctPfcOtherTimeline3').prop('disabled', false);
        $('#slctPfcOtherTimeline4').prop('disabled', false);
        $('#slctPfcOtherTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcOtherTimeline1').prop('disabled', true);
        $('#slctPfcOtherTimeline2').prop('disabled', true);
        $('#slctPfcOtherTimeline3').prop('disabled', true);
        $('#slctPfcOtherTimeline4').prop('disabled', true);
        $('#slctPfcOtherTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcOtherTimeline1').prop('disabled', true);
        $('#slctPfcOtherTimeline2').prop('disabled', true);
        $('#slctPfcOtherTimeline3').prop('disabled', true);
        $('#slctPfcOtherTimeline4').prop('disabled', true);
        $('#slctPfcOtherTimeline5').prop('disabled', true);
    }
});

$('#slctReoCityNotice').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReocitynoticewith').prop('disabled', false);
        $('#slctReocitytimeline1').prop('disabled', false);
        $('#slctReoBusinesstimeline2').prop('disabled', false);
        $('#slctReodaystimeline3').prop('disabled', false);
        $('#slctReovacancytimeline4').prop('disabled', false);
    } else {
        $('#slctReocitynoticewith').prop('disabled', true);
        $('#slctReocitytimeline1').prop('disabled', true);
        $('#slctReoBusinesstimeline2').prop('disabled', true);
        $('#slctReodaystimeline3').prop('disabled', true);
        $('#slctReovacancytimeline4').prop('disabled', true);
    }
});

$('#slctPfcDefault').change(function () {
    if ($(this).val() == 'true') {
        $('#inDefRegTimeline1').prop('disabled', false);
        $('#slctDefRegTimeline2').prop('disabled', false);
        $('#slctDefRegTimeline3').prop('disabled', false);
        $('#slctDefRegTimeline4').prop('disabled', false);
        $('#slctDefRegTimeline5').prop('disabled', false);

    }
    else if ($(this).val() == 'false') {
        $('#inDefRegTimeline1').prop('disabled', true);
        $('#slctDefRegTimeline2').prop('disabled', true);
        $('#slctDefRegTimeline3').prop('disabled', true);
        $('#slctDefRegTimeline4').prop('disabled', true);
        $('#slctDefRegTimeline5').prop('disabled', true);
    }
    else {
        $('#inDefRegTimeline1').prop('disabled', true);
        $('#slctDefRegTimeline2').prop('disabled', true);
        $('#slctDefRegTimeline3').prop('disabled', true);
        $('#slctDefRegTimeline4').prop('disabled', true);
        $('#slctDefRegTimeline5').prop('disabled', true);

    }
});

$('#slctPfcForeclosure').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcDefForclosureTimeline1').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcDefForclosureTimeline1').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcDefForclosureTimeline1').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', true);
    }
});

$('#slctPfcForeclosureVacant').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', false);

        //$(this).css("border-color", "red");
    }
    else if ($(this).val() == 'false') {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);

        //$(this).css("border-color", "b.lue");
    }
    else {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
    }
});

$('#slctPfcVacantTimeline').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcVacantTimeline1').prop('disabled', false);
        $('#slctPfcVacantTimeline2').prop('disabled', false);
        $('#slctPfcVacantTimeline3').prop('disabled', false);
        $('#slctPfcVacantTimeline4').prop('disabled', false);
        $('#slctPfcVacantTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcVacantTimeline1').prop('disabled', true);
        $('#slctPfcVacantTimeline2').prop('disabled', true);
        $('#slctPfcVacantTimeline3').prop('disabled', true);
        $('#slctPfcVacantTimeline4').prop('disabled', true);
        $('#slctPfcVacantTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcVacantTimeline1').prop('disabled', true);
        $('#slctPfcVacantTimeline2').prop('disabled', true);
        $('#slctPfcVacantTimeline3').prop('disabled', true);
        $('#slctPfcVacantTimeline4').prop('disabled', true);
        $('#slctPfcVacantTimeline5').prop('disabled', true);
    }
});

$('#slctTypeOfRegistration').change(function () {
    if ($(this).val() == 'PDF') {
        $('#btnSavePDF').show();
        $(this).closest('div').next().show();
        $('.renewUploadPFC').hide();
        if ($('#slctVmsRenewal').val() == 'true') {
            $('.renewUploadPFC').show();
            $('.renewYes').show();
            $('#btnSavePDFRenew').show();
        }
    } else {
        $(this).closest('div').next().hide();
        $('.renewUploadPFC').hide();
        $('#btnSavePDFRenew').hide();
        $('#btnSavePDF').hide();
        $('#linkPDF').text('');
        $('#linkPDFRenewal').text('');
        $('.renewYes').show();

        if ($('#slctVmsRenewal').val() != 'true') {
            $('.renewYes').hide();
        }
    }
});

$('#slctREOTypeOfRegistration').change(function () {
    if ($(this).val() == 'PDF') {
        $('#btnSavePDF2').show();
        $('.renewUploadREO').hide();
        $(this).closest('div').next().show();

        if ($('#slctREOVmsRenewal').val() == 'true') {
            $('.renewUploadREO').show();
            $('.renewREOYes').show();
            $('#btnSavePDFRenewREO').show();

        }
    } else {
        $(this).closest('div').next().hide();
        $('#btnSavePDF2').hide();
        $('#btnSavePDFRenewREO').hide();
        $('.renewUploadREO').hide();
        $('.renewREOYes').show();
        $('#linkPDFREO').text('');
        $('#linkPDFRenewalREO').text('');

        if ($('#slctREOVmsRenewal').val() != 'true') {
            $('.renewREOYes').hide();
        }
    }
});

$('#slctVmsRenewal').change(function () {
    if ($(this).val() == 'false' || $(this).val() == '') {
        $('.renewYes').hide();
        $('#btnSavePDFRenew').hide();
        $('.renewUploadPFC').hide();
        $('#slctRenewEveryNum').val('');
        $('#slctRenewEveryYears').val('');
        $('#slctRenewOnMonths').val('');
        $('#slctRenewOnNum').val('');

    } else {
        $('.renewYes').show();
        $('.renewUploadPFC').hide();
        $('#linkPDFRenewal').text('');
        if ($('#slctTypeOfRegistration').val() == 'PDF') {
            var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);
            $('#btnSavePDFRenew').show();
            $('.renewUploadPFC').show();
            $('.renewYes').show();
        }
        else {
            var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);
            $('.renewUploadPFC').hide();
            $('.renewYes').show();

            $('#btnSavePDFRenew').hide();

        }
    }
});

$('#slctREOVmsRenewal').change(function () {
    if ($(this).val() == 'false' || $(this).val() == '') {
        $('.renewREOYes').hide();
        $('#btnSavePDFRenewREO').hide();
        $('.renewUploadREO').hide();

        $('#slctREORenewEveryNum').val('');
        $('#slctREORenewEveryYears').val('');
        $('#slctREORenewEveryNum').val('');
        $('#slctREORenewEveryNum').val('');
    } else {
        $('#btnSavePDFRenewREO').hide();
        $('#linkPDFRenewalREO').text('');
        $('.renewREOYes').show();
        if ($('#slctREOTypeOfRegistration').val() == 'PDF') {
            var rdRenewal1 = $('input[name=rdREORenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdREORenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);
            $('#btnSavePDFRenewREO').show();
            $('.renewREOYes').show();
            $('.renewUploadREO').show();
        }
        else {
            var rdRenewal1 = $('input[name=rdREORenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdREORenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');


            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', false);
            divRenewOn2.prop('disabled', false);

            $('#btnSavePDFRenewREO').hide();
            $('.renewREOYes').show();
            $('.renewUploadREO').hide();

        }
    }
});

$('#slctReoBankOwed').change(function () {
    if ($(this).val() == 'true') {
        $('#Drpreobanktime').prop('disabled', false);
        $('#inReoBankOwed1').prop('disabled', false);
        $('#slctReoBankOwed2').prop('disabled', false);
        $('#slctReoBankOwed3').prop('disabled', false);
        $('#slctReoBankOwed4').prop('disabled', false);

    } else {
        $('#Drpreobanktime').prop('disabled', true);
        $('#inReoBankOwed1').prop('disabled', true);
        $('#slctReoBankOwed2').prop('disabled', true);
        $('#slctReoBankOwed3').prop('disabled', true);
        $('#slctReoBankOwed4').prop('disabled', true);
    }
});

$('#slctReoDistressedAbandoned').change(function () {
    if ($(this).val() == 'true') {
        $('#DrpReoAbandoned').prop('disabled', false);
        $('#inReoDistressedAbandoned1').prop('disabled', false);
        $('#slctReoDistressedAbandoned2').prop('disabled', false);
        $('#slctReoDistressedAbandoned3').prop('disabled', false);
        $('#slctReoDistressedAbandoned4').prop('disabled', false);
    } else {
        $('#DrpReoAbandoned').prop('disabled', true);
        $('#inReoDistressedAbandoned1').prop('disabled', true);
        $('#slctReoDistressedAbandoned2').prop('disabled', true);
        $('#slctReoDistressedAbandoned3').prop('disabled', true);
        $('#slctReoDistressedAbandoned4').prop('disabled', true);
    }
});

$('#slctReoVacant').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReowith').prop('disabled', false);
        $('#inReoVacantTimeline1').prop('disabled', false);
        $('#slctReoVacantTimeline2').prop('disabled', false);
        $('#slctReoVacantTimeline3').prop('disabled', false);
        $('#slctReoVacantTimeline4').prop('disabled', false);
    } else {
        $('#slctReowith').prop('disabled', true);
        $('#inReoVacantTimeline1').prop('disabled', true);
        $('#slctReoVacantTimeline2').prop('disabled', true);
        $('#slctReoVacantTimeline3').prop('disabled', true);
        $('#slctReoVacantTimeline4').prop('disabled', true);
    }
});

//$('#slctBondReq').change(function () {
//    if ($(this).val() == 'false') {
//        $('#inBondAmount').prop('disabled', true);
//    } else {
//        $('#inBondAmount').prop('disabled', false);
//    }
//});

$('#slctBondReq').change(function () {
    if ($(this).val() == 'true') {
        $('#inBondAmount').prop('disabled', false);
        $('#chkPayAgainWhenREO').prop('disabled', true);
        $('#chkPayUponRenewal').prop('disabled', true);

        $('#slctBondAmount').prop('disabled', true);
        $('#inBondAmount').prop('disabled', true);
        $('#slctBondAmountUSP').prop('disabled', true);
        $('#btnEscaBond').prop('disabled', true);

        chkPayAgainWhenREO

        $('#dvBondReq').show();

    } else {
        $('#inBondAmount').val('');
        $('#inBondAmount').prop('disabled', true);
        $('#chkPayAgainWhenREO').prop('checked', false)
        $('#dvBondReq').hide();
    }
});

//$('#slctRegCost').change(function () {
//    if ($(this).val() == 'false') {
//        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', true);
//        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', true);
//        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', true);
//    } else {
//        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', false);
//        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', false);
//        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', false);
//    }

//    $(this).prop('disabled', false);
//});

$('#slctComFeeStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt2').val($('#inComRegFee').val());
        $('#slctRenewAmtCurr2').val($('#slctComRegFee').val());
        $('#slctRenewCostEscal2').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal2').prop('disabled', false);
    }
});

$('#slctRenewCostEscal2').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt2').prop('disabled', false);
        $('#slctRenewAmtCurr2').prop('disabled', false);
        $('#btnViewEscalRenewAmt2').prop('disabled', true);
    } else {
        $('#inRenewAmt2').prop('disabled', true);
        $('#slctRenewAmtCurr2').prop('disabled', true);
        $('#inRenewAmt2').val('');
        $('#btnViewEscalRenewAmt2').prop('disabled', false);
    }
});

$('#slctRegCostStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt').val($('#inRegCost').val());
        $('#slctRenewAmtCurr1').val($('#slctRegCostCurr').val());
        $('#slctRenewCostEscal').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal').prop('disabled', false);
    }
});

$('#slctRenewCostEscal').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt').prop('disabled', false);
        $('#slctRenewAmtCurr1').prop('disabled', false);
        $('#btnViewEscalRenewAmt').prop('disabled', true);
    } else {
        $('#inRenewAmt').prop('disabled', true);
        $('#slctRenewAmtCurr1').prop('disabled', true);
        $('#inRenewAmt').val('');
        $('#btnViewEscalRenewAmt').prop('disabled', false);
    }
});

$('#slctInsCriOcc').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaOcc').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaOcc').prop('disabled', true);
    }
});

$('#slctInsCriVac').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaVac').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaVac').prop('disabled', true);
    }
});

$('#slctInsFeePay').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsFeePaymentFreq').prop('disabled', false);
    } else {
        $('#btnViewInsFeePaymentFreq').prop('disabled', true);
    }
});

$('#slctInsFeeReq').change(function () {
    if ($(this).val() == 'true') {
        $('#inInsFeeAmt').prop('disabled', false);
        $('#slctInsFeeAmt').prop('disabled', false);
    } else {
        $('#inInsFeeAmt').val('');
        $('#inInsFeeAmt').prop('disabled', true);
        $('#slctInsFeeAmt').prop('disabled', true);
    }
});

$('input[name=rdInsCriteriaOccFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaOccDaily').show();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').show();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').show();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').show();
    }
});

$('input[name=rdInsCriteriaVacFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaVacDaily').show();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').show();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').show();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').show();
    }
});

$('input[name=rdInsFeePayFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsFeePayFreqDaily').show();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').show();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').show();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').show();
    }
});

$('input[name=rdFreq1]:radio').change(function () {
    if ($(this).val() == 'every1') {
        $('#inInsCriteriaOccDay').val('');
    } else if ($(this).val() == 'every2') {
        $("input[name = 'cbFreq']").prop('checked', false);
    }
})

$('#slctHowToDereg').change(function () {

    if ($(this).val() == 'PDF') {
        $('#debtnSavePDF').show();
        $(this).closest('div').next().show();
    } else {
        $(this).closest('div').next().hide();
        $('#debtnSavePDF').hide();
        $('#delinkPDF').text('');
    }
});

$('#pfcReg').change(function () {
    if (this.checked) {
        $('#tabPFC select').prop('disabled', true);
        $('#tabPFC input[type=number]').prop('disabled', true);
        $('#tabPFC input[type=text]').prop('disabled', true);
        $('#tabPFC select').val('');
        $('#tabPFC input[type=number]').val('');
        $('#tabPFC input[type=text]').val('');
    } else {
        $('#tabPFC select').prop('disabled', false);
        $('#tabPFC input[type=number]').prop('disabled', false);
        $('#tabPFC input[type=text]').prop('disabled', false);
        $('#tabPFC select').val('');
        $('#tabPFC input[type=number]').val('');
        $('#tabPFC input[type=text]').val('');
    }
});

$('#reoReg').change(function () {
    if (this.checked) {
        $('#tabREO select').prop('disabled', true);
        $('#tabREO input[type=number]').prop('disabled', true);
        $('#tabREO input[type=text]').prop('disabled', true);
        $('#tabREO select').val('');
        $('#tabREO input[type=number]').val('');
        $('#tabREO input[type=text]').val('');
    } else {
        $('#tabREO select').prop('disabled', false);
        $('#tabREO input[type=number]').prop('disabled', false);
        $('#tabREO input[type=text]').prop('disabled', false);
        $('#tabREO select').val('');
        $('#tabREO input[type=number]').val('');
        $('#tabREO input[type=text]').val('');
    }
});
$('#slctReoCodeViolation').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReoviolationwith').prop('disabled', false);
        $('#slctReoViolationtimeline1').prop('disabled', false);
        $('#slctReoviolationtimeline2').prop('disabled', false);
        $('#slctReoviolationdaystimeline3').prop('disabled', false);
        $('#slctReoviolationvacancytimeline4').prop('disabled', false);
    } else {
        $('#slctReoviolationwith').prop('disabled', true);
        $('#slctReoViolationtimeline1').prop('disabled', true);
        $('#slctReoviolationtimeline2').prop('disabled', true);
        $('#slctReoviolationdaystimeline3').prop('disabled', true);
        $('#slctReoviolationvacancytimeline4').prop('disabled', true);
    }
});

$('#slctReoBoardedOnly').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReoBoardedwith').prop('disabled', false);
        $('#slctReoBoardedtimeline1').prop('disabled', false);
        $('#slctReoBoardedtimeline2').prop('disabled', false);
        $('#slctReoBoardeddaystimeline3').prop('disabled', false);
        $('#slctReoBoardedtimeline4').prop('disabled', false);
    } else {
        $('#slctReoBoardedwith').prop('disabled', true);
        $('#slctReoBoardedtimeline1').prop('disabled', true);
        $('#slctReoBoardedtimeline2').prop('disabled', true);
        $('#slctReoBoardeddaystimeline3').prop('disabled', true);
        $('#slctReoBoardedtimeline4').prop('disabled', true);
    }
});

$('#slctRentalRegistration').change(function () {
    if ($(this).val() == 'true') {
        $('#slctRentalFormwithin').prop('disabled', false);
        $('#slctReorentaltimeline1').prop('disabled', false);
        $('#slctReorentaltimeline2').prop('disabled', false);
        $('#slctReorentaltimeline3').prop('disabled', false);
        $('#slctReorentaltimeline4').prop('disabled', false);
    } else {
        $('#slctRentalFormwithin').prop('disabled', true);
        $('#slctReorentaltimeline1').prop('disabled', true);
        $('#slctReorentaltimeline2').prop('disabled', true);
        $('#slctReorentaltimeline3').prop('disabled', true);
        $('#slctReorentaltimeline4').prop('disabled', true);
    }
});

$('#slctReoOther').change(function () {
    if ($(this).val() == 'true') {
        $('#slctreootherwithin').prop('disabled', false);
        $('#inReoOtherTimeline1').prop('disabled', false);
        $('#slctReoOtherTimeline2').prop('disabled', false);
        $('#slctReoOtherTimeline3').prop('disabled', false);
        $('#slctReoOtherTimeline4').prop('disabled', false);
    } else {
        $('#slctreootherwithin').prop('disabled', true);
        $('#inReoOtherTimeline1').prop('disabled', true);
        $('#slctReoOtherTimeline2').prop('disabled', true);
        $('#slctReoOtherTimeline3').prop('disabled', true);
        $('#slctReoOtherTimeline4').prop('disabled', true);
    }
});

$('#btnSaveOrdinance').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var ordinance_num = $('#tbOrdinanceNum').val();
    var ordinance_name = $('#tbOrdinanceName').val();
    var description = $('#tbDescription').val();
    var section = $('#tbSection').val();
    var source = $('#tbSource').val();
    var enacted_date = "";
    if ($('#dpEnacted').val() == "") {
        enacted_date = "";
    } else {
        enacted_date = $('#dpEnacted').val()
    }
    var revision_date = "";
    if ($('#dpRevision').val() == "") {
        revision_date = "";
    } else {
        revision_date = $('#dpRevision').val();
    }

    var ordinance_file = $('#linkOrdinanceFile').text();

    var id = "";
    var myData = [
       'regOrdinance',
       state,
       city,
       ordinance_num,
       ordinance_name,
       description,
       section,
       source,
       enacted_date,
       revision_date,
       ordinance_file,
    ];

    var btns = $('#tabOrdinance .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });
    var user = $('#usr').text();
    if (count == 0) {

        ordinance();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '", id: "' + id + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    CheckApplyButton();
                    alertify.alert('Success', 'Settings saved successfuly!', function () {


                    });
                } else {
                    alertify.alert('Error in saving to database');
                }
                
                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Warning','Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});


$('#btnOrdinanceFile').click(function () {

    if ($('#fiOrdinanceFile').val() == '') {
        alertify.alert('Please select PDF!')
    } else {
        $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
        serv = "";
        var name = $('#fiOrdinanceFile').val().replace('C:\\fakepath\\', '');
        $('#linkOrdinanceFile').text(name);
        var client = '';
        var myData = [name, client, serv, $('#fiOrdinanceFile').get(0)]
        pdfUpload(myData);

        $('#linkOrdinanceFile').show();
    }

});

function pdflinkOrdinance(val) {
    return '<a data-id="' + val + '" class="btn btn-link" onclick="openPDF(this);">' + val + '</a>';
}

function GetOrdi() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetOrdinanceSettings',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var ordinance = d.data.ordinance;
                var lookregOrd = d.data.check;

                //$('#slctContReg').val(regCont[0].cont_reg);
                //$('#slctUpdateReg').val(regCont[0].update_reg);
                 CheckApplyButton();
                if (ordinance.length > 0) {
                    if (ordinance[0].tag == 'CURRENTLY UNDER REVIEW' || ordinance[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || ordinance[0].tag == 'PENDING FOR APPROVAL' || ordinance[0].tag == null) {
                        $('#tabOrdinance div.tag').remove();
                        $('#tabOrdinance div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforOrdinance = ordinance[0].id;
                        console.log(existingSettingforOrdinance);
                        //console.log(ordinance[0].id);

                        //var revdate = ordinance[0].revision_date;

                        $('#tbOrdinanceNum').val(ordinance[0].ordinance_num);
                        $('#tbOrdinanceName').val(ordinance[0].ordinance_name);
                        $('#tbDescription').val(ordinance[0].description);
                        $('#tbSection').val(ordinance[0].section);
                        $('#tbSource').val(ordinance[0].source);
                        $('#dpEnacted').val(ordinance[0].enacted_date);
                        $('#dpRevision').val(ordinance[0].revision_date);

                        if (ordinance[0].ordinance_file != '' || ordinance[0].ordinance_file != null) {
                            $('#linkOrdinanceFile').text(ordinance[0].ordinance_file);
                            $('#linkOrdinanceFile').show();
                        }

                    }
                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabOrdinance div.valid').remove();
                    $('#tabOrdinance').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                if (lookregOrd != undefined) {
                    if (lookregOrd[0].ordinanceSetting == '2') {
                        //$('#tabPFC div.tag').remove();
                        $('#tabOrdinance div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabOrdinance div.tag').remove();
                        //$('#tabPFC').prepend(
                        //    '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                        //        '<label class="text-green">CURRENTLY ' + regPFC[0].tag + '!</label>' +
                        //    '</div>'
                        //);
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabOrdinance div.tag').remove();
                        $('#tabOrdinance').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-green">' + ordinance[0].tag + '!</label>' +
                            '</div>'
                        );
                    }
                }
                if (ordinance[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabOrdinance div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabOrdinance div.tag').remove();
                    $('#tabOrdinance').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + ordinance[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabOrdinance div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    //$('.btnApply').prop('disable', true);
                }

               
                $('#tabOrdinance').find('div.audit input').prop('disabled', true);
                $('#tabOrdinance').find('div.audit select').prop('disabled', true);
                $('#tabOrdinance').find('div.audit button').prop('disabled', true);
                $('#tabOrdinance').find('div.audit textarea').prop('disabled', true);
                $('#modalLoading').modal('hide');
            }
        }
    });
}

function getHistoryOrdinance() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/getOrdinanceHistory',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                    $('#tblOrdinanceHist').bootstrapTable('destroy');

                    $('#tblOrdinanceHist').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinanceHist thead tr th:eq(3) div:eq(0)').attr('style', 'width: 500px');
                

                $('#modalLoading').modal('hide');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}


function ordinance() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var ordinaceNum = $('#tbOrdinanceNum').val();
    var ordinanceName = $('#tbOrdinanceName').val();
    var desc = $('#tbDescription').val();
    var ordinanceCopy = $('#linkOrdinanceFile').text();
    var section = $('#tbSection').val();
    var source = $('#tbSource').val();
    var enacted = $('#dpEnacted').val();
    var revision = $('#dpRevision').val();
    var user = $('#usr').text();

    var tag = 'ordinance';

    var ordinance = [muniName, muniCode, ordinaceNum, ordinanceName, desc, ordinanceCopy, section, source, enacted, revision];

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(ordinance) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function deregistration() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var deregReq = $('#slctDeregRequired option:selected').text();
    var conveyed = $('#slctConveyed option:selected').text();
    var occupied = $('#slctOccupied option:selected').text();
    var deregister = $('#slctHowToDereg option:selected').text();

    var pdf = (deregister == 'City Registration Form') ? $('#delinkPDF').text(): '';

    var ownerInfo = $('#slctNewOwnerInfoReq option:selected').text();
    var conveyance = $('#slctProofOfConveyReq option:selected').text();
    var saleReq = $('#slctDateOfSaleReq option:selected').text();
    var user = $('#usr').text();

    var tag = 'deregistration';

    var dereg = [muniName, muniCode, deregReq, conveyed, occupied, deregister, pdf, ownerInfo, conveyance, saleReq];

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(dereg) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function municipalityInfo()
{
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var muniDept = $('#inMuniDept').val();
    var phone = $('#inMuniPhone').val();
    var email = $('#inMuniEmail').val();
    var mail = $('#inMuniMailStr').val() + ' ' + $('#slctMuniMailSta option:selected').text() + ' ' + $('#slctMuniMailCt option:selected').text() + ' ' + $('#slctMuniMailZip option:selected').text();
    var website = $('#inWebsite').val();
    var contact = $('#inContact').val();
    var title = $('#inTitle').val();
    var department = $('#inDept').val();
    var phone2 = $('#inPhone').val();
    var emailAdd = $('#inEmail').val();
    var mailAdd = $('#inMailing').val();
    var from = $('#inHrsFrom').val();
    var to = $('#inHrsTo').val();

    var user = $('#usr').text();

    var tag = 'municipalityInfo'

    var municipality = [
        muniName,
        muniCode,
        muniDept,
        phone,
        email,
        mail,
        website,
        contact,
        title,
        department,
        phone2,
        emailAdd,
        mailAdd,
        from,
        to
    ];

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(municipality) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function contReg() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var contreg1 = $('#slctContReg option:selected').text();
    var contreg2 = $('#slctUpdateReg option:selected').text();

    var user = $('#usr').text();

    var contOriginal = [
            muniName,
            muniCode,
            contreg1,
            contreg2
    ];

    var tag = 'continue';

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(contOriginal) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function propertyTypeAudit() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    
    var residential = $('#slctPropResidential option:selected').text();
    var single = $('#slctSingleFamily option:selected').text();
    var multi = $('#slctMultiFamily option:selected').text();
    var unit2 = $('#slct2units option:selected').text();
    var unit3 = $('#slct3units option:selected').text();
    var unit4 = $('#slct4units option:selected').text();
    var unit5 = $('#slct5units option:selected').text();
    var rental = $('#slctPropRental option:selected').text();
    var commercial = $('#slctPropCommercial option:selected').text();
    var condo = $('#slctPropCondo option:selected').text();
    var townhome = $('#slctPropTownhome option:selected').text();
    var vacant = $('#slctPropVacantLot option:selected').text();
    var mobilehome = $('#slctPropMobilehome option:selected').text();

    var user = $('#usr').text();

    var propOriginal = [
                muniName,
                muniCode,
                residential,
                single,
                multi,
                unit2,
                unit3,
                unit4,
                unit5,
                rental,
                commercial,
                condo,
                townhome,
                vacant,
                mobilehome
    ];
    var tag = 'property';

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(propOriginal) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });

}

function reoAudit() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var bankowned = $('#slctReoBankOwed option:selected').text();
    var bankownedTL = $('#Drpreobanktime option:selected').text() + ' '
                         + $('#inReoBankOwed1').val() + ' '
                         + $('#slctReoBankOwed2 option:selected').text() + ' '
                         + $('#slctReoBankOwed3 option:selected').text() + ' '
                         + $('#slctReoBankOwed4 option:selected').text();

    var vacant = $('#slctReoVacant option:selected').text();
    var vacantTL = $('#slctReowith option:selected').text() + ' '
                         + $('#inReoVacantTimeline1').val() + ' '
                         + $('#slctReoVacantTimeline2 option:selected').text() + ' '
                         + $('#slctReoVacantTimeline3 option:selected').text() + ' '
                         + $('#slctReoVacantTimeline4 option:selected').text();

    var citynotice = $('#slctReoCityNotice option:selected').text();
    var citynoticeTL = $('#slctReocitynoticewith option:selected').text() + ' '
                         + $('#slctReocitytimeline1').val() + ' '
                         + $('#slctReodaystimeline3 option:selected').text() + ' '
                         + $('#slctReodaystimeline3 option:selected').text() + ' '
                         + $('#slctReovacancytimeline4 option:selected').text();

    var codeViolation = $('#slctReoCodeViolation option:selected').text();
    var codeViolationTL = $('#slctReoviolationwith option:selected').text() + ' '
                         + $('#slctReoViolationtimeline1').val() + ' '
                         + $('#slctReoviolationtimeline2 option:selected').text() + ' '
                         + $('#slctReoviolationdaystimeline3 option:selected').text() + ' '
                         + $('#slctReoviolationvacancytimeline4 option:selected').text();

    var codeViolation = $('#slctReoCodeViolation option:selected').text();
    var codeViolationTL = $('#slctReoviolationwith option:selected').text() + ' '
                         + $('#slctReoViolationtimeline1').val() + ' '
                         + $('#slctReoviolationtimeline2 option:selected').text() + ' '
                         + $('#slctReoviolationdaystimeline3 option:selected').text() + ' '
                         + $('#slctReoviolationvacancytimeline4 option:selected').text();

    var boarded = $('#slctReoBoardedOnly option:selected').text();
    var boardedTL = $('#slctReoBoardedwith option:selected').text() + ' '
                         + $('#slctReoBoardedtimeline1').val() + ' '
                         + $('#slctReoBoardedtimeline2 option:selected').text() + ' '
                         + $('#slctReoBoardeddaystimeline3 option:selected').text() + ' '
                         + $('#slctReoBoardedtimeline4 option:selected').text();

    var distress = $('#slctReoDistressedAbandoned option:selected').text();
    var distressTL = $('#DrpReoAbandoned option:selected').text() + ' '
                         + $('#inReoDistressedAbandoned1').val() + ' '
                         + $('#slctReoDistressedAbandoned2 option:selected').text() + ' '
                         + $('#slctReoDistressedAbandoned3 option:selected').text() + ' '
                         + $('#slctReoDistressedAbandoned4 option:selected').text();

    var rental = $('#slctRentalRegistration option:selected').text();
    var rentalTL = $('#slctRentalFormwithin option:selected').text() + ' '
                         + $('#slctReorentaltimeline1').val() + ' '
                         + $('#slctReorentaltimeline2 option:selected').text() + ' '
                         + $('#slctReoDistressedAbandoned3 option:selected').text() + ' '
                         + $('#slctReoDistressedAbandoned4 option:selected').text();
    
    var other = $('#slctReoOther option:selected').text();
    var otherTL = $('#slctreootherwithin option:selected').text() + ' '
                         + $('#inReoOtherTimeline1').val() + ' '
                         + $('#slctReoOtherTimeline2 option:selected').text() + ' '
                         + $('#slctReoOtherTimeline3 option:selected').text() + ' '
                         + $('#slctReoOtherTimeline4 option:selected').text();

    var payment = $('#slctREOPaymentType option:selected').text();

    var typeofreg2 = $('#slctREOTypeOfRegistration option:selected').text();
    var typeofreg3 = $('#linkPDFREO').text();

    var renewal2 = $('#slctREOVmsRenewal option:selected').text();
    var renewal3 = $('#linkPDFRenewalREO').text();
    var renewalEvery = "";
    var renewalOn = "";

    if ($('#Reordvery').is(':checked')) {
        renewalEvery = $('#slctREORenewEveryNum option:selected').text() + ' ' + $('#slctREORenewEveryYears option:selected').text()
    }

    if ($('#Reordon').is(':checked')) {
        renewalOn = $('#slctREORenewOnMonths option:selected').text() + ' ' + $('#slctREORenewOnNum option:selected').text();
    }

    var user = $('#usr').text();

    var reoOriginal = [
                muniName,
                muniCode,
                bankowned,
                bankownedTL,
                vacant,
                vacantTL,
                citynotice,
                citynoticeTL,
                codeViolation,
                codeViolationTL,
                boarded,
                boardedTL,
                distress,
                distressTL,
                rental,
                rentalTL,
                other,
                otherTL,
                payment,
                typeofreg2,
                typeofreg3,
                renewal2,
                renewal3,
                renewalEvery,
                renewalOn
    ];

    var tag = 'reo';

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(reoOriginal) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function pfcAudit() {
    var muniName = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
    var muniCode = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var default2 = $('#slctPfcDefault option:selected').text();
    var default4 = $('#inDefRegTimeline1 option:selected').text() + ' '
                        + $('#slctDefRegTimeline2').val() + ' '
                        + $('#slctDefRegTimeline3 option:selected').text() + ' '
                        + $('#slctDefRegTimeline4 option:selected').text() + ' '
                        + $('#slctDefRegTimeline5 option:selected').text();

    var vacant2 = $('#slctPfcVacantTimeline option:selected').text();
    var vacant4 = $('#inPfcVacantTimeline1 option:selected').text() + ' '
                        + $('#slctPfcVacantTimeline2').val() + ' '
                        + $('#slctPfcVacantTimeline3 option:selected').text() + ' '
                        + $('#slctPfcVacantTimeline4 option:selected').text() + ' '
                        + $('#slctPfcVacantTimeline5 option:selected').text();

    var foreclosure2 = $('#slctPfcForeclosure option:selected').text();
    var foreclosure4 = $('#inPfcDefForclosureTimeline1 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline2').val() + ' '
                        + $('#slctPfcDefForclosureTimeline3 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline4 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline5 option:selected').text();

    var foreclosurevacant2 = $('#slctPfcForeclosureVacant option:selected').text();
    var foreclosurevacant4 = $('#inPfcDefForclosureTimeline1 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline2').val() + ' '
                        + $('#slctPfcDefForclosureTimeline3 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline4 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline5 option:selected').text();

    var citynotice2 = $('#slctPfcCityNotice option:selected').text();
    var citynotice4 = $('#inPfcDefForclosureTimeline1 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline2').val() + ' '
                        + $('#slctPfcDefForclosureTimeline3 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline4 option:selected').text() + ' '
                        + $('#slctPfcDefForclosureTimeline5 option:selected').text();

    var codeviolation2 = $('#slctPfcCityNotice option:selected').text();
    var codeviolation4 = $('#inPfcCodeViolationTimeline1 option:selected').text() + ' '
                        + $('#slctPfcCodeViolationTimeline2').val() + ' '
                        + $('#slctPfcCodeViolationTimeline3 option:selected').text() + ' '
                        + $('#slctPfcCodeViolationTimeline4 option:selected').text() + ' '
                        + $('#slctPfcCodeViolationTimeline5 option:selected').text();

    var boarded2 = $('#slctPfcBoarded option:selected').text();
    var boarded4 = $('#inPfcBoardedTimeline1 option:selected').text() + ' '
                        + $('#slctPfcBoardedTimeline2').val() + ' '
                        + $('#slctPfcBoardedTimeline3 option:selected').text() + ' '
                        + $('#slctPfcBoardedTimeline4 option:selected').text() + ' '
                        + $('#slctPfcBoardedTimeline5 option:selected').text();

    var other2 = $('#slctPfcOther option:selected').text();
    var other4 = $('#inPfcOtherTimeline1 option:selected').text() + ' '
                        + $('#slctPfcOtherTimeline2').val() + ' '
                        + $('#slctPfcOtherTimeline3 option:selected').text() + ' '
                        + $('#slctPfcOtherTimeline4 option:selected').text() + ' '
                        + $('#slctPfcOtherTimeline5 option:selected').text();

    var payment2 = $('#slctPaymentType option:selected').text();

    var typeofreg2 = $('#slctTypeOfRegistration option:selected').text();
    var typeofreg3 = $('#linkPDF').text();

    var renewal2 = $('#slctVmsRenewal option:selected').text();
    var renewal3 = $('#linkPDFRenewal').text();
    var renewalEvery = "";
    var renewalOn = "";

    if ($('#RDEVERY').is(':checked'))
    {
        renewalEvery = $('#slctRenewEveryNum option:selected').text() + ' ' + $('#slctRenewEveryYears option:selected').text()
    }

    if ($('#RDON').is(':checked'))
    {
        renewalOn = $('#slctRenewOnMonths option:selected').text() + ' ' + $('#slctRenewOnNum option:selected').text();
    }

    var user = $('#usr').text();
    
    var pfcOriginal = [
                    muniName,
                    muniCode,
                    default2,
                    default4,
                    vacant2,
                    vacant4,
                    foreclosure2,
                    foreclosure4,
                    foreclosurevacant2,
                    foreclosurevacant4,
                    citynotice2,
                    citynotice4,
                    codeviolation2,
                    codeviolation4,
                    boarded2,
                    boarded4,
                    other2,
                    other4,
                    payment2,
                    typeofreg2,
                    typeofreg3,
                    renewal2,
                    renewal3,
                    renewalEvery,
                    renewalOn
    ];
    var tag = 'pfc';
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/AuditTrail',
        data: '{data: ' + JSON.stringify(pfcOriginal) + ', user: "' + user + '", tag: "' + tag + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                console.log('Success in Audit');
            } else {
                console.log('Error in Audit');
            }

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnAddNewInspection').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 12) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Municipal Inspection Fee');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Municipal Inspection Fee');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Municipal Inspection Fee');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Succeeding fees are the same amount</div>');
        }
    }
});

$('#chkAltisourceForm1').click(function () {
    if (this.checked) {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
        $('#chkUploadForm1').prop('checked', false);
        $('#chkClientFrom1').prop('checked', false);
    }
    else {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
});
$('#chkAltisourceForm2').click(function () {
    if (this.checked) {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
        $('#chkUploadForm2').prop('checked', false);
        $('#chkClientFrom2').prop('checked', false);
    }
    else {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
    }
});
$('#chkAltisourceForm3').click(function () {
    if (this.checked) {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
        $('#chkUploadForm3').prop('checked', false);
        $('#chkClientFrom3').prop('checked', false);
    }
    else {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
});
$('#chkAltisourceForm4').click(function () {
    if (this.checked) {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
        $('#chkUploadForm4').prop('checked', false);
        $('#chkClientFrom4').prop('checked', false);
    }
    else {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
});
$('#chkAltisourceForm5').click(function () {
    if (this.checked) {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
        $('#chkUploadForm5').prop('checked', false);
        $('#chkClientFrom5').prop('checked', false);
    }
    else {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
});
$('#chkAltisourceForm6').click(function () {
    if (this.checked) {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
        $('#chkUploadForm6').prop('checked', false);
        $('#chkClientFrom6').prop('checked', false);
    }
    else {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
});
$('#chkAltisourceForm7').click(function () {
    if (this.checked) {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
        $('#chkUploadForm7').prop('checked', false);
        $('#chkClientFrom7').prop('checked', false);
    }
    else {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
});
$('#chkAltisourceForm8').click(function () {
    if (this.checked) {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
        $('#chkUploadForm8').prop('checked', false);
        $('#chkClientFrom8').prop('checked', false);
    }
    else {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
});
$('#chkAltisourceForm9').click(function () {
    if (this.checked) {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
        $('#chkUploadForm9').prop('checked', false);
        $('#chkClientFrom9').prop('checked', false);
    }
    else {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
});
$('#chkAltisourceForm10').click(function () {
    if (this.checked) {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
        $('#chkUploadForm10').prop('checked', false);
        $('#chkClientFrom10').prop('checked', false);
    }
    else {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }
});
$('#chkClientFrom1').click(function () {
    if (this.checked) {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
        $('#chkUploadForm1').prop('checked', false);
        $('#chkAltisourceForm1').prop('checked', false);
    }
    else {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
});
$('#chkClientFrom2').click(function () {
    if (this.checked) {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
        $('#chkUploadForm2').prop('checked', false);
        $('#chkAltisourceForm2').prop('checked', false);
    }
    else {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
    }
});
$('#chkClientFrom3').click(function () {
    if (this.checked) {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
        $('#chkUploadForm3').prop('checked', false);
        $('#chkAltisourceForm3').prop('checked', false);
    }
    else {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
});
$('#chkClientFrom4').click(function () {
    if (this.checked) {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
        $('#chkUploadForm4').prop('checked', false);
        $('#chkAltisourceForm4').prop('checked', false);
    }
    else {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
});
$('#chkClientFrom5').click(function () {
    if (this.checked) {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
        $('#chkUploadForm5').prop('checked', false);
        $('#chkAltisourceForm5').prop('checked', false);
    }
    else {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
});
$('#chkClientFrom6').click(function () {
    if (this.checked) {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
        $('#chkUploadForm6').prop('checked', false);
        $('#chkAltisourceForm6').prop('checked', false);
    }
    else {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
});
$('#chkClientFrom7').click(function () {
    if (this.checked) {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
        $('#chkUploadForm7').prop('checked', false);
        $('#chkAltisourceForm7').prop('checked', false);
    }
    else {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
});
$('#chkClientFrom8').click(function () {
    if (this.checked) {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
        $('#chkUploadForm8').prop('checked', false);
        $('#chkAltisourceForm8').prop('checked', false);
    }
    else {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
});
$('#chkClientFrom9').click(function () {
    if (this.checked) {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
        $('#chkUploadForm9').prop('checked', false);
        $('#chkAltisourceForm9').prop('checked', false);
    }
    else {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
});
$('#chkClientFrom10').click(function () {
    if (this.checked) {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
        $('#chkUploadForm10').prop('checked', false);
        $('#chkAltisourceForm10').prop('checked', false);
    }
    else {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }
});

$('#chkUploadForm1').click(function () {
    if (this.checked) {
        //$('.dvUploadForm1').show();
        $("#UploadFile1,#btnUpload1").prop("disabled", false);
        $('#chkClientFrom1').prop('checked', false);
        $('#chkAltisourceForm1').prop('checked', false);

    }
    else {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
});
$('#chkUploadForm2').click(function () {
    if (this.checked) {
        $("#UploadFile2,#btnUpload2").prop("disabled", false);
        $('#chkClientFrom2').prop('checked', false);
        $('#chkAltisourceForm2').prop('checked', false);
    }
    else {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);

    }
});
$('#chkUploadForm3').click(function () {
    if (this.checked) {
        $("#UploadFile3,#btnUpload3").prop("disabled", false);
        $('#chkClientFrom3').prop('checked', false);
        $('#chkAltisourceForm3').prop('checked', false);
    }
    else {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
});
$('#chkUploadForm4').click(function () {
    if (this.checked) {
        $("#UploadFile4,#btnUpload4").prop("disabled", false);
        $('#chkClientFrom4').prop('checked', false);
        $('#chkAltisourceForm4').prop('checked', false);
    }
    else {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
});
$('#chkUploadForm5').click(function () {
    if (this.checked) {
        $("#UploadFile5,#btnUpload5").prop("disabled", false);
        $('#chkClientFrom5').prop('checked', false);
        $('#chkAltisourceForm5').prop('checked', false);
    }
    else {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
});
$('#chkUploadForm6').click(function () {
    if (this.checked) {
        $("#UploadFile6,#btnUpload6").prop("disabled", false);
        $('#chkClientFrom6').prop('checked', false);
        $('#chkAltisourceForm6').prop('checked', false);
    }
    else {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
});
$('#chkUploadForm7').click(function () {
    if (this.checked) {
        $("#UploadFile7,#btnUpload7").prop("disabled", false);
        $('#chkClientFrom7').prop('checked', false);
        $('#chkAltisourceForm7').prop('checked', false);
    }
    else {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
});
$('#chkUploadForm8').click(function () {
    if (this.checked) {
        $("#UploadFile8,#btnUpload8").prop("disabled", false);
        $('#chkClientFrom8').prop('checked', false);
        $('#chkAltisourceForm8').prop('checked', false);
    }
    else {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
});
$('#chkUploadForm9').click(function () {
    if (this.checked) {
        $("#UploadFile9,#btnUpload9").prop("disabled", false);
        $('#chkClientFrom9').prop('checked', false);
        $('#chkAltisourceForm9').prop('checked', false);
    }
    else {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
});
$('#chkUploadForm10').click(function () {
    if (this.checked) {
        $("#UploadFile10,#btnUpload10").prop("disabled", false);
        $('#chkClientFrom10').prop('checked', false);
        $('#chkAltisourceForm10').prop('checked', false);
    }
    else {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }
});

$('#slcBondForm').change(function () {

    if ($("#slcBondForm").val() == 'true') {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", false);
    }
    else if ($("#slcBondForm").val() == 'false') {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", true);
        $('#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1').prop('checked', false)
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", true);
        $('#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1').prop('checked', false)
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
});
$('#slcInsurance').change(function () {

    if ($("#slcInsurance").val() == 'true') {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", false);
    }
    else if ($("#slcInsurance").val() == 'false') {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", true);
        $('#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2').prop('checked', false)
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", true);
        $('#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2').prop('checked', false)
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
    }
});
$('#slcMaintenance').change(function () {

    if ($("#slcMaintenance").val() == 'true') {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", false);
    }
    else if ($("#slcMaintenance").val() == 'false') {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", true);
        $('#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3').prop('checked', false)
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", true);
        $('#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3').prop('checked', false)
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
});
$('#slcNonOwner').change(function () {

    if ($("#slcNonOwner").val() == 'true') {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", false);
    }
    else if ($("#slcNonOwner").val() == 'false') {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", true);
        $('#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4').prop('checked', false)
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", true);
        $('#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4').prop('checked', false)
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
});
$('#slcOutOfCountry').change(function () {

    if ($("#slcOutOfCountry").val() == 'true') {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", false);
    }
    else if ($("#slcOutOfCountry").val() == 'false') {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", true);
        $('#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5').prop('checked', false)
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", true);
        $('#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5').prop('checked', false)
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
});
$('#slcProofOf').change(function () {

    if ($("#slcProofOf").val() == 'true') {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", false);
    }
    else if ($("#slcProofOf").val() == 'false') {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", true);
        $('#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6').prop('checked', false)
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", true);
        $('#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6').prop('checked', false)
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
});
$('#slcSignage').change(function () {

    if ($("#slcSignage").val() == 'true') {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", false);
    }
    else if ($("#slcSignage").val() == 'false') {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", true);
        $('#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7').prop('checked', false)
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", true);
        $('#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7').prop('checked', false)
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
});
$('#slcStatement').change(function () {

    if ($("#slcStatement").val() == 'true') {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", false);
    }
    else if ($("#slcStatement").val() == 'false') {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", true);
        $('#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8').prop('checked', false)
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", true);
        $('#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8').prop('checked', false)
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
});
$('#slcStructure').change(function () {

    if ($("#slcStructure").val() == 'true') {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", false);
    }
    else if ($("#slcStructure").val() == 'false') {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", true);
        $('#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9').prop('checked', false)
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", true);
        $('#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9').prop('checked', false)
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
});
$('#slcTrespass').change(function () {

    if ($("#slcTrespass").val() == 'true') {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", false);
    }
    else if ($("#slcTrespass").val() == 'false') {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", true);
        $('#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10').prop('checked', false)
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }
    else {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", true);
        $('#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10').prop('checked', false)
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }

});


function btnslcBondForm() {

    if ($("#slcBondForm").val() == 'true') {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", false);
    }
    else if ($("#slcBondForm").val() == 'false') {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm1,#chkClientFrom1,#chkUploadForm1").attr("disabled", true);
    }
}
function btnslcInsurance() {

    if ($("#slcInsurance").val() == 'true') {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", false);
    }
    else if ($("#slcInsurance").val() == 'false') {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm2,#chkClientFrom2,#chkUploadForm2").attr("disabled", true);
    }
}
function btnslcMaintenance() {

    if ($("#slcMaintenance").val() == 'true') {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", false);
    }
    else if ($("#slcMaintenance").val() == 'false') {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm3,#chkClientFrom3,#chkUploadForm3").attr("disabled", true);
    }
}
function btnslcNonOwner() {

    if ($("#slcNonOwner").val() == 'true') {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", false);
    }
    else if ($("#slcNonOwner").val() == 'false') {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm4,#chkClientFrom4,#chkUploadForm4").attr("disabled", true);
    }
}
function btnslcOutOfCountry() {

    if ($("#slcOutOfCountry").val() == 'true') {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", false);
    }
    else if ($("#slcOutOfCountry").val() == 'false') {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm5,#chkClientFrom5,#chkUploadForm5").attr("disabled", true);
    }
}
function btnslcProofOf() {

    if ($("#slcProofOf").val() == 'true') {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", false);
    }
    else if ($("#slcProofOf").val() == 'false') {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm6,#chkClientFrom6,#chkUploadForm6").attr("disabled", true);
    }
}
function btnslcSignage() {

    if ($("#slcSignage").val() == 'true') {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", false);
    }
    else if ($("#slcSignage").val() == 'false') {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm7,#chkClientFrom7,#chkUploadForm7").attr("disabled", true);
    }
}
function btnslcStatement() {

    if ($("#slcStatement").val() == 'true') {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", false);
    }
    else if ($("#slcStatement").val() == 'false') {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm8,#chkClientFrom8,#chkUploadForm8").attr("disabled", true);
    }
}
function btnslcStructure() {

    if ($("#slcStructure").val() == 'true') {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", false);
    }
    else if ($("#slcStructure").val() == 'false') {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm9,#chkClientFrom9,#chkUploadForm9").attr("disabled", true);
    }
}
function btnslcTrespass() {

    if ($("#slcTrespass").val() == 'true') {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", false);
    }
    else if ($("#slcTrespass").val() == 'false') {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", true);
    }
    else {
        $("#chkAltisourceForm10,#chkClientFrom10,#chkUploadForm10").attr("disabled", true);
    }

}

function btnchkUploadForm1() {
    if ($("#chkUploadForm1").is(':checked')) {
        //$('.dvUploadForm1').show();
        $("#UploadFile1,#btnUpload1").prop("disabled", false);

    }
    else {
        $("#UploadFile1,#btnUpload1").prop("disabled", true);
    }
}
function btnchkUploadForm2() {
    if ($("#chkUploadForm2").is(':checked')) {
        $("#UploadFile2,#btnUpload2").prop("disabled", false);
    }
    else {
        $("#UploadFile2,#btnUpload2").prop("disabled", true);
    }
}
function btnchkUploadForm3() {
    if ($("#chkUploadForm3").is(':checked')) {
        $("#UploadFile3,#btnUpload3").prop("disabled", false);
    }
    else {
        $("#UploadFile3,#btnUpload3").prop("disabled", true);
    }
}
function btnchkUploadForm4() {
    if ($("#chkUploadForm4").is(':checked')) {
        $("#UploadFile4,#btnUpload4").prop("disabled", false);
    }
    else {
        $("#UploadFile4,#btnUpload4").prop("disabled", true);
    }
}
function btnchkUploadForm5() {
    if ($("#chkUploadForm5").is(':checked')) {
        $("#UploadFile5,#btnUpload5").prop("disabled", false);
    }
    else {
        $("#UploadFile5,#btnUpload5").prop("disabled", true);
    }
}
function btnchkUploadForm6() {
    if ($("#chkUploadForm6").is(':checked')) {
        $("#UploadFile6,#btnUpload6").prop("disabled", false);
    }
    else {
        $("#UploadFile6,#btnUpload6").prop("disabled", true);
    }
}
function btnchkUploadForm7() {
    if ($("#chkUploadForm7").is(':checked')) {
        $("#UploadFile7,#btnUpload7").prop("disabled", false);
    }
    else {
        $("#UploadFile7,#btnUpload7").prop("disabled", true);
    }
}
function btnchkUploadForm8() {
    if ($("#chkUploadForm8").is(':checked')) {
        $("#UploadFile8,#btnUpload8").prop("disabled", false);
    }
    else {
        $("#UploadFile8,#btnUpload8").prop("disabled", true);
    }
}
function btnchkUploadForm9() {
    if ($("#chkUploadForm9").is(':checked')) {
        $("#UploadFile9,#btnUpload9").prop("disabled", false);
    }
    else {
        $("#UploadFile9,#btnUpload9").prop("disabled", true);
    }
}
function btnchkUploadForm10() {
    if ($("#chkUploadForm10").is(':checked')) {
        $("#UploadFile10,#btnUpload10").prop("disabled", false);
    }
    else {
        $("#UploadFile10,#btnUpload10").prop("disabled", true);
    }
}

$('#btnSaveAdditionalForms').click(function () {
    var check = [];

    for (var i = 1; i <= 10; i++) {
        check[i] = $('#chkAltisourceForm' + i).is(':checked') ? "Altisource Form" : $('#chkClientFrom' + i).is(':checked') ? "Client Form" : $('#chkUploadForm' + i).is(':checked') ? "Upload Form" : "";    
    }
  

    var myData = [$("#slcBondForm").val(), check[1], $('#ViewValue1').text(),
    $("#slcInsurance").val(), check[2], $('#ViewValue2').text(),
    $("#slcMaintenance").val(), check[3], $('#ViewValue3').text(),
    $("#slcNonOwner").val(), check[4], $('#ViewValue4').text(),
    $("#slcOutOfCountry").val(), check[5], $('#ViewValue5').text(),
    $("#slcProofOf").val(), check[6], $('#ViewValue6').text(),
    $("#slcSignage").val(), check[7], $('#ViewValue7').text(),
    $("#slcStatement").val(), check[8], $('#ViewValue8').text(),
    $("#slcStructure").val(), check[9], $('#ViewValue9').text(),
    $("#slcTrespass").val(), check[10], $('#ViewValue10').text()];

    var btns = $('#tabAddForms .fa-check').closest('button');
    var count = 0;

    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    var user = $('#usr').text();
   
    if (count == 0) {
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/SaveAdditional',
        data: '{data: ' + JSON.stringify(myData) + ',state: "' + state + '", city: "' + city + '", user: "'+user+'"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            if (data.d == '1') {
                CheckApplyButton();
                alertify.alert('Success', 'Settings saved successfuly!', function () {
                });

            } else {
                alertify.alert('Error', 'Error Saving!');
            }


        }, error: function (response) {
            console.log(response.responseText);
        }
    });
    } else {
        alertify.alert('Warning', 'Please check all fields before saving!');
    }

});

$('#btnUpload1').click(function () {
    var fileUpload = $("#UploadFile1").get(0);
    var filenamePDF = $('#UploadFile1').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue1').text(filenamePDF);
    $('#ViewUpload1').show();
    UploadFilePDF(test);
});
$('#btnUpload2').click(function () {
    var fileUpload = $("#UploadFile2").get(0);
    var filenamePDF = $('#UploadFile2').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue2').text(filenamePDF);
    $('#ViewUpload2').show();
    UploadFilePDF(test);
});
$('#btnUpload3').click(function () {
    var fileUpload = $("#UploadFile3").get(0);
    var filenamePDF = $('#UploadFile3').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue3').text(filenamePDF);
    $('#ViewUpload3').show();
    UploadFilePDF(test);
});
$('#btnUpload4').click(function () {
    var fileUpload = $("#UploadFile4").get(0);
    var filenamePDF = $('#UploadFile4').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue4').text(filenamePDF);
    $('#ViewUpload4').show();
    UploadFilePDF(test);
});
$('#btnUpload5').click(function () {
    var fileUpload = $("#UploadFile5").get(0);
    var filenamePDF = $('#UploadFile5').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue5').text(filenamePDF);
    $('#ViewUpload5').show();
    UploadFilePDF(test);
});
$('#btnUpload6').click(function () {
    var fileUpload = $("#UploadFile6").get(0);
    var filenamePDF = $('#UploadFile6').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue6').text(filenamePDF);
    $('#ViewUpload6').show();
    UploadFilePDF(test);
});
$('#btnUpload7').click(function () {
    var fileUpload = $("#UploadFile7").get(0);
    var filenamePDF = $('#UploadFile7').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue7').text(filenamePDF);
    $('#ViewUpload7').show();
    UploadFilePDF(test);
});
$('#btnUpload8').click(function () {
    var fileUpload = $("#UploadFile8").get(0);
    var filenamePDF = $('#UploadFile8').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue8').text(filenamePDF);
    $('#ViewUpload8').show();
    UploadFilePDF(test);
});
$('#btnUpload9').click(function () {
    var fileUpload = $("#UploadFile9").get(0);
    var filenamePDF = $('#UploadFile9').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue9').text(filenamePDF);
    $('#ViewUpload9').show();
    UploadFilePDF(test);
});
$('#btnUpload10').click(function () {
    var fileUpload = $("#UploadFile10").get(0);
    var filenamePDF = $('#UploadFile10').val().replace('C:\\fakepath\\', '');
    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filenamePDF.replace('.pdf', ''), files[i]);
        var textFile = files[i];
    }
    $('#ViewValue10').text(filenamePDF);
    $('#ViewUpload10').show();
    UploadFilePDF(test);
});
function UploadFilePDF(test) {
    $.ajax({
        url: "FileUpload.ashx",
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            alertify.alert('Success',result);
            $('#modalUpload').modal('hide');
            $('#modalLoading').modal('hide');
        },
        error: function (err) {
            alertify.alert(err.responseText);
        }
    });
}


$('#btnView1').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue1').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView2').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue2').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView3').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue3').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView4').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue4').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView5').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue5').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView6').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue6').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView7').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue7').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView8').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue8').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView9').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue9').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnView10').click(function () {
    var filePath = 'Files/PDF Files/' + $('#ViewValue10').text()
    $('#pdfFile').attr('src', filePath);
    $('#modalViewPDF').modal({ backdrop: 'static', keyboard: false });

})
$('#btnDelete1').click(function () {
    $('#ViewValue1').text('');
    $('#ViewUpload1').hide();
})
$('#btnDelete2').click(function () {
    $('#ViewValue2').text('');
    $('#ViewUpload2').hide();
})
$('#btnDelete3').click(function () {
    $('#ViewValue3').text('');
    $('#ViewUpload3').hide();
})
$('#btnDelete4').click(function () {
    $('#ViewValue4').text('');
    $('#ViewUpload4').hide();
})
$('#btnDelete5').click(function () {
    $('#ViewValue5').text('');
    $('#ViewUpload5').hide();
})
$('#btnDelete6').click(function () {
    $('#ViewValue6').text('');
    $('#ViewUpload6').hide();
})
$('#btnDelete7').click(function () {
    $('#ViewValue7').text('');
    $('#ViewUpload7').hide();
})
$('#btnDelete8').click(function () {
    $('#ViewValue8').text('');
    $('#ViewUpload8').hide();
})
$('#btnDelete9').click(function () {
    $('#ViewValue9').text('');
    $('#ViewUpload9').hide();
})
$('#btnDelete10').click(function () {
    $('#ViewValue10').text('');
    $('#ViewUpload10').hide();
})



$('#AddForms').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetAdditionalForms',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);
            if (d.Success) {

                var records = d.data.asd;
                var look = d.data.check;

                if (records.length > 0) {
                    if (records[0].tag == 'CURRENTLY UNDER REVIEW' || records[0].tag == 'CURRENTLY UNDER FINAL REVIEW' || records[0].tag == 'PENDING FOR APPROVAL') {
                        $('#tabAddForms div.tag').remove();
                        $('#tabAddForms div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        existingSettingforAddForms = records[0].id;
                        console.log(existingSettingforAddForms);
                        if (records[0].BondForm_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm1').prop('checked', true);
                        } else if (records[0].BondForm_Check1 == "Client Form") {
                            $('#chkClientFrom1').prop('checked', true);
                        } else if (records[0].BondForm_Check1 == "Upload Form") {
                            $('#chkUploadForm1').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm1').prop('cheked', false);
                            $('#chkClientFrom1').prop('checked', false);
                            $('#chkUploadForm1').prop('checked', false);
                        }

                        if (records[0].InsuranceCer_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm2').prop('checked', true);
                        } else if (records[0].InsuranceCer_Check1 == "Client Form") {
                            $('#chkClientFrom2').prop('checked', true);
                        } else if (records[0].InsuranceCer_Check1 == "Upload Form") {
                            $('#chkUploadForm2').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm2').prop('cheked', false);
                            $('#chkClientFrom2').prop('checked', false);
                            $('#chkUploadForm2').prop('checked', true);
                        }

                        if (records[0].MaintenancePlan_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm3').prop('checked', true);
                        } else if (records[0].MaintenancePlan_Check1 == "Client Form") {
                            $('#chkClientFrom3').prop('checked', true);
                        } else if (records[0].MaintenancePlan_Check1 == "Upload Form") {
                            $('#chkUploadForm3').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm3').prop('cheked', false);
                            $('#chkClientFrom3').prop('checked', false);
                            $('#chkUploadForm3').prop('checked', true);
                        }

                        if (records[0].NonOwner_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm4').prop('checked', true);
                        } else if (records[0].NonOwner_Check1 == "Client Form") {
                            $('#chkClientFrom4').prop('checked', true);
                        } else if (records[0].NonOwner_Check1 == "Upload Form") {
                            $('#chkUploadForm4').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm4').prop('cheked', false);
                            $('#chkClientFrom4').prop('checked', false);
                            $('#chkUploadForm4').prop('checked', true);
                        }

                        if (records[0].OutOfCountry_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm5').prop('checked', true);
                        } else if (records[0].OutOfCountry_Check1 == "Client Form") {
                            $('#chkClientFrom5').prop('checked', true);
                        } else if (records[0].OutOfCountry_Check1 == "Upload Form") {
                            $('#chkUploadForm5').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm5').prop('cheked', false);
                            $('#chkClientFrom5').prop('checked', false);
                            $('#chkUploadForm5').prop('checked', true);
                        }

                        if (records[0].ProofOfUtilities_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm6').prop('checked', true);
                        } else if (records[0].ProofOfUtilities_Check1 == "Client Form") {
                            $('#chkClientFrom6').prop('checked', true);
                        } else if (records[0].ProofOfUtilities_Check1 == "Upload Form") {
                            $('#chkUploadForm6').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm6').prop('cheked', false);
                            $('#chkClientFrom6').prop('checked', false);
                            $('#chkUploadForm6').prop('checked', true);
                        }

                        if (records[0].SignageForm_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm7').prop('checked', true);
                        } else if (records[0].SignageForm_Check1 == "Client Form") {
                            $('#chkClientFrom7').prop('checked', true);
                        } else if (records[0].SignageForm_Check1 == "Upload Form") {
                            $('#chkUploadForm7').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm7').prop('cheked', false);
                            $('#chkClientFrom7').prop('checked', false);
                            $('#chkUploadForm7').prop('checked', true);
                        }

                        if (records[0].Statement_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm8').prop('checked', true);
                        } else if (records[0].Statement_Check1 == "Client Form") {
                            $('#chkClientFrom8').prop('checked', true);
                        } else if (records[0].Statement_Check1 == "Upload Form") {
                            $('#chkUploadForm8').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm8').prop('cheked', false);
                            $('#chkClientFrom8').prop('checked', false);
                            $('#chkUploadForm8').prop('checked', true);
                        }

                        if (records[0].Structure_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm9').prop('checked', true);
                        } else if (records[0].Structure_Check1 == "Client Form") {
                            $('#chkClientFrom9').prop('checked', true);
                        } else if (records[0].Structure_Check1 == "Upload Form") {
                            $('#chkUploadForm9').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm9').prop('cheked', false);
                            $('#chkClientFrom9').prop('checked', false);
                            $('#chkUploadForm9').prop('checked', true);
                        }

                        if (records[0].TrespassAffidavit_Check1 == "Altisource Form") {
                            $('#chkAltisourceForm10').prop('checked', true);
                        } else if (records[0].TrespassAffidavit_Check1 == "Client Form") {
                            $('#chkClientFrom10').prop('checked', true);
                        } else if (records[0].TrespassAffidavit_Check1 == "Upload Form") {
                            $('#chkUploadForm10').prop('checked', true);
                        } else {
                            $('#chkAltisourceForm10').prop('cheked', false);
                            $('#chkClientFrom10').prop('checked', false);
                            $('#chkUploadForm10').prop('checked', true);
                        }


                        $.each(records, function (idx, val) {
                            $('#slcBondForm').val(val.BondForm);
                            btnslcBondForm();

                            if (val.BondForm == 'true') {
                                $('#chkUploadForm1').prop('checked', true);
                                //btnchkUploadForm1();
                            }

                            if (!val.BondForm_File == '') {
                                $('#ViewValue1').text(val.BondForm_File);
                                $('#ViewUpload1').show();
                            }
                            else {
                                $('#ViewUpload1').hide();
                            }

                            $('#slcInsurance').val(val.InsuranceCer);
                            btnslcInsurance();

                            if (val.InsuranceCer == 'true') {
                                $('#chkUploadForm2').prop('checked', true);
                                //btnchkUploadForm2();
                            }

                            if (!val.InsuranceCer_File == '') {
                                $('#ViewValue2').text(val.InsuranceCer_File);
                                $('#ViewUpload2').show();
                                $('#chkUploadForm2').prop('checked', true);
                                //btnchkUploadForm2();
                            }
                            else {
                                $('#ViewUpload2').hide();
                            }

                            $('#slcMaintenance').val(val.MaintenancePlan);
                            btnslcMaintenance();

                            if (val.MaintenancePlan == 'true') {
                                $('#chkUploadForm3').prop('checked', true);
                                //btnchkUploadForm3();
                            }

                            if (!val.MaintenancePlan_File == '') {
                                $('#ViewValue3').text(val.MaintenancePlan_File);
                                $('#ViewUpload3').show();
                                $('#chkUploadForm3').prop('checked', true);
                                //btnchkUploadForm3();
                            }
                            else {
                                $('#ViewUpload3').hide();
                            }


                            $('#slcNonOwner').val(val.NonOwner);
                            btnslcNonOwner();

                            if (val.NonOwner == 'true') {
                                $('#chkUploadForm4').prop('checked', true);
                                //btnchkUploadForm4();
                            }

                            if (!val.NonOwner_File == '') {
                                $('#ViewValue4').text(val.NonOwner_File);
                                $('#ViewUpload4').show();
                                $('#chkUploadForm4').prop('checked', true);
                                //btnchkUploadForm4();
                            }
                            else {
                                $('#ViewUpload4').hide();
                            }

                            $('#slcOutOfCountry').val(val.OutOfCountry);
                            btnslcNonOwner();

                            if (val.OutOfCountry == 'true') {
                                $('#chkUploadForm5').prop('checked', true);
                                // btnchkUploadForm5();
                            }

                            if (!val.OutOfCountry_File == '') {
                                $('#ViewValue5').text(val.OutOfCountry_File);
                                $('#ViewUpload5').show();
                                $('#chkUploadForm5').prop('checked', true);
                                //btnchkUploadForm5();
                            }
                            else {
                                $('#ViewUpload5').hide();
                            }

                            $('#slcProofOf').val(val.ProofOfUtilities);
                            btnslcProofOf();

                            if (val.ProofOfUtilities == 'true') {
                                $('#chkUploadForm6').prop('checked', true);
                                //btnchkUploadForm6();
                            }

                            if (!val.ProofOfUtilities_File == '') {
                                $('#ViewValue6').text(val.ProofOfUtilities_File);
                                $('#ViewUpload6').show();
                                $('#chkUploadForm6').prop('checked', true);
                                //btnchkUploadForm6();
                            }
                            else {
                                $('#ViewUpload6').hide();
                            }

                            $('#slcSignage').val(val.SignageForm);
                            btnslcSignage();

                            if (val.SignageForm == 'true') {
                                $('#chkUploadForm7').prop('checked', true);

                                //btnchkUploadForm7();
                            }

                            if (!val.SignageForm_File == '') {
                                $('#ViewValue7').text(val.SignageForm_File);
                                $('#ViewUpload7').show();
                                $('#chkUploadForm7').prop('checked', true);
                                //btnchkUploadForm7();
                            }
                            else {
                                $('#ViewUpload7').hide();
                            }


                            $('#slcStatement').val(val.Statement);
                            btnslcStatement();

                            if (val.Statement == 'true') {
                                $('#chkUploadForm8').prop('checked', true);
                                //btnchkUploadForm8();
                            }

                            if (!val.Statement_File == '') {
                                $('#ViewValue8').text(val.Statement_File);
                                $('#ViewUpload8').show();
                                $('#chkUploadForm8').prop('checked', true);
                                //btnchkUploadForm8();
                            }
                            else {
                                $('#ViewUpload8').hide();
                            }

                            $('#slcStructure').val(val.Structure);
                            btnslcStructure();

                            if (val.Structure == 'true') {
                                $('#chkUploadForm9').prop('checked', true);
                                //btnchkUploadForm9();
                            }

                            if (!val.Structure_File == '') {
                                $('#ViewValue9').text(val.Structure_File);
                                $('#ViewUpload9').show();
                                $('#chkUploadForm9').prop('checked', true);
                                //btnchkUploadForm9();
                            }
                            else {
                                $('#ViewUpload9').hide();
                            }


                            $('#slcTrespass').val(val.TrespassAffidavit);
                            btnslcTrespass();

                            if (val.TrespassAffidavit == 'true') {
                                $('#chkUploadForm10').prop('checked', true);
                                //btnchkUploadForm10();
                            }

                            if (!val.TrespassAffidavit_File == '') {
                                $('#ViewValue10').text(val.TrespassAffidavit_File);
                                $('#ViewUpload10').show();
                                $('#chkUploadForm10').prop('checked', true);
                                //btnchkUploadForm10();
                            }
                            else {
                                $('#ViewUpload10').hide();
                            }



                            btnchkUploadForm1();
                            btnchkUploadForm2();
                            btnchkUploadForm3();
                            btnchkUploadForm4();
                            btnchkUploadForm5();
                            btnchkUploadForm6();
                            btnchkUploadForm7();
                            btnchkUploadForm8();
                            btnchkUploadForm9();
                            btnchkUploadForm10();





                        });
                        
                        
                    }
                }else{
                    for (i = 1; i < 10; i++) {
                        $("#UploadFile" + i + ",#btnUpload" + i).prop("disabled", true);
                        $('#ViewUpload' + i).hide();
                    }

                }
                $('#modalLoading').modal('hide');
                btnchkUploadForm1();
                btnchkUploadForm2();
                btnchkUploadForm3();
                btnchkUploadForm4();
                btnchkUploadForm5();
                btnchkUploadForm6();
                btnchkUploadForm7();
                btnchkUploadForm8();
                btnchkUploadForm9();
                btnchkUploadForm10();

                $('#tabAddForms').find('div.audit radio').prop('disabled', true);
                $('#tabAddForms').find('div.audit select').prop('disabled', true);
                $('#tabAddForms').find('div.audit button').prop('disabled', true);


                if (look != undefined) {
                    if (look[0].additionalSetting == '2') {
                        $('#tabAddForms div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabAddForms div.tag').remove();
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-times').closest('button').show();
                    }
                    if (sessionStorage.getItem('applyThree') == 'true') {
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabAddForms div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#tabAddForms').prepend(
                                '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                    '<label class="text-green">' + records[0].tag + '!</label>' +
                                '</div>'
                            );

                    }
                }
                if (records[0].tag == 'CURRENTLY UNDER FINAL REVIEW') {
                    $('#tabAddForms div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabAddForms div.tag').remove();
                    $('#tabAddForms').prepend(
                        '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                            '<label class="text-red">' + records[0].tag + '!</label>' +
                        '</div>'
                    );
                    $('#tabAddForms div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabAddForms div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }
        
                CheckApplyButton();
        }

        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
});

var countBond = 2;

function CountSt(i) {
    var j = i % 10,
        k = i % 100;
    var ii;
    if (j == 1 && k != 11) {
        return ii = i + "st";
    }
    if (j == 2 && k != 12) {
        return ii = i + "nd";
    }
    if (j == 3 && k != 13) {
        return ii = i + "rd";
    }
    return ii = i + "th";
}
$('#btnBondAppend').click(function () {
    var data = '';

    for (i = countBond; i < 100; i++) {
        data += '<div class="col-xs-12 " id="dvBond' + i + '" style="padding-top:5px">';
        data += '<div class="col-xs-1"> ' + CountSt(i) + ' </div>';
        data += '<div class="col-xs-3">';
        data += '<input id="txtBond' + i + '" type="text" class="form-control input-sm" />';
        data += '</div>';
        data += '<div class="col-xs-3">';
        data += '<select id="slcBond' + i + '" class="form-control input-sm">';
        data += '<option value="" selected="selected">USD</option>';
        data += '</select>';
        data += '</div>';
        data += '</div>';

        break;
    }
    countBond++;


    $("#dvappendBond").append(data);

});
$('#btnEscaBond').click(function () {
    for (i = 2; i < countBond; i++) {
        $("#dvBond" + i).remove();
    }
    countBond = 2
});
$('#btnRemoveBondAppend').click(function () {
    if (countBond != 2) {
        countBond = countBond - 1
        $("#dvBond" + countBond).remove();
    }
});
$('#slctOneTime').change(function () {

    if ($('#slctOneTime').val() == 'true') {
        $('#chkPayAgainWhenREO').prop('disabled', true);
        $('#chkPayUponRenewal').prop('disabled', true);
        $('#chkPayAgainWhenREO').prop('checked', false)
        $('#chkPayUponRenewal').prop('checked', false)

    }
    else if ($('#slctOneTime').val() == 'false') {
        $('#chkPayAgainWhenREO').prop('disabled', false);
        $('#chkPayUponRenewal').prop('disabled', false);

    }
});
function btnslctOneTime() {

    if ($('#slctOneTime').val() == 'true') {
        $('#chkPayAgainWhenREO').prop('disabled', true);
        $('#chkPayUponRenewal').prop('disabled', true);
        $('#chkPayAgainWhenREO').prop('checked', false)
        $('#chkPayUponRenewal').prop('checked', false)

    }
    else if ($('#slctOneTime').val() == 'false') {
        $('#chkPayAgainWhenREO').prop('disabled', false);
        $('#chkPayUponRenewal').prop('disabled', false);

    }
}

$('#chkPayAgainWhenREO').click(function () {
    if (this.checked) {
        $('#chkPayUponRenewal').prop('checked', false)
        $('#inBondAmount').prop('disabled', true)
        $('#slctBondAmountUSP').prop('disabled', true)
        $('#btnEscaBond').prop('disabled', false)
        $('#slctBondAmount').prop('disabled', false)

    }
    else {
        $('#btnEscaBond').prop('disabled', true)
        $('#slctBondAmount').prop('disabled', true)
    }
});



$('#chkPayUponRenewal').click(function () {
    if (this.checked) {
        $('#slctBondAmount').prop('disabled', false)
        $('#chkPayAgainWhenREO').prop('checked', false)
        $('#btnEscaBond').prop('disabled', true)
    }
    else {
        $('#slctBondAmount').prop('disabled', true)
        $('#slctBondAmount').val('')
    }
});
$('#slctBondAmount').change(function () {
    if ($('#slctBondAmount').val() == 'false') {
        $('#inBondAmount').prop('disabled', false);
        $('#slctBondAmountUSP').prop('disabled', false);

    }
    else if ($('#slctBondAmount').val() == 'true') {
        $('#inBondAmount').prop('disabled', true);
        $('#slctBondAmountUSP').prop('disabled', true);
    }
    else {
        $('#inBondAmount').prop('disabled', true);
        $('#slctBondAmountUSP').prop('disabled', true);
    }
});
function btnchkPayAgainWhenREO() {
    if ($('#chkPayAgainWhenREO').is(':checked')) {
        $('#chkPayUponRenewal').prop('checked', false)
        $('#inBondAmount').prop('disabled', true)
        $('#slctBondAmountUSP').prop('disabled', true)
        $('#btnEscaBond').prop('disabled', false)
        $('#slctBondAmount').prop('disabled', false)

    }
    else {
        $('#btnEscaBond').prop('disabled', true)
        $('#slctBondAmount').prop('disabled', true)
    }
}

function btnslctBondReq() {
    if ($('#slctBondReq').val() == 'true') {
        $('#inBondAmount').prop('disabled', false);
        $('#chkPayAgainWhenREO').prop('disabled', true);
        $('#chkPayUponRenewal').prop('disabled', true);

        $('#slctBondAmount').prop('disabled', true);
        $('#inBondAmount').prop('disabled', true);
        $('#slctBondAmountUSP').prop('disabled', true);
        $('#btnEscaBond').prop('disabled', true);

        chkPayAgainWhenREO


        $('#dvBondReq').show();

    } else {
        $('#inBondAmount').val('');
        $('#inBondAmount').prop('disabled', true);
        $('#chkPayAgainWhenREO').prop('checked', false)
        $('#dvBondReq').hide();
    }
}
$('#slctFeePaymentFreq').change(function () {
    if ($(this).val() == 'Weekly') {
        $('#inPayon1').prop('disabled', false);
        $('#inPayon2').prop('disabled', false);
        $('#inPayon3').prop('disabled', false);
        $('#inPayon4').prop('disabled', false);
        $('#inPayon1').val('');
        $('#inPayon2').val('');
        $('#inPayon3').val('');
        $('#inPayon4').val('');
    } else if ($(this).val() == 'Biweekly') {
        $('#inPayon1').prop('disabled', false);
        $('#inPayon2').prop('disabled', false);
        $('#inPayon3').prop('disabled', true);
        $('#inPayon4').prop('disabled', true);
        $('#inPayon1').val('');
        $('#inPayon2').val('');
    } else if ($(this).val() == 'Monthly' || $(this).val() == 'Bimonthly' || $(this).val() == 'Quarterly' ||
        $(this).val() == 'Every 6 Months' || $(this).val() == 'Yearly') {
        $('#inPayon1').prop('disabled', false);
        $('#inPayon2').prop('disabled', true);
        $('#inPayon3').prop('disabled', true);
        $('#inPayon4').prop('disabled', true);
        $('#inPayon1').val('');
    } else {
        $('#inPayon1').prop('disabled', true);
        $('#inPayon2').prop('disabled', true);
        $('#inPayon3').prop('disabled', true);
        $('#inPayon4').prop('disabled', true);
    }
});
function inspectionValue() {
    if ($('#slctMuniInsReq').val() == '') {
        $('#slctOneTimeUponReg').prop('disabled', false);
        $('#slctFeePaymentFreq').prop('disabled', false);
        $('#inPayon1').val('');
        $('#inPayon2').val('');
        $('#inPayon3').val('');
        $('#inPayon4').val('');
        $('#inPayon1').prop('disabled', false);
        $('#inPayon2').prop('disabled', false);
        $('#inPayon3').prop('disabled', false);
        $('#inPayon4').prop('disabled', false);
        $('#slctFreeEscalating').prop('disabled', false);
        $('#inMunicipalInsFee').prop('disabled', false);
        $('#inMunicpalInsFeeCurr').prop('disabled', false);
        $('#btnViewEscalInspection').prop('disabled', false);

        if ($('#slctOneTimeUponReg').val() == 'true') {
            $('#inPayon1').val('');
            $('#inPayon2').val('');
            $('#inPayon3').val('');
            $('#inPayon4').val('');
            $('#slctFreeEscalating').val('');
            $('#slctFreeEscalating').prop('disabled', true);
            $('#btnViewEscalInspection').prop('disabled', true);
            $('#slctFeePaymentFreq').val('');
            $('#slctFeePaymentFreq').prop('disabled', true);
            $('#inPayon1').prop('disabled', true);
            $('#inPayon2').prop('disabled', true);
            $('#inPayon3').prop('disabled', true);
            $('#inPayon4').prop('disabled', true);
        } else {
            $('#inPayon1').prop('disabled', false);
            $('#inPayon2').prop('disabled', false);
            $('#inPayon3').prop('disabled', false);
            $('#inPayon4').prop('disabled', false);
            $('#slctFreeEscalating').prop('disabled', false);
            $('#btnViewEscalInspection').prop('disabled', false);
            $('#slctFeePaymentFreq').prop('disabled', false);

            if ($('#slctFeePaymentFreq').val() == 'Weekly') {
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', false);
                $('#inPayon3').prop('disabled', false);
                $('#inPayon4').prop('disabled', false);
            } else if ($('#slctFeePaymentFreq').val() == 'Biweekly') {
                $('#inPayon1').val('');
                $('#inPayon2').val('');
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', false);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            } else if ($('#slctFeePaymentFreq').val() == 'Monthly' || $('#slctFeePaymentFreq').val() == 'Bimonthly' || $('#slctFeePaymentFreq').val() == 'Quarterly' ||
                $('#slctFeePaymentFreq').val() == 'Every 6 Months' || $('#slctFeePaymentFreq').val() == 'Yearly') {
                $('#inPayon1').val('');
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', true);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            } else {
                $('#inPayon1').prop('disabled', true);
                $('#inPayon2').prop('disabled', true);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            }

            if ($('#slctFreeEscalating').val() == 'true') {
                $('#inMunicipalInsFee').val('');
                $('#inMunicipalInsFee').prop('disabled', true);
                $('#btnViewEscalInspection').prop('disabled', false);
            } else if ($('#slctFreeEscalating').val() == 'false') {
                $('#inMunicipalInsFee').prop('disabled', false);
                $('#btnViewEscalInspection').prop('disabled', true);
                $.each($('#divEscalInspection div.escalInput div.col-xs-12'), function (idx, val) {
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(0) input').val('');
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(1) input').val('');
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(2) input').val('');

                    if ($('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(3)').length != 0) {
                        if ($('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(3) input').is(':checked')) {
                            reg_escal_succeedingVacant = 'false';
                        } else {
                            reg_escal_succeedingVacant = 'false';
                        }
                    } else {
                        reg_escal_succeedingVacant = 'false';
                    }
                });
            } else {
                $('#inMunicipalInsFee').prop('disabled', false);
                $('#btnViewEscalInspection').prop('disabled', false);
            }
        }
    }
    else if ($('#slctMuniInsReq').val() == 'false') {
        $('#inPayon1').val('');
        $('#inPayon2').val('');
        $('#inPayon3').val('');
        $('#inPayon4').val('');
        $('#slctOneTimeUponReg').prop('disabled', true);
        $('#slctFeePaymentFreq').prop('disabled', true);
        $('#inPayon1').prop('disabled', true);
        $('#inPayon2').prop('disabled', true);
        $('#inPayon3').prop('disabled', true);
        $('#inPayon4').prop('disabled', true);
        $('#slctFreeEscalating').prop('disabled', true);
        $('#inMunicipalInsFee').prop('disabled', true);
        $('#inMunicpalInsFeeCurr').prop('disabled', true);
        $('#btnViewEscalInspection').prop('disabled', true);
    }
    else {
        $('#slctOneTimeUponReg').prop('disabled', false);
        $('#slctFeePaymentFreq').prop('disabled', false);
        //$('#inPayon1').val('');
        //$('#inPayon2').val('');
        //$('#inPayon3').val('');
        //$('#inPayon4').val('');
        $('#inPayon1').prop('disabled', false);
        $('#inPayon2').prop('disabled', false);
        $('#inPayon3').prop('disabled', false);
        $('#inPayon4').prop('disabled', false);
        $('#slctFreeEscalating').prop('disabled', false);
        $('#inMunicipalInsFee').prop('disabled', false);
        $('#inMunicpalInsFeeCurr').prop('disabled', false);
        $('#btnViewEscalInspection').prop('disabled', false);

        if ($('#slctOneTimeUponReg').val() == 'true') {
            $('#slctFreeEscalating').val('');
            $('#slctFreeEscalating').prop('disabled', true);
            $('#btnViewEscalInspection').prop('disabled', true);
            $('#slctFeePaymentFreq').prop('disabled', true);
            $('#inPayon1').prop('disabled', true);
            $('#inPayon2').prop('disabled', true);
            $('#inPayon3').prop('disabled', true);
            $('#inPayon4').prop('disabled', true);
        } else {
            $('#inPayon1').prop('disabled', false);
            $('#inPayon2').prop('disabled', false);
            $('#inPayon3').prop('disabled', false);
            $('#inPayon4').prop('disabled', false);
            $('#slctFreeEscalating').prop('disabled', false);
            $('#btnViewEscalInspection').prop('disabled', false);
            $('#slctFeePaymentFreq').prop('disabled', false);

            if ($('#slctFeePaymentFreq').val() == 'Weekly') {
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', false);
                $('#inPayon3').prop('disabled', false);
                $('#inPayon4').prop('disabled', false);
            } else if ($('#slctFeePaymentFreq').val() == 'Biweekly') {
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', false);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            } else if ($('#slctFeePaymentFreq').val() == 'Monthly' || $('#slctFeePaymentFreq').val() == 'Bimonthly' || $('#slctFeePaymentFreq').val() == 'Quarterly' ||
                $('#slctFeePaymentFreq').val() == 'Every 6 Months' || $('#slctFeePaymentFreq').val() == 'Yearly') {
                $('#inPayon1').prop('disabled', false);
                $('#inPayon2').prop('disabled', true);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            } else {
                $('#inPayon1').prop('disabled', true);
                $('#inPayon2').prop('disabled', true);
                $('#inPayon3').prop('disabled', true);
                $('#inPayon4').prop('disabled', true);
            }

            if ($('#slctFreeEscalating').val() == 'true') {
                $('#inMunicipalInsFee').val('');
                $('#inMunicipalInsFee').prop('disabled', true);
                $('#btnViewEscalInspection').prop('disabled', false);
            } else if ($('#slctFreeEscalating').val() == 'false') {
                $('#inMunicipalInsFee').prop('disabled', false);
                $('#btnViewEscalInspection').prop('disabled', true);
                $.each($('#divEscalInspection div.escalInput div.col-xs-12'), function (idx, val) {
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(0) input').val('');
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(1) input').val('');
                    $('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(2) input').val('');

                    if ($('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(3)').length != 0) {
                        if ($('#divEscalInspection div.escalInput div.col-xs-12').find('div:eq(3) input').is(':checked')) {
                            reg_escal_succeedingVacant = 'false';
                        } else {
                            reg_escal_succeedingVacant = 'false';
                        }
                    } else {
                        reg_escal_succeedingVacant = 'false';
                    }
                });
            } else {
                $('#inMunicipalInsFee').prop('disabled', false);
                $('#btnViewEscalInspection').prop('disabled', false);
            }
        }
    }

    if ($('#slctInsUpReq').val() == 'false') {
        $('#slctInsCriOccc').prop('disabled', true);
        $('#slctInsCriVac').prop('disabled', true);
        $('#slctInsFeePay').prop('disabled', true);
    } else {
        $('#slctInsCriOccc').prop('disabled', false);
        $('#slctInsCriVac').prop('disabled', false);
        $('#slctInsFeePay').prop('disabled', false);
    }
}