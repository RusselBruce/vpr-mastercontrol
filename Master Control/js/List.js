﻿function rowClick(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var row = $(val).find('th');
        var rowText = $(val).find('th').text();

        $('#divSettings').fadeIn();

        getList(rowText.substring(0, 3));

        $('#tblFunc tbody tr').removeClass('rowActive');
        $(val).addClass('rowActive');

}

function checkbox(val) {
	return '<input data-id="' + val + '" type="checkbox" class="iCheck chkBoxList" />'
}

function checkbox1(val) {
    return '<input data-id="' + val + '" type="checkbox" class="iCheck chkBoxList1" />'
}

function cbCity(val) {

	if (val == true) {
		return '<input data-id="' + val + '" type="checkbox" checked="checked" class="iCheck cbCity" />'
	} else {
		return '<input data-id="' + val + '" type="checkbox" class="iCheck cbCity" />'
	}
}

function toggle(val) {

    if (val == true) {
        return '<input data-id="' + val + '" type="checkbox" class="toggle" checked="checked" />'
    } else {
        return '<input data-id="' + val + '" type="checkbox" class="toggle" />'
    }

}

$('#liClient').click(function () {

    getList("Client");
});


function getList(type) {
    $.ajax({
        type: 'POST',
        url: 'List.aspx/GetList',
        data: '{type: "' + type + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);


            if (type == "Client")
            {
                if (d.Success) {
                    $('#tblClient').bootstrapTable('destroy');

                    var records = d.data.record;
                    $('#tblClient').bootstrapTable({
                        data: records,
                        height: 360
                    });
                }
            }
            else
            {
                if (d.Success) {
                    $('#tblSettings').bootstrapTable('destroy');

                    var records = d.data.record;
                    $('#tblSettings').bootstrapTable({
                        data: records,
                        height: 360
                    });
                }
            }
            

            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });

            $('.iCheck').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnAdd').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var utilityStat = $('#inUtilityStat').val();
    var desc = $('#taDesc').val();
    var active;
    if ($('#chkBoxActive').is(':checked')) {
        active = "True";
    } else {
        active = "False";
    }
    var type = $('#tblFunc tbody tr.rowActive').find('th').text().substring(0, 3);

    var myData = [
		utilityStat,
		desc,
		active,
		type
    ]

    if (utilityStat != "") {
        
        $.ajax({
            type: 'POST',
            url: 'List.aspx/AddList',
            data: '{data: ' + JSON.stringify(myData) + '}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
            	getList(type);
            	$('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }, failure: function (response) {
                console.log(response.responseText);
            }
        });
    }

    $('.toggle').bootstrapToggle({
        on: 'Active',
        off: 'Inactive'
    });

    $('.iCheck').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('#modalAddSet').modal('hide');

});

$('#btnAdd1').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var utilityStat = $('#inClientName').val();
    var desc = $('#inClientDesc').val();
    var active;
    if ($('#chkBoxActiveClient').is(':checked')) {
        active = "True";
    } else {
        active = "False";
    }
    var type = "Client";

    var myData = [
		utilityStat,
		desc,
		active,
		type
    ]

    if (utilityStat != "") {

        $.ajax({
            type: 'POST',
            url: 'List.aspx/AddList',
            data: '{data: ' + JSON.stringify(myData) + '}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('#inClientName').val("");
                $('#inClientDesc').val("");
                $('#chkBoxActiveClient').prop('checked', 'false');
                getList(type);
                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }, failure: function (response) {
                console.log(response.responseText);
            }
        });
    }

    $('.toggle').bootstrapToggle({
        on: 'Active',
        off: 'Inactive'
    });

    $('.iCheck').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('#modalClient').modal('hide');

});

$('#btnDelete').click(function () {
	var type = $('#tblFunc tbody tr.rowActive').find('th').text().substring(0, 3);
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('.chkBoxList').each(function () {
        if (this.checked) {
            var listID = $(this).attr('data-id');

            if (listID != "") {
                $.ajax({
                    type: 'POST',
                    url: 'List.aspx/DeleteList',
                    data: '{listID: "' + listID + '"}',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                    	getList(type);
                    	$('#modalLoading').modal('hide');
                    }, error: function (response) {
                        console.log(response.responseText);
                    }, failure: function (response) {
                        console.log(response.responseText);
                    }
                });
            } else {
                $(this).closest('tr').remove();
            }
        }
    });
});

$('#btnDelete1').click(function () {
    var type = "Client";
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $('.chkBoxList1').each(function () {
        if (this.checked) {
            var listID = $(this).attr('data-id');

            if (listID != "") {
                $.ajax({
                    type: 'POST',
                    url: 'List.aspx/DeleteList',
                    data: '{listID: "' + listID + '"}',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        getList(type);
                        $('#modalLoading').modal('hide');
                    }, error: function (response) {
                        console.log(response.responseText);
                    }, failure: function (response) {
                        console.log(response.responseText);
                    }
                });
            } else {
                $(this).closest('tr').remove();
            }
        }
    });
});

$(document).ready(function () {
	getState();
});

function getState() {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'List.aspx/GetState',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				$('#tblState tbody').empty();
				$.each(records, function (idx, val) {

					var checked = (val.isStateActive == true ? 'checked="checked"' : '');

					$('#tblState tbody').append(
						'<tr class="rowHover" onclick="getCity(this);">' +
							'<td>' + val.State + '</td>' +
							'<td class="text-center"><input type="checkbox" class="iCheck cbState" ' + checked + ' data-id="' + val.State + '" /></td>' +
						'</tr>'
					);
				});
			}
			$('.iCheck').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			});

			$('.cbState').on('ifToggled', function () {

				var isActive = '';
				if ($(this).is(':checked')) {
					isActive = '1';
				} else {
					isActive = '0';
				}

				var state = $(this).attr('data-id');				

				$.ajax({
					type: 'POST',
					url: 'List.aspx/SetActive',
					data: '{type: "state", isActive: "' + isActive + '", state: "' + state + '", city: "", zip: ""}',
					contentType: 'application/json; charset=utf-8',
					success: function (data) {
						console.log(data);
					}, error: function (response) {
						console.log(response.responseText);
					}, error: function (response) {
						console.log(response.responseText);
					}
				});

			});

			$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function getCity(val) {
	var state = $(val).find('td').text();
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'List.aspx/GetCitySet',
		data: '{state: "' + state + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				console.log(records);
				$('#tblCity').bootstrapTable('destroy');
				$('#tblCity').bootstrapTable({
					data: records
				});
			}
			$('.iCheck').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			});
			$('#modalLoading').modal('hide');
		}, error: function (response) {
			console.log(response.responseText);
		}, failure: function (response) {
			console.log(response.responseText);
		}
	});
}

$('#btnSave').click(function () {
	$('#tblCity tbody tr').each(function () {
		var city = $(this).find('td:eq(0)').text();
		var zip = $(this).find('td:eq(1)').text()
		var isActive = ($(this).find('td:eq(3) div').hasClass('checked') ? '1' : '0');

		$.ajax({
			type: 'POST',
			url: 'List.aspx/SetActive',
			data: '{type: "city", isActive: "' + isActive + '", state: "", city: "' + city + '", zip: "' + zip + '"}',
			contentType: 'application/json; charset=utf-8',
			success: function (data) {
				console.log(data);
			}, error: function (response) {
				console.log(response.responseText);
			}, failure: function (response) {
				console.log(response.responseText);
			}
		});

	});
});

function uploadfile() {
    $("#modalLoading").modal({ backdrop: 'static', keyboard: false });

    var fileUpload = $("#inUploadFile").get(0);
    var files = fileUpload.files;
    var test = new FormData();
    var type = $('#tblFunc tbody tr.rowActive').find('th').text().substring(0, 3);
    var user = $('#user').text();
    var arr = [type, user];
    console.log(arr);
    for (var i = 0; i < files.length; i++) {
        test.append(type, files[i]);
    }

    console.log(test);

    $.ajax({
        url: "UploadFile.ashx",
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            alert(result);
            location.reload();
            $("#modalLoading").modal('hide');
        },
        error: function (err) {
            console.log(err.responseStatus);
        },
        failure: function (dt) {
            console.log(dt.responseText);
        }
    });
}