﻿$(document).ready(function () {

    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	//auditTable();

	$('.datepicker').datepicker({
		autoclose: true
	});

	populateActionBy();

});

$('#btnApply').click(function () {
	auditTable();
});

function fnExcelReport() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j = 0;
    tab = document.getElementById('tblAudit1'); // id of table

    for (j = 0 ; j < tab.rows.length ; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}


function auditTable()
{
	var time = $('#slctTime').val();
	var dtFrom = $('#inDateFrom').val();
	var dtTo = $('#inDateTo').val();
	var action = $('#slctActionBy').val();

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Audit.aspx/auditList',
        data: '{time: "' + time + '", dtFrom: "' + dtFrom + '", dtTo: "' + dtTo + '", action: "'+action+'"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            $('#tblAudit,#tblAudit1').bootstrapTable('destroy');

            $('#tblAudit').bootstrapTable({
                data: data.d.tblAuditList,
                height: 500,
                width: 1000,
                pagination: true
            });
            $('#tblAudit1').bootstrapTable({
                data: data.d.tblAuditList,
            });

            $('#tblAudit').show();

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#slctTime').on('change', function () {
	if ($(this).val() != '') {
		$('#inDateFrom, #inDateTo').val('');
		$('#inDateFrom, #inDateTo').prop('disabled', true);
	} else {
	    $('#inDateFrom, #inDateTo').val('');
	    $('#inDateFrom, #inDateTo').prop('disabled', false);
	}
});

function populateActionBy()
{

    $.ajax({
        type: 'POST',
        url: 'Audit.aspx/actionBy',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('#slctActionBy').empty();

                $('#slctActionBy').append('<option value="" selected="selected">- Select Action By -</option>');
                
               $.each(records, function (idx, val) {
                   $('#slctActionBy').append('<option value="' + val.action + '">' + val.action + '</option>');
               });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function rowStyle(row, index) {

        return {
            css: { "width": "10px" }
        };
}

function cellStyle(value, row, index, field) {
    return {
        classes: 'text-nowrap another-class'
        //css: { "width": "auto" },
    };
}