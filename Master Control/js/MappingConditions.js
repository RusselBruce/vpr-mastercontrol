﻿$(document).ready(function () {
    getList();
    getCreatedCol();
});

function getList() {
    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/GetList',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('#tblCondList').bootstrapTable('destroy');
                $('#tblCondList').bootstrapTable({
                    data: records,
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function custColName(val) {
    var arr = val.split(",");

    var rt = "";

    $.each(arr, function (idx, val) {
        if (val != '') {
            rt += val + "<br /><br />";
        }
    });
    

    return rt;
}

function deleteRow(val) {
    var updt = '<button type="button" class="btn btn-link" data-id="' + val + '" onclick="updtRow($(this))"><i class="fa fa-pencil-square-o"></i></button>';
    var del = '<button type="button" class="btn btn-link text-red" data-id="' + val + '" onclick="delRow($(this))"><i class="fa fa-trash"></i></button>';

    return updt + del;
}

function delRow(val) {
    var dataid = $(val).attr('data-id');
    var usr = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/DeleteRow',
        data: '{id: "' + dataid + '", usr: "' + usr + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            getList();
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function updtRow(val) {
    var dataid = $(val).attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/UpdateRow',
        data: '{dataid: "' + dataid + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('#condList').hide();
                $('#crtCond').show();

                $.each(records, function (idx, val) {
                    $('#inFN').val(records[0].field_name);
                    $('#slctCF').val(records[0].column_name);

                    var closeDiv = $('#slctCF:eq(' + idx + ')').closest('.divC');

                    chngVal(records[0].column_val, closeDiv);

                    $('#slctCond').val(records[0].logical_operator);
                    $('#slctCFVal').val(records[0].column_val);
                    $('#slctCond2').val(records[0].and_or_val);
                    $('#inAddCond').val(records[0].add_cond);
                    $('#inText').val(records[0].text_val);
                    $('#slctYN').val(records[0].yes_no_val);
                    $('#slctCB').val(records[0].chkbox_val);

                    if (records[0].column_name2 != '') {
                        $('.divC:eq(1)').remove();

                        $('#slctCond2').change();
                        $('.divC:eq(1)').find('#slctCF').val(records[0].column_name2);
                        $('.divC:eq(1)').find('#slctCond').val(records[0].logical_operator2);

                        closeDiv = $('.divC:eq(1)');

                        chngVal(records[0].column_val2, closeDiv);

                        $('.divC:eq(1)').find('#slctCFVal').val(records[0].column_val2);
                        $('.divC:eq(1)').find('#slctCond2').val(records[0].and_or_val2);
                    }

                    if (records[0].column_name3 != '') {

                        $('.divC:eq(2)').remove();
                        
                        $('.div:eq(1) #slctCond2').change();
                        $('.divC:eq(2)').find('#slctCF').val(records[0].column_name3);
                        $('.divC:eq(2)').find('#slctCond').val(records[0].logical_operator3);

                        closeDiv = $('.divC:eq(2)');

                        chngVal(records[0].column_val3, closeDiv);

                        $('.divC:eq(2)').find('#slctCFVal').val(records[0].column_val3);
                        $('.divC:eq(2)').find('#slctCond2').val(records[0].and_or_val3);
                    }
                });

                $('#btnUpdt').attr('data-id', dataid);
                $('#btnSave').hide();
                $('#btnUpdt').show();
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#btnAddCond').click(function () {
    $('#condList').hide();
    $('#crtCond').show();

    $('input[type=text]').val('');
    $('select option:eq(0)').prop('selected', true);

    $('#btnSave').show();
    $('#btnUpdt').hide();
});

$('#btnBack').click(function () {
    $('#condList').show();
    $('#crtCond').hide();
});

function btnAddC(val) {

    var lenDivC = $(val).closest('.cond').find('.divC').length;

    if (lenDivC != 3 && $(val).val() != '') {
        var divCond = $(val).closest('.cond');

        divCond.append('<div class="col-xs-12 divC" style="margin-top:1%"></div>');

        var divC = $(val).closest('.cond').find('.divC').last();
        var divCol1 = divC.prev().find('.col-xs-5');

        var clone = divCol1.clone();

        //$(val).hide();
        //$(val).closest('div').find('.bt').show();

        divC.append('<div class="col-xs-1">&nbsp;</div>');

        clone.appendTo(divC);

        divC.find('.if').hide();
        divC.find('.bt').show();

        divC.append('<div class="col-xs-6">&nbsp;</div>');

        if (lenDivC == 1) {
            divCond.find('.divC:last-child .ba').show();
            divCond.find('.divC:last-child .bt').show();
        } else if (lenDivC == 2) {
            divCond.find('.divC:last-child #slctCond2').hide();
            divCond.find('.divC:last-child .ba').hide();
            divCond.find('.divC:last-child .bt').show();
        }
    }

    $(val).closest('.cond').find('.divC:eq(0) .bt').hide();
}

function btnRmvC(val) {

    var lenDivC = $(val).closest('.cond').find('.divC').length;

    var slct1 = $(val).closest('.divC').find('#slctCF').val();
    var slct2 = $(val).closest('.divC').find('#slctCFVal').val();

    if (lenDivC != 1) {
        var closeDivC = $(val).closest('.divC');

        closeDivC.prev().find('.ba').show();
        if (lenDivC == 2) {
            closeDivC.prev().find('.bt').hide();
        }

        closeDivC.remove()
    }

    $('.divC:eq(0) .bt').hide();

    //var lbl = $('#lblFN').text();
    //var rpl = lbl.replace('AND ' + slct1 + ' ' + slct2, '');

    //$('#lblFN').text(rpl);
}

function btnAdd(val) {
    var ct = $('.cond').length;
    var len = $('.cond').length - 1;
    var cond = $(val).closest('.cond');
    var cont = $(val).closest('.cond').find('.divC:eq(0)');

    $('.cont').append('<div class="col-xs-12 cond" style="margin-top:1%; padding: 1% 0%; border: 1px solid black;"></div>');
    cont.clone().appendTo('.cond:last-child');

    $('.cond:eq(' + len + ')').find('.bAdd').hide();

    if (ct >= 1) {
        $('.cond:last-child .divC .bAdd').show();
        $('.cond .divC .bRmv').show();
    }
}

function btnRmv(val) {

    var lenCond = $('.cond').length;

    if (lenCond != 1) {
        var closeCond = $(val).closest('.cond');

        //if ()
        closeCond.prev().find('.bAdd').show();
        if (lenCond == 2) {
            closeCond.prev().find('.bRmv').hide();
        }

        closeCond.remove()
    }

    //$(val).closest('.cond').remove();
}

function getCreatedCol() {
    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/GetColumns',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('#slctCF').empty();
                $('#slctCF').append('<option>--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('#slctCF').append(
                        '<option>' + val.COLUMN_NAME + '</option>'
                    );
                });
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function chngVal(colVal, val2) {
    var div = $(val2).closest('.cond');
    var divC = val2;
    var opt = divC.find('#slctCF option');
    var cfVal;

    $.each(opt, function (idx, val) {
        if ($(this).is(':selected')) {
            cfVal = $(this).text();
        };
    });

    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/GetVal',
        data: '{cfVal: "' + cfVal + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                divC.find('#slctCFVal').empty();
                divC.find('#slctCFVal').append('<option>--Select One--</option>');

                if (cfVal == 'FC/FCH Codes') {

                    var fc = (colVal == 'FC') ? 'selected="selected"' : '';
                    var fch = (colVal == 'FCH') ? 'selected="selected"' : '';

                    divC.find('#slctCFVal').append('<option ' + fc + '>FC</option>');
                    divC.find('#slctCFVal').append('<option ' + fch + '>FCH</option>');
                } else {
                    $.each(records, function (idx, val) {

                        if (records[idx][cfVal] != '') {
                            if (colVal == records[idx][cfVal]) {
                                divC.find('#slctCFVal').append(
                                    '<option selected="selected">' + records[idx][cfVal] + '</option>'
                                );
                            } else {
                                divC.find('#slctCFVal').append(
                                    '<option>' + records[idx][cfVal] + '</option>'
                                );
                            }
                        }
                    });
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function changeVal(val) {
    var div = $(val).closest('.cond');
    var divC = $(val).closest('.divC');
    var opt = divC.find('#slctCF option');
    var cfVal;

    $.each(opt, function (idx, val) {
        if ($(this).is(':selected')) {
            cfVal = $(this).text();
        };
    });

    $.ajax({
        type: 'POST',
        url: 'MappingConditions.aspx/GetVal',
        data: '{cfVal: "' + cfVal + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                divC.find('#slctCFVal').empty();
                divC.find('#slctCFVal').append('<option>--Select One--</option>');

                if (cfVal == 'FC/FCH Codes') {
                    divC.find('#slctCFVal').append('<option>FC</option>');
                    divC.find('#slctCFVal').append('<option>FCH</option>');
                } else {
                    $.each(records, function (idx, val) {
                        if (records[idx][cfVal] != '') {
                            divC.find('#slctCFVal').append(
                                '<option>' + records[idx][cfVal] + '</option>'
                            );
                        }
                    });
                }
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function changeVal2(val) {
    var div = $(val).closest('.cond');
    var divC = div.find('.divC');
    var slct1, slct2;
    //var lbl = div.find('#lblFN');

    //lbl.text('');

    $.each(divC, function (idx, val) {
        slct1 = $(this).find('#slctCF').val();
        slct2 = $(this).find('#slctCFVal').val();

        //if (divC.length > 1 && lbl.text() != '') {
        //    lbl.text(lbl.text() + ' AND ' + slct1 + ' ' + slct2);
        //} else {
        //    lbl.text(slct1 + ' ' + slct2);
        //}
    });

}

$('#btnSave').click(function () {

    var usr = $('#usr').text();

    var ct = 0;

    var field_name = '', col_name = '', logical_optr = '', col_val = '', add_cond = '', text_val = '',
        yes_no_val = '', chkbox_val = '', col_name2 = '', logical_optr2 = '', col_val2 = '', col_name3 = '',
        logical_optr3 = '', col_val3 = '', and_or_val = '', and_or_val2 = '', and_or_val3 = '';

    $.each($('.cond'), function (idx, val) {
        var len = $(this).length;

        var divC = $(this).find('.divC');

        field_name = $(this).find('#inFN').val();

        $.each(divC, function (idx, val) {
            if (idx == 0) {
                col_name = $(this).find('#slctCF').val();
                logical_optr = $(this).find('#slctCond').val();
                col_val = $(this).find('#slctCFVal').val();
                and_or_val = $(this).find('#slctCond2').val();
                add_cond = $(this).find('input[type=text]:eq(1)').val();
                text_val = $(this).find('input[type=text]:eq(2)').val();
                yes_no_val = $(this).find('select:eq(4) option:selected').val();
                chkbox_val = $(this).find('select:eq(5) option:selected').val();
            } else if (idx == 1) {
                col_name2 = $(this).find('#slctCF').val();
                logical_optr2 = $(this).find('#slctCond').val();
                col_val2 = $(this).find('#slctCFVal').val();
                and_or_val2 = $(this).find('#slctCond2').val();
            } else if (idx == 2) {
                col_name3 = $(this).find('#slctCF').val();
                logical_optr3 = $(this).find('#slctCond').val();
                col_val3 = $(this).find('#slctCFVal').val();
                and_or_val3 = $(this).find('#slctCond2').val();
            }
        });

        var myData = [field_name, col_name, logical_optr, col_val, and_or_val, col_name2, logical_optr2, col_val2, and_or_val2,
            col_name3, logical_optr3, col_val3, and_or_val3, add_cond, text_val, yes_no_val, chkbox_val];

        $.ajax({
            type: 'POST',
            url: 'MappingConditions.aspx/SaveConditions',
            data: '{myData: ' + JSON.stringify(myData) + ', usr: "' + usr + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);
                if (idx == (len - 1)) {
                    $('#condList').show();
                    $('#crtCond').hide();
                    getList();
                }
            },
            error: function (response) {
                console.log(response.responseText);
                ct = 0;
            }
        });
    });
});

$('#btnUpdt').click(function () {
    var dataid = $(this).attr('data-id');
    var usr = $('#usr').text();

    var field_name = '', col_name = '', logical_optr = '', col_val = '', add_cond = '', text_val = '',
        yes_no_val = '', chkbox_val = '', col_name2 = '', logical_optr2 = '', col_val2 = '', col_name3 = '',
        logical_optr3 = '', col_val3 = '', and_or_val = '', and_or_val2 = '', and_or_val3 = '';

    $.each($('.cond'), function (idx, val) {
        var len = $(this).length;

        var divC = $(this).find('.divC');

        field_name = $(this).find('#inFN').val();

        $.each(divC, function (idx, val) {
            if (idx == 0) {
                col_name = $(this).find('#slctCF').val();
                logical_optr = $(this).find('#slctCond').val();
                col_val = $(this).find('#slctCFVal').val();
                and_or_val = $(this).find('#slctCond2').val();
                add_cond = $(this).find('input[type=text]:eq(1)').val();
                text_val = $(this).find('input[type=text]:eq(2)').val();
                yes_no_val = $(this).find('select:eq(3) option:selected').val();
                chkbox_val = $(this).find('select:eq(4) option:selected').val();
            } else if (idx == 1) {
                col_name2 = $(this).find('#slctCF').val();
                logical_optr2 = $(this).find('#slctCond').val();
                col_val2 = $(this).find('#slctCFVal').val();
                and_or_val2 = $(this).find('#slctCond2').val();
            } else if (idx == 2) {
                col_name3 = $(this).find('#slctCF').val();
                logical_optr3 = $(this).find('#slctCond').val();
                col_val3 = $(this).find('#slctCFVal').val();
                and_or_val3 = $(this).find('#slctCond2').val();
            }
        });

        var myData = [field_name, col_name, logical_optr, col_val, and_or_val, col_name2, logical_optr2, col_val2, and_or_val2,
             col_name3, logical_optr3, col_val3, and_or_val3, add_cond, text_val, yes_no_val, chkbox_val];

        $.ajax({
            type: 'POST',
            url: 'MappingConditions.aspx/UpdtConditions',
            data: '{myData: ' + JSON.stringify(myData) + ', dataid: "' + dataid + '", usr: "' + usr + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);
                if (idx == (len - 1)) {
                    $('#condList').show();
                    $('#crtCond').hide();
                    getList();
                }
            },
            error: function (response) {
                console.log(response.responseText);
                ct = 0;
            }
        });
    });
});