﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Repository.aspx.cs" Inherits="Master_Control.Repository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Repository</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
				<span><i class="fa fa-map-marker"></i>&nbsp;Mappings</span>
			</li>
			<li class="active">
				<span><i class="fa fa-database"></i>&nbsp;Repository</span>
			</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul id="ulNav" class="nav nav-tabs">
						<li class="active">
							<button href="#tabPDF" data-toggle="tab" type="button" class="btn btn-primary active" style="width: 100px;">PDF</button>
						</li>
						<li>
							<button href="#tabData" data-toggle="tab" type="button" class="btn btn-primary" style="width: 100px;">Data</button>
						</li>
						<li>
							<button href="#tabCustom" data-toggle="tab" type="button" class="btn btn-primary" style="width: 100px;">Custom Data</button>
						</li>
					</ul>
					<div class="tab-content">
						<div id="tabPDF" class="tab-pane active">
							<table id="tblPDF" class="table">
								<thead>
									<tr>
										<th data-field="file_name" data-formatter="pdflink">File Name</th>
										<th data-field="description">Description</th>
										<th data-field="modified_by">Modified By</th>
										<th data-field="date_modified" data-formatter="dateFormat">Modified Date and Time</th>
										<th data-field="id" data-formatter="action">Action</th>
									</tr>
								</thead>
							</table>
						</div>
						<div id="tabData" class="tab-pane fade">
							<table id="tblData" class="table">
								<thead>
									<tr>
										<th data-field="GroupName" data-formatter="datalink">File Name</th>
										<th data-field="Description">Description</th>
										<th data-field="ModifiedBy">Modified By</th>
										<th data-field="DateModified" data-formatter="dateFormat">Modified Date and Time</th>
										<th data-field="GroupId" data-formatter="action">Action</th>
									</tr>
								</thead>
							</table>
						</div>
						<div id="tabCustom" class="tab-pane fade">
							<table id="tblCustom" class="table">
								<thead>
									<tr>
										<th data-field="GroupName" data-formatter="customlink">Group Name</th>
										<th data-field="description">Description</th>
										<th data-field="modified_by">Modified By</th>
										<th data-field="date_modified" data-formatter="dateFormat">Modified Date and Time</th>
										<th data-field="GroupName" data-formatter="action">Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="width: 1000px;">
			<div class="modal-content">
				<div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Document Preview</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<label id="lblPDF"></label>
						</div>
						<div class="col-xs-12">
							<iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Data Preview</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div id="divFieldsData" class="col-xs-12">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalCustom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Data Preview</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div id="divFieldsCustom" class="col-xs-12">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Edit</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-2">
							<label>File Name:</label>
						</div>
						<div class="col-xs-10">
							<input id="inName" type="text" class="form-control input-sm" />
						</div>
					</div>
					<div class="row" style="margin-top: 1%;">
						<div class="col-xs-2">
							<label>Description:</label>
						</div>
						<div class="col-xs-10">
							<textarea id="taDesc" class="form-control input-sm" style="height: 100px; resize: none;"></textarea>
						</div>
					</div>
					<div class="row" style="margin-top: 1%;">
						<div class="col-xs-12 text-right">
							<button id="btnSave" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
	<script src="js/Repository.js"></script>
</asp:Content>
