﻿<%@ Page Title="Prioritization" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Prioritization.aspx.cs" Inherits="Master_Control.Prioritization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		#tblCustom1 tbody tr:hover, #tblCustom2 tbody tr:hover {
			cursor: pointer;
			color: #000;
			background-color: #eff8b3;
		}

		.activeRow {
			color: #fff;
			background-color: #4d6578;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Workflow Prioritization</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">
				<span><i class="fa fa-list-ol"></i>&nbsp;Workflow  Prioritization</span>
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-default">
			<div class="box-body">
				<div class="col-xs-12" style="padding-left: 0px;">
					<div class="panel panel-primary">
						<div class="panel-body">
							<table id="tblPrio" class="table no-border" data-single-select="true">
								<thead>
									<tr>
										<th data-field="id" data-formatter="chkbox"></th>
										<th data-field="title">Workflow Prioritization Title</th>
										<th data-field="description">Description</th>
										<th data-field="created_by">Created By</th>
										<th data-field="created_date">Created Day</th>
										<th data-field="id" data-formatter="action"></th>
									</tr>
								</thead>
								<tbody id="bdtblPrio">
								</tbody>
							</table>
						</div>
						<div class="text-left" style="margin-top: 1%;">
							<button type="button" class="btn btn-link btn-sm" id="newPrioSet"><i class="fa fa-plus"></i>&nbsp;Add New Priority Settings</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalNewAccess">
		<div class="modal-dialog" role="document">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close btnClose"  aria-label="Close"><span aria-hidden="true">×</span></button>
					<label id="panelTitle">Add New Workflow Prioritization Settings</label>
				</div>
				<div class="panel-body" id="mainModal" style="display: none;">
					<div class="col-md-12">
						<label id="prioId" style="display: none;"></label>
						<table class="table no-border">
							<tr>
								<td>Title</td>
								<td>
									<input type="text" id="titleId" class="form-control input-sm" /></td>
								<td>Distribute based on:</td>
							</tr>
							<tr>
								<td>Description</td>
								<td rowspan="4">
									<textarea id="descId" style="margin: 0px; height: 140px; width: 249px; resize: none;" class="form-control input-sm"></textarea></td>
								<td>Location</td>
								<td>
									<select id="locationId" class="form-control input-sm" multiple="multiple">
										<option value="Atlanta">Sacramento</option>
										<option value="Bangalore">Los Angeles</option>
										<option value="Luxembourg">Irvine</option>
										<option value="Manila">Tempe</option>
										<option value="Montevideo">Denver</option>
										<option value="Mumbai">St Louis</option>
                                        <option value="Mumbai">Plano</option>
                                        <option value="Mumbai">Boston</option>
                                        <option value="Mumbai">Southfield</option>
                                        <option value="Mumbai">Endicott</option>
                                        <option value="Mumbai">Fort Washington</option>
                                        <option value="Mumbai">Atlanta</option>
                                        <option value="Mumbai">Montevideo</option>
                                        <option value="Mumbai">Luxembourg</option>
                                        <option value="Mumbai">Mumbai</option>
                                        <option value="Mumbai">Navi Mumbai</option>
                                        <option value="Mumbai">Bangalore</option>
                                        <option value="Mumbai">Manila</option>
									</select></td>
                                <td>Business Unit</td>
								<td>
									<select id="buId" style="max-height:100px" class="form-control input-sm" multiple="multiple">
										<option value="">Facilities</option>
									<option value="">RHSS Rentals</option>
                                        <option value="">Revel</option>
                                        <option value="">Business Support Group</option>
                                        <option value="">Costumer Solutions</option>
                                        <option value="">Corporate</option>
                                        <option value="">Hubzu</option>
                                        <option value="">Opex</option>
                                        <option value="">Property Registration</option>
                                        <option value="">Training</option>
                                        <option value="">Property Preservation and Inspection</option>
                                        <option value="">AFS</option>
                                        <option value="">Choice</option>
                                        <option value="">HR</option>
                                        <option value="">Short Sale</option>
                                        <option value="">Human Resources</option>
                                        <option value="">Finance</option>
                                        <option value="">Recruitment</option>
                                        <option value="">Rental Property Management</option>
                                        <option value="">Inside Sales</option>
                                        <option value="">Technology</option>
									</select>

								</td>

								<%--<td>Business Segment</td>
								<td>
									<select id="bsId" class="form-control input-sm" multiple="multiple">
										<option value="Consumer Real Estate Solutions">Consumer Real Estate Solutions</option>
										<option value="Origination Solutions">Origination Solutions</option>
										<option value="Real Estate Investor Solutions">Real Estate Investor Solutions</option>
										<option value="Servicer Solutions">Servicer Solutions</option>
									</select></td>--%>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td>Level</td>
								<td>
									<select id="levelId" class="form-control input-sm" multiple="multiple">
										<option value="EE">EE</option>
										<option value="TL">TL</option>
										<option value="MNGR">MNGR</option>
										<option value="DIR">DIR</option>
										<option value="VP">VP</option>
									</select></td>
									<td>Activate until:</td>
								<td>
									<input type="text" class="form-control input-sm datepicker" id="actuntilId" /></td>
								<td>
									<input type="checkbox" id="dneChk" />DO NOT END</td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td>Activate on:</td>
								<td>
									<input type="text" class="form-control input-sm datepicker" id="actonId" /></td>
							
							</tr>
							<tr>
								<td colspan="2"></td>
								<td>Keep Active Every:</td>
								<td>
									<select id="keepactId" class="form-control input-sm" multiple="multiple">
										<option value="Sunday">Sunday</option>
										<option value="Monday">Monday</option>
										<option value="Tuesday">Tuesday</option>
										<option value="Wednesday">Wednesday</option>
										<option value="Thursday">Thursday</option>
										<option value="Friday">Friday</option>
										<option value="Saturday">Saturday</option>
									</select></td>
								<%--<td>Users
								</td>
								<td>
									<select id="usersId" class="form-control input-sm" multiple="multiple">
										<option value="sainibha">sainibha</option>
										<option value="eyajoel">eyajoel</option>
										<option value="salomeoj">salomeoj</option>
										<option value="orculloj">orculloj</option>
										<option value="saludare">saludare</option>
									</select>
								</td>--%>
							</tr>
						</table>

					</div>
					<div class="row">
						<div class="col-xs-2" style="padding-left: 10px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label>Parameters</label>
								</div>
								<div class="panel-body">
									<ul style="list-style-type: none; margin: 0; padding: 0;">
										<li>Client</li>
										<li>Occupancy Status</li>
										<li>SLA</li>
										<li>State</li>
										<li>City</li>
										<li>Zip code</li>
										<li>Total Aging</li>
										<%--<li>Days left</li>--%>
										<li>Property ID</li>
										<%--<li>Property #</li>--%>
										<li>Product</li>
										<li>VPR Service Type</li>
										<li>Property  Status</li>
										<li>VPR Status</li>
										<li>Created Date</li>
										<li>VPR Status Age</li>
										<li>Client Status</li>
										<%--<li>Assigned Associate</li>--%>
										<li>Property Address</li>
										<li>Vendor</li>
										<li>Municipality Name</li>
										<%--<li>POD</li>--%>
										<%--<li>Follow up Date</li>--%>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-10" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label>Workflow Prioritization List</label>
								</div>
								<div class="panel-body">
									<table id="tblPrioList" class="table no-border">
										<tbody style="text-align: center;">
										</tbody>
									</table>
									<div>
										<a id="lnkAddNew" style="cursor: pointer; font-style: italic;"><i class="fa fa-plus"></i>&nbsp;Add New</a>
									</div>
								
                                        <button id="btnSave" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                        <button id="btnDelete" type="button" class="btn btn-primary btn-sm"><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
                                    </div>
								</div>
							</div>
						</div>
					</div>

					<div class="text-center">
						<button id="btnNew_Type" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
					</div>


				<div class="panel-body" id="custom1" style="display: none;">
					<div class="col-xs-5">
						<div class="panel panel-default">
							<div class="panel-body" style="height: 550px; overflow: auto;">
								<table id="tblCustom1" class="table no-border noPadMar">
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-2">
						<button type="button" class="btn btn-link" style="margin-top: 150px" onclick="leftArrow();"><i class="fa fa-arrow-left fa-2x"></i></button>
						<br />
						<button type="button" class="btn btn-link" style="margin-top: 30px" onclick="rightArrow();"><i class="fa fa-arrow-right fa-2x"></i></button>
						<br />
						<button type="button" class="btn btn-primary btn-sm" style="margin-top: 30px; width: 50px" onclick="allActive();">All</button>
						<br />
						<button type="button" class="btn btn-primary btn-sm" style="margin-top: 30px; width: 50px" onclick="clearActive();">Clear</button>
					</div>
					<div class="col-xs-5">
						<div class="panel panel-default">
							<div class="panel-body" style="height: 550px; overflow: auto;">
								<table id="tblCustom2" class="table no-border">
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="text-right" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm btnSave"><i class="fa fa-save"></i>&nbsp;Save</button>
					</div>
				</div>
				<div class="panel-body" id="custom2" style="display: none;">
					<table id="tblCustDays" class="table no-border">
						<tbody>
							<tr>
								<td>
									<input type="text" class="form-control input-sm" />
								</td>
								<td>
									<select class="form-control input-sm">
										<option value="" selected="selected"></option>
										<option value="">Calendar</option>
										<option value="">Business</option>
										<option value="">Fiscal</option>
									</select>
								</td>
								<td>
									<select class="form-control input-sm">
										<option value="" selected="selected"></option>
										<option value="">Days</option>
										<option value="">Weeks</option>
										<option value="">Months</option>
										<option value="">Years</option>
									</select>
								</td>
								<td>
									<label>From</label>
								</td>
								<td>
									<select class="form-control input-sm">
										<option value="" selected="selected"></option>
										<option value="">Boarded Date</option>
										<option value="">Vacancy</option>
										<option value="">City Notice</option>
										<option value="">Foreclosure</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="text-right" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm btnSave"><i class="fa fa-save"></i>&nbsp;Save</button>
					</div>
				</div>
                
				</div>
			</div>
		</div>
<%--	</div>--%>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
	<script src="js/Prioritization.js"></script>
</asp:Content>
