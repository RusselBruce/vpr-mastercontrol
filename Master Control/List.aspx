﻿<%@ Page Title="Data List" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Master_Control.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		.rowHover:hover {
			cursor: pointer;
			background-color: #eff8b3;
		}

		.rowActive {
			background-color: #4d6578;
			color: #fff;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Field Labels</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">
				<span><i class="fa fa-list-alt"></i>&nbsp;Field Labels</span>
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-default">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tabStatSet" data-toggle="tab">Status Settings</a>
						</li>
						<li>
							<a href="#tabCity" data-toggle="tab">City, State and Zip</a>
						</li>
                        <li>
							<a href="#tabClient" data-toggle="tab" id="liClient">Client</a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
                    <div id="tabClient" class="tab-pane">
                        <div class="col-xs-12" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label>Settings</label>
								</div>
								<div id="divClient" class="panel-body">
									<table id="tblClient" class="table no-border">
										<thead>
											<tr>
												<th data-field="id" data-formatter="checkbox1"></th>
												<th data-field="title">Client Name</th>
												<th data-field="description">Description</th>
												<th data-field="isActive" data-formatter="toggle">Show</th>
											</tr>
										</thead>
									</table>
									<div class="text-left" style="margin-top: 1%;">
										<button id="lnkAdd1" data-toggle="modal" href="#modalClient" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New Settings</button>
									</div>
									<div class="text-right" style="margin-top: 1%;">
										<button id="btnUpload1" type="button" class="btn btn-primary btn-sm" data-toggle="modal" href="#modalChooseFile"><i class="fa fa-upload"></i>&nbsp;Upload Files</button>
										<button id="btnDelete1" type="button" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i>&nbsp;Delete</button>
									</div>
								</div>
							</div>
						</div>
                    </div>
					<div id="tabStatSet" class="tab-pane active">
						<div class="col-xs-2" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label>Functions</label>
								</div>
								<div class="panel-body">
									<table id="tblFunc" class="table">
										<tbody>
											<tr class="rowHover" onclick="rowClick(this);">
												<th>REO Dropdown List</th>
											</tr>
											<tr class="rowHover" onclick="rowClick(this);">
												<th>PFC Dropdown List</th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-xs-10" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label>Settings</label>
								</div>
								<div id="divSettings" class="panel-body" style="display: none;">
									<table id="tblSettings" class="table no-border">
										<thead>
											<tr>
												<th data-field="id" data-formatter="checkbox"></th>
												<th data-field="title">Title</th>
												<th data-field="description">Description</th>
												<th data-field="isActive" data-formatter="toggle">Show</th>
											</tr>
										</thead>
									</table>
									<div class="text-left" style="margin-top: 1%;">
										<button id="lnkAdd" data-toggle="modal" href="#modalAddSet" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New Settings</button>
									</div>
									<div class="text-right" style="margin-top: 1%;">
										<button id="btnUpload" type="button" class="btn btn-primary btn-sm" data-toggle="modal" href="#modalChooseFile"><i class="fa fa-upload"></i>&nbsp;Upload Files</button>
										<button id="btnDelete" type="button" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i>&nbsp;Delete</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="tabCity" class="tab-pane">
						<div class="col-xs-3" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-body noPadMar" style="height: 610px; overflow-y: scroll">
									<table id="tblState" class="table no-border noPadMar">
										<thead>
											<tr style="color: #fff; background-color: #4d6578;" class="text-nowrap">
												<th>States</th>
												<th>Set as Active</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-xs-9" style="padding-left: 0px;">
							<div class="panel panel-primary">
								<div class="panel-heading text-center">
									<label id="lblCity">City</label>
								</div>
								<div class="panel-body noPadMar" style="height: 534px; overflow-y: scroll">
									<table id="tblCity" class="table no-border able-hover" data-single-select="true">
										<thead>
											<tr>
												<th data-sortable="true" data-field="City">City</th>
												<th data-sortable="true" data-field="Zip">Zip Code</th>
												<th data-sortable="true" data-field="County">County</th>
												<th data-field="isCityActive" data-formatter="cbCity">Set as Active</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>

								</div>
								<div class="text-right" style="margin-top: 1%;">
									<button id="btnSave" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
									<%--<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-times"></i>&nbsp;Cancel</button>--%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

	<div class="modal" id="modalAddSet" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add New Settings</h4>
				</div>
				<div class="modal-body">
					<table class="table no-border">
						<tbody>
							<tr>
								<th>Utility Status</th>
								<td>
									<input id="inUtilityStat" type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Description</th>
								<td>
									<textarea id="taDesc" class="form-control"></textarea>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<input id="chkBoxActive" type="checkbox" class="iCheck" />&nbsp; I want to show this in the list
								</td>
							</tr>
							<tr>
								<td colspan="2" class="text-center">
									<button id="btnAdd" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

    <div class="modal" id="modalClient" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add New Settings</h4>
				</div>
				<div class="modal-body">
					<table class="table no-border">
						<tbody>
							<tr>
								<th>Client Name</th>
								<td>
									<input id="inClientName" type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Description</th>
								<td>
									<textarea id="inClientDesc" class="form-control"></textarea>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<input id="chkBoxActiveClient" type="checkbox" class="iCheck" />&nbsp; I want to show this in the list
								</td>
							</tr>
							<tr>
								<td colspan="2" class="text-center">
									<button id="btnAdd1" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalChooseFile">
		<div class="modal-dialog" role="document">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<label>Choose File:</label>
				</div>
				<div class="panel-body">
					<div class="col-xs-10">
						<input id="inUploadFile" type="file" class="file" data-show-preview="false" />
						<%--<div class="col-xs-10">
							<asp:FileUpload ID="FileUpload1" runat="server" CssClass="file" />
						</div>--%>
					</div>
					<div class="col-xs-2">
						<%--<asp:Button ID="filUploadBtn" runat="server" Text="Upload" CssClass="btn btn-primary" ClientIDMode="Static" OnClick="filUploadBtn_Click" />--%>
						<button id="btnSaveFile" type="button" class="btn btn-primary btn-sm" onclick="uploadfile();"><i class="fa fa-save"></i>&nbsp;Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
	<script src="js/List.js"></script>
</asp:Content>
