﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Site1 : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			clsConnection cls = new clsConnection();

			Session.Clear();

			string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
			if (domain == "")
			{
				domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
			}

			string[] sUser = domain.Split('/');
			string sUserId = sUser[1].ToLower();
			Session["user"] = sUserId;
			usr.InnerText = Session["user"].ToString();

			string qryRole = "select * from tbl_VPR_Access_User where ntid = '" + sUserId + "'";

			DataTable dtUser = cls.GetData(qryRole);

			if (dtUser.Rows.Count > 0)
			{
				string qryFunc = "select * from tbl_VPR_Access_Role_Type_Function where role_id = '" + dtUser.Rows[0]["role_id"].ToString() + "'";

				DataTable dtFunc = cls.GetData(qryFunc);

				if (dtFunc.Rows.Count > 0)
				{
					string func = dtFunc.Rows[0]["function_on"].ToString();

					string[] ids = func.Split(',');

					ArrayList arr = new ArrayList();

					foreach (string id in ids)
					{
						Control li = new Control();
						li = FindControl("_" + id);
						if (li != null)
						{
							li.Visible = true;
						}
					}
				}
				else
				{
					ArrayList list = new ArrayList();
					list.Add("_55");
					list.Add("_56");
					list.Add("_57");
					list.Add("_58");
					list.Add("_59");
					list.Add("_60");
					list.Add("_61");
					list.Add("_62");
                    list.Add("Li1");

					foreach (string id in list)
					{
						Control li = new Control();
						li = FindControl(id);
						if (li != null)
						{
							li.Visible = true;
						}
					}
				}
			}
			else
			{
				ArrayList list = new ArrayList();
				list.Add("_55");
				list.Add("_56");
				list.Add("_57");
				list.Add("_58");
				list.Add("_59");
				list.Add("_60");
				list.Add("_61");
				list.Add("_62");
                list.Add("Li1");
				foreach (string id in list)
				{
					Control li = new Control();
					li = FindControl(id);
					if (li != null)
					{
						li.Visible = true;
					}
				}
			}
		}

        protected void Page_Init(object sender, EventArgs e)
        {
            string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
            if (domain == "")
            {
                domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
            }

            string[] sUser = domain.Split('/');
            string sUserId = sUser[1].ToLower();
            Session["user"] = sUserId;
            usr.InnerText = Session["user"].ToString();
        }

        [WebMethod]
        public static string GetCheckerCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply in ('1','2')";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }
        [WebMethod]
        public static string GetApproverCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply  = '2'";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }

	}
}