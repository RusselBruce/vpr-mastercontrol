﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Standardization.aspx.cs" Inherits="Master_Control.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Standardization Tables</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-map-marker"></i>&nbsp;Mappings</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;PDF Mapping</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;Standardization Tables</span>
            </li>
        </ol>
    </section>

    <section class="content">

        <div class="box box-default noPadMar">
            <div class="box-header">
                <label>Source File Header</label>
            </div>
            <div class="box-body">
                <div class="col-xs-12">
                    <div class="col-xs-2">
                        <select id="slct" class="form-control input-sm">
                            <option value=""></option>
                            <option value="occupancy">Occupancy Status</option>
                            <option value="property">Property Type</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-7 prop" style="display: none; max-height: 600px; overflow: auto;">
                    <table id="tblProperty" class="table">
                        <thead>
                            <tr>
                                <th>Property Type</th>
                                <th>Standard Label</th>
                                <th>Registration Condition Classification</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-5 occu" style="display: none;">
                    <table id="tblOccupancy" class="table">
                        <thead>
                            <tr>
                                <th>Occupancy Status</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/Standardization.js"></script>
</asp:Content>
