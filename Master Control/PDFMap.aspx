﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PDFMap.aspx.cs" Inherits="PDFCreator.PDFMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
            margin-top: 1%;
        }

        .activeHeader {
            background-color: #367fa9;
            color: #fff;
        }

        #divDrop {
            cursor: pointer;
            text-align: center;
            border: 1px dotted;
        }
        
        .hdrs {
            cursor: pointer;
            background-color: #CCC;
        }

        .divBox {
            float: left;
            width: 100%;
            padding-left: 0;
            text-align: center;
            border: 5px solid #999;
        }

        .slot1 {
            position: relative;
            width: 100%;
            padding: 5px;
            border: 1px dotted;
            padding: 5px;
            background: #9cd09c;
            /*height: 37px !important;*/
            font-size: 16px;
        }

        .item {
            /*height: 28px;*/
            word-wrap: break-word;
            z-index: 1;
            background-color: #CCC;
            position: absolute;
            width: 97% !important;
        }

        .ui-selecting {
            background: #FECA40;
        }

        .ui-selected {
            background-color: #F90;
        }

        .green3 {
            background-color: #D9FFE2;
        }

        #tblSource {
            width: 100%;
            border-collapse: collapse;
        }

            #tblSource, #tblSource th, #tblSource td {
                border: 1px solid #000;
                text-align: center;
            }

                #tblSource thead {
                    background-color: #4d6578;
                    color: #fff;
                }

                #tblSource th {
                    padding: 5px;
                }

                #tblSource td {
                    padding: 5px 20px;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Mapping Tool</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-map-marker"></i>&nbsp;Mappings</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;PDF Mapping</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;Mapping Tool</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default noPadMar">
            <div class="box-body noPadMar">
                <div class="col-xs-4 noPadMar" style="border: 1px solid #000;">
                    <div class="col-xs-12 noPadMar">
                        <div class="col-xs-3">
                            <label>Select PDF:</label>
                        </div>
                        <div class="col-xs-6 noPadMar">
                            <select id="slctPDF" class="select2 form-control input-sm">
                            </select>
                        </div>
                        <div class="col-xs-3 text-center">
                            <button id="btnUpload" type="button" class="btn btn-primary btn-sm" style="width: 100%;">Upload PDF</button>
                        </div>
                    </div>
                    <div class="col-xs-12 noPadMar">
                        <div class="col-xs-3">
                            <label>Select Source:</label>
                        </div>
                        <div class="col-xs-6 noPadMar">
                            <a href="#" data-toggle="popover" id="lblSrc"></a>
                            <label id="GroupId" style="display: none;"></label>
                            <label id="GroupName" style="display: none;"></label>
                        </div>
                        <div class="col-xs-3 text-center">
                            <button id="btnSlect" type="button" class="btn btn-primary btn-sm" style="width: 100%;">Source</button>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center" style="background-color: #1e2e3b; color: #fff;">
                        <label>Data Fields</label>
                    </div>
                    <div id="divFields" class="col-xs-12" style="min-height: 520px; max-height: 520px; overflow: auto;">
                    </div>
                    <div class="col-xs-12">
                        <button id="btnPreview" type="button" class="btn btn-primary btn-sm">Preview</button>
                        <button id="btnSave" type="button" class="btn btn-primary btn-sm" disabled="disabled">Save</button>
                        <button id="btnAddCustom" type="button" class="btn btn-primary btn-sm">Add Custom</button>
                    </div>
                </div>
                <div id="divHeaders" class="col-xs-2 noPadMar" style="min-height: 682px; max-height: 682px; overflow: auto;">
                </div>
                <div class="col-xs-6 noPadMar">
                    <iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalUpload" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Upload File</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <input id="inUploadFile" type="file" class="file input-sm" data-show-preview="false" />
                        </div>
                        <div class="col-xs-12">
                            <%--<div class="col-xs-2 noPadMar">
                                <label>File Name</label>
                            </div>--%>
                            <%--<div class="col-xs-10 noPadMar">
                                <input id="inFileName" type="text" class="form-control input-sm" />
                            </div>--%>
                             <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>Type of Registration:</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="slcreg" class="form-control input-sm">
                                          <option value=""></option>
                                        <option value="City Registration Form">City Registration Form</option>
                                        <option value="City Website">City Website</option>
                                        <option value="Prochamps Online">Prochamps Online</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>Client:</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="slctClient" class="form-control input-sm">
                                        <option value="REO">REO</option>
                                        <option value="PFC">PFC</option>
                                        <option value="RESI">RESI</option>
                                    </select>
                                </div>
                            </div>
                            <%--<div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>State:</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="slctState" class="form-control input-sm">
                                        <option value="">--Select One--</option>
                                    </select>
                                </div>
                            </div>--%>
                            <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>Municipality:</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="slctMunicipality" class="select2 form-control input-sm" style="width: 100%">
                                        <option value="">--Select One--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>Service Type:</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="slctServiceType" class="form-control input-sm">
                                        <option value="Property Registration">Property Registration</option>
                                        <option value="Renewal">Renewal</option>
<%--                                        <option value="Renewal-2">Renewal-2</option>
                                        <option value="Renewal-3">Renewal-3</option>
                                        <option value="Renewal-4">Renewal-4</option>
                                        <option value="Renewal-5">Renewal-5</option>--%>
                                        <option value="Delist">Delist</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <label>Page:</label>
                                </div>
                                <div class="col-xs-10">
                                    <input id="Inppdf" type="number" min="-1" class="form-control input-sm">

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 text-center">
                            <button id="btnUploadPDF" type="button" class="btn btn-primary btn-sm">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSource" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Repository</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="nav-tabs-custom">
                            <ul id="ulNav" class="nav nav-tabs" style="padding-left: 1%;">
                                <li class="active">
                                    <button href="#tabData" data-toggle="tab" type="button" class="btn btn-primary" style="width: 100px;">Data</button>
                                </li>
                                <li>
                                    <button href="#tabCustom" data-toggle="tab" type="button" class="btn btn-primary" style="width: 100px;">Custom Data</button>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabData" class="tab-pane active">
                                    <div class="col-xs-12">
                                        <label>Please choose which data mapping is applicable</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <table id="tblRepository" class="table" data-pagination="true">
                                            <thead>
                                                <tr>
                                                    <th data-field="GroupId" data-formatter="rdID" data-halign="center"></th>
                                                    <th data-field="GroupName" data-sortable="true" data-halign="center">File Name</th>
                                                    <th data-field="Description" data-sortable="true" data-halign="center">Description</th>
                                                    <th data-field="CreatedBy" data-sortable="true" data-halign="center">Modified By</th>
                                                    <th data-field="DateCreated" data-sortable="true" data-halign="center">Modified Date and Time</th>
                                                    <th data-field="GroupId" data-formatter="actionFormat" data-halign="center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align: center; vertical-align: middle;"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="tabCustom" class="tab-pane">
                                    <div class="col-xs-12">
                                        <label>Please choose which custom data is applicable</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <table id="tblCustom" class="table" data-pagination="true">
                                            <thead>
                                                <tr>
                                                    <th data-field="GroupName" data-formatter="rdID" data-halign="center"></th>
                                                    <th data-field="GroupName" data-sortable="true" data-halign="center">Group Name</th>
                                                    <th data-field="description" data-sortable="true" data-halign="center">Description</th>
                                                    <th data-field="modified_by" data-sortable="true" data-halign="center">Modified By</th>
                                                    <th data-field="date_modified" data-sortable="true" data-halign="center">Modified Date and Time</th>
                                                    <th data-field="GroupName" data-formatter="actionFormat" data-halign="center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody style="text-align: center; vertical-align: middle;"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 text-right">
                                <button id="btnApplySrc" type="button" class="btn btn-primary btn-sm">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSaveDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Save Changes</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div id="divMap" class="col-xs-12" style="display: none;">
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <label>Description:</label>
                            </div>
                            <div class="col-xs-10">
                                <textarea id="taDescription" style="width: 100%; height: 100px; resize: none;" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 text-center">
                            <button id="btnSaveDesc" type="button" class="btn btn-primary btn-sm">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popover-content" class="col-xs-12" style="display: none;">
        <table id="tblSource" style="width: 100%; border: 1px solid #000;">
            <thead style="background-color: #4d6578; color: #fff">
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Type</th>
                </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/PDFMap.js"></script>
</asp:Content>
