﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class CompRulesApprover : System.Web.UI.Page
    {
        public static string auditStates = "";
        public static string auditCities = "";
        public static string auditZips = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            ForAudit();
        }

        public static void ForAudit()
        {
            clsConnection cls = new clsConnection();

            auditStates = "";
            auditCities = "";
            auditZips = "";

            //if (dtAudit.Rows.Count > 0)
            //{
            //    for (int x = 0; x <= dtAudit.Rows.Count; x++)
            //    {
            //        auditStates += "'" + dtAudit.Rows[1][x].ToString() + "',";
            //        auditCities += "'" + dtAudit.Columns[x].ToString() + "',";
            //    }
            //    auditStates = auditStates.Remove(auditStates.Length - 1, 1);
            //    auditCities = auditCities.Remove(auditCities.Length - 1, 1);
            //}

            //string qry = "select distinct state from tbl_VPR_Workable_Registration_PFC where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Registration_REO where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Registration_Property where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Registration_PropReq where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Registration_Cost where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Inspection where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from tbl_VPR_Workable_Municipality where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct state from dbo.tbl_VPR_Workable_Deregistration where tag = 'FOR REVIEW' ";

            //DataTable dtAudit = cls.GetData(qry);

            string queryAbbr = @"select state from tbl_VPR_MainSettings where isApply  = '2'";
            DataTable dtAudit = cls.GetData(queryAbbr);


            if (dtAudit.Rows.Count > 0)
            {
                for (int x = 0; x <= dtAudit.Rows.Count - 1; x++)
                {
                    auditStates += "'" + dtAudit.Rows[x][0].ToString() + "',";
                }
                auditStates = auditStates.Remove(auditStates.Length - 1, 1);
            }


            string queryAbbr1 = @"select city from tbl_VPR_MainSettings where isApply  = '2'";
            DataTable dtAudit1 = cls.GetData(queryAbbr1);

            if (dtAudit1.Rows.Count > 0)
            {
                for (int x = 0; x <= dtAudit1.Rows.Count - 1; x++)
                {
                    auditCities += "'" + dtAudit1.Rows[x][0].ToString() + "',";
                }
                auditCities = auditCities.Remove(auditCities.Length - 1, 1);
            }
            //string qry2 = "select distinct city from tbl_VPR_Workable_Registration_PFC where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Registration_REO where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Registration_Property where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Registration_PropReq where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Registration_Cost where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Inspection where tag = 'FOR REVIEW' ";
            //qry2 += "UNION ";
            //qry2 += "select distinct city from tbl_VPR_Workable_Municipality where tag = 'FOR REVIEW' ";
            //qry += "UNION ";
            //qry += "select distinct city from dbo.tbl_VPR_Workable_Deregistration where tag = 'FOR REVIEW' ";

            //DataTable dtAudit2 = cls.GetData(qry2);

            //if (dtAudit2.Rows.Count > 0)
            //{
            //    for (int y = 0; y <= dtAudit2.Rows.Count - 1; y++)
            //    {
            //        auditCities += "'" + dtAudit2.Rows[y][0].ToString() + "',";
            //    }

            //    auditCities = auditCities.Remove(auditCities.Length - 1, 1);
            //}
        }

        [WebMethod]
        public static string GetCheckerCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply in ('1','2')";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }
        [WebMethod]
        public static string GetApproverCount()
        {
            clsConnection cls = new clsConnection();
            DataTable dtCount = new DataTable();

            string query = @"select Count(*) as CountNum from tbl_VPR_MainSettings where isApply  = '2'";

            dtCount = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCount } });
        }
        [WebMethod]
        public static string GetState()
        {
            clsConnection cls = new clsConnection();
            DataTable dtState = new DataTable();

            string query = @"select DISTINCT(LTRIM(RTRIM(State))) [State], State_Abbr from tbl_DB_US_States (nolock) 
                            where isStateActive = 1 and State is not null and State <> '' and State_Abbr in (" + auditStates + ") order by [State]";

            dtState = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
        }

        [WebMethod]
        public static string GetMuniState()
        {
            clsConnection cls = new clsConnection();
            DataTable dtState = new DataTable();

            string query = @"select DISTINCT(LTRIM(RTRIM(State))) [State], State_Abbr from tbl_DB_US_States (nolock) 
                            where isStateActive = 1 and State is not null and State <> '' order by [State]";

            dtState = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
        }

        [WebMethod]
        public static string GetMunicipality(string state)
        {
            clsConnection cls = new clsConnection();
            DataTable dtMunicipality = new DataTable();

            string query = "";

            if (state != "")
            {
                //query = "select LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where municipality_code in (" + auditCities + ") and isActive = 1 and LEFT(municipality_name, 2) = '" + state + "' ";
                //query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = '" + state + "') group by municipality_name, municipality_code order by municipality_name";
                query = "select distinct(LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4))))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where municipality_code in (" + auditCities + ") and isActive = 1 and LEFT(municipality_name, 2) = '" + state + "' ";
                //query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = '" + state + "') group by municipality_name, municipality_code order by municipality_name";
            }
            else
            {
                query = "select LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where isActive = 1 ";
                query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = 'CT') group by municipality_name, municipality_code order by municipality_name";
            }

            //if (state != "")
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' and City in (" + auditCities + ") order by [City]";
            //}
            //else
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and City is not null and City <> '' order by [City]";
            //}

            dtMunicipality = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtMunicipality } });
        }

        [WebMethod]
        public static string GetCity(string state)
        {
            clsConnection cls = new clsConnection();
            DataTable dtCity = new DataTable();

            string query = "";

            if (state != "")
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' order by [City]";
            }
            else
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and City is not null and City <> '' order by [City]";
            }

            dtCity = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
        }

        //[WebMethod]
        //public static string GetCity(string state)
        //{
        //    clsConnection cls = new clsConnection();
        //    DataTable dtCity = new DataTable();

        //    string query = "";

        //    if (state != "")
        //    {
        //        //query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) 
        //        //where isCityActive = 1 and LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' and City in (" + auditCities + ") order by [City]";
        //        query = "select LTRIM(RTRIM(RIGHT(municipality_name, (LEN(municipality_name) - 4)))) [municipality_name], municipality_code from tbl_VPR_Municipality (nolock) where isActive = 1 and LEFT(municipality_name, 2) = '" + state + "' ";
        //        query += "OR (municipality_name like '%Statewide%' AND LEFT(municipality_name, 2) = '" + state + "') group by municipality_name, municipality_code order by municipality_name";
        //    }
        //    else
        //    {
        //        query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and City is not null and City <> '' order by [City]";
        //    }

        //    dtCity = cls.GetData(query);

        //    return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
        //}

        [WebMethod]
        public static string GetZip(string city)
        {
            clsConnection cls = new clsConnection();
            DataTable dtZip = new DataTable();

            string query = "";

            //if (city != "")
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and LTRIM(RTRIM(City)) = '" + city + "' and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            //}
            //else
            //{
            //    query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            //}

            if (city != "")
            {
                query = "select d.muni_name, dbus.ZipCode, dbus.isZipActive from tbl_DB_US_States [dbus] (nolock) outer apply (select LEFT(a.municipality_name, 2) [State_Abbr], ";
                query += "b.state, RIGHT(a.municipality_name, (LEN(a.municipality_name) - 4)) [muni_name] from tbl_VPR_Municipality a (nolock) outer apply ";
                query += "(select State_Abbr, LTRIM(RTRIM(State)) [state], ZipCode from tbl_DB_US_States (nolock) where ";
                query += "State_Abbr = UPPER(LEFT(a.municipality_name, 2)) group by State_Abbr, LTRIM(RTRIM(State)),ZipCode) b ";
                query += "where UPPER(LEFT(a.municipality_name, 2)) <> '--' and a.municipality_name ";
                query += "not in ('Send to Peak','VACANT REGISTRY','ATTORNEY - STERN & EISENBERG PC')and a.municipality_code = '" + city + "'";
                query += "group by LEFT(a.municipality_name, 2), b.state, a.municipality_name ) d where dbus.State_Abbr = d.State_Abbr ";
                query += "and (LTRIM(RTRIM(dbus.Municipality)) = LTRIM(RTRIM(d.muni_name)) OR ";
                query += "LTRIM(RTRIM(dbus.Municipality)) = REPLACE(LTRIM(RTRIM(d.muni_name)),'(2)','')) order by d.muni_name";
            }
            else
            {
                query = "select d.muni_name, dbus.ZipCode, dbus.isZipActive from tbl_DB_US_States [dbus] outer apply (select LEFT(a.municipality_name, 2) [State_Abbr], ";
                query += "b.state, RIGHT(a.municipality_name, (LEN(a.municipality_name) - 4)) [muni_name] from tbl_VPR_Municipality a outer apply ";
                query += "(select State_Abbr, LTRIM(RTRIM(State)) [state], ZipCode from tbl_DB_US_States where ";
                query += "State_Abbr = UPPER(LEFT(a.municipality_name, 2)) group by State_Abbr, LTRIM(RTRIM(State)),ZipCode) b ";
                query += "where UPPER(LEFT(a.municipality_name, 2)) <> '--' and a.municipality_name ";
                query += "not in ('Send to Peak','VACANT REGISTRY','ATTORNEY - STERN & EISENBERG PC') ";
                query += "group by LEFT(a.municipality_name, 2), b.state, a.municipality_name ) d where dbus.State_Abbr = d.State_Abbr ";
                query += "and (LTRIM(RTRIM(dbus.Municipality)) = LTRIM(RTRIM(d.muni_name)) OR ";
                query += "LTRIM(RTRIM(dbus.Municipality)) = REPLACE(LTRIM(RTRIM(d.muni_name)),'(2)','')) order by d.muni_name";
            }


            dtZip = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtZip } });
        }

        [WebMethod]
        public static string getContactZip(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string strQuery = "";
            DataTable dtContactZip = new DataTable();

            strQuery = @"select distinct ZipCode from tbl_DB_US_States (nolock) 
                                where isCityActive = 1 and 
                                [State] = '" + state + "' and City = '" + city + "' order by ZipCode";

            dtContactZip = cls.GetData(strQuery);

            return JsonConvert.SerializeObject(
                new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        contactZip = dtContactZip
                    }
                });
        }

        [WebMethod]
        public static string getMuniZip(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string strQuery = "";
            DataTable dtmuniZip = new DataTable();

            strQuery = @"select distinct ZipCode from tbl_DB_US_States (nolock) 
                                where isCityActive = 1 and 
                                [State] = '" + state + "' and City = '" + city + "' order by ZipCode";

            dtmuniZip = cls.GetData(strQuery);

            return JsonConvert.SerializeObject(
                new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        muniZip = dtmuniZip
                    }
                });
        }

        [WebMethod]
        public static string getOrdinanceHistory(string state, string city)
        {
            clsConnection cls = new clsConnection();
            DataTable dtOrdinance = new DataTable();

            string query = @"select id, ordinance_num, ordinance_name, ordinance_file, description, section, source, [enacted_date], 
                            [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance
                            (nolock) where state = '" + state + "' and city = '" + city + "'";

            dtOrdinance = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtOrdinance } });
        }
        [WebMethod]
        public static string GetData(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPFC = "", strQuery = "";

            DataTable dtRegPFC, dtLook, dtApply = new DataTable();

            qryRegPFC = "select top 1 * from tbl_VPR_Workable_Registration_PFC (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPFC = cls.GetData(qryRegPFC);

            string applyquery = "";

            applyquery = @"select * from tbl_VPR_MainSettings where isApply = '3' and state = '" + state + "' and city = '" + city + "'";
            dtApply = cls.GetData(applyquery);

            if (dtApply.Rows.Count == 0)
            {
                strQuery += "update tbl_VPR_Workable_Registration_PFC set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Registration_REO set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Registration_Property set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Registration_PropReq set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Registration_Cost set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Inspection set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Additional_Forms set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Municipality set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Ordinance set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable_Deregistration set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                strQuery += "update tbl_VPR_Workable set tag = 'CURRENTLY UNDER FINAL REVIEW' where state = '" + state + "' and city = '" + city + "';";
                cls.ExecuteQuery(strQuery);
            }

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["pfcSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPFC = dtRegPFC,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPFC = dtRegPFC
                    }
                });
            }


        }

        [WebMethod]
        public static string GetDataREO(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegREO;
            DataTable dtRegREO = new DataTable();
            DataTable dtLook = new DataTable();

            qryRegREO = "select top 1 * from tbl_VPR_Workable_Registration_REO (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegREO = cls.GetData(qryRegREO);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["reoSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regREO = dtRegREO,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regREO = dtRegREO
                    }
                });
            }
        }
        [WebMethod]
        public static string GetDataPropType(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPropType;

            DataTable dtRegPropType = new DataTable();
            DataTable dtLook = new DataTable();

            qryRegPropType = "select top 1 * from tbl_VPR_Workable_Registration_Property (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPropType = cls.GetData(qryRegPropType);


            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["propTypeSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPropType = dtRegPropType,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPropType = dtRegPropType
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataReq(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPropReq;

            DataTable dtRegPropReq = new DataTable();
            DataTable dtLook = new DataTable();

            qryRegPropReq = @"select top 1 a.*, b.*, c.* from tbl_VPR_Workable_Registration_PropReq a (nolock) full join
                                tbl_VPR_Workable_Registration_PropReq_Contact b (nolock) on
                                a.propReqID = b.propReqID 
                                full join  tbl_VPR_Workable_Registration_PropReq_Bond as c  on
                                a.bondID = c.bondID where a.state = '" + state + "' and a.city = '" + city + "' order by a.id desc, date_modified desc";
            dtRegPropReq = cls.GetData(qryRegPropReq);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["requirementSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPropReq = dtRegPropReq,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regPropReq = dtRegPropReq
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataCost(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryCost;

            DataTable dtCost = new DataTable();
            DataTable dtLook = new DataTable();

            qryCost = @"select top 1 * from tbl_VPR_Workable_Registration_Cost cost
                        inner join tbl_VPR_Cost_PFC pfc 
                        on cost.pfc_id = pfc.pfc_id
                        inner join tbl_VPR_Cost_REO reo
                        on cost.reo_id = reo.reo_id
                        inner join tbl_VPR_Cost_Commercial commercial
                        on cost.commercial_id = commercial.commercial_id
                        inner join tbl_VPR_Cost_Vacant vacant
                        on cost.vacant_id = vacant.vacant_id
                        where cost.state = '" + state + "' and cost.city = '" + city + "' order by cost.id desc, cost.date_modified desc";
            dtCost = cls.GetData(qryCost);  

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["costSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regCost = dtCost,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regCost = dtCost
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataConReg(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryCont;

            DataTable dtCont = new DataTable();
            DataTable dtLook = new DataTable();

            qryCont = "select top 1 * from tbl_VPR_Workable (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtCont = cls.GetData(qryCont);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["contRegSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regCont = dtCont,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        regCont = dtCont
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataInspection(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryInspect;

            DataTable dtInspect = new DataTable();
            DataTable dtLook = new DataTable();

            qryInspect = "select top 1 * from tbl_VPR_Workable_Inspection (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtInspect = cls.GetData(qryInspect);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["inspectionSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        inspect = dtInspect,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        inspect = dtInspect
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataMunicipality(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryMunicipal;

            DataTable dtMunicipal = new DataTable();
            DataTable dtLook = new DataTable();

            qryMunicipal = "select top 1 * from tbl_VPR_Workable_Municipality (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtMunicipal = cls.GetData(qryMunicipal);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["municipalitySetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        municipal = dtMunicipal,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        municipal = dtMunicipal
                    }
                });
            }
        }

        [WebMethod]
        public static string GetDataDeregistration(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryDereg;

            DataTable dtDereg = new DataTable();
            DataTable dtLook = new DataTable();

            qryDereg = "select top 1 * from tbl_VPR_Workable_Deregistration (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtDereg = cls.GetData(qryDereg);


            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["deregistrationSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        dereg = dtDereg,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        dereg = dtDereg
                    }
                });
            }
        }

        [WebMethod]
        public static string GetOrdinanceSettings(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryOrdinance = "";

            DataTable dtOrdinance = new DataTable();
            DataTable dtLook = new DataTable();

            qryOrdinance = "select top 1 * from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtOrdinance = cls.GetData(qryOrdinance);

            string queryLook = "";

            queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["ordinanceSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        ordinance = dtOrdinance,
                        check = dtLook
                    }
                });
            }
            else
            {
                return JsonConvert.SerializeObject(new
                {
                    Success = true,
                    Message = "Success",
                    data = new
                    {
                        ordinance = dtOrdinance
                    }
                });
            }
        }
        [WebMethod]
        public static string AuditTrail(List<string> data, string user, string tag)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string insert = "", top = "", muniName = data[0].ToString();

            if (tag == "pfc")
            {
                string mainTab = "Registration Criteria";
                string sub = "PFC";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_PFC where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);

                insert = @"insert into tbl_VPR_Audit_PFC ([Municipality Name], [Municipality Code], [PFC Default], [PFC Default Timeline],
                            [PFC Vacant], [PFC Vacant Timeline], [PFC Foreclosure], [PFC Foreclosure Timeline], [PFC Foreclosure and Vacant],
                            [PFC Foreclosure and Vacant Timeline], [PFC City Notice], [PFC City Notice Timeline], [PFC Code Violation], [PFC Code Violation Timeline],
                            [PFC Boarded], [PFC Boarded Timeline], [PFC Other], [PFC Other Timeline], [Payment Type], [Type of Registration], [PDF], [Renewal], [Renewal PDF],
                            [Renewal Every], [Renewal On], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', '" + data[17].ToString() + "', '" +
                            data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" + data[21].ToString() + "', '" + data[22].ToString() + "', '" +
                            data[23].ToString() + "', '" + data[24].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "reo")
            {
                string mainTab = "Registration Criteria";
                string sub = "REO";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_REO where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_REO ([Municipality Name], [Municipality Code], [REO Bank-owned], [REO Bank-owned Timeline],
                            [REO Vacant], [REO Vacant Timeline], [REO City Notice], [REO City Notice Timeline], [REO Code Violation],
                            [REO Code Violation Timeline], [REO Boarded Only], [REO Boarded Timeline], [REO Distressed/Abandoned], [REO Distressed/Abandoned Timeline],
                            [Rental Registration], [Rental Registration Timeline], [REO Other], [REO Other Timeline], [Payment Type], [Type of Registration], [PDF], [Renewal], [Renewal PDF],
                            [Renewal Every], [Renewal On], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', '" + data[17].ToString() + "', '" +
                            data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', '" + data[21].ToString() + "', '" + data[22].ToString() + "', '" +
                            data[23].ToString() + "', '" + data[24].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "property")
            {
                string mainTab = "Registration Criteria";
                string sub = "Property Type";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_PropertyType where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_PropertyType ([Municipality Name], [Municipality Code], [Residential], [Single Family],
                            [Multi Family], [2 Units], [3 Units], [4 Units], [5 Units or more],
                            [Rental], [Commercial], [Condo], [Townhome], [Vacant Lot],
                            [Mobile Home], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {
                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }
                }
            }
            else if (tag == "continue")
            {
                string mainTab = "Registration Criteria";
                string sub = "Continuing Registration";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_ContinuingRegistration where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_ContinuingRegistration ([Municipality Name], [Municipality Code], [Continue Registration from PFC to REO?], [Update Registration with Municipality],
                           [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "municipalityInfo")
            {
                string mainTab = "Municipality Contact Information";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_MunicipalityInfo where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_MunicipalityInfo ([Municipality Name], [Municipality Code], [Municipality Department], 
                            [Municipality Phone Number], [Municipality Email Address], [Municipality Mailing Address], [Municipality Website], 
                            [Contact Person], [Title], [Department], [Phone Number], [Email Address], [Mailing Address], [From Hours],	
                            [To Hours],	[Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                            data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                            data[8].ToString() + "', '" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', '" +
                            data[13].ToString() + "', '" + data[14].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "deregistration")
            {
                string mainTab = "Deregistration";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_Deregistration where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);

                insert = @"insert into tbl_VPR_Audit_Deregistration ([Municipality Name], [Municipality Code], [Deregistration Required], [Only if Conveyed],	
                        [Only if Occupied], [How to Deregister], [PDF], [New Owner Information Required], [Proof of Conveyance Required], [Date of Sale Required],	
                        [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" +
                        data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                        data[8].ToString() + "', '" + data[9].ToString() + "', '" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }

                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }
            else if (tag == "ordinance")
            {
                string mainTab = "Ordinance Settings";
                string sub = " - ";
                string action = "";

                top = "select top 1 * from tbl_VPR_Audit_Ordinance where [Municipality Name] = '" + data[0].ToString() + "' and [Municipality Code] = '" + data[1].ToString() + "' order by id desc";
                dt = cls.GetData(top);


                insert = @"insert into tbl_VPR_Audit_Ordinance ([Municipality Name], [Municipality Code], [Ordinance Number], [Ordinance Name], [Description], [Ordinance Copy], 
                         [Section], [Source], [Enacted Date], [Revision Date], [Action By], [Date], [Time]) values ('" + data[0].ToString() + "', '" + data[1].ToString() +
                        "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', '" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" +
                        data[8].ToString() + "', '" + data[9].ToString() + "','" + user.ToString() + "', CAST(GETDATE() as date), CAST(GETDATE() as time))";

                if (dt.Rows.Count > 0)
                {
                    action = "Changed";
                    dt.Columns.Remove("id");
                }
                else
                {
                    action = "Saved";
                }


                for (int x = 0; x < data.Count; x++)
                {

                    if (dt.Rows.Count == 0)
                    {
                        try
                        { cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x + 2].ColumnName.ToString(), action, "-", data[x + 2].ToString()); }
                        catch
                        { break; }
                    }
                    else
                    {
                        if (data[x].ToString() != dt.Rows[0][x].ToString())
                        {
                            cls.FUNC.AuditNew(muniName, mainTab, sub, dt.Columns[x].ColumnName.ToString(), action, dt.Rows[0][x].ToString(), data[x].ToString());
                        }
                    }

                }
            }

            int exec = cls.ExecuteQuery(insert);

            string result = (exec > 0) ? "1" : "2";

            return result;
        }
        [WebMethod]
        public static string SaveWorkable(List<string> data, string user, string id)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string query = "", mainSettings = "", queryAudit = "", checkAudit = "";
            string tag = "";
            if (data[0] == "regPFC")
            {
                query = @"update tbl_VPR_Workable_Registration_PFC set [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() +
                            "', pfc_default = '" + data[3].ToString() + "', def_reg_timeline1 = '" + data[4].ToString() + "', def_reg_timeline2 = '" +
                            data[5].ToString() + "', def_reg_timeline3 = '" + data[6].ToString() + "', def_reg_timeline4 = '" + data[7].ToString() +
                            "', def_reg_timeline5 = '" + data[8].ToString() +
                            "', pfc_vacant = '" + data[9].ToString() + "', pfc_vacant_timeline1 = '" + data[10].ToString() + "', pfc_vacant_timeline2 = '" + data[11].ToString() +
                            "', pfc_vacant_timeline3 = '" + data[12].ToString() + "', pfc_vacant_timeline4 = '" + data[13].ToString() + "', pfc_vacant_timeline5 = '" + data[14].ToString() +
                            "', pfc_foreclosure = '" + data[15].ToString() + "', pfc_def_foreclosure_timeline1 = '" + data[16].ToString() +
                            "', pfc_def_foreclosure_timeline2 = '" + data[17].ToString() + "', pfc_def_foreclosure_timeline3 = '" + data[18].ToString() +
                            "', pfc_def_foreclosure_timeline4 = '" + data[19].ToString() + "', pfc_def_foreclosure_timeline5 = '" + data[20].ToString() +
                            "', pfc_foreclosure_vacant = '" + data[21].ToString() + "', pfc_foreclosure_vacant_timeline1 = '" + data[22].ToString() +
                            "', pfc_foreclosure_vacant_timeline2 = '" + data[23].ToString() + "', pfc_foreclosure_vacant_timeline3 = '" + data[24].ToString() +
                            "', pfc_foreclosure_vacant_timeline4 = '" + data[25].ToString() + "', pfc_foreclosure_vacant_timeline5 = '" + data[26].ToString() +
                            "', pfc_city_notice = '" + data[27].ToString() + "', pfc_city_notice_timeline1 = '" + data[28].ToString() +
                            "', pfc_city_notice_timeline2 = '" + data[29].ToString() + "', pfc_city_notice_timeline3 = '" + data[30].ToString() +
                            "', pfc_city_notice_timeline4 = '" + data[31].ToString() + "', pfc_city_notice_timeline5 = '" + data[32].ToString() +
                            "', pfc_code_violation = '" + data[33].ToString() + "', pfc_code_violation_timeline1 = '" + data[34].ToString() +
                            "', pfc_code_violation_timeline2 = '" + data[35].ToString() + "', pfc_code_violation_timeline3 = '" + data[36].ToString() +
                            "', pfc_code_violation_timeline4 = '" + data[37].ToString() + "', pfc_code_violation_timeline5 = '" + data[38].ToString() +
                            "', pfc_boarded = '" + data[39].ToString() + "', pfc_boarded_timeline1 = '" + data[40].ToString() +
                            "', pfc_boarded_timeline2 = '" + data[41].ToString() + "', pfc_boarded_timeline3 = '" + data[42].ToString() +
                            "', pfc_boarded_timeline4 = '" + data[43].ToString() + "', pfc_boarded_timeline5 = '" + data[44].ToString() +
                            "', pfc_other = '" + data[45].ToString() + "', pfc_other_timeline1 = '" + data[46].ToString() +
                            "', pfc_other_timeline2 = '" + data[47].ToString() + "', pfc_other_timeline3 = '" + data[48].ToString() +
                            "', pfc_other_timeline4 = '" + data[49].ToString() + "', pfc_other_timeline5 = '" + data[50].ToString() +
                            "', payment_type = '" + data[51].ToString() + "', type_of_registration = '" + data[52].ToString() +
                            "', vms_renewal = '" + data[53].ToString() + "', upload_path = '" + data[54].ToString() + "', no_pfc_reg = '" + data[55].ToString() +
                            "', statewide_reg = '" + data[56].ToString() + "', renew_every = '" + data[57].ToString() + "', renew_on = '" + data[58].ToString() +
                            "', renew_every_num = '" + data[59].ToString() +
                            "', renew_every_years = '" + data[60].ToString() + "', renew_on_months = '" + data[61].ToString() + "', renew_on_num = '" + data[62].ToString() +
                            "', renew_upload_path = '" + data[63].ToString() +
                            "', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set pfcSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regREO")
            {

                query = @"update tbl_VPR_Workable_Registration_REO set 
                        [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', reo_bank_owed = '" + data[3].ToString() +
                                     "', reo_bank_owed_timeline1 = '" + data[4].ToString() +
                        "', reo_bank_owed_timeline2 = '" + data[5].ToString() + "', reo_bank_owed_timeline3 = '" + data[6].ToString() +
                        "', reo_bank_owed_timeline4 = '" + data[7].ToString() +
                        "', reo_bank_owed_timeline5 = '" + data[8].ToString() + "', reo_vacant = '" + data[9].ToString() +
                        "', reo_vacant_timeline1 = '" + data[10].ToString() +
                        "', reo_vacant_timeline2 = '" + data[11].ToString() + "', reo_vacant_timeline3 = '" + data[12].ToString() +
                        "', reo_vacant_timeline4 = '" + data[13].ToString() +
                        "', reo_vacant_timeline5 = '" + data[14].ToString() + "', reo_city_notice = '" + data[15].ToString() + "', reo_city_notice_timeline1 = '" + data[16].ToString() +
                        "', reo_city_notice_timeline2 = '" + data[17].ToString() + "', reo_city_notice_timeline3 = '" + data[18].ToString() +
                        "', reo_city_notice_timeline4 = '" + data[19].ToString() +
                        "', reo_city_notice_timeline5 = '" + data[20].ToString() + "', reo_code_violation = '" + data[21].ToString() +
                        "', reo_code_violation_timeline1 = '" + data[22].ToString() +
                        "', reo_code_violation_timeline2 = '" + data[23].ToString() + "', reo_code_violation_timeline3 = '" + data[24].ToString() +
                        "', reo_code_violation_timeline4 = '" + data[25].ToString() +
                        "', reo_code_violation_timeline5 = '" + data[26].ToString() + "', reo_boarded_only = '" + data[27].ToString() +
                        "', reo_boarded_only_timeline1 = '" + data[28].ToString() +
                        "', reo_boarded_only_timeline2 = '" + data[29].ToString() + "', reo_boarded_only_timeline3 = '" + data[30].ToString() +
                        "', reo_boarded_only_timeline4 = '" + data[31].ToString() +
                        "', reo_boarded_only_timeline5 = '" + data[32].ToString() + "', reo_distressed_abandoned = '" + data[33].ToString() +
                        "', reo_distressed_abandoned1 = '" + data[34].ToString() +
                        "', reo_distressed_abandoned2 = '" + data[35].ToString() + "', reo_distressed_abandoned3 = '" + data[36].ToString() +
                        "',reo_distressed_abandoned4 = '" + data[37].ToString() +
                        "', reo_distressed_abandoned5 = '" + data[38].ToString() + "', rental_registration = '" + data[39].ToString() +
                        "', rental_registration_timeline1 = '" + data[40].ToString() +
                        "', rental_registration_timeline2 = '" + data[41].ToString() + "', rental_registration_timeline3 = '" + data[42].ToString() +
                        "', rental_registration_timeline4 = '" + data[43].ToString() +
                        "', rental_registration_timeline5 = '" + data[44].ToString() + "', reo_other  = '" + data[45].ToString() +
                        "', reo_other_timeline1 = '" + data[46].ToString() + "', reo_other_timeline2 = '" + data[47].ToString() +
                        "', reo_other_timeline3 = '" + data[48].ToString() + "', reo_other_timeline4 = '" + data[49].ToString() +
                        "', reo_other_timeline5 = '" + data[50].ToString() + "', payment_type = '" + data[51].ToString() +
                        "', type_of_registration = '" + data[52].ToString() + "', vms_renewal = '" + data[53].ToString() +
                        "', upload_path = '" + data[54].ToString() + "', no_reo_reg = '" + data[55].ToString() +
                        "', statewide_reg = '" + data[56].ToString() + "', renew_every = '" + data[57].ToString() + "', renew_on = '" + data[58].ToString() +
                        "', renew_every_num = '" + data[59].ToString() +
                        "', renew_every_years = '" + data[60].ToString() + "', renew_on_months = '" + data[61].ToString() +
                        "', renew_on_num = '" + data[62].ToString() + "', reo_renew_uploadpath = '" + data[63].ToString() +
                        "', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set reoSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }

            }
            else if (data[0] == "regProperty")
            {
                query = @"update tbl_VPR_Workable_Registration_Property set
                        [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() +
                        "', residential = '" + data[3].ToString() + "', single_family = '" + data[4].ToString() + "', multi_family = '" + data[5].ToString() +
                        "', unit2 = '" + data[6].ToString() + "', unit3 = '" + data[7].ToString() + "', unit4 = '" + data[8].ToString() + "', unit5 = '" + data[9].ToString() +
                        "', rental = '" + data[10].ToString() + "', commercial = '" + data[11].ToString() + "', condo = '" + data[12].ToString() +
                        "', townhome = '" + data[13].ToString() + "', vacant_lot = '" + data[14].ToString() + "', mobile_home = '" + data[15].ToString() +
                        "', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set propTypeSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }

            }
            else if (data[0] == "regPropReq")
            {

                query = @"update tbl_VPR_Workable_Registration_PropReq set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', presale_definition = '" + data[3].ToString()
                        + "', local_contact_required = '" + data[4].ToString() + "', gse_exclusion = '" + data[5].ToString() + "', insurance_required = '" + data[6].ToString() + "', foreclosure_action_info_needed = '" + data[7].ToString()
                        + "', foreclosure_case_info_needed = '" + data[8].ToString() + "', foreclosure_deed_required = '" + data[9].ToString() + "', bond_required = '" + data[10].ToString()
                        + "', utility_information_required = '" + data[11].ToString() + "', winterization_required = '" + data[12].ToString() + "', signature_required = '" + data[13].ToString()
                        + "', notarization_required = '" + data[14].ToString() + "', recent_inspection_Date = '" + data[15].ToString() + "', first_time_vacancy_date = '" + data[16].ToString()
                        + "', secured_required = '" + data[17].ToString() + "', additional_signage_required = '" + data[18].ToString() + "', pictures_required = '" + data[19].ToString()
                        + "', mobile_vin_number_required = '" + data[20].ToString() + "', parcel_number_required = '" + data[21].ToString() + "', legal_description_required = '" + data[22].ToString()
                        + "', block_lot_number_required = '" + data[23].ToString() + "', attorney_information_required = '" + data[24].ToString() + "', broker_information_required_reo = '" + data[25].ToString()
                        + "', mortgage_contact_name_required = '" + data[26].ToString() + "', client_tax_number_required = '" + data[27].ToString() + "', tag = '', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "'";

                query += @"update tbl_VPR_Workable_Registration_PropReq_Bond set one_time_only = '" + data[28].ToString() + "', bond_amount_escalating = '" + data[29].ToString() + "', bond_amount = '" + data[30].ToString()
                    + "', bond_schedule_amount = '" + data[31].ToString() + "' where bondID = (select bondID from tbl_VPR_Workable_Registration_PropReq where id = '" + id + "')";

                query += @"update tbl_VPR_Workable_Registration_PropReq_Contact set [state] = '" + data[54].ToString() + "', city = '" + data[55].ToString() + "', lcicompany_name = '" + data[32].ToString() +
                                   "', lcifirst_name = '" + data[33].ToString() + "', lcilast_name = '" + data[34].ToString() + "', lcititle = '" + data[35].ToString() + "', lcibusiness_license_num = '" + data[36].ToString() +
                                   "', lciphone_num1 = '" + data[37].ToString() + "', lciphone_num2 = '" + data[38].ToString() + "', lcibusiness_phone_num1 = '" + data[39].ToString() + "', lcibusiness_phone_num2 = '" + data[40].ToString() +
                                   "', lciemergency_phone_num1 = '" + data[41].ToString() + "', lciemergency_phone_num2 = '" + data[42].ToString() + "', lcifax_num1 = '" + data[43].ToString() + "', lcifax_num2 = '" + data[44].ToString() +
                                   "', lcicell_num1 = '" + data[45].ToString() + "', lcicell_num2 = '" + data[46].ToString() + "', lciemail = '" + data[47].ToString() + "', lcistreet = '" + data[48].ToString() +
                                   "', lcistate = '" + data[49].ToString() + "', lcicity = '" + data[50].ToString() + "', lcizip = '" + data[51].ToString() + "', lcihours_from = '" + data[52].ToString() +
                                   "', lcihours_to = '" + data[53].ToString() + "' where propReqID = (select propReqID from tbl_VPR_Workable_Registration_PropReq where id = '" + id + "')";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {

                        mainSettings = "update tbl_VPR_MainSettings set requirementSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCost")
            {
                for (int x = 0; x < data.Count(); x++)
                {
                    if (data[x] == null || data[x] == "undefined")
                    {
                        data[x] = "";
                    }
                }

                query = @"update tbl_VPR_Cost_PFC set pfc_reg_cost = '" + data[3].ToString() + "', pfc_reg_cost_units = '" + data[4].ToString() + "', pfc_reg_cost_units_amount = '" + data[5].ToString()
                             + "', pfc_reg_cost_units_check = '" + data[6].ToString() + "', pfc_reg_cost_square = '" + data[7].ToString() + "', pfc_reg_cost_square_feet = '" + data[8].ToString() + "', pfc_reg_cost_square_amount = '" + data[9].ToString()
                             + "', pfc_reg_cost_square_check = '" + data[10].ToString() + "', pfc_reg_cost_amount = '" + data[11].ToString() + "', pfc_reg_cost_standard = '" + data[12].ToString() + "', pfc_renewal_cost_escalating = '" + data[13].ToString()
                             + "', pfc_renewal_cost_units = '" + data[14].ToString() + "', pfc_renewal_cost_units_amount = '" + data[15].ToString() + "', pfc_renewal_cost_units_check = '" + data[16].ToString() + "', pfc_renewal_cost_square = '" + data[17].ToString()
                             + "', pfc_renewal_cost_square_feet = '" + data[18].ToString() + "', pfc_renewal_cost_square_amount = '" + data[19].ToString() + "', pfc_renewal_cost_square_check = '" + data[20].ToString()
                             + "', pfc_renewal_cost_amount = '" + data[21].ToString() + "', pfc_escalating_renewal_cost_amount = '" + data[22].ToString() + "', pfc_escalating_renewal_succeding = '" + data[23].ToString()
                             + "', pfc_first_escalating_cost_units_amount = '" + data[24].ToString() + "', pfc_first_escalating_cost_units_check = '" + data[25].ToString() + "', pfc_first_escalating_cost_square_feet = '" + data[26].ToString()
                             + "', pfc_first_escalating_cost_square_amount = '" + data[27].ToString() + "', pfc_first_escalating_cost_square_check = '" + data[28].ToString() + "', pfc_second_escalating_cost_units_amount = '" + data[29].ToString()
                             + "', pfc_second_escalating_cost_units_check = '" + data[30].ToString() + "', pfc_second_escalating_cost_square_feet = '" + data[31].ToString() + "', pfc_second_escalating_cost_square_amount = '" + data[32].ToString()
                             + "', pfc_second_escalating_cost_square_check = '" + data[33].ToString() + "', pfc_third_escalating_cost_units_amount = '" + data[34].ToString() + "', pfc_third_escalating_cost_units_check = '" + data[35].ToString()
                             + "', pfc_third_escalating_cost_square_feet = '" + data[36].ToString() + "', pfc_third_escalating_cost_square_amount = '" + data[37].ToString() + "', pfc_third_escalating_cost_square_check = '" + data[38].ToString()
                             + "', pfc_fourth_escalating_cost_units_amount = '" + data[39].ToString() + "', pfc_fourth_escalating_cost_units_check = '" + data[40].ToString() + "', pfc_fourth_escalating_cost_square_feet = '" + data[41].ToString()
                             + "', pfc_fourth_escalating_cost_square_amount = '" + data[42].ToString() + "', pfc_fourth_escalating_cost_square_check = '" + data[43].ToString() + "', pfc_fifth_escalating_cost_units_amount = '" + data[44].ToString()
                             + "', pfc_fifth_escalating_cost_units_check = '" + data[45].ToString() + "', pfc_fifth_escalating_cost_square_feet = '" + data[46].ToString() + "', pfc_fifth_escalating_cost_square_amount = '" + data[47].ToString()
                             + "', pfc_fifth_escalating_cost_square_check = '" + data[48].ToString() + "' where pfc_id = (select pfc_id from tbl_VPR_Workable_Registration_Cost where id = '" + id + "')";

                query += @"update tbl_VPR_Cost_REO set reo_reg_cost = '" + data[49].ToString() + "', reo_reg_cost_units = '" + data[50].ToString() + "', reo_reg_cost_units_amount = '" + data[51].ToString()
                    + "', reo_reg_cost_units_check = '" + data[52].ToString() + "', reo_reg_cost_square = '" + data[53].ToString() + "', reo_reg_cost_square_feet = '" + data[54].ToString() + "', reo_reg_cost_square_amount = '" + data[55].ToString()
                    + "', reo_reg_cost_square_check = '" + data[56].ToString() + "', reo_reg_cost_amount = '" + data[57].ToString() + "', reo_reg_cost_standard = '" + data[58].ToString() + "', reo_renewal_cost_escalating = '" + data[59].ToString()
                    + "', reo_renewal_cost_units = '" + data[60].ToString() + "', reo_renewal_cost_units_amount = '" + data[61].ToString() + "', reo_renewal_cost_units_check = '" + data[62].ToString()
                    + "', reo_renewal_cost_square = '" + data[63].ToString() + "', reo_renewal_cost_square_feet = '" + data[64].ToString() + "', reo_renewal_cost_square_amount = '" + data[65].ToString()
                    + "', reo_renewal_cost_square_check = '" + data[66].ToString() + "', reo_renewal_cost_amount = '" + data[67].ToString() + "', reo_escalating_renewal_cost_amount = '" + data[68].ToString()
                    + "', reo_escalating_renewal_succeding = '" + data[69].ToString() + "', reo_first_escalating_cost_units_amount = '" + data[70].ToString() + "', reo_first_escalating_cost_units_check = '" + data[71].ToString()
                    + "', reo_first_escalating_cost_square_feet = '" + data[72].ToString() + "', reo_first_escalating_cost_square_amount = '" + data[73].ToString() + "', reo_first_escalating_cost_square_check = '" + data[74].ToString()
                    + "', reo_second_escalating_cost_units_amount = '" + data[75].ToString() + "', reo_second_escalating_cost_units_check = '" + data[76].ToString() + "', reo_second_escalating_cost_square_feet = '" + data[77].ToString()
                    + "', reo_second_escalating_cost_square_amount = '" + data[78].ToString() + "', reo_second_escalating_cost_square_check = '" + data[79].ToString() + "', reo_third_escalating_cost_units_amount = '" + data[80].ToString()
                    + "', reo_third_escalating_cost_units_check = '" + data[81].ToString() + "', reo_third_escalating_cost_square_feet = '" + data[82].ToString() + "', reo_third_escalating_cost_square_amount = '" + data[83].ToString()
                    + "', reo_third_escalating_cost_square_check = '" + data[84].ToString() + "', reo_fourth_escalating_cost_units_amount = '" + data[85].ToString() + "', reo_fourth_escalating_cost_units_check = '" + data[86].ToString()
                    + "', reo_fourth_escalating_cost_square_feet = '" + data[87].ToString() + "', reo_fourth_escalating_cost_square_amount = '" + data[88].ToString() + "', reo_fourth_escalating_cost_square_check = '" + data[89].ToString()
                    + "', reo_fifth_escalating_cost_units_amount = '" + data[90].ToString() + "', reo_fifth_escalating_cost_units_check = '" + data[91].ToString() + "', reo_fifth_escalating_cost_square_feet = '" + data[92].ToString()
                    + "', reo_fifth_escalating_cost_square_amount = '" + data[93].ToString() + "', reo_fifth_escalating_cost_square_check = '" + data[94].ToString() + "' where reo_id = (select reo_id from tbl_VPR_Workable_Registration_Cost where id = '" + id + "') ";

                query += @"update tbl_VPR_Cost_Commercial set commercial_reg_cost = '" + data[95].ToString() + "', commercial_reg_cost_units = '" + data[96].ToString() + "', commercial_reg_cost_units_amount = '" + data[97].ToString()
                    + "', commercial_reg_cost_units_check = '" + data[98].ToString() + "', commercial_reg_cost_square = '" + data[99].ToString() + "', commercial_reg_cost_square_feet = '" + data[100].ToString()
                    + "', commercial_reg_cost_square_amount = '" + data[101].ToString() + "', commercial_reg_cost_square_check = '" + data[102].ToString() + "', commercial_reg_cost_amount = '" + data[103].ToString() + "', commercial_reg_cost_standard = '" + data[104].ToString()
                    + "', commercial_renewal_cost_escalating = '" + data[105].ToString() + "', commercial_renewal_cost_units = '" + data[106].ToString() + "', commercial_renewal_cost_units_amount = '" + data[107].ToString()
                    + "', commercial_renewal_cost_units_check = '" + data[108].ToString() + "', commercial_renewal_cost_square = '" + data[109].ToString() + "', commercial_renewal_cost_square_feet = '" + data[110].ToString()
                    + "', commercial_renewal_cost_square_amount = '" + data[111].ToString() + "', commercial_renewal_cost_square_check = '" + data[112].ToString() + "', commercial_renewal_cost_amount = '" + data[113].ToString() + "', commercial_escalating_renewal_cost_amount = '" + data[114].ToString()
                    + "', commercial_escalating_renewal_succeding = '" + data[115].ToString() + "', commercial_first_escalating_cost_units_amount = '" + data[116].ToString() + "', commercial_first_escalating_cost_units_check = '" + data[117].ToString() + "', commercial_first_escalating_cost_square_feet = '" + data[118].ToString()
                    + "', commercial_first_escalating_cost_square_amount = '" + data[119].ToString() + "', commercial_first_escalating_cost_square_check = '" + data[120].ToString() + "', commercial_second_escalating_cost_units_amount = '" + data[121].ToString()
                    + "', commercial_second_escalating_cost_units_check = '" + data[122].ToString() + "', commercial_second_escalating_cost_square_feet = '" + data[123].ToString() + "', commercial_second_escalating_cost_square_amount = '" + data[124].ToString()
                    + "', commercial_second_escalating_cost_square_check = '" + data[125].ToString() + "', commercial_third_escalating_cost_units_amount = '" + data[126].ToString() + "', commercial_third_escalating_cost_units_check = '" + data[127].ToString()
                    + "', commercial_third_escalating_cost_square_feet = '" + data[128].ToString() + "', commercial_third_escalating_cost_square_amount = '" + data[129].ToString() + "', commercial_third_escalating_cost_square_check = '" + data[130].ToString()
                    + "', commercial_fourth_escalating_cost_units_amount = '" + data[131].ToString() + "', commercial_fourth_escalating_cost_units_check = '" + data[132].ToString() + "', commercial_fourth_escalating_cost_square_feet = '" + data[133].ToString()
                    + "', commercial_fourth_escalating_cost_square_amount = '" + data[134].ToString() + "', commercial_fourth_escalating_cost_square_check = '" + data[135].ToString() + "', commercial_fifth_escalating_cost_units_amount = '" + data[136].ToString()
                    + "', commercial_fifth_escalating_cost_units_check = '" + data[137].ToString() + "', commercial_fifth_escalating_cost_square_feet = '" + data[138].ToString() + "', commercial_fifth_escalating_cost_square_amount = '" + data[139].ToString()
                    + "', commercial_fifth_escalating_cost_square_check = '" + data[140].ToString() + "' where commercial_id = (select commercial_id from tbl_VPR_Workable_Registration_Cost where id = '" + id + "')";

                query += @"update tbl_VPR_Cost_Vacant set vacant_reg_cost = '" + data[141].ToString() + "', vacant_reg_cost_units = '" + data[142].ToString() + "', vacant_reg_cost_units_amount = '" + data[143].ToString()
                    + "', vacant_reg_cost_units_check = '" + data[144].ToString() + "', vacant_reg_cost_square = '" + data[145].ToString() + "', vacant_reg_cost_square_feet = '" + data[146].ToString() + "', vacant_reg_cost_square_amount = '" + data[147].ToString()
                    + "', vacant_reg_cost_square_check = '" + data[148].ToString() + "', vacant_reg_cost_amount = '" + data[149].ToString() + "', vacant_reg_cost_standard = '" + data[150].ToString() + "', vacant_renewal_cost_escalating = '" + data[151].ToString()
                    + "', vacant_renewal_cost_units = '" + data[152].ToString() + "', vacant_renewal_cost_units_amount = '" + data[153].ToString() + "', vacant_renewal_cost_units_check = '" + data[154].ToString() + "', vacant_renewal_cost_square = '" + data[155].ToString()
                    + "', vacant_renewal_cost_square_feet = '" + data[156].ToString() + "', vacant_renewal_cost_square_amount = '" + data[157].ToString() + "', vacant_renewal_cost_square_check = '" + data[158].ToString() + "', vacant_renewal_cost_amount = '" + data[159].ToString()
                    + "', vacant_escalating_renewal_cost_amount = '" + data[160].ToString() + "', vacant_escalating_renewal_succeding = '" + data[161].ToString() + "', vacant_first_escalating_cost_units_amount = '" + data[162].ToString() + "', vacant_first_escalating_cost_units_check = '" + data[163].ToString()
                    + "', vacant_first_escalating_cost_square_feet = '" + data[164].ToString() + "', vacant_first_escalating_cost_square_amount = '" + data[165].ToString() + "', vacant_first_escalating_cost_square_check = '" + data[166].ToString()
                    + "', vacant_second_escalating_cost_units_amount = '" + data[167].ToString() + "', vacant_second_escalating_cost_units_check = '" + data[168].ToString() + "', vacant_second_escalating_cost_square_feet = '" + data[169].ToString() + "', vacant_second_escalating_cost_square_amount = '" + data[170].ToString()
                    + "', vacant_second_escalating_cost_square_check = '" + data[171].ToString() + "', vacant_third_escalating_cost_units_amount = '" + data[172].ToString() + "', vacant_third_escalating_cost_units_check = '" + data[173].ToString() + "', vacant_third_escalating_cost_square_feet = '" + data[174].ToString()
                    + "', vacant_third_escalating_cost_square_amount = '" + data[175].ToString() + "', vacant_third_escalating_cost_square_check = '" + data[176].ToString() + "', vacant_fourth_escalating_cost_units_amount = '" + data[177].ToString() + "', vacant_fourth_escalating_cost_units_check = '" + data[178].ToString()
                    + "', vacant_fourth_escalating_cost_square_feet = '" + data[179].ToString() + "', vacant_fourth_escalating_cost_square_amount = '" + data[180].ToString() + "', vacant_fourth_escalating_cost_square_check = '" + data[181].ToString() + "', vacant_fifth_escalating_cost_units_amount = '" + data[182].ToString()
                    + "', vacant_fifth_escalating_cost_units_check = '" + data[183].ToString() + "', vacant_fifth_escalating_cost_square_feet = '" + data[184].ToString() + "', vacant_fifth_escalating_cost_square_amount = '" + data[185].ToString() + "', vacant_fifth_escalating_cost_square_check = '" + data[186].ToString()
                    + "' where vacant_id = (select vacant_id from tbl_VPR_Workable_Registration_Cost where id = '" + id + "')";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set costSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCont")
            {
                query = @"update tbl_VPR_Workable set
                                    [state] = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', cont_reg = '" + data[3].ToString() + "', update_reg = '" + data[4].ToString() +
                                    "', modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set contRegSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "inspect")
            {
                query = @"update tbl_VPR_Workable_Inspection set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', municipal_inspection_fee_required = '" + data[3].ToString()
                        + "', onetime_only_upon_reg = '" + data[4].ToString() + "', fee_payment_frequency = '" + data[5].ToString() + "', payon1 = '" + data[6].ToString() + "', payon2 = '" + data[7].ToString()
                        + "', payon3 = '" + data[8].ToString() + "', payon4 = '" + data[9].ToString() + "', free_escalating = '" + data[10].ToString() + "', municipal_inspection_fee = '" + data[11].ToString()
                        + "', escalating_amount = '" + data[12].ToString() + "', escalating_succeeding = '" + data[13].ToString() + "', inspection_report_required = '" + data[14].ToString()
                        + "', inspection_frequency_occupied = '" + data[15].ToString() + "', inspection_frequency_vacant = '" + data[16].ToString() + "', how_to_send_inspection_report = '" + data[17].ToString()
                        + "', modified_by = '" + user.ToString() + "', date_modified = GetDate() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set inspectionSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }

                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "municipalityInfo")
            {
                query = @"update tbl_VPR_Workable_Municipality set
                        [state]  = '" + data[1].ToString() + "', city  = '" + data[2].ToString() + "', municipality_department  = '" + data[3].ToString() +
                        "', municipality_phone_num = '" + data[4].ToString() + "', municipality_email  = '" + data[5].ToString() + "', municipality_st  = '" + data[6].ToString() +
                        "', municipality_city  = '" + data[7].ToString() + "', municipality_state  = '" + data[8].ToString() + "', municipality_zip  = '" + data[9].ToString() +
                        "', contact_person  = '" + data[10].ToString() + "', title = '" + data[11].ToString() + "', department  = '" + data[12].ToString() + "', phone_num  = '" + data[13].ToString() +
                        "', email  = '" + data[14].ToString() + "', address  = '" + data[15].ToString() + "', ops_hrs_from  = '" + data[16].ToString() + "', ops_hrs_to  = '" + data[17].ToString() +
                        "', modified_by  = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set municipalitySetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "dereg")
            {
                query = @"update tbl_VPR_Workable_Deregistration set [state]  = '" + data[1].ToString() + "', city  = '" + data[2].ToString() + "', dereg_req  = '" + data[3].ToString() +
                        "', conveyed = '" + data[4].ToString() + "', occupied  = '" + data[5].ToString() + "', how_to_dereg  = '" + data[6].ToString() +
                        "', upload_file  = '" + data[7].ToString() + "', new_owner_info_req  = '" + data[8].ToString() + "', proof_of_conveyance_req  = '" + data[9].ToString() +
                        "', date_of_sale_req  = '" + data[10].ToString() +
                        "', modified_by  = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + id + "';";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set deregistrationSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regOrdinance")
            {
                query = "insert into tbl_VPR_Workable_Ordinance (state, city, ordinance_num, ordinance_name, description, section, source, enacted_date, revision_date, ordinance_file, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + user.ToString() + "', GETDATE());";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        mainSettings = "update tbl_VPR_MainSettings set ordinanceSetting = '3' where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                        cls.ExecuteQuery(mainSettings);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            
               
            else if (data[0] == "zip")
            {
                query = "update tbl_DB_US_States set " +
                                "isZipActive = " + data[4].ToString() + " " +
                                "where state = '" + data[1].ToString() + "' and " +
                                "city = '" + data[2].ToString() + "' and " +
                                "ZipCode = '" + data[3].ToString() + "'";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Zip Codes");
                        arrPages.Add("");

                        if (data[4].ToString() == "1")
                        {
                            cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
                        }
                        else
                        {
                            cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Inactive", arrPages);
                        }

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }

            return tag;
        }


        [WebMethod]
        public static string checkApply(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string query = "", tag = "";

            DataTable dtCheckApply = new DataTable();
            DataTable dtCheckApply1 = new DataTable();


            query = @"select * from tbl_VPR_MainSettings where pfcSetting = '3' and reoSetting = '3' and propTypeSetting = '3' and
                        requirementSetting = '3' and costSetting = '3' and contRegSetting = '3' and inspectionSetting = '3' and
                        municipalitySetting = '3' and deregistrationSetting = '3' and additionalSetting = '3'
                        and state = '" + state + "' and city = '" + city + "'";
            dtCheckApply = cls.GetData(query);


            if (dtCheckApply.Rows.Count > 0)
            {
                tag = "1";
            }
            else
            {
                tag = "0";
            }

            return tag;
        }

        [WebMethod]
        public static string isApply(string state, string city, string name, string user)
        {
            clsConnection cls = new clsConnection();

            string query = "", queryUpdate = "", tag = "", query1 = "";

            query = @"select * from tbl_VPR_MainSettings where pfcSetting = '3' and reoSetting = '3' and propTypeSetting = '3' and
                    requirementSetting = '3' and costSetting = '3' and contRegSetting = '3' and inspectionSetting = '3' and
                    municipalitySetting = '3' and deregistrationSetting = '3' and ordinanceSetting = '3' and additionalSetting = '3'
                    and state = '" + state + "' and city = '" + city + "'";

                DataTable dtApply = cls.GetData(query);

                if (dtApply.Rows.Count > 0)
                {
                    query = "";
                    query += "update tbl_VPR_Workable_Registration_PFC set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Registration_REO set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "'";
                    query += "update tbl_VPR_Workable_Registration_Property set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Registration_PropReq set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Registration_Cost set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Inspection set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Municipality set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Deregistration set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";
                    query += "update tbl_VPR_Workable_Ordinance set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "'";
                    query += "update tbl_VPR_Workable_Additional_Forms set tag = 'PASSED' where state = '" + state + "' and city = '" + city + "';";

                    query += @"update tbl_VPR_MainSettings set isApply = '3',pfcSetting = '1' 
                             ,reoSetting = '1',propTypeSetting = '1',requirementSetting = '1',costSetting = '1' 
                             ,contRegSetting = '1', inspectionSetting = '1', municipalitySetting= '1'
                             ,deregistrationSetting = '1', ordinanceSetting = '1', additionalSetting = '1' where state = '" + state + "' and city = '" + city + "' and isApply = '2';";

                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }

                }
                else
                {
                    tag = "0";
                }
            
            return tag;
        }
        [WebMethod]
        public static string SaveAdditional(List<string> data, string state, string city,string user)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string tag = "";
            string dataInsert = "";
            string query = "";

            dataInsert += "'" + state + "','" + city + "',";

            for (int i = 0; i < data.Count(); i++)
            {
                dataInsert += "'" + data[i].ToString() + "',";
            }

            dataInsert = dataInsert.Remove(dataInsert.Length - 1, 1);

            string qrySelect = "Select * From tbl_VPR_Workable_Additional_Forms where state = '" + state + "' and city = '" + city + "'";
            dt = cls.GetData(qrySelect);
            if (dt.Rows.Count == 0)
            {
                query = @"Insert Into tbl_VPR_Workable_Additional_Forms (state,city,BondForm,BondForm_File,InsuranceCer,InsuranceCer_File,MaintenancePlan,MaintenancePlan_File,
                                    NonOwner,NonOwner_File,OutOfCountry,OutOfCountry_File,ProofOfUtilities,ProofOfUtilities_File,SignageForm,SignageForm_File,
                                    Statement,Statement_File,Structure,Structure_File,TrespassAffidavit,TrespassAffidavit_File, tag, date_modified, modified_by) values (" + dataInsert + ")";
            }
            else
            {
                query = @"update tbl_VPR_Workable_Additional_Forms Set BondForm = '" + data[0].ToString() + "', BondForm_Check1 = '" + data[1].ToString() + "', BondForm_File = '" + data[2].ToString()
                    + "', InsuranceCer = '" + data[3].ToString() + "', InsuranceCer_Check1 = '" + data[4].ToString() + "', InsuranceCer_File = '" + data[5].ToString() + "', MaintenancePlan = '" + data[6].ToString()
                    + "', MaintenancePlan_Check1 = '" + data[7].ToString() + "', MaintenancePlan_File = '" + data[8].ToString() + "', NonOwner = '" + data[9].ToString() + "', NonOwner_Check1 = '" + data[10].ToString()
                    + "', NonOwner_File = '" + data[11].ToString() + "', OutOfCountry = '" + data[12].ToString() + "', OutOfCountry_Check1 = '" + data[13].ToString() + "', OutOfCountry_File = '" + data[14].ToString()
                    + "', ProofOfUtilities = '" + data[15].ToString() + "', ProofOfUtilities_Check1 = '" + data[16].ToString() + "', ProofOfUtilities_File = '" + data[17].ToString() + "', SignageForm = '" + data[18].ToString()
                    + "',  SignageForm_Check1 = '" + data[19].ToString() + "', SignageForm_File = '" + data[20].ToString() + "', Statement = '" + data[21].ToString() + "', Statement_Check1 = '" + data[22].ToString()
                    + "', Statement_File = '" + data[23].ToString() + "', Structure = '" + data[24].ToString() + "', Structure_Check1 = '" + data[25].ToString() + "', Structure_File = '" + data[26].ToString()
                    + "', TrespassAffidavit = '" + data[27].ToString() + "', TrespassAffidavit_Check1 = '" + data[28].ToString() + "', TrespassAffidavit_File = '" + data[29].ToString() + "', date_modified = GETDATE(), modified_by = '" + user.ToString() + "' where state = '" + state + "' and city = '" + city
                    + "'";
            }
            try
            {
                int exec = cls.ExecuteQuery(query);

                if (exec > 0)
                {
                    string mainSettings = "update tbl_VPR_MainSettings set additionalSetting = '3' where state = '" + state + "' and city = '" + city + "';";
                    cls.ExecuteQuery(mainSettings);

                    tag = "1";
                }
                else
                {
                    tag = "0";
                }
            }
            catch (Exception)
            {
                tag = "0";
            }
            return tag;
        }

        [WebMethod]
        public static string GetAdditionalForms(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qry = @"Select * From tbl_VPR_Workable_Additional_Forms where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            string queryLook = @"select * from tbl_VPR_MainSettings where state = '" + state + "' and city = '" + city + "';";

            DataTable dt = new DataTable();
            DataTable dtLook = new DataTable();
            dt = cls.GetData(qry);
            dtLook = cls.GetData(queryLook);

            if (dtLook.Rows[0]["additionalSetting"].ToString() == "3")
            {
                return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt, check = dtLook } });
            }
            else
            {
                return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { asd = dt } });
            }

        }
    }
}