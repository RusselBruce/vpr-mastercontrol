﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel = Microsoft.Office.Interop.Excel;

namespace Master_Control
{
	public partial class List : System.Web.UI.Page
	{
        public static DataTable dt;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[WebMethod]
		public static string GetList(string type)
		{
			clsConnection cls = new clsConnection();
			DataTable dtList = new DataTable();

			string query = "select * from tbl_VPR_DataList (nolock) where type = '" + type + "' and date_deleted is null order by id";

			dtList = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtList } });
		}

		[WebMethod]
		public static void AddList(ArrayList data)
		{
			clsConnection cls = new clsConnection();

			string query = "insert into tbl_VPR_DataList (title, description, isActive, type, date_added) values ('" + data[0].ToString() + "', '" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', GETDATE());";

			try
			{
				cls.ExecuteQuery(query);
			}
			catch (Exception ex)
			{

			}
		}

		[WebMethod]
		public static void DeleteList(string listID)
		{
			clsConnection cls = new clsConnection();
			string query = "update tbl_VPR_DataList set " +
							"date_deleted = GETDATE() " +
							"where id = " + listID;
			try
			{
				cls.ExecuteQuery(query);
			}
			catch (Exception ex)
			{

			}
		}

        protected void filUploadBtn_Click(object sender, EventArgs e)
        {
			//if( FileUpload1.HasFile)
			//{
			//	string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
			//	string extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
			//	string folderpath = Server.MapPath("~/Excel_Temp/" + filename);
			//	FileUpload1.SaveAs(folderpath);
             
			//	getExcelFile(folderpath);
			//}
        }

        public void getExcelFile(string path)
        {
            //DateTime dtime = DateTime.Today;
            DateTime dtime = Convert.ToDateTime("03-16-2017");
            string fname = dtime.Month < 10 ? "0" + dtime.Month.ToString() : dtime.Month.ToString();
            fname += dtime.Day < 10 ? "0" + dtime.Day.ToString() : dtime.Day.ToString();
            fname += dtime.Year.ToString();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@""+path);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            try
            {

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                DataTable dt = new DataTable();

                ArrayList col = new ArrayList();

                for (int x = 1; x <= colCount; x++)
                {
                    int z = 1;
                    if (x > 1)
                    {
                        if (col.Contains(Convert.ToString(xlRange.Cells[1, x].Value)))
                        {
                            dt.Columns.Add(Convert.ToString(xlRange.Cells[1, x].Value) + z.ToString(), typeof(string));
                            col.Add(Convert.ToString(xlRange.Cells[1, x].Value) + z.ToString());
                        }
                        else
                        {
                            dt.Columns.Add(Convert.ToString(xlRange.Cells[1, x].Value), typeof(string));
                            col.Add(Convert.ToString(xlRange.Cells[1, x].Value));
                        }
                    }
                    else
                    {
                        dt.Columns.Add(Convert.ToString(xlRange.Cells[1, x].Value), typeof(string));
                        col.Add(Convert.ToString(xlRange.Cells[1, x].Value));
                    }

                }

                string qry = "";

                for (int i = 2; i <= rowCount; i++)
                {
                    if (Convert.ToString(xlRange.Cells[i, 1].Value) != null)
                    {
                        string isAc = Convert.ToString(xlRange.Cells[i, 3].Value);
                        if(isAc == "Y")
                        {
                            isAc = "1";
                        }
                        else
                        {
                            isAc = "0";
                        }
                        qry += "INSERT INTO tbl_VPR_DataList(title,description,isActive,type,date_added) ";
                        qry += "VALUES ('" + Convert.ToString(xlRange.Cells[i, 1].Value) + "','" + Convert.ToString(xlRange.Cells[i, 2].Value) + "','" + isAc + "','REO',GETDATE());";

                    }
                }
                //excelFileDGV.DataSource = dt;
                clsConnection cls = new clsConnection();
                cls.ExecuteQuery(qry);
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

            }
            finally
            {
                //cleanup
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:
                //  never use two dots, all COM objects must be referenced and released individually
                //  ex: [somthing].[something].[something] is bad

                //release com objects to fully kill excel process from running in the background
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);

                //close and release
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

                //quit and release
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                
            }

        }

		[WebMethod]
		public static string GetState()
		{
			clsConnection cls = new clsConnection();
			DataTable dtState = new DataTable();

			string query = "select DISTINCT(LTRIM(RTRIM(State))) [State], isStateActive from tbl_DB_US_States where State is not null and State <> '' order by [State]";

			dtState = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
		}

		[WebMethod]
		public static string GetCitySet(string state)
		{
			clsConnection cls = new clsConnection();
			DataTable dtCity = new DataTable();

			string query = "select DISTINCT(LTRIM(RTRIM(City))) [City], LTRIM(RTRIM(ZipCode)) [Zip], case when County is null then '--' else LTRIM(RTRIM(County)) end [County], isCityActive from tbl_DB_US_States where LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' order by [City]";

			dtCity = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
		}

		[WebMethod]
		public static void SetActive(string type, string isActive, string state, string city, string zip)
		{
			clsConnection cls = new clsConnection();

			string query = "";

			if (type == "state")
			{
				query = "update tbl_DB_US_States set isStateActive = '" + isActive + "' where LTRIM(RTRIM(State)) = '" + state + "'";
			}
			else
			{
				query = "update tbl_DB_US_States set isCityActive = '" + isActive + "' where LTRIM(RTRIM(City)) = '" + city + "' and LTRIM(RTRIM(ZipCode)) = '" + zip + "'";
			}

			cls.ExecuteQuery(query);
		}
	}
}