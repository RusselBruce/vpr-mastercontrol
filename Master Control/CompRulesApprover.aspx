﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompRulesApprover.aspx.cs" Inherits="Master_Control.CompRulesApprover" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tblState tbody td, #tblMunicipality tbody td {
            padding: 0px;
            margin: 0px;
        }

        .rowHover:hover {
            cursor: pointer;
            color: #000;
            background-color: #eff8b3;
        }

        .btnApply {
            background-color: #669b31;
            color: white;
            margin-left: 20px;
            width: 100px;
            font-size: 13px;
        }

        .activeRow {
            color: #fff;
            background-color: #4d6578;
        }

        #navReg {
            list-style: none;
            display: inline-flex;
        }

        .yesNo {
            width: 85px;
            margin-left: -70px;
        }

        .crosscheck {
            margin-left: -50px;
        }

        .fromSpan {
            float: left;
        }

        .line1 {
            margin-left: -25px;
        }

        .line2 {
            margin-left: 20px;
        }

        .line3 {
            margin-left: -10px;
        }

        .line4 {
            margin-left: -40px;
        }

        .line5 {
            margin-right: 64px;
            margin-left: -10px;
        }

        .tb34 {
            width: 90px;
        }

        .tb1 {
            width: 80px;
        }

        .tb2 {
            width: 50px;
        }

        .tb5 {
            width: 175px;
        }

        .lblSpan {
            float: left;
            margin-left: -30px;
        }

        #navReg li {
            padding: 5px;
        }

            #navReg li.active {
                border-top-color: #fff !important;
            }

                #navReg li.active button {
                    color: #fff !important;
                }

        #ulWorkSettings li.active a {
            color: #1e3d99;
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Compliance Rules</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li>
                <span><i class="fa fa-tasks"></i>&nbsp;Compliance Rules</span>
            </li>
            <li class="active">
                <span><i class="fa fa-tasks"></i>&nbsp;Compliance Rules - Approver</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default" style="margin-top: 1%;">
            <div class="box-body">
                <div class="col-xs-2" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>States</label>
                        </div>
                        <div class="panel-body" style="min-height: 760px; max-height: 760px; overflow-y: auto;">
                            <table id="tblState" class="table no-border">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>Workable Settings</label>
                        </div>
                        <div class="panel-body" style="padding: 5px;">
                            <div class="col-xs-2 noPadMar">
                                <div class="panel panel-default noPadMar">
                                    <div class="panel-heading text-center">
                                        <label>Municipality</label>
                                    </div>
                                    <div class="panel-body" style="min-height: 710px; max-height: 710px; overflow-y: auto;">
                                        <table id="tblMunicipality" class="table no-border">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="divSettings" class="col-xs-10 noPadMar" style="display: none;">
                                <div class="panel panel-default noPadMar">
                                    <div class="panel-body noPadMar">
                                        <div class="nav-tabs-custom noPadMar">
                                            <ul id="ulWorkSettings" class="nav nav-tabs text-nowrap" style="display: flex;">
                                                <li class="active" id="liRegistration">
                                                    <a href="#tabRegistration" data-toggle="tab" id="idRegistration">Registration Criteria</a>
                                                </li>
                                                <li id="liInspection">
                                                    <a href="#tabInspection" data-toggle="tab" id="idInspection">Inspection</a>
                                                </li>
                                                <li id="liMunicipality">
                                                    <a href="#tabMunicipality" data-toggle="tab" id="idMunicipality">Municipality Contact Information</a>
                                                </li>

                                                <li id="liDeRegistration">
                                                    <a href="#tabDeregistration" data-toggle="tab" id="idDeregistration">Deregistration</a>
                                                </li>
                                                <li id="liOrdinance">
                                                    <a href="#tabOrdinance" data-toggle="tab" id="idOrdinance">Ordinance Settings</a>
                                                </li>
                                                <li id="liZip">
                                                    <a href="#tabZip" data-toggle="tab" id="idZipCodes">Zip Codes</a>
                                                </li>
                                                <li id="liNotif">
                                                    <%--<a href="#tabNotif" data-toggle="tab" id="idNotif">Notification Settings</a>--%>
                                                    <button type="button" class="btn btn-sm btnApply"><i class="fa fa-check"></i>APPLY</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <%-- Registration --%>
                                            <div id="tabRegistration" class="tab-pane active">
                                                <div class="panel panel-default noPadMar">
                                                    <div class="panel-body noPadMar">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-2">
                                                                <%--<label>Copy from:</label>--%>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <%--<select class="form-control input-sm slctCopyMuni select2" style="width: 255px;"></select>--%>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <%--<button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration"><i class="fa fa-chevron-circle-right"></i></button>--%>
                                                            </div>
                                                            <%--<div class="col-xs-2 liApply" style="float: right; padding-left: 50px">
                                                                <button type="button" class="btn btn-md btnApply" style="background-color: #669b31; color: white;" disabled>&nbsp;APPLY</button>
                                                            </div>--%>
                                                        </div>
                                                        <div class="nav-tabs-custom noPadMar">
                                                            <ul class="nav nav-tabs" id="navReg">
                                                                <li class="active">
                                                                    <button type="button" class="btn btn-primary" href="#tabPFC" id="btntabPFC" data-toggle="tab">PFC</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabREO" id="btntabREO" data-toggle="tab">REO</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropType" id="btntabProperty" data-toggle="tab">Property Type</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropReq" id="btntabRequirements" data-toggle="tab">Requirements</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabAddForms" id="AddForms" data-toggle="tab">Additional Forms</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabCost" id="btntabCost" data-toggle="tab">Cost</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabContReg" id="ContReg" data-toggle="tab">Continuing Registration</button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content">
                                                            <%-- PFC DEFAULT--%>

                                                            <div id="tabPFC" class="tab-pane active">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        <input id="pfcReg" type="checkbox" />
                                                                        <label>No PFC Registrations</label>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <input id="pfcStatewideReg" type="checkbox" />
                                                                        <label>Statewide Registration</label>
                                                                    </div>
                                                                </div>
                                                                <%-- PFC DEFAULT --%>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCDefault">
                                                                        PFC Default
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcDefault" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCTImeline">
                                                                        PFC Default Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inDefRegTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctDefRegTimeline2" type="number" min="-1" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctDefRegTimeline3" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctDefRegTimeline4" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>

                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctDefRegTimeline5" class="form-control input-sm tb5">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC DEFAULT --%>

                                                                <%-- PFC VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCVacant">
                                                                        PFC Vacant
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcVacantTimeline" class="form-control input-sm tb1">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCVacantTimeline">
                                                                        PFC Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcVacantTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcVacantTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcVacantTimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcVacantTimeline4" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcVacantTimeline5" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC VACANT --%>

                                                                <%-- PFC FORECLOSURE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCForeclosure">
                                                                        PFC Foreclosure
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcForeclosure" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCForeclosureTimeline">
                                                                        PFC Foreclosure Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcDefForclosureTimeline1" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcDefForclosureTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcDefForclosureTimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcDefForclosureTimeline4" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcDefForclosureTimeline5" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC FORECLOSURE --%>

                                                                <%-- FORECLOSURE AND VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCForeclosureVacant">
                                                                        PFC Foreclosure and Vacant
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcForeclosureVacant" class="form-control input-sm tb1">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCForeclosureVacantTimeline">
                                                                        PFC Foreclosure and Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcForeclosureVacantTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcForeclosureVacantTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline4" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline5" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END FORECLOSURE AND VACANT --%>

                                                                <%-- PFC CITY NOTICE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCCityNotice">
                                                                        PFC City Notice
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcCityNotice" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCCityNoticeTimeline">
                                                                        PFC City Notice Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcCityNoticeTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcCityNoticeTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcCityNoticeTimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcCityNoticeTimeline4" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcCityNoticeTimeline5" class="form-control input-sm tb5">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC CITY NOTICE --%>

                                                                <%-- PFC CODE VIOLATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCCodeViolation">
                                                                        PFC Code Violation
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcCodeViolation" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCCodeViolationTimeline">
                                                                        PFC Code Violation Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcCodeViolationTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcCodeViolationTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcCodeViolationTimeline3" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcCodeViolationTimeline4" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcCodeViolationTimeline5" class="form-control input-sm tb5">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC CODE VIOLATION --%>

                                                                <%-- PFC BOARDED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCBoarded">
                                                                        PFC Boarded
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcBoarded" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCBoardedTimeline">
                                                                        PFC Boarded Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="inPfcBoardedTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctPfcBoardedTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcBoardedTimeline3" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcBoardedTimeline4" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcBoardedTimeline5" class="form-control input-sm tb5">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC BOARDED --%>

                                                                <%-- PFC OTHERS --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCOther">
                                                                        PFC OTHER
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctPfcOther" class="form-control input-sm tb1">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCOtherTimeline">
                                                                        PFC OTHER Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 audit line1">
                                                                        <select id="inPfcOtherTimeline1" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 audit line2">
                                                                        <input id="slctPfcOtherTimeline2" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctPfcOtherTimeline3" class="form-control input-sm tb34">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctPfcOtherTimeline4" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctPfcOtherTimeline5" class="form-control input-sm tb5">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC OTHERS --%>


                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Special Requirements
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctSpcReq" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Payment Type
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit line1">
                                                                        <select id="slctPaymentType" class="form-control input-sm">
                                                                            <option value="">None</option>
                                                                            <option value="check">Check</option>
                                                                            <option value="credit card online">Credit Card - Online</option>
                                                                            <option value="credit card phone">Credit Card - Phone</option>
                                                                            <option value="dpp">DPP</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCTypeofregistration">
                                                                        Type of Registration
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctTypeOfRegistration" class="form-control input-sm ">
                                                                            <option value="">N/A</option>
                                                                            <option value="PDF">City Registration Form</option>
                                                                            <option value="Online">City Website</option>
                                                                            <option value="Prochamps">ProChamps Online</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-5 audit" style="display: none;">
                                                                        <input id="inUploadPath" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDF" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1 audit" style="padding-left: 0px;">
                                                                        <button id="btnSavePDF" type="button" class="btn btn-primary audit" title="Upload File" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblPFCRenewal">
                                                                        Renewals
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit line1">
                                                                        <select id="slctVmsRenewal" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar renewYes renewEvery audit">
                                                                        <input type="radio" id="RDEVERY" name="rdRenewal" value="Every" checked="checked" />
                                                                        Every
                                                                    </div>
                                                                    <div class="col-xs-2 renewYes audit">
                                                                        <select id="slctRenewEveryNum" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2 renewYes audit">
                                                                        <select id="slctRenewEveryYears" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="Year/s">Year/s</option>
                                                                            <option value="Month/s">Month/s</option>
                                                                            <option value="Week/s">Week/s</option>
                                                                            <option value="Day/s">Day/s</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="renewYes" style="margin-top: 4%;">
                                                                        <div class="col-xs-5">
                                                                            &nbsp;
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar renewOn audit" style="margin-left: 69px;">
                                                                            <input type="radio" id="RDON" name="rdRenewal" value="On" />
                                                                            On
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="slctRenewOnMonths" class="form-control input-sm"></select>
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="slctRenewOnNum" class="form-control input-sm"></select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-3">
                                                                        </div>
                                                                        <div class="col-xs-2" style="margin-left: 47px;">
                                                                        </div>
                                                                        <div class="col-xs-5 renewUploadPFC audit" style="display: none;">
                                                                            <input id="inUploadPath1" name="upload1" type="file" class="file form-control input-sm fileinput1" data-show-preview="false" />
                                                                            <button id="linkPDFRenewal" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                        </div>
                                                                        <div class="col-xs-1 audit" style="padding-left: 0px;">
                                                                            <button id="btnSavePDFRenew" type="button" class="btn btn-primary" title="Upload File"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                                        </div>
                                                                    </div>
                                                                </div>



                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPFC" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <%-- REO --%>
                                                            <div id="tabREO" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        <input id="reoReg" type="checkbox" />
                                                                        <label>No REO Registrations</label>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <input id="reoStatewideReg" type="checkbox" />
                                                                        <label>Statewide Registration</label>
                                                                    </div>
                                                                </div>
                                                                <%-- REO BANKED-OWED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOBankedOwed">
                                                                        REO Bank-owned
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoBankOwed" class="form-control input-sm" style="width: 80px;">
                                                                            <%--<option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOBankOwedTimeline">
                                                                        REO Bank-owned Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="Drpreobanktime" class="form-control input-sm" style="width: 80px;">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="inReoBankOwed1" type="number" class="form-control input-sm" style="width: 50px;" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoBankOwed2" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoBankOwed3" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoBankOwed4" class="form-control input-sm" style="width: 175px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <%-- END REO BANKED-OWED --%>

                                                                <%-- REO VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOVacant">
                                                                        REO Vacant
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoVacant" class="form-control input-sm" style="width: 80px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOVacantTimeline">
                                                                        REO Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctReowith" class="form-control input-sm" style="width: 80px;">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="inReoVacantTimeline1" type="number" class="form-control input-sm" style="width: 50px;" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoVacantTimeline2" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoVacantTimeline3" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoVacantTimeline4" class="form-control input-sm" style="width: 175px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO VACANT --%>

                                                                <%-- REO CITY NOTICE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOCityNotice">
                                                                        REO City Notice
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoCityNotice" class="form-control input-sm" style="width: 80px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOCityNoticeTimeline">
                                                                        REO City Notice Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctReocitynoticewith" class="form-control input-sm" style="width: 80px;">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctReocitytimeline1" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoBusinesstimeline2" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReodaystimeline3" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReovacancytimeline4" class="form-control input-sm" style="width: 175px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO CITY NOTICE --%>

                                                                <%-- CODE VIOLATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOCodeViolation">
                                                                        REO Code Violation
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoCodeViolation" class="form-control input-sm" style="width: 80px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOCodeViolationTimeline">
                                                                        REO Code Violation Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctReoviolationwith" class="form-control input-sm" style="width: 80px;">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctReoViolationtimeline1" type="number" class="form-control input-sm" style="width: 50px;" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoviolationtimeline2" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoviolationdaystimeline3" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoviolationvacancytimeline4" class="form-control input-sm" style="width: 175px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO CODE VIOLATION --%>

                                                                <%-- REO BOARDED ONLY --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOBoarded">
                                                                        REO Boarded Only
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoBoardedOnly" class="form-control input-sm" style="width: 80px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOBoardedTimeline">
                                                                        REO Boarded Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctReoBoardedwith" class="form-control input-sm" style="width: 80px;">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctReoBoardedtimeline1" type="number" class="form-control input-sm" style="width: 50px;" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoBoardedtimeline2" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoBoardeddaystimeline3" class="form-control input-sm" style="width: 90px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoBoardedtimeline4" class="form-control input-sm" style="width: 175px;">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO BOARDED ONLY --%>

                                                                <%-- REO DISTRESSED ABANDONED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREODistressed">
                                                                        REO Distressed/Abandoned
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoDistressedAbandoned" class="form-control input-sm tb1">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREODistressedTimeline">
                                                                        REO Distressed/Abandoned Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="DrpReoAbandoned" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="inReoDistressedAbandoned1" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoDistressedAbandoned2" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoDistressedAbandoned3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoDistressedAbandoned4" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO DISTRESSED ABANDONED --%>

                                                                <%-- REO RENTAL REGISTRATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREORental">
                                                                        Rental Registration
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctRentalRegistration" class="form-control input-sm tb1">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREORentalTimeline">
                                                                        Rental Registration Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctRentalFormwithin" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="slctReorentaltimeline1" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReorentaltimeline2" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReorentaltimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReorentaltimeline4" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO RENTAL REGISTRATION --%>

                                                                <%-- REO OTHERS --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOOther">
                                                                        REO Other
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 line1 audit">
                                                                        <select id="slctReoOther" class="form-control input-sm tb1">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOOtherTimeline">
                                                                        REO Other Timeline
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-1 line1 audit">
                                                                        <select id="slctreootherwithin" class="form-control input-sm tb1">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="within">Within</option>
                                                                            <option value="after">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-1 line2 audit">
                                                                        <input id="inReoOtherTimeline1" type="number" class="form-control input-sm tb2" />
                                                                    </div>
                                                                    <div class="col-xs-2 line3 audit">
                                                                        <select id="slctReoOtherTimeline2" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 line4 audit">
                                                                        <select id="slctReoOtherTimeline3" class="form-control input-sm tb34">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span class="lblSpan">From</span>
                                                                    <div class="col-xs-2 line5 audit">
                                                                        <select id="slctReoOtherTimeline4" class="form-control input-sm tb5">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                            <option value="fist legal filing">First Legal Filing</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <%-- REO PAYMENT TYPE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Payment Type
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit line1">
                                                                        <select id="slctREOPaymentType" class="form-control input-sm">
                                                                            <option value="" selected="selected">None</option>
                                                                            <option value="check">Check</option>
                                                                            <option value="credit card online">Credit Card - Online</option>
                                                                            <option value="credit card phone">Credit Card - Phone</option>
                                                                            <option value="dpp">DPP</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <%-- REO TYPE OF REGISTRATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREOTypeofregistration">
                                                                        Type of Registration
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit line1">
                                                                        <select id="slctREOTypeOfRegistration" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="PDF">City Registration Form</option>
                                                                            <option value="Online">City Website</option>
                                                                            <option value="Prochamps">ProChamps Online</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-5 audit" style="display: none;">
                                                                        <input id="inREOUploadPath" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDFREO" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1 audit" style="padding-left: 0px;">
                                                                        <button type="button" class="btn btn-primary" id="btnSavePDF2" title="Upload File">
                                                                            <i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                                    </div>
                                                                </div>

                                                                <%-- REO RENEWAL --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblREORenewal">
                                                                        Renewal
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit" style="margin-left: -25px">
                                                                        <select id="slctREOVmsRenewal" class="form-control input-sm">
                                                                            <%--<option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar renewREOYes audit" style="margin-left: 25px">
                                                                        <input type="radio" id="Reordvery" name="rdREORenewal" value="Every" checked="checked" />
                                                                        Every
                                                                    </div>
                                                                    <div class="col-xs-2 renewREOYes renewalss audit">
                                                                        <select id="slctREORenewEveryNum" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2 renewREOYes renewalss audit">
                                                                        <select id="slctREORenewEveryYears" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="Year/s">Year/s</option>
                                                                            <option value="Month/s">Month/s</option>
                                                                            <option value="Week/s">Week/s</option>
                                                                            <option value="Day/s">Day/s</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="renewREOYes renewalss audit" style="margin-top: 4%;">
                                                                        <div class="col-xs-5">
                                                                            &nbsp;
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar renewalss renewREOon" style="margin-left: 69px;">
                                                                            <input type="radio" id="Reordon" name="rdREORenewal" value="On" />
                                                                            On
                                                                        </div>
                                                                        <div class="col-xs-2 renewalss">
                                                                            <select id="slctREORenewOnMonths" class="form-control input-sm"></select>
                                                                        </div>
                                                                        <div class="col-xs-2 renewalss">
                                                                            <select id="slctREORenewOnNum" class="form-control input-sm"></select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 audit" style="margin-top: 1%;">
                                                                        <div class="col-xs-3">
                                                                        </div>
                                                                        <div class="col-xs-2" style="margin-left: 47px;">
                                                                        </div>
                                                                        <div class="col-xs-5 renewUploadREO" style="display: none;">
                                                                            <input id="Reoupload1" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                            <button id="linkPDFRenewalREO" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                        </div>
                                                                        <div class="col-xs-1" style="padding-left: 0px;">
                                                                            <button id="btnSavePDFRenewREO" type="button" class="btn btn-primary" title="Upload File"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegREO" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <%-- Property Type --%>
                                                            <div id="tabPropType" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblResidential">
                                                                        Residential
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropResidential" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-3 lblSingleFamily">
                                                                        Single Family
                                                                    </div>
                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctSingleFamily" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-3 lblMultiFamily">
                                                                        Multi Family
                                                                    </div>
                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctMultiFamily" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3 lbl2Units">
                                                                        2 Units
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slct2units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3 lbl3Units">
                                                                        3 Units
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slct3units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3 lbl4Units">
                                                                        4 Units
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slct4units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3 lbl5Units">
                                                                        5 Units or more
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slct5units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblRental">
                                                                        Rental
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropRental" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblCommercial">
                                                                        Commercial
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropCommercial" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblCondo">
                                                                        Condo
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropCondo" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblTownhome">
                                                                        Townhome
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropTownhome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblVacantLot">
                                                                        Vacant Lot
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropVacantLot" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblMobileHome">
                                                                        Mobile Home
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropMobilehome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegProperty" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- Property Type End--%>
                                                            <%-- Property Requirements --%>

                                                            <div id="tabPropReq" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lbladdForm">
                                                                            Presale "Owner" Definition
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctPresaleDefinition" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="Current Owner on Recorded Deed">Current Owner on Recorded Deed</option>
                                                                                <option value="Trustee/Mortgagee">Trustee/Mortgagee</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblFirstTimeVacancyDate">
                                                                            First Time Vacancy Date
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctFirstTimeVacancyDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblPresale">
                                                                            Local Contact Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <%--<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="pd"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>--%>
                                                                            <select id="slctLocalContactRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblSecuredRequired">
                                                                            Secured Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctSecuredRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblLocalContact">
                                                                            Local Contact Information
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="lci" id="localContactEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblAdditionalSignage">
                                                                            Additional Signage Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctAddSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblContactInfo">
                                                                            GSE Exclusion
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctGseExclusion" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblPicturesRequired">
                                                                            Pictures Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctPicReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblGSE">
                                                                            Insurance Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctInsuranceReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblMobileVin">
                                                                            Mobile VIN Number Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctMobileVINReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblInsurance">
                                                                            Foreclosure Action Information Needed
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctForeclosureActInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblParcelNumber">
                                                                            Parcel Number Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctParcelReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblForeclosureAction">
                                                                            Foreclosure Case Information Needed
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctForeclosureCaseInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblLegalDescription">
                                                                            Legal Description Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctLegalDescReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblForeclosureCase">
                                                                            Foreclosure Deed Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctForeclosureDeedReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblBlockandLot">
                                                                            Block and Lot Number Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctBlockLotReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblForeClosure">
                                                                            Bond Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctBondReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblAttorney">
                                                                            Attorney Information Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctAttyInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="dvBondReq" style="display: none">


                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-6">
                                                                            <div class="col-xs-6 lblForeClosure">
                                                                                One-Time only
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <select id="slctOneTime" class="form-control input-sm">
                                                                                    <option value="" selected="selected">N/A</option>
                                                                                    <option value="true">Yes</option>
                                                                                    <option value="false">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-6">
                                                                            <div class="col-xs-1">
                                                                                <input id="chkPayAgainWhenREO" type="checkbox" />
                                                                            </div>
                                                                            <div class="col-xs-6" style="padding-left: 0px">
                                                                                Pay again when REO
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-6">
                                                                            <div class="col-xs-1">
                                                                                <input id="chkPayUponRenewal" type="checkbox" />
                                                                            </div>
                                                                            <div class="col-xs-6" style="padding-left: 0px">
                                                                                Pay upon Renewal
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-6">
                                                                            <div class="col-xs-6 lblForeClosure">
                                                                                Bond Amount Escalating
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <select id="slctBondAmount" disabled="disabled" class="form-control input-sm">
                                                                                    <option value="" selected="selected">N/A</option>
                                                                                    <option value="true">Yes</option>
                                                                                    <option value="false">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-8">
                                                                            <div class="col-xs-4 lblBondRequired" style="padding-right: 10px;">
                                                                                Bond Amount
                                                                            </div>
                                                                            <div class="col-xs-3" style="padding-left: 32px; padding-right: 0px">
                                                                                <input id="inBondAmount" disabled="disabled" type="text" class="form-control input-sm number" />
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <select id="slctBondAmountUSP" disabled="disabled" class="form-control input-sm">
                                                                                    <option value="" selected="selected">USD</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <%--  <button type="button" disabled="disabled" id="btnEscaBond" class="btn btn-default btn-sm " data-target="#bond">Escalating Bond Schedule</button>--%>

                                                                                <button id="btnEscaBond" data-toggle="collapse" data-target="#bond" type="button" class="btn btn-default btn-sm dvPFCHide dvPFCHide1 " aria-expanded="true">&nbsp;Escalating Renewal Schedule</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                            <div class="col-xs-6 ">
                                                                                <%--  <select id="slctBondAmountUSP" class="form-control input-sm">
                                                                                <option value="" selected="selected">USD</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>--%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <%--NEW ADD--%>
                                                                    <div id="bond" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black; overflow: auto;">
                                                                        <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                            <button type="button" data-toggle="collapse" data-target="#bond" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                        <div id="dvappendBond">
                                                                            <div class="col-xs-12 " id="dvBond">
                                                                                <div class="col-xs-1">
                                                                                    1st
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <input id="txtBond1" type="text" class="form-control input-sm" />
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <select id="slcBond1" class="form-control input-sm">
                                                                                        <option value="" selected="selected">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                        </div>


                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <button id="btnBondAppend" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="btnRemoveBondAppend" type="button" class="btn btn-link"><i class="fa fa-plus text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblBondAmount">
                                                                            Utility Information Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctUtilityInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblBroker">
                                                                            Broker Information Required If REO
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctBrkInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <%-- <div class="col-xs-6 lblBondAmount">
                                                                            Utility Information Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctUtilityInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>--%>
                                                                        <div class="col-xs-6 lblMaintenance">
                                                                            <%--Maintenance Plan Required--%>
                                                                             Winterization Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <%-- <select id="slctMaintePlanReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>--%>
                                                                            <select id="slctWinterReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblMortgage">
                                                                            Mortgage Contact Name Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctMortContactNameReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblNoTresspass">
                                                                            <%--Add Here 2--%>
                                                                            <%--No Trespass Form Required--%>
                                                                           Signature Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <%--<select id="slctNoTrespassReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>--%>
                                                                            <select id="slctSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblClientTax">
                                                                            Client Tax Number Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctClientTaxReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblSignature">
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblUtility">
                                                                            Notarization Required
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctNotarizationReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblNotarization">
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblWinterization">
                                                                            Recent Inspection Date
                                                                        </div>
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkReq(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossReq(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-4 auditReq">
                                                                            <select id="slctRecInsDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6 lblRecentInspection">
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPropReq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <%--Property Requirements End--%>

                                                            <%--Additional Forms--%>

                                                            <div id="tabAddForms" class="tab-pane" style="max-height: 600px; overflow: auto;">
                                                                <br />

                                                                <div class="col-xs-12 noPadMar">
                                                                    <div class="col-md-2" style="padding-left: 3%">Bond Form</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcBondForm" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="chkAltisourceForm1" type="radio" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>

                                                                        <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input id="chkClientFrom1" type="radio" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input id="chkUploadForm1" type="radio" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile1" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView1" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--                     <button id="btnUpload1" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload1" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload1" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView1" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue1" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete1"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm1">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Insurance Certificate</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcInsurance" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm2" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom2" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm2" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile2" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView2" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--<button id="btnUpload2" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload2" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload2" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView2" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue2" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete2"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm2">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Maintenance Plan and Timetable</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcMaintenance" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm3" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom3" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm3" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px;">
                                                                            <input id="UploadFile3" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView3" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--<button id="btnUpload3" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload3" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload3" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView3" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue3" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete3"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm3">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>

                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Non-Owner-Occupied License Application</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcNonOwner" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm4" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom4" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm4" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile4" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView4" class="btn-link "></button>--%>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--                     <button id="btnUpload4" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload4" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload4" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView4" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue4" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete4"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm4">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>

                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Out-of-Country form and Fee</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcOutOfCountry" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                                                            <input type="radio" id="chkAltisourceForm5" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom5" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm5" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile5" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView5" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--<button id="btnUpload5" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload5" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload5" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView5" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue5" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete5"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm5">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Proof of utilities Connection/ disconnection</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcProofOf" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm6" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom6" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm6" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile6" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView6" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%-- <button id="btnUpload6" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload6" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload6" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView6" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue6" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete6"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm6">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Signage Form</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcSignage" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm7" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom7" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm7" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile7" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%-- <button type="button" id="btnView7" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--  <button id="btnUpload7" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload7" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload7" class="audit" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView7" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue7" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red " id="btnDelete7"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm7">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Statement of Intent</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcStatement" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm8" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom8" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm8" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile8" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView8" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%-- <button id="btnUpload8" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload8" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload8" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView8" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue8" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete8"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm8">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Structure Checklist</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcStructure" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm9" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom9" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm9" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile9" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView9" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%-- <button id="btnUpload9" disabled="disabled" type="button" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload9" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload9" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView9" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue9" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete9"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm9">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>


                                                                <div class="col-xs-12 noPadMar" style="padding-top: 12px">
                                                                    <div class="col-md-2" style="padding-left: 3%">Trespass Affidavit Form</div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-md-3 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -35px;">
                                                                        <div class="col-md-4 audit" style="padding-left: 0px; padding-right: 10px">
                                                                            <select id="slcTrespass" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px">
                                                                            <input type="radio" id="chkAltisourceForm10" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-md-6" style="padding-left: 0px; padding-right: 0px">
                                                                            Altisource Form
                                                                        </div>
                                                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            <input type="radio" id="chkClientFrom10" disabled="disabled" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; margin-left: -5px;">
                                                                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; margin-left: -16px;">
                                                                            Client Form
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-5 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -27px;">

                                                                        <div class="col-lg-1" style="padding-left: 8px; padding-right: 16px">
                                                                            <input type="radio" id="chkUploadForm10" disabled="disabled" />
                                                                        </div>
                                                                        <div class="col-lg-3" style="padding-left: 0px; padding-right: 0px; margin-left: -9px;">
                                                                            Upload Form
                                                                        </div>
                                                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px">
                                                                            <input id="UploadFile10" style="height: 50px" type="file" class="form-control input-sm" data-show-preview="false" />
                                                                            <%--<button type="button" id="btnView10" class="btn-link "></button>--%>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1 audit" style="padding-left: 0px; padding-right: 0px; margin-left: -30px;">
                                                                        <%--  <button id="btnUpload10"  type="button" disabled="disabled" class="btn btn-primary btn-sm">Upload</button>--%>
                                                                        <div class="col-xs-1" style="padding-left: 0px; padding-right: 20px;">
                                                                            <button type="button" id="btnUpload10" style="padding: 0px" class="btn btn-link"><i class="fa fa-upload fa-1x" aria-hidden="true"></i></button>
                                                                        </div>


                                                                        <div id="ViewUpload10" style="display: none">
                                                                            <div class="col-xs-1" style="padding-left: 0px; padding-right: 10px;">
                                                                                <button type="button" id="btnView10" style="padding: 0px" class="btn btn-link text-red"><i class="fa fa-file-pdf-o fa-1x" aria-hidden="true"></i></button>
                                                                                <button type="button" id="ViewValue10" style="display: none"></button>
                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-left: 10px; padding-right: 10px;">
                                                                                <button type="button" style="padding: 0px" class="btn btn-link text-red" id="btnDelete10"><i class="fa fa-times fa-1x" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row  dvUploadForm10">
                                                                    <div class="col-lg-5" style="padding-right: 10px"></div>


                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveAdditionalForms" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <%--Additional Forms--%>


                                                            <%-- Cost --%>
                                                            <div id="tabCost" class="tab-pane" style="max-height: 600px; overflow: auto;">
                                                                <div id="firstDiv">
                                                                    <%-- START REGION PFC --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            PFC Registration Cost Y/N
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="PFCslctRegCost" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDPFCUnits" class="with-gap dvPFCHide1" name="PFCSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="divEscalPFC">
                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div id="divEscalPFCCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="PFCRegCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- add button and checkbox --%>
                                                                    <div class="PFCaddButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="PFCUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddPFCCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemovePFCCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%;">
                                                                                <input type="checkbox" id="RDPFCSquare" class="with-gap dvPFCHide1" name="PFCSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="divEscalPFCSquare">
                                                                        <div id="divEscalPFCSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="PFCRegSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="PFCRegSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="PFCaddbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="PFCSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddPFCSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemovePFCSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            PFC Registration Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="PFCinRegCost" type="text" class="form-control input-sm number dvPFCHide1" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="PFCslctRegCostCurr" class="form-control input-sm dvPFCHide1">
                                                                                <option value="USD" selected="selected">USD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            PFC Registration Cost Standard
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="PFCslctRegCostStandard" class="form-control input-sm dvPFCHide1">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-4" id="PFClblReminder" style="display: none;">
                                                                            <i>*Renewal Cost is the same.</i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost Escalating?
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="PFCslctRenewCostEscal" class="form-control input-sm dvPFCHide dvPFCHide1 costEscalatingYES">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDPFCUnits1" class="dvPFCHide dvPFCHide1 costEscalatingYES" name="PFCUnits1" value="Registration Cost based on Number of Units" <%--checked="checked"--%> />
                                                                                Renewal Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="PFCRenewalCost">
                                                                        <div id="PFCdivEscalRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="PFCRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- add button and checkbox --%>
                                                                    <div class="PFCaddRenewalButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="PFCRenewalUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="PFCbtnAddRenewalCCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="PFCbtnRemoveRenewalCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDPFCSquare1" class="with-gap dvPFCHide dvPFCHide1 costEscalatingYES" name="PFCSquare1" value="Registration Cost based on Square Footage" />
                                                                                Renewal Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="PFCRenewalSquare">
                                                                        <div id="PFCdivEscalRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="PFCRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="PFCRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="PFCaddREnewalbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="PFCRenewalSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="PFCbtnAddRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="PFCbtnRemoveRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="PFCinRenewAmt" type="text" class="form-control input-sm number dvPFCHide dvPFCHide1 costEscalatingYES" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="PFCslctRenewAmtCurr1" class="form-control input-sm dvPFCHide dvPFCHide1 costEscalatingYES">
                                                                                <option value="USD">USD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <button id="PFCbtnViewEscalRenewAmt" data-toggle="collapse" data-target="#PFCdivEscalRenewal" type="button" class="btn btn-default btn-sm dvPFCHide dvPFCHide1 costEscalatingYES">&nbsp;Escalating Renewal Schedule</button>
                                                                        </div>
                                                                    </div>
                                                                    <div id="PFCdivEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black; overflow: auto;">
                                                                        <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                            <button type="button" data-toggle="collapse" data-target="#PFCdivEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-2">
                                                                                    <label>Service Type</label>
                                                                                </div>
                                                                                <div class="col-xs-1"></div>
                                                                                <div class="col-xs-3" style="padding-left: 5px;">
                                                                                    <label>Amount</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 --%>
                                                                        <div class="col-xs-12 PFCescalInput1" style="margin-top: 1%;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>1st Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck1">
                                                                                    <input id="PFCRenewalAmount1" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck1">
                                                                                    <select class="form-control input-sm">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalCostFirst">
                                                                            <div id="PFCdivFirstRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfirstRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="col-xs-12 PFCescalRenewalCheckCost1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalCheckCost1" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonCost1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalSquareFirst">
                                                                            <div id="PFCdivFirstRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="PFCfirstRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfirstRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckSquare1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalSquare" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonSquare1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>



                                                                        <%-- 2 --%>
                                                                        <div class="col-xs-12 PFCescalInput2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>2nd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck2">
                                                                                    <input id="PFCRenewalAmount2" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck2">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 PFChideCheck2">
                                                                                    <input id="PFCRenewalCheck2" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalCostSecond">
                                                                            <div id="PFCdivSecondRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCsecondRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckCost2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalCheckCost2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonCost2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalSquareSecond">
                                                                            <div id="PFCdivSecondRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="PFCsecondRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCsecondRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckSquare2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalSquare2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonSquare2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 3 --%>
                                                                        <div class="col-xs-12 PFCescalInput3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>3rd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck3">
                                                                                    <input id="PFCRenewalAmount3" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck3">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 PFChideCheck3">
                                                                                    <input id="PFCRenewalCheck3" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalCostThird">
                                                                            <div id="PFCdivThirdRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCthirdRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckCost3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalCheckCost3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonCost3" style="margin-top: 1%; display: none; padding-left: 30px;">
                                                                            <button id="PFCbtnAddEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalSquareThird">
                                                                            <div id="PFCdivThirdRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="PFCthirdRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCthirdRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 PFCescalRenewalCheckSquare3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalSquare3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonSquare3" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 4 --%>
                                                                        <div class="col-xs-12 PFCescalInput4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>4th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck4">
                                                                                    <input id="PFCRenewalAmount4" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck4">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 PFChideCheck4">
                                                                                    <input id="PFCRenewalCheck4" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalCostFourth">
                                                                            <div id="PFCdivFourthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfourthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckCost4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalCheckCost4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonCost4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalSquareFourth">
                                                                            <div id="PFCdivFourthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="PFCfourthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfourthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckSquare4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalSquare4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonSquare4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 5 --%>
                                                                        <div class="col-xs-12 PFCescalInput5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>5th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck5">
                                                                                    <input id="PFCRenewalAmount5" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 PFChideCheck5">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 PFChideCheck5">
                                                                                    <input id="PFCRenewalCheck5" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalCostFifth">
                                                                            <div id="PFCdivFifthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfifthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 PFCescalRenewalCheckCost5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalCheckCost5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonCost5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="PFCRenewalSquareFifth">
                                                                            <div id="PFCdivFifthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 PFCescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="PFCfifthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="PFCfifthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalCheckSquare5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="PFCescalRenewalSquare5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 PFCescalRenewalButtonSquare5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="PFCbtnAddEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="PFCbtnRemoveEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <button id="PFCbtnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add Next Renewal</i></button>
                                                                            <button id="PFCbtnRemoveNew1" type="button" class="btn btn-link" disabled><i class="fa fa-times text-red"></i>&nbsp;<i>Remove Last Renewal</i></button>
                                                                        </div>
                                                                    </div>
                                                                    <%-- END REGION PFC --%>
                                                                </div>

                                                                <div class="secondDiv">
                                                                    <%-- START REGION REO --%>
                                                                    <div class="col-xs-12" style="margin-top: 5%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            REO Registration Cost Y/N
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="REOslctRegCost" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDREOUnits" class="with-gap dvREOHide1" name="REOSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="divEscalREO">
                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div id="divEscalREOCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="REORegCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- add button and checkbox --%>
                                                                    <div class="REOaddButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="REOUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddREOCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveREOCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%;">
                                                                                <input type="checkbox" id="RDREOSquare" class="with-gap dvREOHide1" name="REOSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="divEscalREOSquare">
                                                                        <div id="divEscalREOSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="REORegSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="REORegSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="REOaddbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="REOSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddREOSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveREOSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            REO Registration Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="REOinRegCost" type="text" class="form-control input-sm number dvREOHide1" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="REOslctRegCostCurr" class="form-control input-sm dvREOHide1">
                                                                                <option value="USD" selected="selected">USD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            REO Registration Cost Standard
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="REOslctRegCostStandard" class="form-control input-sm dvREOHide1">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-4" id="REOlblReminder" style="display: none;">
                                                                            <i>*Renewal Cost is the same.</i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost Escalating?
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="REOslctRenewCostEscal" class="form-control input-sm dvREOHide dvREOHide1 costEscalatingYES">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDREOUnits1" class="dvREOHide dvREOHide1 costEscalatingYES" name="REOUnits1" value="Registration Cost based on Number of Units" <%--checked="checked"--%> />
                                                                                Renewal Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="REORenewalCost">
                                                                        <div id="REOdivEscalRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="REORenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- add button and checkbox --%>
                                                                    <div class="REOaddRenewalButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="REORenewalUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="REObtnAddRenewalCCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="REObtnRemoveRenewalCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDREOSquare1" class="with-gap dvREOHide dvREOHide1 costEscalatingYES" name="REOSquare1" value="Registration Cost based on Square Footage" />
                                                                                Renewal Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="REORenewalSquare">
                                                                        <div id="REOdivEscalRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="REORenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="REORenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="REOaddREnewalbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="REORenewalSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="REObtnAddRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="REObtnRemoveRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="REOinRenewAmt" type="text" class="form-control input-sm number dvREOHide dvREOHide1 costEscalatingYES" />
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <select id="REOslctRenewAmtCurr1" class="form-control input-sm dvREOHide dvREOHide1 costEscalatingYES">
                                                                                <option value="USD">USD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <button id="REObtnViewEscalRenewAmt" data-toggle="collapse" data-target="#REOdivEscalRenewal" type="button" class="btn btn-default btn-sm dvREOHide dvREOHide1 costEscalatingYES">&nbsp;Escalating Renewal Schedule</button>
                                                                        </div>
                                                                    </div>
                                                                    <div id="REOdivEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black; overflow: auto;">
                                                                        <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                            <button type="button" data-toggle="collapse" data-target="#REOdivEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-2">
                                                                                    <label>Service Type</label>
                                                                                </div>
                                                                                <div class="col-xs-1"></div>
                                                                                <div class="col-xs-3" style="padding-left: 5px;">
                                                                                    <label>Amount</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 --%>
                                                                        <div class="col-xs-12 REOescalInput1" style="margin-top: 1%;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>1st Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck1">
                                                                                    <input id="REORenewalAmount1" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck1">
                                                                                    <select class="form-control input-sm">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalCostFirst">
                                                                            <div id="REOdivFirstRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfirstRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="col-xs-12 REOescalRenewalCheckCost1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalCheckCost1" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonCost1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalSquareFirst">
                                                                            <div id="REOdivFirstRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="REOfirstRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfirstRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckSquare1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalSquare" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonSquare1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>



                                                                        <%-- 2 --%>
                                                                        <div class="col-xs-12 REOescalInput2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>2nd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck2">
                                                                                    <input id="REORenewalAmount2" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck2">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 REOhideCheck2">
                                                                                    <input id="REORenewalCheck2" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalCostSecond">
                                                                            <div id="REOdivSecondRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOsecondRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckCost2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalCheckCost2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonCost2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalSquareSecond">
                                                                            <div id="REOdivSecondRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="REOsecondRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOsecondRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckSquare2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalSquare2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonSquare2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 3 --%>
                                                                        <div class="col-xs-12 REOescalInput3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>3rd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck3">
                                                                                    <input id="REORenewalAmount3" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck3">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 REOhideCheck3">
                                                                                    <input id="REORenewalCheck3" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalCostThird">
                                                                            <div id="REOdivThirdRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOthirdRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckCost3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalCheckCost3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonCost3" style="margin-top: 1%; display: none; padding-left: 30px;">
                                                                            <button id="REObtnAddEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalSquareThird">
                                                                            <div id="REOdivThirdRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="REOthirdRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOthirdRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 REOescalRenewalCheckSquare3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalSquare3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonSquare3" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 4 --%>
                                                                        <div class="col-xs-12 REOescalInput4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>4th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck4">
                                                                                    <input id="REORenewalAmount4" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck4">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 REOhideCheck4">
                                                                                    <input id="REORenewalCheck4" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalCostFourth">
                                                                            <div id="REOdivFourthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfourthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckCost4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalCheckCost4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonCost4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalSquareFourth">
                                                                            <div id="REOdivFourthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="REOfourthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfourthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckSquare4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalSquare4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonSquare4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 5 --%>
                                                                        <div class="col-xs-12 REOescalInput5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>5th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck5">
                                                                                    <input id="RenewalAmount5" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 REOhideCheck5">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 REOhideCheck5">
                                                                                    <input id="REORenewalCheck5" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalCostFifth">
                                                                            <div id="REOdivFifthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfifthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 REOescalRenewalCheckCost5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalCheckCost5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonCost5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="REORenewalSquareFifth">
                                                                            <div id="REOdivFifthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 REOescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="REOfifthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="REOfifthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalCheckSquare5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="REOescalRenewalSquare5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 REOescalRenewalButtonSquare5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="REObtnAddEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="REObtnRemoveEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <button id="REObtnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add Next Renewal</i></button>
                                                                            <button id="REObtnRemoveNew1" type="button" class="btn btn-link"><i class="fa fa-plus text-red"></i>&nbsp;<i>Remove Last Renewal</i></button>
                                                                        </div>
                                                                    </div>
                                                                    <%-- END REGION REO --%>
                                                                </div>

                                                                <div class="thirdDiv">
                                                                    <%-- START REGION COMMERCIAL --%>
                                                                    <div class="col-xs-12" style="margin-top: 5%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Commercial Registration Cost Y/N
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="CommercialslctRegCost" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDCommercialUnits" class="with-gap dvCommercialHide1" name="CommercialSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="divEscalCommercial">
                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div id="divEscalCommercialCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="CommercialRegCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- add button and checkbox --%>
                                                                    <div class="CommercialaddButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="CommercialUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddCommercialCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveCommercialCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%;">
                                                                                <input type="checkbox" id="RDCommercialSquare" class="with-gap dvCommercialHide1" name="CommercialSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Square Footage
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="divEscalCommercialSquare">
                                                                        <div id="divEscalCommercialSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="CommercialRegSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="CommercialRegSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="CommercialaddbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="CommercialSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddCommercialSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveCommercialSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3 audit">
                                                                            Commercial Registration Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="CommercialinRegCost" type="text" class="form-control input-sm number dvCommercialHide1" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="CommercialslctRegCostCurr" class="form-control input-sm dvCommercialHide1">
                                                                                <option value="USD" selected="selected">USD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Commercial Registration Cost Standard
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="CommercialslctRegCostStandard" class="form-control input-sm dvCommercialHide1">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-4" id="CommerciallblReminder" style="display: none;">
                                                                            <i>*Renewal Cost is the same.</i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost Escalating?
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="CommercialslctRenewCostEscal" class="form-control input-sm dvCommercialHide dvCommercialHide1 costEscalatingYES">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDCommercialUnits1" class="dvCommercialHide dvCommercialHide1 costEscalatingYES" name="CommercialUnits1" value="Registration Cost based on Number of Units" <%--checked="checked"--%> />
                                                                                Renewal Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="CommercialRenewalCost">
                                                                        <div id="CommercialdivEscalRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="CommercialRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- add button and checkbox --%>
                                                                    <div class="CommercialaddRenewalButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="CommercialRenewalUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="CommercialbtnAddRenewalCCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="CommercialbtnRemoveRenewalCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDCommercialSquare1" class="with-gap dvCommercialHide dvCommercialHide1 costEscalatingYES" name="CommercialSquare1" value="Registration Cost based on Square Footage" />
                                                                                Renewal Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="CommercialRenewalSquare">
                                                                        <div id="CommercialdivEscalRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="CommercialRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="CommercialRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="CommercialaddREnewalbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="CommercialRenewalSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="CommercialbtnAddRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="CommercialbtnRemoveRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="CommercialinRenewAmt" type="text" class="form-control input-sm number dvCommercialHide dvCommercialHide1 costEscalatingYES" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="CommercialslctRenewAmtCurr1" class="form-control input-sm dvCommercialHide dvCommercialHide1 costEscalatingYES">
                                                                                <option value="USD">USD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <button id="CommercialbtnViewEscalRenewAmt" data-toggle="collapse" data-target="#CommercialdivEscalRenewal" type="button" class="btn btn-default btn-sm dvCommercialHide dvCommercialHide1 costEscalatingYES">&nbsp;Escalating Renewal Cost</button>
                                                                        </div>
                                                                    </div>
                                                                    <div id="CommercialdivEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black; overflow: auto;">
                                                                        <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                            <button type="button" data-toggle="collapse" data-target="#CommercialdivEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-2">
                                                                                    <label>Service Type</label>
                                                                                </div>
                                                                                <div class="col-xs-1"></div>
                                                                                <div class="col-xs-3" style="padding-left: 5px;">
                                                                                    <label>Amount</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 --%>
                                                                        <div class="col-xs-12 CommercialescalInput1" style="margin-top: 1%;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>1st Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck1">
                                                                                    <input id="CommercialRenewalAmount1" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck1">
                                                                                    <select class="form-control input-sm">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalCostFirst">
                                                                            <div id="CommercialdivFirstRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfirstRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="col-xs-12 CommercialescalRenewalCheckCost1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalCheckCost1" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonCost1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalSquareFirst">
                                                                            <div id="CommercialdivFirstRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="CommercialfirstRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfirstRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckSquare1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalSquare" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonSquare1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>



                                                                        <%-- 2 --%>
                                                                        <div class="col-xs-12 CommercialescalInput2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>2nd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck2">
                                                                                    <input id="CommercialRenewalAmount2" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck2">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 CommercialhideCheck2">
                                                                                    <input id="CommercialRenewalCheck2" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalCostSecond">
                                                                            <div id="CommercialdivSecondRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialsecondRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckCost2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalCheckCost2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonCost2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalSquareSecond">
                                                                            <div id="CommercialdivSecondRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="CommercialsecondRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialsecondRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckSquare2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalSquare2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonSquare2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 3 --%>
                                                                        <div class="col-xs-12 CommercialescalInput3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>3rd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck3">
                                                                                    <input id="CommercialRenewalAmount3" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck3">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 CommercialhideCheck3">
                                                                                    <input id="CommercialRenewalCheck3" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalCostThird">
                                                                            <div id="CommercialdivThirdRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialthirdRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckCost3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalCheckCost3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonCost3" style="margin-top: 1%; display: none; padding-left: 30px;">
                                                                            <button id="CommercialbtnAddEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalSquareThird">
                                                                            <div id="CommercialdivThirdRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="CommercialthirdRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialthirdRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 CommercialescalRenewalCheckSquare3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalSquare3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonSquare3" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 4 --%>
                                                                        <div class="col-xs-12 CommercialescalInput4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>4th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck4">
                                                                                    <input id="CommercialRenewalAmount4" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck4">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 CommercialhideCheck4">
                                                                                    <input id="CommercialRenewalCheck4" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalCostFourth">
                                                                            <div id="CommercialdivFourthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfourthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckCost4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalCheckCost4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonCost4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalSquareFourth">
                                                                            <div id="CommercialdivFourthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="CommercialfourthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfourthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckSquare4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalSquare4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonSquare4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 5 --%>
                                                                        <div class="col-xs-12 CommercialescalInput5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>5th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck5">
                                                                                    <input id="CommercialRenewalAmount5" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 CommercialhideCheck5">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 CommercialhideCheck5">
                                                                                    <input id="CommercialRenewalCheck5" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalCostFifth">
                                                                            <div id="CommercialdivFifthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfifthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 CommercialescalRenewalCheckCost5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalCheckCost5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonCost5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="CommercialRenewalSquareFifth">
                                                                            <div id="CommercialdivFifthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 CommercialescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="CommercialfifthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="CommercialfifthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalCheckSquare5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="CommercialescalRenewalSquare5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 CommercialescalRenewalButtonSquare5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="CommercialbtnAddEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="CommercialbtnRemoveEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <button id="CommercialbtnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add Next Renewal</i></button>
                                                                            <button id="CommercialbtnRemoveNew1" type="button" class="btn btn-link"><i class="fa fa-plus text-red"></i>&nbsp;<i>Remove Last Renewal</i></button>
                                                                        </div>
                                                                    </div>
                                                                    <%-- END REGION COMMERCIAL --%>
                                                                </div>

                                                                <div class="fourthDiv">
                                                                    <%-- START REGION VACANT --%>
                                                                    <div class="col-xs-12" style="margin-top: 5%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Vacant Registration Cost Y/N
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="VacantslctRegCost" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDVacantUnits" class="with-gap dvVacantHide1" name="VacantSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="divEscalVacant">
                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div id="divEscalVacantCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="VacantRegCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- add button and checkbox --%>
                                                                    <div class="VacantaddButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="VacantUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddVacantCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveVacantCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%;">
                                                                                <input type="checkbox" id="RDVacantSquare" class="with-gap dvVacantHide1" name="VacantSquare" value="Registration Cost based on Square Footage" />
                                                                                Registration Cost based on Square Footage
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="divEscalVacantSquare">
                                                                        <div id="divEscalVacantSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="VacantRegSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="VacantRegSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="VacantaddbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="VacantSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="btnAddVacantSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="btnRemoveVacantSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Vacant Registration Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="VacantinRegCost" type="text" class="form-control input-sm number dvVacantHide1" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="VacantslctRegCostCurr" class="form-control input-sm dvVacantHide1">
                                                                                <option value="USD" selected="selected">USD</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Vacant Registration Cost Standard
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="VacantslctRegCostStandard" class="form-control input-sm dvVacantHide1">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-4" id="VacantlblReminder" style="display: none;">
                                                                            <i>*Renewal Cost is the same.</i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost Escalating?
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="VacantslctRenewCostEscal" class="form-control input-sm dvVacantHide dvVacantHide1 costEscalatingYES">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDVacantUnits1" class="dvVacantHide dvVacantHide1 costEscalatingYES" name="VacantUnits1" value="Registration Cost based on Number of Units" <%--checked="checked"--%> />
                                                                                Renewal Cost based on Number of Units
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="VacantRenewalCost">
                                                                        <div id="VacantdivEscalRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-right: 0px; margin-right: 50px;">
                                                                                            <label>1 unit</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 0px; width: 125px;">
                                                                                            <input id="VacantRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- add button and checkbox --%>
                                                                    <div class="VacantaddRenewalButtonCheck" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="VacantRenewalUnitAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="VacantbtnAddRenewalCCost" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="VacantbtnRemoveRenewalCost" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%-- NEW ADD --%>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-11 audit">
                                                                            <div style="padding-left: 7%">
                                                                                <input type="checkbox" id="RDVacantSquare1" class="with-gap dvVacantHide dvVacantHide1 costEscalatingYES" name="VacantSquare1" value="Registration Cost based on Square Footage" />
                                                                                Renewal Cost based on Square Footage
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <%-- 1 DATA TARGET --%>
                                                                    <div class="VacantRenewalSquare">
                                                                        <div id="VacantdivEscalRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                            <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                            </div>
                                                                            <div class="col-xs-12" style="margin-top: 0%;">
                                                                                <div class="col-xs-12">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                                    <div class="col-xs-11">
                                                                                        <div class="col-xs-2" style="padding-left: 10px; width: 60px; padding-right: 0px;">
                                                                                            <label>Up to</label>
                                                                                        </div>
                                                                                        <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                            <input id="VacantRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-1" style="padding: 0px;">
                                                                                            <label>Sq. ft.</label>
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 0px; width: 125px;">
                                                                                            <input id="VacantRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                        </div>
                                                                                        <div class="col-xs-3" style="padding-left: 30px; width: 153px;">
                                                                                            <select class="form-control input-sm">
                                                                                                <option value="USD">USD</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <%-- Button and Checkbox Square --%>
                                                                    <div class="VacantaddREnewalbuttoncheckSquare" style="display: none;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-12">
                                                                                    <div class="col-xs-7" style="padding-left: 30px;">
                                                                                        <input id="VacantRenewalSquareAbove" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <button id="VacantbtnAddRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                                    <button id="VacantbtnRemoveRenewalSquare" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-1 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            Renewal Cost
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <input id="VacantinRenewAmt" type="text" class="form-control input-sm number dvVacantHide dvVacantHide1 costEscalatingYES" />
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <select id="VacantslctRenewAmtCurr1" class="form-control input-sm dvVacantHide dvVacantHide1 costEscalatingYES">
                                                                                <option value="USD">USD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 audit">
                                                                            <button id="VacantbtnViewEscalRenewAmt" data-toggle="collapse" data-target="#VacantdivEscalRenewal" type="button" class="btn btn-default btn-sm dvVacantHide dvVacantHide1 costEscalatingYES">&nbsp;Escalating Renewal Cost</button>
                                                                        </div>
                                                                    </div>
                                                                    <div id="VacantdivEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black; overflow: auto;">
                                                                        <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                            <button type="button" data-toggle="collapse" data-target="#VacantdivEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-2">
                                                                                    <label>Service Type</label>
                                                                                </div>
                                                                                <div class="col-xs-1"></div>
                                                                                <div class="col-xs-3" style="padding-left: 5px;">
                                                                                    <label>Amount</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 --%>
                                                                        <div class="col-xs-12 VacantescalInput1" style="margin-top: 1%;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>1st Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck1">
                                                                                    <input id="VacantRenewalAmount1" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck1">
                                                                                    <select class="form-control input-sm">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalCostFirst">
                                                                            <div id="VacantdivFirstRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfirstRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="col-xs-12 VacantescalRenewalCheckCost1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalCheckCost1" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonCost1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingUniversal1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalSquareFirst">
                                                                            <div id="VacantdivFirstRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="VacantfirstRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfirstRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckSquare1" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalSquare" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonSquare1" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingSquare1" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>



                                                                        <%-- 2 --%>
                                                                        <div class="col-xs-12 VacantescalInput2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>2nd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck2">
                                                                                    <input id="VacantRenewalAmount2" type="text" class="form-control input-sm number" />
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck2">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 VacanthideCheck2">
                                                                                    <input id="VacantRenewalCheck2" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalCostSecond">
                                                                            <div id="VacantdivSecondRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantsecondRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckCost2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalCheckCost2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonCost2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingUniversal2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalSquareSecond">
                                                                            <div id="VacantdivSecondRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="VacantsecondRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantsecondRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckSquare2" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalSquare2" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonSquare2" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingSquare2" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 3 --%>
                                                                        <div class="col-xs-12 VacantescalInput3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>3rd Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck3">
                                                                                    <input id="VacantRenewalAmount3" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck3">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 VacanthideCheck3">
                                                                                    <input id="VacantRenewalCheck3" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalCostThird">
                                                                            <div id="VacantdivThirdRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantthirdRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckCost3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalCheckCost3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonCost3" style="margin-top: 1%; display: none; padding-left: 30px;">
                                                                            <button id="VacantbtnAddEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingUniversal3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalSquareThird">
                                                                            <div id="VacantdivThirdRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="VacantthirdRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantthirdRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 VacantescalRenewalCheckSquare3" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalSquare3" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonSquare3" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingSquare3" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <%-- 4 --%>
                                                                        <div class="col-xs-12 VacantescalInput4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>4th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck4">
                                                                                    <input id="VacantRenewalAmount4" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck4">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 VacanthideCheck4">
                                                                                    <input id="VacantRenewalCheck4" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalCostFourth">
                                                                            <div id="VacantdivFourthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfourthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckCost4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalCheckCost4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonCost4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingUniversal4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalSquareFourth">
                                                                            <div id="VacantdivFourthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="VacantfourthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfourthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckSquare4" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalSquare4" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonSquare4" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingSquare4" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 5 --%>
                                                                        <div class="col-xs-12 VacantescalInput5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                                <div class="col-xs-2">
                                                                                    <label>5th Renewal</label>
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck5">
                                                                                    <input id="VacantRenewalAmount5" type="text" class="form-control input-sm number " />
                                                                                </div>
                                                                                <div class="col-xs-2 VacanthideCheck5">
                                                                                    <select class="form-control input-sm ">
                                                                                        <option value="USD">USD</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-xs-5 VacanthideCheck5">
                                                                                    <input id="VacantRenewalCheck5" type="checkbox" />&nbsp; Check if succeeding renewals use this amount
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalCostFifth">
                                                                            <div id="VacantdivFifthRenewalCost1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px;">
                                                                                                <label>1 unit</label>
                                                                                            </div>
                                                                                            <div class="col-xs-1"></div>
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfifthRenewalCostAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-xs-12 VacantescalRenewalCheckCost5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalCheckCost5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonCost5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingUniversal5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>

                                                                        <%-- 1 DATA TARGET --%>
                                                                        <div class="VacantRenewalSquareFifth">
                                                                            <div id="VacantdivFifthRenewalSquare1" class="col-xs-12 collapse" style="margin-top: 0%; border: 0px solid black;">
                                                                                <div class="col-xs-12 text-right" style="margin-top: 0%;">
                                                                                </div>
                                                                                <div class="col-xs-12" style="margin-top: 0%;">
                                                                                    <div class="col-xs-12">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 VacantescalInput" style="margin-top: 1%;">
                                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                                        <div class="col-xs-11">
                                                                                            <div class="col-xs-2" style="padding-left: 0px; width: 43px; padding-right: 0px;">
                                                                                                <label>Up to</label>
                                                                                            </div>
                                                                                            <div class="col-xs-2" style="padding-right: 5px; padding-left: 0px; width: 65px;">
                                                                                                <input id="VacantfifthRenewalSquare1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-1" style="padding: 0px;">
                                                                                                <label>Sq. ft.</label>
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 0px; width: 118px;">
                                                                                                <input id="VacantfifthRenewalSquareAmount1" type="text" class="form-control input-sm number" />
                                                                                            </div>
                                                                                            <div class="col-xs-3" style="padding-left: 20px; width: 143px;">
                                                                                                <select class="form-control input-sm">
                                                                                                    <option value="USD">USD</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalCheckSquare5" style="margin-top: 1%; display: none;">
                                                                            <div class="col-xs-12">
                                                                                <div class="col-xs-7" style="padding-left: 30px;">
                                                                                    <input id="VacantescalRenewalSquare5" type="checkbox" />&nbsp;<i style="color: #337ab7;">And above</i>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xs-12 VacantescalRenewalButtonSquare5" style="margin-top: 1%; padding-left: 30px; display: none;">
                                                                            <button id="VacantbtnAddEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add</i></button>
                                                                            <button id="VacantbtnRemoveEscalatingSquare5" type="button" class="btn btn-link"><i class="fa fa-times text-red"></i>&nbsp;<i>Remove</i></button>
                                                                        </div>


                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <button id="VacantbtnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add Next Renewal</i></button>
                                                                            <button id="VacantbtnRemoveNew1" type="button" class="btn btn-link"><i class="fa fa-plus text-red"></i>&nbsp;<i>Remove Last Renewal</i></button>
                                                                        </div>
                                                                    </div>
                                                                    <%-- END REGION VACANT --%>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCost" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%--Cost End--%>

                                                            <%-- Continuing Registration --%>
                                                            <div id="tabContReg" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblCont">
                                                                        Continue Registration from PFC to REO?
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctContReg" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3 lblUpdate">
                                                                        Update Registration with Municipality
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctUpdateReg" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCont" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%--Continuing Registration--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%-- Inspection --%>

                                            <div id="tabInspection" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <%--<label>Copy from:</label>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<select class="form-control input-sm slctCopyMuni select2" style="width: 255px;"></select>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration"><i class="fa fa-chevron-circle-right"></i></button>--%>
                                                    </div>
                                                    <%--<div class="col-xs-2" style="float: right; padding-left: 50px">
                                                        <button type="button" class="btn btn-md btnApply" style="background-color: #669b31; color: white;" disabled>&nbsp;APPLY</button>
                                                    </div>--%>
                                                </div>

                                                <div class="col-xs-12" style="margin-top: 4%;">
                                                    <div class="col-xs-4">
                                                        Municipal Inspection Fee Required?
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniInsReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        One-time only upon registration?
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctOneTimeUponReg" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Fee Payment Frequency
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctFeePaymentFreq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="Weekly">Weekly</option>
                                                            <option value="Biweekly">Biweekly</option>
                                                            <option value="Monthly">Monthly</option>
                                                            <option value="Bimonthly">Bimonthly</option>
                                                            <option value="Quarterly">Quarterly</option>
                                                            <option value="Every 6 Months">Every 6 Months</option>
                                                            <option value="Yearly">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Pay On
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit" style="width: 84px;">
                                                        <input id="inPayon1" type="number" min="1" max="31" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-2 audit" style="width: 84px;">
                                                        <input id="inPayon2" type="number" min="1" max="31" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-2 audit" style="width: 84px;">
                                                        <input id="inPayon3" type="number" min="1" max="31" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-2 audit" style="width: 84px;">
                                                        <input id="inPayon4" type="number" min="1" max="31" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Fee Escalating?
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctFreeEscalating" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Municipal Inspection Fee
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMunicipalInsFee" type="text" class="form-control input-sm number" />
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="inMunicpalInsFeeCurr" class="form-control input-sm">
                                                            <option value="USD" selected="selected">USD</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <button id="btnViewEscalInspection" data-toggle="collapse" data-target="#divEscalInspection" type="button" class="btn btn-default btn-sm">Escalating Cost</button>
                                                    </div>
                                                </div>

                                                <div id="divEscalInspection" class="col-xs-12 collapse" style="margin-top: 1%; margin-bottom: 1%; border: 1px solid black;">
                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                        <button type="button" data-toggle="collapse" data-target="#divEscalInspection" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                    <%-- 1 --%>
                                                    <div class="col-xs-12 InsescalInput1" style="margin-top: 1%;">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-3" style="padding-left: 0px;">
                                                                <label>1st Municipal Inspection Fee</label>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <input id="inputIns1" type="text" class="form-control input-sm number" />
                                                            </div>
                                                            <div class="col-xs-2 currency">
                                                                <select class="form-control input-sm currency">
                                                                    <option value="USD" selected="selected">USD</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- 2 --%>
                                                    <div class="col-xs-12 InsescalInput2" style="margin-top: 1%; display: none">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-3" style="padding-left: 0px;">
                                                                <label>2nd Municipal Inspection Fee</label>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <input id="inputIns2" type="text" class="form-control input-sm number" />
                                                            </div>
                                                            <div class="col-xs-2 currency">
                                                                <select class="form-control input-sm currency">
                                                                    <option value="USD" selected="selected">USD</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <input type="checkbox" id="inspectCheck2" />&nbsp;Succeeding fees are the same amount
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- 3 --%>
                                                    <div class="col-xs-12 InsescalInput3" style="margin-top: 1%; display: none">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-3" style="padding-left: 0px;">
                                                                <label>3rd Municipal Inspection Fee</label>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <input id="inputIns3" type="text" class="form-control input-sm number" />
                                                            </div>
                                                            <div class="col-xs-2 currency">
                                                                <select class="form-control input-sm currency">
                                                                    <option value="USD" selected="selected">USD</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <input type="checkbox" id="inspectCheck3" />&nbsp;Succeeding fees are the same amount
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- 4 --%>
                                                    <div class="col-xs-12 InsescalInput4" style="margin-top: 1%; display: none">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-3" style="padding-left: 0px;">
                                                                <label>4th Municipal Inspection Fee</label>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <input id="inputIns4" type="text" class="form-control input-sm number" />
                                                            </div>
                                                            <div class="col-xs-2 currency">
                                                                <select class="form-control input-sm currency">
                                                                    <option value="USD" selected="selected">USD</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <input type="checkbox" id="inspectCheck4" />&nbsp;Succeeding fees are the same amount
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- 5 --%>
                                                    <div class="col-xs-12 InsescalInput5" style="margin-top: 1%; display: none">
                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                            <div class="col-xs-3" style="padding-left: 0px;">
                                                                <label>5th Municipal Inspection Fee</label>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <input id="inputIns5" type="text" class="form-control input-sm number" />
                                                            </div>
                                                            <div class="col-xs-2 currency">
                                                                <select class="form-control input-sm currency">
                                                                    <option value="USD" selected="selected">USD</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <input type="checkbox" id="inspectCheck5" />&nbsp;Succeeding fees are the same amount
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <button id="btnAddNewInspection" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;<i>Add New</i></button>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12" style="margin-top: 5%;">
                                                    <div class="col-xs-4">
                                                        Inspection Report Required?
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsUpReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Inspection Frequency - Occupied
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsCriOccc" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="Weekly">Weekly</option>
                                                            <option value="Biweekly">Biweekly</option>
                                                            <option value="Monthly">Monthly</option>
                                                            <option value="Bimonthly">Bimonthly</option>
                                                            <option value="Quarterly">Quarterly</option>
                                                            <option value="Every 6 Months">Every 6 Months</option>
                                                            <option value="Yearly">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        Inspection Frequency - Vacant
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsCriVac" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="Weekly">Weekly</option>
                                                            <option value="Biweekly">Biweekly</option>
                                                            <option value="Monthly">Monthly</option>
                                                            <option value="Bimonthly">Bimonthly</option>
                                                            <option value="Quarterly">Quarterly</option>
                                                            <option value="Every 6 Months">Every 6 Months</option>
                                                            <option value="Yearly">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-4">
                                                        How to send Inspection Report 
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsFeePay" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="Email">Email</option>
                                                            <option value="Online">Online</option>
                                                            <option value="Shipping">Shipping</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveInspection" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Municipality --%>
                                            <div id="tabMunicipality" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <%--<label>Copy from:</label>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<select class="form-control input-sm slctCopyMuni select2" style="width: 255px;"></select>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration"><i class="fa fa-chevron-circle-right"></i></button>--%>
                                                    </div>
                                                    <%--<div class="col-xs-2" style="float: right; padding-left: 50px">
                                                        <button type="button" class="btn btn-md btnApply" style="background-color: #669b31; color: white;" disabled>&nbsp;APPLY</button>
                                                    </div>--%>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Department
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inMuniDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Phone Number
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inMuniPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Email Address
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inMuniEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Mailing Address
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMuniMailStr" type="text" class="form-control input-sm" placeholder="Street Name" />
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailSta" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailCt" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailZip" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Website
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inWebsite" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Contact Person
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inContact" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Title
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inTitle" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Department
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Phone Number
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Email Address
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Mailing Address
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="inMailing" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Hours of Operation
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-1">from:</div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inHrsFrom" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1">to:</div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inHrsTo" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveMunicipalInfo" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Deregistration --%>
                                            <div id="tabDeregistration" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <%--<label>Copy from:</label>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<select class="form-control input-sm slctCopyMuni select2" style="width: 255px;"></select>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration"><i class="fa fa-chevron-circle-right"></i></button>--%>
                                                    </div>
                                                    <%--<div class="col-xs-2" style="float: right; padding-left: 50px">
                                                        <button type="button" class="btn btn-md btnApply" style="background-color: #669b31; color: white;" disabled>&nbsp;APPLY</button>
                                                    </div>--%>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Deregistration Required
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctDeregRequired" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Conveyed
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctConveyed" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Occupied
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctOccupied" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        How to Deregister
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctHowToDereg" style="width: 100%" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="PDF">City Registration Form</option>
                                                            <option value="Online">City Website</option>
                                                            <option value="Prochamps">ProChamps Online</option>
                                                            <option value="Email">Email</option>
                                                            <option value="Letter">Letter</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-4 audit" style="display: none;">
                                                        <input id="inUploadPathD" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                        <button id="delinkPDF" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                    </div>
                                                    <div class="col-xs-1 audit" style="padding-left: 0px;">
                                                        <button id="debtnSavePDF" type="button" class="btn btn-primary" title="Upload File" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        New Owner Information Required
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctNewOwnerInfoReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Proof of Conveyance Required
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctProofOfConveyReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Date of Sale Required
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctDateOfSaleReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveDeregistration" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Ordinance Settings --%>
                                            <div id="tabOrdinance" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <%--<label>Copy from:</label>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<select class="form-control input-sm slctCopyMuni select2" style="width: 255px;"></select>--%>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <%--<button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration"><i class="fa fa-chevron-circle-right"></i></button>--%>
                                                    </div>
                                                    <%--<div class="col-xs-2" style="float: right; padding-left: 50px">
                                                        <button type="button" class="btn btn-md btnApply" style="background-color: #669b31; color: white;" disabled>&nbsp;APPLY</button>
                                                    </div>--%>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-sm-3">
                                                        Ordinance Number
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-sm-4 audit">
                                                        <input id="tbOrdinanceNum" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Ordinance Name
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="tbOrdinanceName" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Description
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-8 audit">
                                                        <textarea class="form-control input-sm" id="tbDescription" style="max-height: 140px; max-width: 431px; min-height: 140px; min-width: 400px;"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Ordinance Copy
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-5 audit">
                                                        <input id="fiOrdinanceFile" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                        <button id="linkOrdinanceFile" type="button" class="btn btn-link input-sm" onclick="openPDF(this);" style="display: none;"></button>
                                                    </div>
                                                    <div class="col-xs-1 audit">
                                                        <button id="btnOrdinanceFile" type="button" class="btn btn-primary btn-sm" title="Upload File" style="margin-left: -31px;"><i class="fa fa-upload" aria-hidden="true"></i>Upload</button>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Section
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="tbSection" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Source
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input type="text" id="tbSource" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Enacted Date
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="dpEnacted" type="text" class="form-control input-sm datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Revision Date
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-1x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-1x"></i></button>
                                                    </div>
                                                    <div class="col-xs-4 audit">
                                                        <input id="dpRevision" type="text" class="form-control input-sm datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <%--<button id="btnAddOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add New</button>--%>
                                                    <div class="text-right">
                                                        <button id="btnHistory" type="button" class="btn btn-primary btn-sm text-left"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;History</button>
                                                        <button type="button" class="btn btn-primary btn-sm" id="btnSaveOrdinance"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                        <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                    </div>
                                                </div>

                                            </div>

                                            <%-- Zip Code --%>
                                            <div id="tabZip" class="tab-pane">
                                                <table id="tblZip" class="table no-border">
                                                    <tbody></tbody>
                                                </table>
                                                <div class="text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveZip" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Document Preview</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <label id="lblPDF"></label>
                        </div>
                        <div class="col-xs-12">
                            <iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit">
        <div class="modal-dialog" role="document">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label id="lblEdit"></label>
                </div>
                <div id="lci" class="panel-body" style="display: none;">
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Company</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciCompany" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact First Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciFirstName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact Last Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciLastName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Title</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciTitle" class="form-control input-sm">
                                <option value="">N/A</option>
                                <option value="Broker">Broker</option>
                                <option value="SPI Vendor">SPI Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business License Number</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciBusinessLicenseNum" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>24-Hour Emergency Phone</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Fax Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Cell</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Email Address</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciEmail" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Street Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciStreet" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>State</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciState" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>City</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciCity" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Zip</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciZip" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Hours of Operation</label>
                        </div>
                        <div class="col-xs-1">
                            from:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsFrom" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                        <div class="col-xs-1">
                            to:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsTo" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="rr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="ico" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="icv" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="dr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalOrdinanceHist">
        <div class="modal-dialog" role="document" style="width: 1000px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label>Add Ordinance</label>
                </div>
                <div class="panel-body">
                    <table id="tblOrdinanceHist" class="table no-border">
                        <thead>
                            <tr>
                                <th data-field="ordinance_num">Ordinance Number</th>
                                <th data-field="ordinance_name">Ordinance Name</th>
                                <th data-field="description">Description</th>
                                <th data-field="section">Section</th>
                                <th data-field="source">Source</th>
                                <th data-field="enacted_date">Enacted Date</th>
                                <th data-field="revision_date">Revision Date</th>
                                <th data-field="added_by">Added By</th>
                                <th data-field="date_added">Date Added</th>
                                <th data-field="modified_by">Modified By</th>
                                <th data-field="date_modified">Date Modified</th>
                                <th data-field="deleted_by">Deleted By</th>
                                <th data-field="date_deleted">Date Deleted</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="text-center" style="margin-top: 1%;">
                        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-apply"></i>Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalViewPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">View PDF File</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 noPadMar">
                            <iframe id="pdfFile" style="overflow: scroll; width: 100%; height: 642px; border: 0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/CompRulesApprover.js"></script>
    <script src="js/CompRulesApprover2.js"></script>
</asp:Content>
