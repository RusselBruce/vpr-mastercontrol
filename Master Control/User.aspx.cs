﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<AccessRole> loadAccess(string type)
        {
            List<AccessRole> ls = new List<AccessRole>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where isActive = 1 and type='" + type + "' and tag = 'VPR' ");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AccessRole ar = new AccessRole();
                ar.id = dt.Rows[i][0].ToString();
                ar.name = dt.Rows[i][1].ToString();
                ls.Add(ar);
            }

            return ls;
        }

        [WebMethod]
        public static List<AccessFunc> loadFunction(string id, string type)
        {
            List<AccessFunc> ls = new List<AccessFunc>();
            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();

            string qry = "Select * from tbl_VPR_Access_Role_Type_Function where tag = 'VPR' and role_id =" + id;
            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                string avl = dt.Rows[0][2].ToString();

                //current function
                qry = "Select * from tbl_VPR_Access_Role_Function where id in (" + avl + ") and tag= 'VPR'";
                dt = cls.GetData(qry);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Console.WriteLine(dt.Rows[i][1].ToString());
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "True";
                    ls.Add(ar);
                }
                //Available function
                Console.WriteLine("\n");
                qry = "Select * from tbl_VPR_Access_Role_Function where id not in (" + avl + ") and functype='" + type + "'  and tag= 'VPR'";
                dt = cls.GetData(qry);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }
            else
            {
                //if null
                qry = "Select * from tbl_VPR_Access_Role_Function where functype='" + type + "'  and tag= 'VPR'";
                dt = cls.GetData(qry);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AccessFunc ar = new AccessFunc();
                    ar.id = dt.Rows[i][0].ToString();
                    ar.name = dt.Rows[i][1].ToString();
                    ar.isOn = "False";
                    ls.Add(ar);
                }
            }


            return ls;
        }


        [WebMethod]
        public static AccessRole loadLineAccess(string id)
        {

            DataTable dt = new DataTable();
            clsConnection cls = new clsConnection();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where id=" + id);
            AccessRole ar = new AccessRole();
            ar.id = dt.Rows[0][0].ToString();
            ar.name = dt.Rows[0][1].ToString();
            ar.desc = dt.Rows[0][2].ToString();
            return ar;
        }

        [WebMethod]
        public static void addAccess(string name, string desc, string type)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Type(accessname,accessdesc,type,isActive,tag) Values ('" + name + "', '" + desc + "','" + type + "',1,'VPR')");
            //cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Function(role_id,AI,QM,CH,MC,RE) Values ((select MAX(ID) from tbl_VPR_Access_Role_Type) , 0,0,0,0,0)");
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

            cls.FUNC.Audit("Create New Role/Skill: " + name, arrPages);

        }

        [WebMethod]
        public static string editAccess(string id, string name, string desc)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type set accessname = '" + name + "', accessdesc='" + desc + "' where id=" + id);
            DataTable dt = new DataTable();
            dt = cls.GetData("Select type from tbl_VPR_Access_Role_Type where id =" + id);

            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

            cls.FUNC.Audit("Update Role/Skill: " + name, arrPages);


            return dt.Rows[0][0].ToString();
            

        }
        [WebMethod]
        public static string deleteLine(string id)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type set isActive = 0 where id=" + id);
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where id =" + id);

            cls.FUNC.Audit("Deleted Role/Skill: " + dt.Rows[0][1].ToString() , arrPages);
            return dt.Rows[0][3].ToString();

        }

        [WebMethod]
        public static void updateFunction(string id, string newStr)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
			dt = cls.GetData("Select * from tbl_VPR_Access_Role_Type a join tbl_VPR_Access_Role_Type_Function b on a.id = b.role_id where a.id =" + id);
            if (dt.Rows.Count > 0)
            {
                cls.ExecuteQuery("Update tbl_VPR_Access_Role_Type_Function set function_on = '" + newStr + "' where role_id=" + id);
            }
            else
            {
                cls.ExecuteQuery("INSERT INTO tbl_VPR_Access_Role_Type_Function(role_id,function_on,tag) VALUES (" + id + ",'" + newStr + "','VPR')");
            }

            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");

			DataTable dtRole = new DataTable();
			dtRole = cls.GetData("Select * from tbl_VPR_Access_Role_Type where id =" + id);

			cls.FUNC.Audit("Updated Role/Skill Function: " + dtRole.Rows[0][1].ToString(), arrPages);

        }
        [WebMethod]
        public static UserList userList(string cat, string im, string user, string name)
        {
            clsConnection cls = new clsConnection();
            UserList ul = new UserList();
            List<UserDetails> lud = new List<UserDetails>();
            DataTable dt = new DataTable();
            string qry = "";

			string where = "";
			string nameVal, catVal, imVal, userVal = "";
			nameVal = (name != "") ? "EmpName like '%" + name + "%'" : "";
			catVal = (cat != "") ? "EmpLevel = '" + cat + "' and " : "";
			imVal = (im != "") ? "MngrName = '" + im + "'  and " : "";
			userVal = (user != "") ? "EmpName = '" + user + "' and " : "";

			if (name != "")
			{
				where = "where " + nameVal + " ";
			}
			else if (im != "" || user != "")
			{
				where = "where " + imVal + userVal + " ";
				where = where.Remove(where.Length - 5, 4);
			}
			else if (cat != "")
			{
				where = "where " + catVal + " ";
				where = where.Remove(where.Length - 5, 4);
			}

            qry += "select a.id [id],b.empname [empname],b.emplevel [emplevel], ";
            qry += "case when role_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = role_id) end [role_id], ";
            qry += "case when skill_id is null then '-' else (Select accessname from tbl_VPR_Access_Role_Type where id = skill_id) end [skill_id], ";
            qry += "MngrName [immsupp]";
            qry += "from tbl_VPR_Access_User a ";
            qry += "inner join tbl_HRMS_EmployeeMaster b on a.ntid = b.NTID ";
			qry += where;
            qry += "order by empname asc";

            dt = cls.GetData(qry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserDetails ud = new UserDetails();
                    ud.id = dt.Rows[i][0].ToString();
                    ud.empname = dt.Rows[i][1].ToString();
                    ud.emplevel = dt.Rows[i][2].ToString();
                    ud.role_id = dt.Rows[i][3].ToString();
                    ud.skill_id = dt.Rows[i][4].ToString();
                    ud.immsupp = dt.Rows[i][5].ToString();
                    lud.Add(ud);
                }
                ul.tblUserList = lud;
            }
            else
            {

            }

            return ul;
        }

        [WebMethod]
        public static List<RoleSkill> popRS()
        {
            clsConnection cls = new clsConnection();
            List<RoleSkill> lrs = new List<RoleSkill>();
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where isActive = 1 and tag = 'VPR'");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RoleSkill rs = new RoleSkill();
                    rs.id = dt.Rows[i][0].ToString();
                    rs.rsname = dt.Rows[i][1].ToString();
                    rs.rstype = dt.Rows[i][3].ToString();

                    lrs.Add(rs);
                }
            }

            return lrs;
        }

        [WebMethod]
        public static void applyAction(string role_id, string skill_id, string emp_id)
        {
            clsConnection cls = new clsConnection();
            cls.ExecuteQuery("Update tbl_VPR_Access_User set role_id =" + role_id + ", skill_id=" + skill_id + " where id in (" + emp_id + ")");
            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("User");
            DataTable dt = new DataTable();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where id =" + role_id);
            string role = dt.Rows[0][1].ToString();
            dt = cls.GetData("select * from tbl_VPR_Access_Role_Type where id =" + skill_id);
            string skill = dt.Rows[0][1].ToString();
            cls.FUNC.Audit("Applied Role: " + role + " and Skill: " + skill + " to certain users", arrPages);
        }

		[WebMethod]
		public static List<string> empCatFilter()
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();

			dt = cls.GetData("select Distinct(case when EmpLevel = 'AM' then 'Assistant Manager' when EmpLevel = 'DIR' then 'Director' when EmpLevel = 'Manager' or EmpLevel = 'MGR' then 'Manager' when EmpLevel = 'SR. MGR' then 'Senior Manager' when EmpLevel = 'TL' then 'Team Lead' when EmpLevel = 'VP' then 'Vice President' end) EmpLevel from tbl_HRMS_EmployeeMaster where AccessLevel is not null and EmpLevel <> 'EE' order by EmpLevel;");

			List<string> list = new List<string>();

			for (int x = 0; x < dt.Rows.Count; x++)
			{
				string empLevel = dt.Rows[x]["EmpLevel"].ToString();

				list.Add(empLevel);
			}

			return list;
		}

		[WebMethod]
		public static List<string> empIMFilter(string cat)
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();
			string emplvl = "";
			if (cat != "")
			{
				if (cat == "Assistant Manager")
				{
					emplvl = "and EmpLevel = 'AM'";
				}
				else if (cat == "Director")
				{
					emplvl = "and EmpLevel = 'DIR'";
				}
				else if (cat == "Manager")
				{
					emplvl = "and EmpLevel in ('MANAGER', 'MGR')";
				}
				else if (cat == "Senior Manager")
				{
					emplvl = "and EmpLevel = 'SR. MGR'";
				}
				else if (cat == "Team Lead")
				{
					emplvl = "and EmpLevel = 'TL'";
				}
				else if (cat == "Vice President")
				{
					emplvl = "and EmpLevel = 'VP'";
				}

				dt = cls.GetData("select Distinct(EmpName) from tbl_HRMS_EmployeeMaster a join tbl_VPR_Access_User b on a.NTID = b.ntid where AccessLevel is not null and EmpName <> '' " + emplvl + ";");
			}
			else
			{
				dt = cls.GetData("select Distinct(EmpName) from tbl_HRMS_EmployeeMaster a join tbl_VPR_Access_User b on a.NTID = b.ntid where AccessLevel is not null and EmpName <> '' and EmpLevel <> 'EE';");
			}

			List<string> list = new List<string>();

			for (int x = 0; x < dt.Rows.Count; x++)
			{
				string mngrName = dt.Rows[x]["EmpName"].ToString();

				list.Add(mngrName);
			}

			return list;
		}

		[WebMethod]
		public static List<string> empUserFilter(string im)
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();

			if (im != "")
			{
				dt = cls.GetData("select Distinct(EmpName) from tbl_HRMS_EmployeeMaster a join tbl_VPR_Access_User b on a.NTID = b.ntid where AccessLevel is not null and EmpName <> '' and MngrName = '" + im + "';");
			}
			else
			{
				dt = cls.GetData("select Distinct(EmpName) from tbl_HRMS_EmployeeMaster a join tbl_VPR_Access_User b on a.NTID = b.ntid where AccessLevel is not null and EmpName <> '';");
			}

			List<string> list = new List<string>();

			for (int x = 0; x < dt.Rows.Count; x++)
			{
				string empName = dt.Rows[x]["EmpName"].ToString();

				list.Add(empName);
			}

			return list;
		}

    }
}