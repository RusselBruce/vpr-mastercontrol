﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="MappingConditions.aspx.cs" Inherits="Master_Control.MappingConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Mapping Conditions</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-map-marker"></i>&nbsp;Mappings</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;PDF Mapping</span>
            </li>
            <li class="active">
                <span><i class="fa fa-map-o"></i>&nbsp;Mapping Conditions</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div id="condList" class="box box-default noPadMar">
            <div class="box-header">
                <label>Conditions</label>
            </div>
            <div class="box-body noPadMar">
                <div id="toolbar">
                    <button id="btnAddCond" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add Condition</button>
                </div>
                <table id="tblCondList" class="table" data-search="true" data-toolbar="#toolbar">
                    <thead>
                        <tr>
                            <th data-field="field_name" data-sortable="true" data-halign="center">Field Name</th>
                            <th data-field="column_name" data-formatter="custColName" data-sortable="true" data-halign="center">Column Name</th>
                            <th data-field="logical_operator" data-formatter="custColName" data-sortable="true" data-halign="center">Logical Operator</th>
                            <th data-field="column_val" data-formatter="custColName" data-sortable="true" data-halign="center">Column Value</th>
                            <th data-field="and_or_val" data-formatter="custColName" data-sortable="true" data-halign="center">AND/OR</th>
                            <%--<th data-field="column_name" data-sortable="true" data-halign="center">Column Name</th>
                            <th data-field="logical_operator" data-sortable="true" data-halign="center">Logical Operator</th>
                            <th data-field="column_val" data-sortable="true" data-halign="center">Column Value</th>--%>
                            <th data-field="add_cond" data-sortable="true" data-halign="center">Additional Condition</th>
                            <th data-field="text_val" data-sortable="true" data-halign="center">Text</th>
                            <th data-field="yes_no_val" data-sortable="true" data-halign="center">Yes/No</th>
                            <th data-field="chkbox_val" data-sortable="true" data-halign="center">Checkbox</th>
                            <th data-field="modified_by" data-sortable="true" data-halign="center">Modified By</th>
                            <th data-field="modified_date" data-sortable="true" data-halign="center" data-formatter="getDateTime">Modified Date</th>
                            <th data-field="id" data-formatter="deleteRow"></th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                    </tbody>
                </table>
            </div>
        </div>
        <div id="crtCond" class="box box-default noPadMar" style="display: none;">
            <div class="box-body noPadMar condBody">
                <div class="col-xs-12">
                    <button id="btnBack" type="button" class="btn btn-link"><i class="fa fa-angle-double-left"></i>&nbsp;Back To List</button>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <label>Field Name</label>
                    </div>
                    <div class="col-xs-2 text-center noPadMar">
                        <label>Additional Condition</label><br />
                        <label>(If Needed)</label>
                    </div>
                    <div class="col-xs-1 text-center">
                        <label>Text</label>
                    </div>
                    <div class="col-xs-1 text-center">
                        <label>Yes/No</label>
                    </div>
                    <div class="col-xs-1 text-center">
                        <label>Checkbox</label>
                    </div>
                </div>
                <div class="col-xs-12 cont">
                    <div class="col-xs-12 cond" style="margin-top:1%; padding: 1% 0%; border: 1px solid black;">
                        <div class="col-xs-12 divC">
                            <div class="col-xs-1 noPadMar">
                                <%--<label id="lblFN" class="text-green"></label>--%>
                                <input id="inFN" type="text" class="form-control input-sm" />
                            </div>
                            <div class="col-xs-5 noPadMar">
                                <div class="col-xs-1">
                                    <label class="inline if">If</label>
                                    <button type="button" class="btn btn-link btn-sm text-red noPadMar bt" onclick="btnRmvC($(this))" style="display: none;"><i class="fa fa-times"></i></button>
                                </div>
                                <div class="col-xs-4 noPadMar">
                                    <select id="slctCF" class="form-control input-sm inline" onchange="changeVal($(this))"></select>
                                </div>
                                <div class="col-xs-2 noPadMar">
                                    <select id="slctCond" class="form-control input-sm">
                                        <option value="equal">equal</option>
                                        <option value="not equal">not equal</option>
                                        <option value="greater than">greater than</option>
                                        <option value="less than">less than</option>
                                        <option value="contains">contains</option>
                                    </select>
                                </div>
                                <div class="col-xs-3 noPadMar">
                                    <select id="slctCFVal" class="form-control input-sm" onchange="changeVal2($(this))">
                                        <option>--Select One--</option>
                                    </select>
                                </div>
                                <div class="col-xs-2 noPadMar">
                                    <select id="slctCond2" class="form-control input-sm noPadMar" onchange="btnAddC($(this))">
                                        <option value="" selected="selected">--Select One--</option>
                                        <option value="AND">AND</option>
                                        <option value="OR">OR</option>
                                    </select>
                                    <%--<button type="button" class="btn btn-link btn-sm noPadMar ba" onclick="btnAddC($(this))"><i class="fa fa-plus"></i></button>--%>
                                </div>
                            </div>
                            <div class="col-xs-6 noPadMar">
                                <label class="pull-left">Then</label>
                                <div class="col-xs-3">
                                    <input id="inAddCond" type="text" class="form-control input-sm pull-left inline" />
                                </div>
                                <div class="col-xs-3">
                                    <input id="inText" type="text" class="form-control input-sm" />
                                </div>
                                <div class="col-xs-2">
                                    <select id="slctYN" class="form-control input-sm">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <select id="slctCB" class="form-control input-sm">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-link bAdd pull-left noPadMar" onclick="btnAdd($(this))"><i class="fa fa-plus"></i></button>
                                <button type="button" class="btn btn-link text-red bRmv pull-left noPadMar" onclick="btnRmv($(this))" style="display: none;"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-right">
                    <button type="button" id="btnSave" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                    <button type="button" id="btnUpdt" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Update</button>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/MappingConditions.js"></script>
</asp:Content>
