﻿using Master_Control;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Prioritization : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[WebMethod]
		public static PrioClass GetPrio()
		{
			clsConnection cls = new clsConnection();
			PrioClass prio = new PrioClass();
			List<PrioList> prioList = new List<PrioList>();
			DataTable dtPrio = new DataTable();

			string query = "select * from tbl_VPR_Prioritization where date_deleted is null";
			dtPrio = cls.GetData(query);

			for (int x = 0; x <= dtPrio.Rows.Count - 1; x++)
			{
				PrioList prioVal = new PrioList();

				prioVal.id = dtPrio.Rows[x]["id"].ToString();
				prioVal.isActive = dtPrio.Rows[x]["isActive"].ToString();
				prioVal.sortBy = dtPrio.Rows[x]["sort_by"].ToString();
				prioVal.orderBy = dtPrio.Rows[x]["order_by"].ToString();
				prioVal.appliedTo = dtPrio.Rows[x]["applied_to"].ToString();

				prioList.Add(prioVal);
			}

			prio.tblPrio = prioList;

			return prio;
		}

		[WebMethod]
		public static void SavePrio(string prioID, string active, string sort, string order)
		{
			clsConnection cls = new clsConnection();

			string query = "";
			DataTable dt = new DataTable();
			dt = cls.GetData("Select MAX(id) from tbl_VPR_Prioritization_Main");
			int id = Convert.ToInt16(dt.Rows[0][0]);
			if (prioID == "")
			{
				query = "insert into tbl_VPR_Prioritization (main_id,isActive, sort_by, order_by, date_added) values " +
						"(" + id + ",'" + active + "', '" + sort + "', '" + order + "', GETDATE())";
			}
			else
			{
				query = "update tbl_VPR_Prioritization set " +
						"isActive = '" + active + "', " +
						"sort_by = '" + sort + "', " +
						"order_by = '" + order + "', " +
						"date_modified = GETDATE() " +
						"where id = " + prioID;
			}

			try
			{
				cls.ExecuteQuery(query);
			}
			catch (Exception) { }
		}

		[WebMethod]
		public static void DeletePrio(string prioId)
		{
			//prioID = prioID.Replace("del_", "");
			clsConnection cls = new clsConnection();
			string query = "update tbl_VPR_Prioritization_Main set " +
							"isActive = 0, date_deleted = GETDATE() " +
							"where id = " + prioId;

			try
			{
				cls.ExecuteQuery(query);
			}
			catch (Exception ex)
			{

			}

		}

		[WebMethod]
		public static void SaveMainPrio(PrioMain priomain)
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();
			dt = cls.GetData("Select * from tbl_VPR_Prioritization_Main where id = '" + priomain.id + "'");

			string qry = "";

			if (dt.Rows.Count > 0)
			{
				qry = "update tbl_VPR_Prioritization_Main set ";
				qry += "title = '" + priomain.title + "',";
				qry += "description = '" + priomain.description + "',";
				qry += "location = '" + priomain.location + "',";
				qry += "level = '" + priomain.level + "',";
				qry += "bunit = '" + priomain.bunit + "',";
				qry += "bsegment = '" + priomain.bsegment + "',";
				qry += "actOn = '" + priomain.actOn + "',";
				qry += "actUntil = '" + priomain.actUntil + "',";
				qry += "keepAct = '" + priomain.keepAct + "',";
				qry += "users = '" + priomain.users + "',";
				qry += "created_by = '" + priomain.created_by + "',";
				qry += "do_not_end = '" + priomain.do_not_end + "',";
				qry += "created_date = GETDATE(), isActive = 1 ";
				qry += "where id = '" + priomain.id + "'";
			}
			else
			{
				qry = "INSERT into tbl_VPR_Prioritization_Main(title,description,location,level,bunit,bsegment,actOn,actUntil,keepAct,users,do_not_end,created_by,created_date,isActive) ";
				qry += " Values(";
				qry += "'" + priomain.title + "',";
				qry += "'" + priomain.description + "',";
				qry += "'" + priomain.location + "',";
				qry += "'" + priomain.level + "',";
				qry += "'" + priomain.bunit + "',";
				qry += "'" + priomain.bsegment + "',";
				if (priomain.actOn != "") {
					qry += "'" + priomain.actOn + "',";
				}
				else
				{
					qry += "NULL,";
				}
				if (priomain.actUntil != "")
				{
					qry += "'" + priomain.actUntil + "',";
				}
				else
				{
					qry += "NULL,";
				}
				qry += "'" + priomain.keepAct + "',";
				qry += "'" + priomain.users + "',";
				qry += "'" + priomain.do_not_end + "',";
				qry += "'" + priomain.created_by + "',";
				qry += "GETDATE(),1)";
			}

			cls.ExecuteQuery(qry);

		}

		[WebMethod]
		public static PrioClass loadPrio(string prioId)
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();
			DataTable dtList = new DataTable();
			PrioClass prioClass = new PrioClass();
			List<PrioMain> lpm = new List<PrioMain>();
			List<PrioList> lpl = new List<PrioList>();

			if (prioId == "" || prioId == "undefined")
			{
				dt = cls.GetData("select id,title,[description],location,level,bunit,bsegment,actOn,actUntil,keepAct,users,do_not_end,case when created_date is null then '-' else Convert(varchar,created_date) end [created_date],case when created_by is null then '-' else created_by end [created_by] from tbl_VPR_Prioritization_Main where isActive = 1 and date_deleted is null");
				for (int i = 0; i < dt.Rows.Count; i++)
				{
					PrioMain pm = new PrioMain();
					pm.id = dt.Rows[i][0].ToString();
					pm.title = dt.Rows[i][1].ToString();
					pm.description = dt.Rows[i][2].ToString();
					pm.location = dt.Rows[i][3].ToString();
					pm.level = dt.Rows[i][4].ToString();
					pm.bunit = dt.Rows[i][5].ToString();
					pm.bsegment = dt.Rows[i][6].ToString();
					pm.actOn = dt.Rows[i][7].ToString();
					pm.actUntil = dt.Rows[i][8].ToString();
					pm.keepAct = dt.Rows[i][9].ToString();
					pm.users = dt.Rows[i][10].ToString();
					pm.do_not_end = dt.Rows[i][11].ToString();
					pm.created_date = dt.Rows[i][12].ToString();
					pm.created_by = dt.Rows[i][13].ToString();
					lpm.Add(pm);
				}
			}
			else
			{
				dt = cls.GetData("select a.id,a.title,a.[description],a.location,a.level,a.bunit,a.bsegment,a.actOn,a.actUntil,a.keepAct,a.users, " +
								"a.do_not_end,case when a.created_date is null then '-' else Convert(varchar,a.created_date) end [created_date], " +
								"case when a.created_by is null then '-' else a.created_by end [created_by] from tbl_VPR_Prioritization_Main a " +
								"where a.id = '" + prioId + "' and a.isActive = 1 and a.date_deleted is null");
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; i < dt.Rows.Count; i++)
					{
						PrioMain pm = new PrioMain();
						pm.id = dt.Rows[i][0].ToString();
						pm.title = dt.Rows[i][1].ToString();
						pm.description = dt.Rows[i][2].ToString();
						pm.location = dt.Rows[i][3].ToString();
						pm.level = dt.Rows[i][4].ToString();
						pm.bunit = dt.Rows[i][5].ToString();
						pm.bsegment = dt.Rows[i][6].ToString();
						if (dt.Rows[i][7].ToString() != "")
						{
							DateTime dtime = DateTime.Parse(dt.Rows[i][7].ToString());
							pm.actOn = dtime.Month + "/" + dtime.Day + "/" + dtime.Year;
						}
						else
						{
							pm.actOn = DateTime.Now.ToShortDateString();
						}
						if (dt.Rows[i][8].ToString() != "")
						{
							DateTime dtime = DateTime.Parse(dt.Rows[i][8].ToString());
							pm.actUntil = dtime.Month + "/" + dtime.Day + "/" + dtime.Year;
						}
						else
						{
							pm.actUntil = DateTime.Now.ToShortDateString();
						}
						pm.keepAct = dt.Rows[i][9].ToString();
						pm.users = dt.Rows[i][10].ToString();
						pm.do_not_end = dt.Rows[i][11].ToString();
						pm.created_date = dt.Rows[i][12].ToString();
						pm.created_by = dt.Rows[i][13].ToString();

						lpm.Add(pm);
					}
				}

				dtList = cls.GetData("select b.main_id, b.isActive, b.sort_by, b.order_by, b.applied_to from tbl_VPR_Prioritization b where main_id = '" + prioId + "'");

				if (dtList.Rows.Count > 0)
				{
					for (int i = 0; i < dtList.Rows.Count; i++)
					{
						PrioList pl = new PrioList();
						pl.id = dtList.Rows[i][0].ToString();
						pl.isActive = dtList.Rows[i][1].ToString();
						pl.sortBy = dtList.Rows[i][2].ToString();
						pl.orderBy = dtList.Rows[i][3].ToString();

						lpl.Add(pl);
					}
				}

			}

			prioClass.tblPrioMain = lpm;
			prioClass.tblPrio = lpl;

			return prioClass;

		}

		[WebMethod]
		public static List<string> GetCustom(string sort, string prioId)
		{
			clsConnection cls = new clsConnection();

			string query = "select * from tbl_VPR_Prioritization where main_id = '" + prioId + "' and sort_by = '" + sort + "'";

			DataTable dt = new DataTable();


			dt = cls.GetData(query);
			string order = "";
			List<string> orderArr = new List<string>();
			if (dt.Rows.Count > 0)
			{
				order = dt.Rows[0]["order_by"].ToString();
				orderArr = order.Split(',').ToList<string>();
			}

			return orderArr;
		}

		[WebMethod]
		public static string GetBU(string segment)
		{
			clsConnection cls = new clsConnection();

			string qry, ct;

			DataTable dt = new DataTable();

			ct = checkifAll(segment) == "" ? "" : "bs in (" + checkifAll(segment) + ") ";

			qry = "select Distinct bu from tbl_VPR_Loc_BS_BU where " + ct + " order by bu";

			dt = cls.GetData(qry);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
		}

		public static string checkifAll(string st)
		{
			string lst;
			lst = "";
			string[] ls;
			if (st.Contains("All Selected") || st == "" || st == "None" || st == "None selected")
			{
				return lst = "";
			}
			else
			{
				string dltspc;
				dltspc = "";
				for (int z = 0; z < st.Length; z++)
				{
					if (z == 0)
					{
						dltspc += st[z];
					}
					else
					{
						if (st[z - 1] == ',' && st[z] == ' ')
						{

						}
						else
						{
							dltspc += st[z];
						}
					}
				}
				dltspc = dltspc.Replace("'", "''");
				ls = dltspc.Split(',');
				ls = ls.Where(x => x != "None").ToArray();
				foreach (string s in ls)
				{
					lst += "'" + s + "',";
				}
				lst = lst.Remove(lst.Length - 1);
			}

			return lst;

		}
	}
}