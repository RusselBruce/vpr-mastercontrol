﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Master_Control.User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tabAccess" id="accessTab" data-toggle="tab">Access</a>
                        </li>
                        <li>
                            <a href="#tabUser" id="userTab" data-toggle="tab">User</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="tabAccess" class="tab-pane active">
                        <div class="col-xs-2 noPadMar">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div>
                                        <button type="button" id="roleBtn" class="btn btn-link">Role</button>
                                    </div>
                                    <div>
                                        <button type="button" id="skillBtn" class="btn btn-link">Skill Level</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-10 noPadMar" id="roleDiv">
                            <div class="panel panel-default noPadMar">
                                <div class="panel-heading">
                                    <h4 id="headType">Role</h4>
                                </div>
                            </div>

                            <div class="col-xs-4 noPadMar">
                                <div class="panel panel-default usrPanel">
                                    <div class="panel-heading text-center">
                                        <label>Access Type</label>
                                    </div>
                                    <div class="panel-body">
                                        <div>
                                            <table class="table">
                                                <tbody id="tbdy">
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-right" style="margin-top: 1%;">
                                            <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#modalNewAccess"><i class="fa fa-plus"></i>Add New Access</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 noPadMar">
                                <div class="panel panel-default usrPanel">
                                    <div class="panel-heading text-center">
                                        <label>Current Functions</label>
                                        <label id="conId" hidden></label>
                                    </div>
                                    <div class="panel-body" style="overflow: auto; max-height: 460px;">
                                        <div>
                                            <table class="table">
                                                <tbody id="tison">
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 noPadMar ">
                                <div class="panel panel-default usrPanel text-center">
                                    <div style="margin-top: 200%">
                                        <button type="button" id="remFunc" class="btn btn-link btn-lg"><i class="fa fa-2x fa-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                    <br />
                                    <div>
                                        <button type="button" id="appFunc" class="btn btn-link btn-lg"><i class="fa fa-2x fa-arrow-left" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3 noPadMar">
                                <div class="panel panel-default usrPanel">
                                    <div class="panel-heading text-center">
                                        <label>Functions</label>
                                    </div>
                                    <div class="panel-body" style="overflow: auto; max-height: 460px;">
                                        <div>
                                            <table class="table">
                                                <tbody id="tFunc">
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="tabUser" class="tab-pane">
                        <div class="col-xs-10">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table id="tblUser" class="table table-bordered table-hover" data-single-select="true">
                                        <thead>
                                            <tr>
                                                <th data-field="id" data-formatter="idFormatter1"></th>
                                                <th data-sortable="true" data-field="empname">Employee Name</th>
                                                <th data-sortable="true" data-field="emplevel">Employee Category</th>
                                                <th data-sortable="true" data-field="immsupp">Immediate Superior</th>
                                                <th data-sortable="true" data-field="role_id">Role</th>
                                                <th data-sortable="true" data-field="skill_id">Skill</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="panel panel-default">
                                <div class="panel-heading text-center">
                                    <label>Actions</label>
                                </div>
                                <div class="panel-body">
                                    <table class="table no-border">
                                        <tbody>
                                            <tr>
                                                <th>Role</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control input-sm" id="roleSlct"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Skill Level</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control input-sm" id="skillSlct"></select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="text-center" style="margin-top: 1%;">
                                        <button type="button" class="btn btn-primary btn-sm" id="applyBtn"><i class="fa fa-check-circle"></i>&nbsp;Apply</button>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading text-center">
                                    <label>Filter</label>
                                </div>
                                <div class="panel-body">
                                    <table class="table no-border">
                                        <tbody>
                                            <tr>
                                                <th>Employee Category</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control input-sm" id="roleFSlct">
                                                        <%--<option value="DIR">Director</option>
                                                        <option value="MANAGER">Manager</option>
                                                        <option value ="AM">Assistant Manager</option>
                                                        <option value ="TL" selected>Team Lead</option>--%>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Immediate Superior</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control input-sm" id="immSlct"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>User</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control input-sm" id="userSelect">
                                                        <option value="">ALL</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">or</td>
                                            </tr>
                                            <tr>
                                                <th>User Search</th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <input type="text" id="userFltr" class="form-control" /></th>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <div class="text-center" style="margin-top: 1%;">
                                        <button type="button" class="btn btn-primary btn-sm" id="searchFBtn"><i class="fa fa-search"></i>&nbsp;Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalNewAccess">
            <div class="modal-dialog" role="document">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <label>New Access Type</label>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">

                            <table class="table">
                                <tr>
                                    <td style="width: 20%;">
                                        <label class="inline">Name: </label>
                                    </td>
                                    <td style="width: 80%;">
                                        <input id="accessTypeName" type="text" class="inline form-control" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="inline">Description: </label>
                                    </td>
                                    <td>
                                        <input id="accessTypeDesc" type="text" class="inline form-control" /></td>
                                </tr>
                            </table>

                        </div>
                        <div class="text-center">
                            <button id="btnNew_Type" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalEditAccess">
            <div class="modal-dialog" role="document">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <label>New Access Type</label>
                        <label hidden class="inline" id="lineId"></label>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">

                            <table class="table">
                                <tr>
                                    <td style="width: 20%;">
                                        <label class="inline">Name: </label>
                                    </td>
                                    <td style="width: 80%;">
                                        <input id="eaccessTypeName" type="text" class="inline form-control" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="inline">Description: </label>
                                    </td>
                                    <td>
                                        <input id="eaccessTypeDesc" type="text" class="inline form-control" /></td>
                                </tr>
                            </table>

                        </div>
                        <div class="text-center">
                            <button id="btnEdit_Type" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="js/User.js"></script>
    <style>
        .usrPanel {
            height: 500px;
        }
    </style>
</asp:Content>
