﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Repository : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[WebMethod]
		public static string GetPDF()
		{
			clsConnection cls = new clsConnection();

			string qry = "select * from tbl_VPR_Municipality where date_deleted is null";

			DataTable dtPdf = cls.GetData(qry);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtPdf } });
		}

		[WebMethod]
		public static string GetData()
		{
			clsConnection cls = new clsConnection();

			string qry = "select * from tbl_VPR_Mapping_Main where DateDeleted is null";

			DataTable dtData = cls.GetData(qry);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtData } });
		}

		[WebMethod]
		public static string GetCustom()
		{
			clsConnection cls = new clsConnection();

			string qry = "select Distinct(GroupName), CAST(description as VARCHAR(MAX)) [description], modified_by, date_modified from tbl_VPR_Mapping_Group where custom_tag = 1 and date_deleted is null";

			DataTable dtCustom = cls.GetData(qry);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCustom } });
		}

		[WebMethod]
		public static string GetFieldsData(string groupname)
		{
			clsConnection cls = new clsConnection();

			string qryMain = "select * from tbl_VPR_Mapping_Main where GroupName = '" + groupname + "'";

			DataTable dtGroup = cls.GetData(qryMain);

			string qryGrp = "select * from tbl_VPR_Mapping_Group where GroupId = " + dtGroup.Rows[0]["GroupId"] + " and custom_tag is null and date_deleted is null";

			DataTable dtFields = cls.GetData(qryGrp);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtFields } });
		}

		[WebMethod]
		public static string GetFieldsCust(string groupname)
		{
			clsConnection cls = new clsConnection();

			string qryGrp = "select * from tbl_VPR_Mapping_Group where GroupName = '" + groupname + "' and custom_tag = 1 and date_deleted is null";

			DataTable dtFields = cls.GetData(qryGrp);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtFields } });
		}

		[WebMethod]
		public static string DeleteRow(string active, string param)
		{
			clsConnection cls = new clsConnection();

			string qry = "";

			if (active == "PDF")
			{
				qry = "update tbl_VPR_Municipality set date_deleted = GETDATE() where id = '" + param + "'";
			}
			else if (active == "Data")
			{
				qry = "update tbl_VPR_Mapping_Main set DateDeleted = GETDATE() where GroupId = '" + param + "'";
			}
			else if (active == "Custom Data")
			{
				qry = "update tbl_VPR_Mapping_Group set date_deleted = GETDATE() where GroupName = '" + param + "'";
			}

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add(active);

                    cls.FUNC.Audit("Delete Record", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}

		[WebMethod]
		public static string UpdateRep(ArrayList myData, string usr)
		{
			clsConnection cls = new clsConnection();

			string qry = "";
			DataTable dt = new DataTable();

			string desc = (myData[2].ToString() == "-") ? "NULL" : "'" + myData[2].ToString() + "'";
			string path = (myData[2].ToString() == "-") ? "NULL" : "'~/Files/" + myData[1].ToString();

			if (myData[3].ToString() == "PDF")
			{
				qry = "select * from tbl_VPR_Municipality where id = " + myData[0].ToString();
				dt = cls.GetData(qry);

				string oldFN = HttpContext.Current.Server.MapPath("~/Files/Mapped PDF/" + dt.Rows[0]["filename"]);
				string newFN = HttpContext.Current.Server.MapPath("~/Files/Mapped PDF/" + myData[1].ToString());

				qry = "select * from tbl_VPR_Municipality where filename = '" + myData[1].ToString() + "'";
				dt = cls.GetData(qry);

				if (dt.Rows.Count == 0)
				{
					qry = "update tbl_VPR_Municipality set file_name = '" + myData[1].ToString() + "', description = " + desc + ", ";
                    qry += "modified_by = '" + usr + "', modified_date = GETDATE() where id = " + myData[0].ToString();
				}
				else
				{
					return "Filename Exists!";
				}

				try
				{
                    int exec = cls.ExecuteQuery(qry);

                    if (exec != 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Repository");
                        arrPages.Add(myData[3].ToString());

                        cls.FUNC.Audit("Update Repository", arrPages);
                    }

					if (path != "NULL")
					{
						File.Move(oldFN, newFN);
					}

					return "1";
				}
				catch (Exception ex)
				{
					return "Error: " + ex.ToString();
				}
			}
			else if (myData[3].ToString() == "Data")
			{
				qry = "select * from tbl_VPR_Mapping_Main where GroupName = '" + myData[1].ToString() + "'";
				dt = cls.GetData(qry);

				if (dt.Rows.Count == 0)
				{
					qry = "update tbl_VPR_Mapping_Main set GroupName = '" + myData[1].ToString() + "', Description = " + desc + ", ";
                    qry += "ModifiedBy = '" + usr + "', DateModified = GETDATE() where GroupId = " + myData[0].ToString();
				}
				else
				{
					return "Filename Exists!";
				}
			}
			else if (myData[3].ToString() == "Custom Data")
			{
				qry = "select * from tbl_VPR_Mapping_Group where GroupName = '" + myData[1].ToString() + "'";
				dt = cls.GetData(qry);

				if (dt.Rows.Count == 0)
				{
					qry = "update tbl_VPR_Mapping_Group set GroupName = '" + myData[1].ToString() + "', description = " + desc + ", ";
                    qry += "modified_by = '" + usr + "', date_modified = GETDATE() where GroupName = '" + myData[0].ToString() + "'";
				}
				else
				{
					return "Group Name Exists!";
				}
			}

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add(myData[3].ToString());

                    cls.FUNC.Audit("Update Repository", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}

		[WebMethod]
		public static string SaveEditData(ArrayList myData)
		{
			clsConnection cls = new clsConnection();

			string qry = "update tbl_VPR_Mapping_Group set FieldName = '" + myData[1].ToString() + "', SampleData = '" + myData[2].ToString() + "', date_modified = GETDATE() where FieldId = " + myData[0].ToString();

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add("Custom Data");

                    cls.FUNC.Audit("Edit Data", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}

		[WebMethod]
		public static string DeleteEditData(string id)
		{
			clsConnection cls = new clsConnection();

			string qry = "update tbl_VPR_Mapping_Group set date_deleted = GETDATE() where FieldId = " + id;

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add("Custom Data");

                    cls.FUNC.Audit("Delete Data", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}

		[WebMethod]
		public static string SaveEditCust(ArrayList myData, string usr)
		{
			clsConnection cls = new clsConnection();

			string qry = "update tbl_VPR_Mapping_Group set FieldName = '" + myData[1].ToString() + "', ";
            qry += "SampleData = '" + myData[2].ToString() + "', modified_by = '" + usr + "', date_modified = GETDATE() ";
            qry += "where FieldId = " + myData[0].ToString();

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add("Custom Data");

                    cls.FUNC.Audit("Edit Data", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}

		[WebMethod]
		public static string DeleteEditCust(string id, string usr)
		{
			clsConnection cls = new clsConnection();

			string qry = "update tbl_VPR_Mapping_Group set deleted_by = '" + usr + "', date_deleted = GETDATE() where FieldId = " + id;

			try
			{
                int exec = cls.ExecuteQuery(qry);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("Repository");
                    arrPages.Add("Custom Data");

                    cls.FUNC.Audit("Delete Data", arrPages);
                }

				return "1";
			}
			catch (Exception ex)
			{
				return "Error: " + ex.ToString();
			}
		}
	}
}