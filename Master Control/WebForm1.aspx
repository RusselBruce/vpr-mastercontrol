﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Master_Control.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.6 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="plugins/bootstrap-multiselect/bootstrap-multiselect.css" rel="stylesheet" />

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="plugins/jQueryUI/jquery-ui.js"></script>


    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <script src="plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <select id="sample" class="form-control input-sm" multiple="multiple"></select>
        </div>
        <fieldset>
            <legend>Assign Signatory</legend>
            <table class="table">
                <tr>
                    <td>First Signatory</td>
                    <td>
                        <input type="text" id="firstSignatory" class="form-control" />
                    </td>
                    <td>Back-up</td>
                    <td>
                        <input type="text" id="firstBackup" class="form-control" />
                    </td>
                </tr>
                <tr>
                    <td>Second Signatory</td>
                    <td>
                        <input type="text" id="secondSignatory" class="form-control" />
                    </td>
                    <td>Back-up</td>
                    <td>
                        <input type="text" id="secondBackup" class="form-control" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            //$.ajax({
            //    type: 'POST',
            //    url: 'WebForm1.aspx/Sample',
            //    contentType: 'application/json; charset=utf-8',
            //    success: function (data) {
            //        var d = $.parseJSON(data.d);

            //        if (d.Success) {
            //            var records = d.data.record;
            //            //console.log(records);
            //            $('#sample').empty();
            //            $.each(records, function (idx, val) {
            //                $('#sample').append(
            //                    '<option>' + val.Vendor_Name + '</option>' +
            //                    '<option>' + val.Vendor_Name + '</option>' +
            //                    '<option>' + val.Vendor_Name + '</option>'
            //                );
            //            });

            //            $('#sample').multiselect({
            //                numberDisplayed: 1,
            //                includeSelectAllOption: true,
            //                buttonWidth: '100%'
            //            })
            //        }
            //        $('#modalLoading').modal('hide');
            //    }, error: function (response) {
            //        console.log(response.responseText);
            //    }, failure: function (response) {
            //        console.log(response.responseText);
            //    }
            //})


        });
    </script>
</body>
</html>
