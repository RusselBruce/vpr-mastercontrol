﻿using MasterReports;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Reporting : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[WebMethod]
		public static string runReport(clsCreateReport data)
		{
			try
			{
				var fields = data.fields;
				var flds = "";
				foreach (fields s in fields)
				{
					flds += s.name + " [" + s.alias + "],";
				}
				flds = flds.Substring(0, flds.Length - 1);

				clsConnection cls = new clsConnection();

				String qry = "Select " + flds + " from dbo.vVprReports";

				if (data.where.Count > 0)
				{
					var where = " WHERE ";
					foreach (whereData d in data.where)
					{
						where += " CAST(" + d.field + " as VARCHAR) " + d.field_operator + " '" + d.field_values + "' and";
					}
					where = where.Substring(0, where.Length - 3);
					qry += where;
				}

				var dt = cls.GetData(qry);

				var dtCloned = cls.FUNC.convertAllColumnsToString(dt);

				ArrayList arrPages = new ArrayList();
				arrPages.Add("");
				arrPages.Add("");

				cls.FUNC.Audit("Run report", arrPages);

				return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { columns = cls.FUNC.GetAllColumnName(dt), record = dtCloned, qry = qry } });

			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { Success = false, Message = ex.Message });

			}
		}
	}
}