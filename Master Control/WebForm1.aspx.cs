﻿using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

        [WebMethod]
        public static string Sample()
        {
            string connStrs = ConfigurationManager.ConnectionStrings["piv"].ConnectionString.ToString();

            SqlConnection con = new SqlConnection();

            con = new SqlConnection(connStrs);
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            string qry = "Select * from [vw_VID]";
            da = new SqlDataAdapter(qry, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }
	}
}