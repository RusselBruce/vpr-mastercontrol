﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Audit : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

        [WebMethod]
        public static string actionBy()
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();

            string query = "";

            query = "select distinct([Action By]) as action from tbl_VPR_Audit_New";
            dt = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static AuditList auditList(string time, string dtFrom, string dtTo,string action)
        {
            clsConnection cls = new clsConnection();
            AuditList al = new AuditList();
            List<AuditClass> acl = new List<AuditClass>();
            DataTable dt = new DataTable();

			string param = "", param2 = "";
			DateTime now = DateTime.Now;

			if (time == "year")
			{
                param = "where YEAR(Date) = '" + now.Year.ToString() + "'";
			}
			else if (time == "month")
			{
                param = "where MONTH(Date) = '" + now.Month.ToString() + "'";
			}
			else if (time == "week")
			{
				//DateTime today = DateTime.Today;
				int currentDayOfWeek = (int)now.DayOfWeek + 1;
				DateTime sunday = now.AddDays(-currentDayOfWeek);
				DateTime monday = sunday.AddDays(1);
				// If we started on Sunday, we should actually have gone *back*
				// 6 days instead of forward 1...
				if (currentDayOfWeek == 0)
				{
					monday = monday.AddDays(-7);
				}
				var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();

				param = "where Date between '" + dates[0].ToShortDateString() + "' and '" + dates[6].ToShortDateString() + "'";
			}
			else if (time == "yesterday")
			{
				param = "where Date = '" + now.AddDays(-1).ToShortDateString() + "'";
			}
			else if (time == "today")
			{
				param = "where Date = '" + now.ToShortDateString() + "'";
			}
			else
			{
				param = "where Date between '" + dtFrom + "' and '" + dtTo + "'";
			}

            if (action != "")
            {
                param2 = "and [Action By] = '" + action + "' ";
            }
            else
            {
                param2 = " ";
            }

            string query = @"select id,([Action By]+' '+LOWER(Action)+' settings for '+Municipality+' in '+Page+', '+[Sub - Tab]+ ', '+Field+' from '+[Original Value]+' to '
                            +[New Value]+' on '+CAST(CONVERT(date,Date,101)as varchar)+' at '+CAST(CONVERT(varchar,CAST([Time] AS TIME),100)as varchar)) 
                            as [Complete Action],Municipality,Page,[Main Tab],[Sub - Tab],Field,Action,[Original Value],[New Value],[Action By],CAST(CONVERT(date,Date,101)as varchar),
                            CAST(CONVERT(varchar,CAST([Time] AS TIME),100)as varchar) from tbl_VPR_Audit_New " + param + " "+param2+"order by [Date];";

            dt = cls.GetData(query);
			for (int i = 0; i < dt.Rows.Count; i++)
			{
				AuditClass ac = new AuditClass();
				ac.id = dt.Rows[i][0].ToString();
				ac.CompleteAction = dt.Rows[i][1].ToString();
				ac.Municipality = dt.Rows[i][2].ToString();
				ac.Page = dt.Rows[i][3].ToString();
				ac.MainTab = dt.Rows[i][4].ToString();
				ac.SubTab = dt.Rows[i][5].ToString();
				ac.Field = dt.Rows[i][6].ToString();
				ac.Action = dt.Rows[i][7].ToString();
                ac.OriginalValue = dt.Rows[i][8].ToString();
                ac.NewValue = dt.Rows[i][9].ToString();
                ac.ActionBy = dt.Rows[i][10].ToString();
                ac.Date = dt.Rows[i][11].ToString();
                ac.Time = dt.Rows[i][12].ToString();
				acl.Add(ac);
			}

			al.tblAuditList = acl;

            return al;
            
        }
	}
}